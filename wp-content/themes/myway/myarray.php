<?php
header("Access-Control-Allow-Origin: *");

	$Fayett = array('Brooks'=> 'Brooks' ,'Fayetteville'=>'Fayetteville','PeachtreeCity'=>'Peachtree City','Tyrone'=>'Tyrone','Woolsey'=>'Woolsey');

	$Clayton = array("Conley"=>"Conley","ForestPark"=>"ForestPark","Jonesboro"=>"Jonesboro","Lovejoy"=>"Lovejoy", "Morrow"=>"Morrow","Rex"=>"Rex","Riverdale"=>"Riverdale");

	$Cherokee = array("BallGround"=>"Ball Ground","Canton"=>"Canton","HollySprings"=>"Holly Springs","Lebanon"=>"Lebanon","Nelson"=>"Nelson","Waleska"=>"Waleska","Woodstock"=>"Woodstock");

	$Coweta = array("Grantville"=>"Grantville","Haralson"=>"Haralson","Moreland"=>"Moreland","Newnan"=>"Newnan","Sargent"=>"Sargent","Senoia"=>"Senoia","Sharpsburg"=>"Sharpsburg","Turin"=>"Turin");

	$Carroll = array("Bowdon"=>"Bowdon",'Carrollton' => 'Carrollton', "MountZion"=>"Mount Zion", "Temple"=>"Temple", "VillaRica"=>"Villa Rica","Whitesburg"=>"Whitesburg" );

	$Forsyth = array('Cumming' => 'Cumming');

	$Hall = array("ChestnutMountain"=>"Chestnut Mountain","Clermont"=>"Clermont","FloweryBranch"=>"Flowery Branch","Gainesville"=>"Gainesville","Lula"=>"Lula","Oakwood"=>"Oak wood");

	$Douglas = array("Douglasville"=>"Douglasville","LithiaSprings"=>"Lithia Springs","Winston"=>"Winston");


	$Dekalb = array("Atlanta"=>"Atlanta","AvondaleEstates"=>"Avondale Estates","Chamblee"=>"Chamblee","Clarkston"=>"Clarkston","Decatur"=>"Decatur","Doraville"=>"Doraville","Dunwoody"=>"Dunwoody","Ellenwood"=>"Ellenwood","Lithonia"=>"Lithonia","PineLake"=>"Pine Lake","Redan"=>"Redan","Scottdale"=>"Scottdale","StoneMountain"=>"Stone Mountain","Tucker"=>"Tucker");

	$Gwinnett = array("Auburn "=>"Auburn", "Buford"=>"Buford","BerkeleyLake"=>"Berkeley Lake", "Dacula"=>"Dacula", "Duluth"=>"Duluth", "Grayson"=>"Grayson", "Lawrenceville"=>"Lawrenceville", "Lilburn"=>"Lilburn", "Loganville"=>"Loganville", "Norcross"=>"Norcross","PeachtreeCorners"=>"Peachtree Corners", "Snellville"=>"Snellville","SugarHill"=>"Sugar Hill","Suwanee"=>"Suwanee");


	$Cobb = array("Acworth"=>"Acworth","Austell"=>"Austell","Clarkdale"=>"Clarkdale","Kennesaw"=>"Kennesaw","Mableton"=>"Mableton","Marietta"=>"Marietta","PowderSprings"=>"Powder Springs","Smyrna"=>"Smyrna");

	$Henry = array('Hampton' =>'Hampton','LocustGrove'=>'Locust Grove','Mcdonough'=>'Mcdonough','Stockbridge'=>'Stockbridge');

	$Rockdale=array('Conyers'=>'Conyers');


	$Atlanta = array("31106"=>"31106","31107"=>"31107","31126"=>"31126","31131"=>"31131","31136"=>"31136","31139"=>"31139","31150"=>"31150","31156"=>"31156","31191"=>"31191","31192"=>"31192","31193"=>"31193","31195"=>"31195","31196"=>"31196","31197"=>"31197","31198"=>"31198","31199"=>"31199","30301"=>"30301","30302"=>"30302","30303"=>"30303","30304"=>"30304","30305"=>"30305","30306"=>"30306","30307"=>"30307","30308"=>"30308","30309"=>"30309","30310"=>"30310","30311"=>"30311","30312"=>"30312","30313"=>"30313","30314"=>"30314","30315"=>"30315","30316"=>"30316","30318"=>"30318","30320"=>"30320","30321"=>"30321","30324"=>"30324","30325"=>"30325","30326"=>"30326","30327"=>"30327","30328"=>"30328","30330"=>"30330","30331"=>"30331","30332"=>"30332","30334"=>"30334","30336"=>"30336","30337"=>"30337","30339"=>"30339","30342"=>"30342","30343"=>"30343","30344"=>"30344","30347"=>"30347","30348"=>"30348","30349"=>"30349","30350"=>"30350","30353"=>"30353","30354"=>"30354","30355"=>"30355","30357"=>"30357","30358"=>"30358","30361"=>"30361","30363"=>"30363","30364"=>"30364","30368"=>"30368","30369"=>"30369","30370"=>"30370","30371"=>"30371","30374"=>"30374","30375"=>"30375","30376"=>"30376","30377"=>"30377","30378"=>"30378","30379"=>"30379","30380"=>"30380","30384"=>"30384","30385"=>"30385","30386"=>"30386","30387"=>"30387","30388"=>"30388","30389"=>"30389","30390"=>"30390","30392"=>"30392","30394"=>"30394","30396"=>"30396","30398"=>"30398","30399"=>"30399","30301"=>"30301","30302"=>"30302","30303"=>"30303","39901"=>"39901");

	$Auburn=array("30011"=>"30011");

	$Buford=array("30515"=>"30515", "30518"=>"30518", "30519"=>"30519");

	$Dacula=array("30019"=>"30019");

	$Grayson=array("30017"=>"30017");

	$Lawrenceville=array("30042"=>"30042","30043"=>"30043", "30044"=>"30044", "30045"=>"30045", "30046"=>"30046","30049"=>"30049");

	$Lilburn=array("30047"=>"30047","30048"=>"30048");

	$Loganville =array("30052"=>"30052");

	$Norcross =array("30003"=>"30003", "30010"=>"30010", "30071"=>"30071", "30091"=>"30091", "30092"=>"30092", "30093"=>"30093");

	$Snellville =array("30039"=>"30039","30078"=>"30078");

	$Suwanee=array("30024"=>"30024");





	$Alpharetta = array("30004"=>"30004","30005"=>"30005","30009"=>"30009","30022"=>"30022","30023"=>"30023");

	$BallGround = array('30107' => '30107');

	$Canton = array("30114"=>"30114","30115"=>"30115","30115"=>"30115");

	$Carrollton = array("30112"=>"30112","30116"=>"30116","30117"=>"30117","30118"=>"30118","30119"=>"30119");

	$ChestnutMountain = array('30502' => '30502');

	$Clarkdale = array('30111' =>'30111');

	$Conley = array('30288' =>'30288' );

	$Conyers = array('30012' =>'30012','30013' =>'30013','30094'=>'30094');

	$Cumming = array("30028"=>"30028","30040"=>"30040","30041"=>"30041");

	$Douglasville = array("30133"=>"30133","30134"=>"30134","30135"=>"30135","30154"=>"30154");


	$Duluth = array("30097"=>"30097","30095"=>"30095","30096"=>"30096","30098"=>"30098","30099"=>"30099");

	$Ellenwood = array('30294' =>'30294');

	$Fairburn = array('30213' =>'30213');

	$FloweryBranch = array('30542' =>'30542');

	$Fayetteville = array("30215"=>"30215","30542"=>"30542");

	$ForestPark = array("30297"=>"30297","30298"=>"30298");

	$Gainesville = array("30501"=>"30501","30503"=>"30503","30504"=>"30504","30506"=>"30506","30507"=>"30507");

	$Austell = array('30168' =>'30168');
	$Grantville = array('30220' => '30220');
	$Haralson  = array('30229' =>'30229');
	$Hampton = array("30228"=>"30228","30229"=>"30229");

	$HollySprings = array("30142"=>"30142","30142"=>"30142");

	$Kennesaw = array("30144"=>"30144","30152"=>"30152","30156"=>"30156","30160"=>"30160");

	$Lebanon = array("30146"=>"30146","30146"=>"30146");

	$LithiaSprings = array('30122' =>'30122');

	$LocustGrove = array('30248' =>'30248');

	$Mableton = array('30126' =>'30126');

	$Marietta = array('30006' =>'30006','30007' =>'30007','30008' =>'30008','30060' =>'30060','30061' =>'30061','30062' =>'30062','30063' =>'30063','30064' =>'30064','30065' =>'30065','30066' =>'30066','30067' =>'30067','30068' =>'30068','30069' =>'30069','30090' =>'30090');

	$Moreland = array('30259' => '30259');

	$Mcdonough = array("30252"=>"30252","30253"=>"30253");

	$Morrow = array('30260' =>'30260',"30287"=>"30287");

	$Nelson = array('30151' =>'30151');

	$Newnan = array("30263"=>"30263","30264"=>"30264","30265"=>"30265","30271"=>"30271");
	// last added

	$Brooks=array("30205"=>"30205");

	$Woolsey=array("30214"=>"30214","30215"=>"30215");

	$Jonesboro=array("30236"=>"30236","30237"=>"30237","30238"=>"30238");

	$Lovejoy=array("30250"=>"30250");

	$Riverdale=array("30274"=>"30274","30296"=>"30296");


	$Bowdon=array("30108"=>"30108");

	$MountZion=array("30150"=>"30150");

	$Temple=array("30179"=>"30179");

	$VillaRica=array("30180"=>"30180");

	$Whitesburg=array("30185"=>"30185");

	$Lula=array("30554"=>"30554");

	$Clermont=array("30527"=>"30527");

	$Chamblee=array("30341"=>"30341","30366"=>"30366");



	$AvondaleEstates =array("30002"=>"30002");

	$Clarkston=array("30021"=>"30021");

	$Decatur=array("30030"=>"30030","30031"=>"30031", "30032"=>"30032", "30033"=>"30033", "30034"=>"30034", "30035"=>"30035", "30036"=>"30036", "30037"=>"30037");

	$Doraville=array("30340"=>"30340", "30360"=>"30360","30362"=>"30362");

	$Dunwoody=array("30346"=>"30346");

	$Lithonia=array("30038" => "30038", "30058"=>"30058");

	$PineLake=array("30072"=>"30072");



	$BerkeleyLake=array("30092"=>"30092");

	$PeachtreeCorners=array("30092"=>"30092");

	$SugarHill=array("30518"=>"30518");



	$Acworth=array("30101"=>"30101","30102"=>"30102");

	 $PowderSprings=array("30127"=>"30127");

	$ChattahoocheeHills=array("30185"=>"30185","30213"=>"30213","30268"=>"30268");

  	$CollegePark=array("30337"=>"30337","30349"=>"30349");

  	$EastPoint=array("30344"=>"30344","30364"=>"30364","30348"=>"30348");

  	$Hapeville=array("30353"=>"30353","30354"=>"30354");

  	$JohnsCreek=array("30005"=>"30005","30022"=>"30022","30024"=>"30024","30097"=>"30097","30098"=>"30098");

  	$Milton=array("30004"=>"30004","30009"=>"30009");

  	$MountainPark=array("30075"=>"30075","30087"=>"30087");

  	$SandySprings=array("30319"=>"30319");

	// end here

	$Oakwood = array('30566' => '30566' );

	$Palmetto = array('30268' => '30268' );

	$PeachtreeCity = array('30269' => '30269','30270' => '30270' );

	$RedOak = array('30272' => '30272' );

	$Redan = array('30074' => '30074');

	$Rex = array('30273' => '30273');

	$Sargent = array('30275' => '30275');

	$Scottdale = array('30079' => '30079');

	$Roswell = array("30075"=>"30075","30076"=>"30076","30077"=>"30077","30075"=>"30075","30076"=>"30076","30077"=>"30077");
	$Senoia = array('30276' => '30276');

	$Sharpsburg = array('30277' => '30277');

	$Smyrna = array("30080"=>"30080","30081"=>"30081","30082"=>"30082");

	$Snellville = array('30078' => '30078');

	$Stockbridge = array('30281' => '30281');

	$StoneMountain = array("30083"=>"30083","30086"=>"30086","30087"=>"30087","30088"=>"30088");

	$Tucker = array("30084"=>"30084","30085"=>"30085");

	$Turin = array('30289' => '30289');

	$Tyrone = array('30290' => '30290');

	$UnionCity = array('30291' => '30291');

	$Waleska = array('30183' => '30183');

	$Winston = array('30187' => '30187' );

	$Woodstock = array("30188"=>"30188","30189"=>"30189","30188"=>"30188","30189"=>"30189" );

	$Fulton= array("Alpharetta"=>"Alpharetta","Atlanta"=>'Atlanta',"ChattahoocheeHills"=>"Chattahoochee Hills","CollegePark"=>"College Park","EastPoint"=>"East Point","Fairburn"=>"Fairburn","Hapeville"=>"Hapeville","JohnsCreek"=>"Johns Creek","Milton"=>"Milton","MountainPark"=>"Mountain Park","Palmetto"=>"Palmetto","RedOak"=>"Red Oak","Roswell"=>"Roswell","SandySprings"=>"Sandy Springs","UnionCity"=>"Union City"
		);

	$county  = array("Fulton"=> $Fulton,"Clayton"=> $Clayton,"Cherokee"=> $Cherokee,"Coweta"=>$Coweta,"Carroll"=>$Carroll,"Forsyth"=>$Forsyth,"Hall"=>$Hall,"Rockdale"=>$Rockdale,"Henry"=>$Henry,"Douglas"=>$Douglas,"Dekalb"=>$Dekalb,"Gwinnett"=>$Gwinnett,"Cobb"=>$Cobb,"Fayett"=>$Fayett);
	$zip_codes = array("Atlanta"=>$Atlanta,"Duluth"=>$Duluth,"Fairburn"=>$Fairburn,"Palmetto"=>$Palmetto,"RedOak"=>$RedOak,"UnionCity"=>$UnionCity,"Alpharetta"=>$Alpharetta,"Roswell"=>$Roswell,"Fayetteville"=>$Fayetteville,"PeachtreeCity"=>$PeachtreeCity,"Tyrone"=>$Tyrone,"Morrow"=>$Morrow,"Rex"=>$Rex,"Conley"=>$Conley,"ForestPark"=>$ForestPark,"BallGround"=>$BallGround,"Lebanon"=>$Lebanon,"Nelson"=>$Nelson,"Waleska"=>$Waleska,"Canton"=>$Canton,"HollySprings"=>$HollySprings,"Woodstock"=>$Woodstock,"Haralson"=>$Haralson,"Moreland"=>$Moreland,"Newnan"=>$Newnan,"Sargent"=>$Sargent,"Senoia"=>$Senoia,"Sharpsburg"=>$Sharpsburg,"Turin"=>$Turin,"Grantville"=>$Grantville,"Carrollton" =>$Carrollton,"Cumming" =>$Cumming,"FloweryBranch"=>$FloweryBranch,"Gainesville"=>$Gainesville,"ChestnutMountain"=>$ChestnutMountain,"Oakwood"=>$Oakwood,"LithiaSprings"=>$LithiaSprings,"Douglasville"=>$Douglasville,"Winston"=>$Winston,"Redan"=>$Redan,"Scottdale"=>$Scottdale,"Tucker"=>$Tucker,"StoneMountain"=>$StoneMountain,"Ellenwood"=>$Ellenwood,"Atlanta"=>$Atlanta,"Snellville"=>$Snellville,"Norcross"=>$Norcross,"Duluth"=>$Duluth,"Smyrna"=>$Smyrna,"Marietta"=>$Marietta,"Clarkdale"=>$Clarkdale,"Mableton"=>$Mableton,"Kennesaw"=>$Kennesaw,"Austell"=>$Austell,"Hampton" =>$Hampton,"LocustGrove"=>$LocustGrove,"Mcdonough"=>$Mcdonough,"Stockbridge"=>$Stockbridge,"Conyers" =>$Conyers,"Auburn"=>$Auburn, "Buford"=>$Buford,"Dacula"=>$Dacula,"Grayson"=>$Grayson, "Lawrenceville"=>$Lawrenceville, "Lilburn"=>$Lilburn, "Loganville"=>$Loganville, "Suwanee"=>$Suwanee,"Brooks"=>$Brooks,"Woolsey"=>$Woolsey,"Jonesboro"=>$Jonesboro,"Lovejoy"=>$Lovejoy,"Riverdale"=>$Riverdale,"Bowdon"=>$Bowdon,"MountZion"=>$MountZion,"Temple"=>$Temple, "VillaRica"=>$VillaRica,"Whitesburg"=>$Whitesburg,"Lula"=>$Lula,"Clermont"=>$Clermont,"Chamblee"=>$Chamblee,"AvondaleEstates"=>$AvondaleEstates, "Clarkston"=>$Clarkston,"Decatur"=>$Decatur,"Doraville"=>$Doraville,"Dunwoody"=>$Dunwoody,"Lithonia"=>$Lithonia,"PineLake"=>$PineLake,"BerkeleyLake"=>$BerkeleyLake,"PeachtreeCorners"=>$PeachtreeCorners,"SugarHill"=>$SugarHill,"Acworth"=>$Acworth,"PowderSprings"=>$PowderSprings,"ChattahoocheeHills"=>$ChattahoocheeHills,"CollegePark"=>$CollegePark,"EastPoint"=>$EastPoint,"Hapeville"=>$Hapeville,"JohnsCreek"=>$JohnsCreek,"Milton"=>$Milton,"MountainPark"=>$MountainPark,"SandySprings"=>$SandySprings);


	$milage_1_50 = array('30002' => '30002','30004' => '30004','30005' => '30005','30006' => '30006','30007' => '30007','30008' => '30008','30009' => '30009','30010' => '30010','30012' => '30012','30017' => '30017','30021' => '30021','30022' => '30022','30023' => '30023','30024' => '30024','30026' => '30026','30029' => '30029','30030' => '30030','30031' => '30031','30032' => '30032','30033' => '30033','30034' => '30034','30035' => '30035','30036' => '30036','30037' => '30037','30038' => '30038','30039' => '30039','30042' => '30042','30043' => '30043','30044' => '30044','30045' => '30045','30046' => '30046','30047' => '30047','30048' => '30048','30049' => '30049','30058' => '30058','30060' => '30060','30061' => '30061','30062' => '30062','30063' => '30063','30064' => '30064','30065' => '30065','30066' => '30066','30067' => '30067','30068' => '30068','30069' => '30069','30071' => '30071','30072' => '30072','30074' => '30074','30075' => '30075','30076' => '30076','30077' => '30077','30078' => '30078','30079' => '30079','30080' => '30080','30081' => '30081','30082' => '30082','30083' => '30083','30084' => '30084','30085' => '30085','30086' => '30086','30087' => '30087','30088' => '30088','30090' => '30090','30091' => '30091','30092' => '30092','30093' => '30093','30095' => '30095','30096' => '30096','30097' => '30097','30098' => '30098','30099' => '30099','30111' => '30111','30115' => '30115','30126' => '30126','30142' => '30142','30144' => '30144','30146' => '30146','30156' => '30156','30160' => '30160','30168' => '30168','30188' => '30188','30189' => '30189','30260' => '30260','30273' => '30273','30288' => '30288','30294' => '30294','30297' => '30297','30298' => '30298','30301' => '30301','30302' => '30302','30303' => '30303','39901' => '39901',"30515"=>"30515", "30518"=>"30518", "30519"=>"30519","30350"=>"30350","30205"=>"30205","30287"=>"30287","30236"=>"30236","30237"=>"30237","30238"=>"30238","30250"=>"30250","30274"=>"30274","30296"=>"30296","30108"=>"30108","30150"=>"30150","30179"=>"30179","30180"=>"30180","30185"=>"30185","30554"=>"30554","30527"=>"30527","30341"=>"30341","30366"=>"30366","30346"=>"30346","30340"=>"30340", "30360"=>"30360","30362"=>"30362","30101"=>"30101","30102"=>"30102","30127"=>"30127","30152"=>"30152","30327"=>"30327","30328"=>"30328", '30311' => '30311', '30314' => '30314', '30315' => '30315',
		'31106'=>'31106','31107'=>'31107','31126'=>'31126','31131'=>'31131','31136'=>'31136',
		'31139'=>'31139','31150'=>'31150','31156'=>'31156','31191'=>'31191','31192'=>'31192',
		'31193'=>'31193','31195'=>'31195','31196'=>'31196','31197'=>'31197','31198'=>'31198',
		'31199'=>'31199','30304'=>'30304','30313'=>'30313','30316'=>'30316','30320'=>'30320',
		'30321'=>'30321','30324'=>'30324','30325'=>'30325','30326'=>'30326','30330'=>'30330',
		'30331'=>'30331','30332'=>'30332','30334'=>'30334','30336'=>'30336','30339'=>'30339',
		'30342'=>'30342','30343'=>'30343','30347'=>'30347','30355'=>'30355','30357'=>'30357',
		'30358'=>'30358','30361'=>'30361','30363'=>'30363','30368'=>'30368','30369'=>'30369',
		'30370'=>'30370','30371'=>'30371','30374'=>'30374','30375'=>'30375','30376'=>'30376',
		'30377'=>'30377','30378'=>'30378','30379'=>'30379','30380'=>'30380','30384'=>'30384',
		'30385'=>'30385','30386'=>'30386','30387'=>'30387','30388'=>'30388','30389'=>'30389',
		'30390'=>'30390','30392'=>'30392','30394'=>'30394','30396'=>'30396','30398'=>'30398',
		'30399'=>'30399');


		$milage_1_60 = array('30122'=>'30122',"30133"=>"30133","30154"=>"30154",'30134'=>'30134','30135'=>'30135','30187'=>'30187','30012'=>'30012','30013'=>'30013','30094'=>'30094','30228'=>'30228','30248'=>'30248','30252'=>'30252','30253'=>'30253','30281'=>'30281','30214'=>'30214','30215'=>'30215','30269'=>'30269','30290'=>'30290','30107'=>'30107','30114'=>'30114','30115'=>'30115','30142'=>'30142','30146'=>'30146','30151'=>'30151','30183'=>'30183','30188'=>'30188','30189'=>'30189','30270' => '30270',"30337"=>"30337","30349"=>"30349","30344"=>"30344","30364"=>"30364","30348"=>"30348","30213"=>"30213","30353"=>"30353","30354"=>"30354",'30291' => '30291',
			'30011'=>'30011','30052'=>'30052','30003'=>'30003','30117'=>'30117','30118'=>'30118',
			'30119'=>'30119','30268'=>'30268','30319'=>'30319','30272'=>'30272');

		$milage_1_70=array('30220'=>'30220','30229'=>'30229','30259'=>'30259','30263'=>'30263','30264'=>'30264','30265'=>'30265','30271'=>'30271','30275'=>'30275','30276'=>'30276','30277'=>'30277','30289'=>'30289','30112'=>'30112','30116'=>'30116','30017'=>'30017','30018'=>'30018','30019'=>'30019','30028'=>'30028','30040'=>'30040','30041'=>'30041','30542'=>'30542','30507'=>'30507','30501'=>'30501','30504'=>'30504','30566'=>'30566','30502'=>'30502','30503'=>'30503',"30506"=>"30506");

		$flat_15= array( '30301' => '30301', '30303' => '30303', '30305' => '30305','30306' => '30306', '30308' => '30308','30307' => '30307','30309' => '30309','30310' => '30310','30312' => '30312','30317' => '30317','30318' => '30318', '30311' => '30311', '30314' => '30314', '30315' => '30315');

		$check = array('milage_1_50'  =>$milage_1_50 , 'milage_1_60'  =>$milage_1_60 ,'milage_1_70'  =>$milage_1_70,'flat_15'=>$flat_15);
		$rate=array('milage_1_50'  =>"1.5" , 'milage_1_60'  =>"1.60" ,'milage_1_70'  =>"1.70",'flat_15'=>'15');

		function distancefind($pickup,$dropoff){

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,"https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$pickup."&destinations=".$dropoff."&units=imperial&key=AIzaSyAgDxM11UdD6OlL6QRNDWy89GA4PsnMt18");
			curl_setopt($ch, CURLOPT_HEADER, 'Content-Type: application/json');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			$results = curl_exec($ch);
			curl_close($ch);



			$flag=false;
			$miles="";
			$json=json_decode($results,true);
			if(strpos($json['rows'][0]["elements"][0]['distance']['text'], ',')){

				$flag=true;
			}

			if($flag==true){

				$miles=str_replace(",","",$json['rows'][0]["elements"][0]['distance']['text']);
			}
			else{

				$miles=$json['rows'][0]["elements"][0]['distance']['text'];
			}

			return $miles;
}


	if(isset($_POST['action'])){


		if($_POST['action']=='citycode'){

			$response=$county[$_POST['pickup']];

			echo json_encode($response);
		}

		if($_POST['action']=='zipcode'){

			$response=$zip_codes[$_POST['pickup']];

			echo json_encode($response);
		}

		if($_POST['action']=='checkit'){

			$placeto=$_POST['placeto'];
		    $placefrom=$_POST['placefrom'];
		    $pickupfound="";
			$dropofffound="";



			foreach ($check as $key => $value) {



				if(array_key_exists($placefrom, $value)){

					$pickupfound=$key;


				}

				if(array_key_exists($placeto, $value)){

					$dropofffound=$key;



				}



			}



		if($pickupfound=="flat_15" &&  $dropofffound=="flat_15"){



				$response['rate']="15";
				$response['type']="Atlanta";

				echo json_encode($response);

			}

			else{

				if( ($pickupfound=="milage_1_50" && $dropofffound=="milage_1_50")||
					($pickupfound=="milage_1_50" && $dropofffound=="flat_15")||
					($pickupfound=="flat_15" && $dropofffound=="milage_1_50")
					){

					$miles=distancefind($placeto,$placefrom);


					$response['miles']=$miles;
					$response['rate']="1.5";
					$response['type']="1.5";

					echo json_encode($response);
			}

			if(($pickupfound=="milage_1_60"  && $dropofffound=="milage_1_60")||
				($pickupfound=="milage_1_60" &&  $dropofffound=="flat_15") ||
				($pickupfound=="flat_15" &&  $dropofffound=="milage_1_60") ||
				($pickupfound=="milage_1_60" &&  $dropofffound=="milage_1_50") ||
				($pickupfound=="milage_1_50" &&  $dropofffound=="milage_1_60")){

					$miles=distancefind($placeto,$placefrom);

					$response['miles']=$miles;
					$response['rate']="1.6";
					$response['type']="1.6";

					echo json_encode($response);

			}

			if( ($pickupfound=="milage_1_70" && $dropofffound=="milage_1_70")||
				($pickupfound=="milage_1_70" &&  $dropofffound=="milage_1_60") ||
				($pickupfound=="milage_1_60" &&  $dropofffound=="milage_1_70") ||
				($pickupfound=="milage_1_70" &&  $dropofffound=="flat_15") ||
				($pickupfound=="flat_15"     &&  $dropofffound=="milage_1_70") ||
				($pickupfound=="milage_1_70" &&  $dropofffound=="milage_1_50") ||
				($pickupfound=="milage_1_50" &&  $dropofffound=="milage_1_70")){


					$miles=distancefind($placeto,$placefrom);

					$response['miles']=$miles;
					$response['rate']="1.7";
					$response['type']="1.7";

					echo json_encode($response);

			}

		}



			if(empty($pickupfound)||empty($dropofffound)){


				$abc="emailme";
				echo json_encode($abc);



			}


		}
	}
?>
