var j = jQuery.noConflict();
var x = jQuery.noConflict();

			
//Initialize flex slider
x(window).load(function (){

	"use strict";
	x('.flexslider').flexslider({
		animation:"slide",
		start:function() {
			x('body').removeClass('loading');
		}
	});					

	if (x('#projects').length > 0) {
		//Projects filter 
		var xcontainer = x('#projects');
		// items filter
		x('.project-filter li a').click(function () {
			var selector = x(this).attr('data-filter');
			xcontainer.isotope({
				filter: selector,
				itemSelector: '.project-item',
				layoutMode: 'fitRows',
				animationEngine: 'jQuery'
			});
			x('.project-filter').find('a.active').removeClass('active');
			x(this).addClass('active');
			return false;
		});

		//causes initial setup
		setTimeout(function(){
			x('.project-filter li a:eq(0)').click();
		},1000);
	}	

});

//back to top
(function ($) {
		jQuery('.back_to_top').click(function(){ 
		jQuery('html, body').animate({scrollTop:0}, 'fast'); 
		return false; 
	});
})(jQuery);

//Initialize video background
jQuery(function(){
	'use strict';
    jQuery(".player").mb_YTPlayer();
});
		
//Initialize counter for numbers	
jQuery(document).ready(function ($) {
	jQuery('.counter').counterUp({
	delay: 10,
	time: 2000
	});
});

(function() {
	"use strict";
	j('body').addClass('notouch');
})(jQuery);

//Initialize stellar parallax
x(function(){
	"use strict";
	x.stellar({
		horizontalScrolling: false,
		verticalOffset: 0,
		responsive: false
	});
});

(jQuery);