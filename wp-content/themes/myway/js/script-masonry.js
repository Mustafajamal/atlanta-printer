var j = jQuery.noConflict();
var x = jQuery.noConflict();

			
//Initialize flex slider
x(window).load(function (){

	"use strict";
	x('.flexslider').flexslider({
		animation:"slide",
		start:function() {
			x('body').removeClass('loading');
		}
	});					

	if (x('#projects').length > 0) {
		//Projects filter 
		var xcontainer = x('#projects');
		// items filter
		x('.project-filter li a').click(function () {
			var selector = x(this).attr('data-filter');
			xcontainer.isotope({
				filter: selector,
				itemSelector: '.project-item',
				layoutMode: 'packery',
				animationEngine: 'jQuery',
			});
			x('.project-filter').find('a.active').removeClass('active');
			x(this).addClass('active');
			return false;
		});

		//causes initial setup
		setTimeout(function(){
			x('.project-filter li a:eq(0)').click();
		},1000);
	}	

});