var j = jQuery.noConflict();
var x = jQuery.noConflict();

//back to top
(function ($) {
		jQuery('.back_to_top').click(function(){ 
		jQuery('html, body').animate({scrollTop:0}, 'fast'); 
		return false; 
	});
})(jQuery);

//Initialize video background
jQuery(function(){
	'use strict';
    jQuery(".player").mb_YTPlayer();
});
		
//Initialize counter for numbers	
jQuery(document).ready(function ($) {
	jQuery('.counter').counterUp({
	delay: 10,
	time: 2000
	});
});

(function() {
	"use strict";
	j('body').addClass('notouch');
})(jQuery);

//Initialize stellar parallax
x(function(){
	"use strict";
	x.stellar({
		horizontalScrolling: false,
		verticalOffset: 0,
		responsive: false
	});
});

(jQuery);