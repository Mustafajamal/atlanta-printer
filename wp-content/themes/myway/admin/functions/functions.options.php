<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		/*$bg_images_path = get_stylesheet_directory(). '/images/icons/';
		$bg_images_url = get_template_directory_uri().'/images/icons/'; 
		$icon_images_url = get_template_directory_uri().'/images/icons/'; 
		$bg_images = array(); 
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); 
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		} */
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

$sliders_array = array(
						"none" => "None",
						//"nivo" => "Nivo Slider",
						"flex" => "Flex Slider"
					);

	
$slidersfx_array = array(
						"fade" => "fade",
						"slide" => "slide"
						);

$enable_disable = array(
		'enable'	=> 'enable',
		'disable'	=> 'disable'
	);
	
$per_row = array(
		'2'	=> '2',
		'3'	=> '3',
		'4'	=> '4',
		'6'	=> '6'
	);


$font_family_google = array(
							'0' => 'Select Desired Font',
							"ABeeZee" => "ABeeZee",
							"Abel" => "Abel",
							"Abril Fatface" => "Abril Fatface",
							"Aclonica" => "Aclonica",
							"Acme" => "Acme",
							"Actor" => "Actor",
							"Adamina" => "Adamina",
							"Advent Pro" => "Advent Pro",
							"Aguafina Script" => "Aguafina Script",
							"Akronim" => "Akronim",
							"Aladin" => "Aladin",
							"Aldrich" => "Aldrich",
							"Alegreya" => "Alegreya",
							"Alegreya SC" => "Alegreya SC",
							"Alex Brush" => "Alex Brush",
							"Alfa Slab One" => "Alfa Slab One",
							"Alice" => "Alice",
							"Alike" => "Alike",
							"Alike Angular" => "Alike Angular",
							"Allan" => "Allan",
							"Allerta" => "Allerta",
							"Allerta Stencil" => "Allerta Stencil",
							"Allura" => "Allura",
							"Almendra" => "Almendra",
							"Almendra Display" => "Almendra Display",
							"Almendra SC" => "Almendra SC",
							"Amarante" => "Amarante",
							"Amaranth" => "Amaranth",
							"Amatic SC" => "Amatic SC",
							"Amethysta" => "Amethysta",
							"Anaheim" => "Anaheim",
							"Andada" => "Andada",
							"Andika" => "Andika",
							"Angkor" => "Angkor",
							"Annie Use Your Telescope" => "Annie Use Your Telescope",
							"Anonymous Pro" => "Anonymous Pro",
							"Antic" => "Antic",
							"Antic Didone" => "Antic Didone",
							"Antic Slab" => "Antic Slab",
							"Anton" => "Anton",
							"Arapey" => "Arapey",
							"Arbutus" => "Arbutus",
							"Arbutus Slab" => "Arbutus Slab",
							"Architects Daughter" => "Architects Daughter",
							"Archivo Black" => "Archivo Black",
							"Archivo Narrow" => "Archivo Narrow",
							"Arimo" => "Arimo",
							"Arizonia" => "Arizonia",
							"Armata" => "Armata",
							"Artifika" => "Artifika",
							"Arvo" => "Arvo",
							"Asap" => "Asap",
							"Asset" => "Asset",
							"Astloch" => "Astloch",
							"Asul" => "Asul",
							"Atomic Age" => "Atomic Age",
							"Aubrey" => "Aubrey",
							"Audiowide" => "Audiowide",
							"Autour One" => "Autour One",
							"Average" => "Average",
							"Average Sans" => "Average Sans",
							"Averia Gruesa Libre" => "Averia Gruesa Libre",
							"Averia Libre" => "Averia Libre",
							"Averia Sans Libre" => "Averia Sans Libre",
							"Averia Serif Libre" => "Averia Serif Libre",
							"Bad Script" => "Bad Script",
							"Balthazar" => "Balthazar",
							"Bangers" => "Bangers",
							"Basic" => "Basic",
							"Battambang" => "Battambang",
							"Baumans" => "Baumans",
							"Bayon" => "Bayon",
							"Belgrano" => "Belgrano",
							"Belleza" => "Belleza",
							"BenchNine" => "BenchNine",
							"Bentham" => "Bentham",
							"Berkshire Swash" => "Berkshire Swash",
							"Bevan" => "Bevan",
							"Bigelow Rules" => "Bigelow Rules",
							"Bigshot One" => "Bigshot One",
							"Bilbo" => "Bilbo",
							"Bilbo Swash Caps" => "Bilbo Swash Caps",
							"Bitter" => "Bitter",
							"Black Ops One" => "Black Ops One",
							"Bokor" => "Bokor",
							"Bonbon" => "Bonbon",
							"Boogaloo" => "Boogaloo",
							"Bowlby One" => "Bowlby One",
							"Bowlby One SC" => "Bowlby One SC",
							"Brawler" => "Brawler",
							"Bree Serif" => "Bree Serif",
							"Bubblegum Sans" => "Bubblegum Sans",
							"Bubbler One" => "Bubbler One",
							"Buda" => "Buda",
							"Buenard" => "Buenard",
							"Butcherman" => "Butcherman",
							"Butterfly Kids" => "Butterfly Kids",
							"Cabin" => "Cabin",
							"Cabin Condensed" => "Cabin Condensed",
							"Cabin Sketch" => "Cabin Sketch",
							"Caesar Dressing" => "Caesar Dressing",
							"Cagliostro" => "Cagliostro",
							"Calligraffitti" => "Calligraffitti",
							"Cambo" => "Cambo",
							"Candal" => "Candal",
							"Cantarell" => "Cantarell",
							"Cantata One" => "Cantata One",
							"Cantora One" => "Cantora One",
							"Capriola" => "Capriola",
							"Cardo" => "Cardo",
							"Carme" => "Carme",
							"Carrois Gothic" => "Carrois Gothic",
							"Carrois Gothic SC" => "Carrois Gothic SC",
							"Carter One" => "Carter One",
							"Caudex" => "Caudex",
							"Cedarville Cursive" => "Cedarville Cursive",
							"Ceviche One" => "Ceviche One",
							"Changa One" => "Changa One",
							"Chango" => "Chango",
							"Chau Philomene One" => "Chau Philomene One",
							"Chela One" => "Chela One",
							"Chelsea Market" => "Chelsea Market",
							"Chenla" => "Chenla",
							"Cherry Cream Soda" => "Cherry Cream Soda",
							"Cherry Swash" => "Cherry Swash",
							"Chewy" => "Chewy",
							"Chicle" => "Chicle",
							"Chivo" => "Chivo",
							"Cinzel" => "Cinzel",
							"Cinzel Decorative" => "Cinzel Decorative",
							"Clicker Script" => "Clicker Script",
							"Coda" => "Coda",
							"Coda Caption" => "Coda Caption",
							"Codystar" => "Codystar",
							"Combo" => "Combo",
							"Comfortaa" => "Comfortaa",
							"Coming Soon" => "Coming Soon",
							"Concert One" => "Concert One",
							"Condiment" => "Condiment",
							"Content" => "Content",
							"Contrail One" => "Contrail One",
							"Convergence" => "Convergence",
							"Cookie" => "Cookie",
							"Copse" => "Copse",
							"Corben" => "Corben",
							"Courgette" => "Courgette",
							"Cousine" => "Cousine",
							"Coustard" => "Coustard",
							"Covered By Your Grace" => "Covered By Your Grace",
							"Crafty Girls" => "Crafty Girls",
							"Creepster" => "Creepster",
							"Crete Round" => "Crete Round",
							"Crimson Text" => "Crimson Text",
							"Croissant One" => "Croissant One",
							"Crushed" => "Crushed",
							"Cuprum" => "Cuprum",
							"Cutive" => "Cutive",
							"Cutive Mono" => "Cutive Mono",
							"Damion" => "Damion",
							"Dancing Script" => "Dancing Script",
							"Dangrek" => "Dangrek",
							"Dawning of a New Day" => "Dawning of a New Day",
							"Days One" => "Days One",
							"Delius" => "Delius",
							"Delius Swash Caps" => "Delius Swash Caps",
							"Delius Unicase" => "Delius Unicase",
							"Della Respira" => "Della Respira",
							"Denk One" => "Denk One",
							"Devonshire" => "Devonshire",
							"Didact Gothic" => "Didact Gothic",
							"Diplomata" => "Diplomata",
							"Diplomata SC" => "Diplomata SC",
							"Domine" => "Domine",
							"Donegal One" => "Donegal One",
							"Doppio One" => "Doppio One",
							"Dorsa" => "Dorsa",
							"Dosis" => "Dosis",
							"Dr Sugiyama" => "Dr Sugiyama",
							"Droid Sans" => "Droid Sans",
							"Droid Sans Mono" => "Droid Sans Mono",
							"Droid Serif" => "Droid Serif",
							"Duru Sans" => "Duru Sans",
							"Dynalight" => "Dynalight",
							"EB Garamond" => "EB Garamond",
							"Eagle Lake" => "Eagle Lake",
							"Eater" => "Eater",
							"Economica" => "Economica",
							"Electrolize" => "Electrolize",
							"Elsie" => "Elsie",
							"Elsie Swash Caps" => "Elsie Swash Caps",
							"Emblema One" => "Emblema One",
							"Emilys Candy" => "Emilys Candy",
							"Engagement" => "Engagement",
							"Englebert" => "Englebert",
							"Enriqueta" => "Enriqueta",
							"Erica One" => "Erica One",
							"Esteban" => "Esteban",
							"Euphoria Script" => "Euphoria Script",
							"Ewert" => "Ewert",
							"Exo" => "Exo",
							"Expletus Sans" => "Expletus Sans",
							"Fanwood Text" => "Fanwood Text",
							"Fascinate" => "Fascinate",
							"Fascinate Inline" => "Fascinate Inline",
							"Faster One" => "Faster One",
							"Fasthand" => "Fasthand",
							"Federant" => "Federant",
							"Federo" => "Federo",
							"Felipa" => "Felipa",
							"Fenix" => "Fenix",
							"Finger Paint" => "Finger Paint",
							"Fjalla One" => "Fjalla One",
							"Fjord One" => "Fjord One",
							"Flamenco" => "Flamenco",
							"Flavors" => "Flavors",
							"Fondamento" => "Fondamento",
							"Fontdiner Swanky" => "Fontdiner Swanky",
							"Forum" => "Forum",
							"Francois One" => "Francois One",
							"Freckle Face" => "Freckle Face",
							"Fredericka the Great" => "Fredericka the Great",
							"Fredoka One" => "Fredoka One",
							"Freehand" => "Freehand",
							"Fresca" => "Fresca",
							"Frijole" => "Frijole",
							"Fruktur" => "Fruktur",
							"Fugaz One" => "Fugaz One",
							"GFS Didot" => "GFS Didot",
							"GFS Neohellenic" => "GFS Neohellenic",
							"Gabriela" => "Gabriela",
							"Gafata" => "Gafata",
							"Galdeano" => "Galdeano",
							"Galindo" => "Galindo",
							"Gentium Basic" => "Gentium Basic",
							"Gentium Book Basic" => "Gentium Book Basic",
							"Geo" => "Geo",
							"Geostar" => "Geostar",
							"Geostar Fill" => "Geostar Fill",
							"Germania One" => "Germania One",
							"Gilda Display" => "Gilda Display",
							"Give You Glory" => "Give You Glory",
							"Glass Antiqua" => "Glass Antiqua",
							"Glegoo" => "Glegoo",
							"Gloria Hallelujah" => "Gloria Hallelujah",
							"Goblin One" => "Goblin One",
							"Gochi Hand" => "Gochi Hand",
							"Gorditas" => "Gorditas",
							"Goudy Bookletter 1911" => "Goudy Bookletter 1911",
							"Graduate" => "Graduate",
							"Grand Hotel" => "Grand Hotel",
							"Gravitas One" => "Gravitas One",
							"Great Vibes" => "Great Vibes",
							"Griffy" => "Griffy",
							"Gruppo" => "Gruppo",
							"Gudea" => "Gudea",
							"Habibi" => "Habibi",
							"Hammersmith One" => "Hammersmith One",
							"Hanalei" => "Hanalei",
							"Hanalei Fill" => "Hanalei Fill",
							"Handlee" => "Handlee",
							"Hanuman" => "Hanuman",
							"Happy Monkey" => "Happy Monkey",
							"Headland One" => "Headland One",
							"Henny Penny" => "Henny Penny",
							"Herr Von Muellerhoff" => "Herr Von Muellerhoff",
							"Holtwood One SC" => "Holtwood One SC",
							"Homemade Apple" => "Homemade Apple",
							"Homenaje" => "Homenaje",
							"IM Fell DW Pica" => "IM Fell DW Pica",
							"IM Fell DW Pica SC" => "IM Fell DW Pica SC",
							"IM Fell Double Pica" => "IM Fell Double Pica",
							"IM Fell Double Pica SC" => "IM Fell Double Pica SC",
							"IM Fell English" => "IM Fell English",
							"IM Fell English SC" => "IM Fell English SC",
							"IM Fell French Canon" => "IM Fell French Canon",
							"IM Fell French Canon SC" => "IM Fell French Canon SC",
							"IM Fell Great Primer" => "IM Fell Great Primer",
							"IM Fell Great Primer SC" => "IM Fell Great Primer SC",
							"Iceberg" => "Iceberg",
							"Iceland" => "Iceland",
							"Imprima" => "Imprima",
							"Inconsolata" => "Inconsolata",
							"Inder" => "Inder",
							"Indie Flower" => "Indie Flower",
							"Inika" => "Inika",
							"Irish Grover" => "Irish Grover",
							"Istok Web" => "Istok Web",
							"Italiana" => "Italiana",
							"Italianno" => "Italianno",
							"Jacques Francois" => "Jacques Francois",
							"Jacques Francois Shadow" => "Jacques Francois Shadow",
							"Jim Nightshade" => "Jim Nightshade",
							"Jockey One" => "Jockey One",
							"Jolly Lodger" => "Jolly Lodger",
							"Josefin Sans" => "Josefin Sans",
							"Josefin Slab" => "Josefin Slab",
							"Joti One" => "Joti One",
							"Judson" => "Judson",
							"Julee" => "Julee",
							"Julius Sans One" => "Julius Sans One",
							"Junge" => "Junge",
							"Jura" => "Jura",
							"Just Another Hand" => "Just Another Hand",
							"Just Me Again Down Here" => "Just Me Again Down Here",
							"Kameron" => "Kameron",
							"Karla" => "Karla",
							"Kaushan Script" => "Kaushan Script",
							"Kavoon" => "Kavoon",
							"Keania One" => "Keania One",
							"Kelly Slab" => "Kelly Slab",
							"Kenia" => "Kenia",
							"Khmer" => "Khmer",
							"Kite One" => "Kite One",
							"Knewave" => "Knewave",
							"Kotta One" => "Kotta One",
							"Koulen" => "Koulen",
							"Kranky" => "Kranky",
							"Kreon" => "Kreon",
							"Kristi" => "Kristi",
							"Krona One" => "Krona One",
							"La Belle Aurore" => "La Belle Aurore",
							"Lancelot" => "Lancelot",
							"Lato" => "Lato",
							"League Script" => "League Script",
							"Leckerli One" => "Leckerli One",
							"Ledger" => "Ledger",
							"Lekton" => "Lekton",
							"Lemon" => "Lemon",
							"Libre Baskerville" => "Libre Baskerville",
							"Life Savers" => "Life Savers",
							"Lilita One" => "Lilita One",
							"Limelight" => "Limelight",
							"Linden Hill" => "Linden Hill",
							"Lobster" => "Lobster",
							"Lobster Two" => "Lobster Two",
							"Londrina Outline" => "Londrina Outline",
							"Londrina Shadow" => "Londrina Shadow",
							"Londrina Sketch" => "Londrina Sketch",
							"Londrina Solid" => "Londrina Solid",
							"Lora" => "Lora",
							"Love Ya Like A Sister" => "Love Ya Like A Sister",
							"Loved by the King" => "Loved by the King",
							"Lovers Quarrel" => "Lovers Quarrel",
							"Luckiest Guy" => "Luckiest Guy",
							"Lusitana" => "Lusitana",
							"Lustria" => "Lustria",
							"Macondo" => "Macondo",
							"Macondo Swash Caps" => "Macondo Swash Caps",
							"Magra" => "Magra",
							"Maiden Orange" => "Maiden Orange",
							"Mako" => "Mako",
							"Marcellus" => "Marcellus",
							"Marcellus SC" => "Marcellus SC",
							"Marck Script" => "Marck Script",
							"Margarine" => "Margarine",
							"Marko One" => "Marko One",
							"Marmelad" => "Marmelad",
							"Marvel" => "Marvel",
							"Mate" => "Mate",
							"Mate SC" => "Mate SC",
							"Maven Pro" => "Maven Pro",
							"McLaren" => "McLaren",
							"Meddon" => "Meddon",
							"MedievalSharp" => "MedievalSharp",
							"Medula One" => "Medula One",
							"Megrim" => "Megrim",
							"Meie Script" => "Meie Script",
							"Merienda" => "Merienda",
							"Merienda One" => "Merienda One",
							"Merriweather" => "Merriweather",
							"Merriweather Sans" => "Merriweather Sans",
							"Metal" => "Metal",
							"Metal Mania" => "Metal Mania",
							"Metamorphous" => "Metamorphous",
							"Metrophobic" => "Metrophobic",
							"Michroma" => "Michroma",
							"Milonga" => "Milonga",
							"Miltonian" => "Miltonian",
							"Miltonian Tattoo" => "Miltonian Tattoo",
							"Miniver" => "Miniver",
							"Miss Fajardose" => "Miss Fajardose",
							"Modern Antiqua" => "Modern Antiqua",
							"Molengo" => "Molengo",
							"Molle" => "Molle",
							"Monda" => "Monda",
							"Monofett" => "Monofett",
							"Monoton" => "Monoton",
							"Monsieur La Doulaise" => "Monsieur La Doulaise",
							"Montaga" => "Montaga",
							"Montez" => "Montez",
							"Montserrat" => "Montserrat",
							"Montserrat Alternates" => "Montserrat Alternates",
							"Montserrat Subrayada" => "Montserrat Subrayada",
							"Moul" => "Moul",
							"Moulpali" => "Moulpali",
							"Mountains of Christmas" => "Mountains of Christmas",
							"Mouse Memoirs" => "Mouse Memoirs",
							"Mr Bedfort" => "Mr Bedfort",
							"Mr Dafoe" => "Mr Dafoe",
							"Mr De Haviland" => "Mr De Haviland",
							"Mrs Saint Delafield" => "Mrs Saint Delafield",
							"Mrs Sheppards" => "Mrs Sheppards",
							"Muli" => "Muli",
							"Mystery Quest" => "Mystery Quest",
							"Neucha" => "Neucha",
							"Neuton" => "Neuton",
							"New Rocker" => "New Rocker",
							"News Cycle" => "News Cycle",
							"Niconne" => "Niconne",
							"Nixie One" => "Nixie One",
							"Nobile" => "Nobile",
							"Nokora" => "Nokora",
							"Norican" => "Norican",
							"Nosifer" => "Nosifer",
							"Nothing You Could Do" => "Nothing You Could Do",
							"Noticia Text" => "Noticia Text",
							"Noto Sans" => "Noto Sans",
							"Noto Serif" => "Noto Serif",
							"Nova Cut" => "Nova Cut",
							"Nova Flat" => "Nova Flat",
							"Nova Mono" => "Nova Mono",
							"Nova Oval" => "Nova Oval",
							"Nova Round" => "Nova Round",
							"Nova Script" => "Nova Script",
							"Nova Slim" => "Nova Slim",
							"Nova Square" => "Nova Square",
							"Numans" => "Numans",
							"Nunito" => "Nunito",
							"Odor Mean Chey" => "Odor Mean Chey",
							"Offside" => "Offside",
							"Old Standard TT" => "Old Standard TT",
							"Oldenburg" => "Oldenburg",
							"Oleo Script" => "Oleo Script",
							"Oleo Script Swash Caps" => "Oleo Script Swash Caps",
							"Open Sans" => "Open Sans",
							"Open Sans Condensed" => "Open Sans Condensed",
							"Oranienbaum" => "Oranienbaum",
							"Orbitron" => "Orbitron",
							"Oregano" => "Oregano",
							"Orienta" => "Orienta",
							"Original Surfer" => "Original Surfer",
							"Oswald" => "Oswald",
							"Over the Rainbow" => "Over the Rainbow",
							"Overlock" => "Overlock",
							"Overlock SC" => "Overlock SC",
							"Ovo" => "Ovo",
							"Oxygen" => "Oxygen",
							"Oxygen Mono" => "Oxygen Mono",
							"PT Mono" => "PT Mono",
							"PT Sans" => "PT Sans",
							"PT Sans Caption" => "PT Sans Caption",
							"PT Sans Narrow" => "PT Sans Narrow",
							"PT Serif" => "PT Serif",
							"PT Serif Caption" => "PT Serif Caption",
							"Pacifico" => "Pacifico",
							"Paprika" => "Paprika",
							"Parisienne" => "Parisienne",
							"Passero One" => "Passero One",
							"Passion One" => "Passion One",
							"Patrick Hand" => "Patrick Hand",
							"Patrick Hand SC" => "Patrick Hand SC",
							"Patua One" => "Patua One",
							"Paytone One" => "Paytone One",
							"Peralta" => "Peralta",
							"Permanent Marker" => "Permanent Marker",
							"Petit Formal Script" => "Petit Formal Script",
							"Petrona" => "Petrona",
							"Philosopher" => "Philosopher",
							"Piedra" => "Piedra",
							"Pinyon Script" => "Pinyon Script",
							"Pirata One" => "Pirata One",
							"Plaster" => "Plaster",
							"Play" => "Play",
							"Playball" => "Playball",
							"Playfair Display" => "Playfair Display",
							"Playfair Display SC" => "Playfair Display SC",
							"Podkova" => "Podkova",
							"Poiret One" => "Poiret One",
							"Poller One" => "Poller One",
							"Poly" => "Poly",
							"Pompiere" => "Pompiere",
							"Pontano Sans" => "Pontano Sans",
							"Port Lligat Sans" => "Port Lligat Sans",
							"Port Lligat Slab" => "Port Lligat Slab",
							"Prata" => "Prata",
							"Preahvihear" => "Preahvihear",
							"Press Start 2P" => "Press Start 2P",
							"Princess Sofia" => "Princess Sofia",
							"Prociono" => "Prociono",
							"Prosto One" => "Prosto One",
							"Puritan" => "Puritan",
							"Purple Purse" => "Purple Purse",
							"Quando" => "Quando",
							"Quantico" => "Quantico",
							"Quattrocento" => "Quattrocento",
							"Quattrocento Sans" => "Quattrocento Sans",
							"Questrial" => "Questrial",
							"Quicksand" => "Quicksand",
							"Quintessential" => "Quintessential",
							"Qwigley" => "Qwigley",
							"Racing Sans One" => "Racing Sans One",
							"Radley" => "Radley",
							"Raleway" => "Raleway",
							"Raleway Dots" => "Raleway Dots",
							"Rambla" => "Rambla",
							"Rammetto One" => "Rammetto One",
							"Ranchers" => "Ranchers",
							"Rancho" => "Rancho",
							"Rationale" => "Rationale",
							"Redressed" => "Redressed",
							"Reenie Beanie" => "Reenie Beanie",
							"Revalia" => "Revalia",
							"Ribeye" => "Ribeye",
							"Ribeye Marrow" => "Ribeye Marrow",
							"Righteous" => "Righteous",
							"Risque" => "Risque",
							"Roboto" => "Roboto",
							"Roboto Condensed" => "Roboto Condensed",
							"Roboto Slab" => "Roboto Slab",
							"Rochester" => "Rochester",
							"Rock Salt" => "Rock Salt",
							"Rokkitt" => "Rokkitt",
							"Romanesco" => "Romanesco",
							"Ropa Sans" => "Ropa Sans",
							"Rosario" => "Rosario",
							"Rosarivo" => "Rosarivo",
							"Rouge Script" => "Rouge Script",
							"Ruda" => "Ruda",
							"Rufina" => "Rufina",
							"Ruge Boogie" => "Ruge Boogie",
							"Ruluko" => "Ruluko",
							"Rum Raisin" => "Rum Raisin",
							"Ruslan Display" => "Ruslan Display",
							"Russo One" => "Russo One",
							"Ruthie" => "Ruthie",
							"Rye" => "Rye",
							"Sacramento" => "Sacramento",
							"Sail" => "Sail",
							"Salsa" => "Salsa",
							"Sanchez" => "Sanchez",
							"Sancreek" => "Sancreek",
							"Sansita One" => "Sansita One",
							"Sarina" => "Sarina",
							"Satisfy" => "Satisfy",
							"Scada" => "Scada",
							"Schoolbell" => "Schoolbell",
							"Seaweed Script" => "Seaweed Script",
							"Sevillana" => "Sevillana",
							"Seymour One" => "Seymour One",
							"Shadows Into Light" => "Shadows Into Light",
							"Shadows Into Light Two" => "Shadows Into Light Two",
							"Shanti" => "Shanti",
							"Share" => "Share",
							"Share Tech" => "Share Tech",
							"Share Tech Mono" => "Share Tech Mono",
							"Shojumaru" => "Shojumaru",
							"Short Stack" => "Short Stack",
							"Siemreap" => "Siemreap",
							"Sigmar One" => "Sigmar One",
							"Signika" => "Signika",
							"Signika Negative" => "Signika Negative",
							"Simonetta" => "Simonetta",
							"Sintony" => "Sintony",
							"Sirin Stencil" => "Sirin Stencil",
							"Six Caps" => "Six Caps",
							"Skranji" => "Skranji",
							"Slackey" => "Slackey",
							"Smokum" => "Smokum",
							"Smythe" => "Smythe",
							"Sniglet" => "Sniglet",
							"Snippet" => "Snippet",
							"Snowburst One" => "Snowburst One",
							"Sofadi One" => "Sofadi One",
							"Sofia" => "Sofia",
							"Sonsie One" => "Sonsie One",
							"Sorts Mill Goudy" => "Sorts Mill Goudy",
							"Source Code Pro" => "Source Code Pro",
							"Source Sans Pro" => "Source Sans Pro",
							"Special Elite" => "Special Elite",
							"Spicy Rice" => "Spicy Rice",
							"Spinnaker" => "Spinnaker",
							"Spirax" => "Spirax",
							"Squada One" => "Squada One",
							"Stalemate" => "Stalemate",
							"Stalinist One" => "Stalinist One",
							"Stardos Stencil" => "Stardos Stencil",
							"Stint Ultra Condensed" => "Stint Ultra Condensed",
							"Stint Ultra Expanded" => "Stint Ultra Expanded",
							"Stoke" => "Stoke",
							"Strait" => "Strait",
							"Sue Ellen Francisco" => "Sue Ellen Francisco",
							"Sunshiney" => "Sunshiney",
							"Supermercado One" => "Supermercado One",
							"Suwannaphum" => "Suwannaphum",
							"Swanky and Moo Moo" => "Swanky and Moo Moo",
							"Syncopate" => "Syncopate",
							"Tangerine" => "Tangerine",
							"Taprom" => "Taprom",
							"Tauri" => "Tauri",
							"Telex" => "Telex",
							"Tenor Sans" => "Tenor Sans",
							"Text Me One" => "Text Me One",
							"The Girl Next Door" => "The Girl Next Door",
							"Tienne" => "Tienne",
							"Tinos" => "Tinos",
							"Titan One" => "Titan One",
							"Titillium Web" => "Titillium Web",
							"Trade Winds" => "Trade Winds",
							"Trocchi" => "Trocchi",
							"Trochut" => "Trochut",
							"Trykker" => "Trykker",
							"Tulpen One" => "Tulpen One",
							"Ubuntu" => "Ubuntu",
							"Ubuntu Condensed" => "Ubuntu Condensed",
							"Ubuntu Mono" => "Ubuntu Mono",
							"Ultra" => "Ultra",
							"Uncial Antiqua" => "Uncial Antiqua",
							"Underdog" => "Underdog",
							"Unica One" => "Unica One",
							"UnifrakturCook" => "UnifrakturCook",
							"UnifrakturMaguntia" => "UnifrakturMaguntia",
							"Unkempt" => "Unkempt",
							"Unlock" => "Unlock",
							"Unna" => "Unna",
							"VT323" => "VT323",
							"Vampiro One" => "Vampiro One",
							"Varela" => "Varela",
							"Varela Round" => "Varela Round",
							"Vast Shadow" => "Vast Shadow",
							"Vibur" => "Vibur",
							"Vidaloka" => "Vidaloka",
							"Viga" => "Viga",
							"Voces" => "Voces",
							"Volkhov" => "Volkhov",
							"Vollkorn" => "Vollkorn",
							"Voltaire" => "Voltaire",
							"Waiting for the Sunrise" => "Waiting for the Sunrise",
							"Wallpoet" => "Wallpoet",
							"Walter Turncoat" => "Walter Turncoat",
							"Warnes" => "Warnes",
							"Wellfleet" => "Wellfleet",
							"Wendy One" => "Wendy One",
							"Wire One" => "Wire One",
							"Yanone Kaffeesatz" => "Yanone Kaffeesatz",
							"Yellowtail" => "Yellowtail",
							"Yeseva One" => "Yeseva One",
							"Yesteryear" => "Yesteryear",
							"Zeyada" => "Zeyada"
);

$font_family_google_weight = array(
							'0'=>'Select Font Weight For Heading Font',
							'300' => '300',
							'400' => '400',
							'500' => '500',
							'600' => '600',
							'700' => '700'
);


$animations = array(
		'0' => 'No animations',
		'fadeIn'=>'fadeIn',
		'fadeInUp'=>'fadeInUp',
		'fadeInUpLarge'=>'fadeInUpLarge',
		'fadeInDown'=>'fadeInDown',
		'fadeInDownLarge'=>'fadeInDownLarge',
		'fadeInLeft'=>'fadeInLeft',
		'fadeInLeftLarge'=>'fadeInLeftLarge',
		'fadeInRight'=>'fadeInRight',
		'fadeInRightLarge'=>'fadeInRightLarge',
		'bounceIn'=>'bounceIn',
		'bounceInLarge'=>'bounceInLarge',
		'bounceInUp'=>'bounceInUp',
		'bounceInUpLarge'=>'bounceInUpLarge',
		'bounceInDown'=>'bounceInDown',
		'bounceInDownLarge'=>'bounceInDownLarge',
		'bounceInLeft'=>'bounceInLeft',
		'bounceInLeftLarge'=>'bounceInLeftLarge',
		'bounceInRight'=>'bounceInRight',
		'bounceInRightLarge'=>'bounceInRightLarge',
		'zoomIn'=>'zoomIn',
		'zoomInUp'=>'zoomInUp',
		'zoomInUpLarge'=>'zoomInUpLarge',
		'zoomInDown'=>'zoomInDown',
		'zoomInDownLarge'=>'zoomInDownLarge',
		'zoomInLeft'=>'zoomInLeft',
		'zoomInLeftLarge'=>'zoomInLeftLarge',
		'zoomInRight'=>'zoomInRight',
		'zoomInRightLarge'=>'zoomInRightLarge',
);
					

// Set the Options Array
global $of_options;
$of_options = array();
						
				
		$of_options[] = array( "name" => "General",
								"icon"		=> "cogs",
								"type" => "heading");
		
		$of_options[] = array( "name" => "Favicon",
							"desc" => "Upload a favicon for your website. (PNG Only) ",
							"id" => "favicon",
							"std" => "",
							"type" => "upload");
							
		$of_options[] = array( "name" => "Enable smooth scrolling?",
							"desc" => "Enable this if you want a smooth scrolling on your page.",
							"id" => "smooth_scroll",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable);

		$of_options[] = array( "name" => "Homepage Title",
							"desc" => "Enter the title of homepage.",
							"id" => "home_title",
							"std" => "",
							"type" => "text");		
		
		/*$of_options[] = array( "name" => "Google Analytics",
							"desc" => "",
							"id" => "google_analytics",
							"std" => "",
							"type" => "textarea");*/
		
		$of_options[] = array( "name" => "Header",
								"icon"		=> "list-ul",
								"type" => "heading");
								
		$of_options[] = array( "name" => "Logo",
							"desc" => 'To change the logo of your theme please go <a href="'.esc_url(admin_url('themes.php?page=mmpm_options_mega_main_menu')).'">here</a>',
							"id" => "logo",
							"std" => '',
							"type" => "");	
							
		$of_options[] = array( "name" => "Logo height",
							"desc" => "Add a custom height logo. Enter only numbers no px.>",
							"id" => "logo_height",
							"std" => "",
							"type" => "text");
		
		$of_options[] = array( "name" => "Sticky menu background",
								"desc" => "Pick a color for the sticky menu.",
								"id" => "sticky_menu_bg_color",
								"std" => "",
								"type" => "color");
		
		$of_options[] = array( "name" => "Sticky menu opacity background",
							"desc" => "Enter transparency for sticky menu background like this 0.1. Note 0.1 is the lowest 1.0 it's the highest.",
							"id" => "sticky_menu_bg_opacity",
							"std" => "",
							"type" => "text");
									
		
		$of_options[] = array( "name" => "Enable shout-box breadcrumb?",
							"desc" => "Shoutbox breadcrumb navigation enable/disable.",
							"id" => "shoutbox_enable",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable);	

		$of_options[] = array( "name" => "Shoutbox breadcrumb font size",
							"desc" => "Add a custom font size for breadcrumb. Enter only numbers no px.>",
							"id" => "shoutbox_font_size",
							"std" => "",
							"type" => "text");							

		
		$of_options[] = array( "name" => "Header Background Image",
							"desc" => "Add a Header Background Image",
							"id" => "header_bg_image",
							"std" => "",
							"type" => "upload");


		/*$of_options[] = array( "name" => "Breadcrumb Background Image",
							"desc" => "Add a Breadcrumb Background Image",
							"id" => "breadcrumb_bg_image",
							"std" => get_template_directory_uri().'/images/breadcrumb2.jpg',
							"type" => "upload");*/				
	
		$of_options[] = array( "name" => "Appearance",
						"icon"		=> "paint-brush",
						"type" => "heading");
						
		$of_options[] = array( "name" => "Theme Main Color",
								"desc" => "Pick a color scheme for the theme.",
								"id" => "color_theme",
								"std" => "",
								"type" => "color"
								);
						
		$of_options[] = array( "name" => "Theme Background",
							"desc" => "Upload an image asa a background.",
							"id" => "background_bg",
							"std" => "",
							"type" => "upload");
							
		$of_options[] = array( "name" => "Background Color",
								"desc" => "Pick a background color for the theme (default: #fff).",
								"id" => "color_bg",
								"std" => "",
								"type" => "color"
								);
											
		$of_options[] = array( "name" => "Enable animations?",
							"desc" => "Turn animations on/off.",
							"id" => "animate",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable);
							
		/*$of_options[] = array( "name" => "Enable orange dot at the end of headings?",
							"desc" => "Enable/disable the orange dot at the end of headings",
							"id" => "red_dot_enable",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable); */
	
		$of_options[] = array( "name" => "Boxed width",
							"desc" => "By checking this the theme will use boxed layout.",
							"id" => "layoutboxed2",
							"std" => "",
							"type" => "checkbox");
							
		$of_options[] = array( "name" => "Container width",
							"desc" => "Enter a container width. Default is 1170px. Enter only numbers, no px.>",
							"id" => "container_width",
							"std" => "",
							"type" => "text");
		
		$of_options[] = array( "name" => "Show theme option panel",
							"desc" => "By checking this the option panel will show in front-end.",
							"id" => "pannelshow",
							"std" => "",
							"type" => "checkbox");			
								

		$of_options[] = array( "name" => "Typography",
						"icon"		=> "font",
						"type" => "heading");
									

		$of_options[] = array( "name" => "Headings Font Family",
							"desc" => "Select a font family for your headings",
							"id" => "heading_font_face",
							"std" => "",
							"type" => "select",
							"options"=>$font_family_google);
		
		$of_options[] = array( "name" => "Headings Font Weight",
							"desc" => "Select a font weight for your headings",
							"id" => "heading_font_weight",
							"std" => "",
							"type" => "select",
							"options"=>$font_family_google_weight);
		
		$of_options[] = array( "name" => "Body Font Family",
							"desc" => "Select a font family for your body",
							"id" => "body_font_face",
							"std" => "",
							"type" => "select",
							"options"=>$font_family_google);
		
		$of_options[] = array( "name" => "Body Font Weight",
							"desc" => "Select a font weight for your body",
							"id" => "body_font_weight",
							"std" => "",
							"type" => "select",
							"options"=>$font_family_google_weight);
	
		
		$of_options[] = array( "name" => "Portfolio",
								"icon"		=> "suitcase",
								"type" => "heading");
		
		
		$of_options[] = array( "name" => "How many portfolio items per page?",
							"desc" => "",
							"id" => "portfolio_no",
							"std" => "",
							"type" => "text");			
		
		$of_options[] = array( "name" => "Portfolio Posts Animations",
							"desc" => "Select portfolio posts appear animations",
							"id" => "portfolio_posts_animations",
							"std" => "",
							"type" => "select",
							"options"=>$animations);
		
		
		$of_options[] = array( "name" => "Portfolio delay appear animation?",
							"desc" => "",
							"id" => "portfolio_posts_animations_delay",
							"std" => "",
							"type" => "text");
		
		
		$of_options[] = array( "name" => "Blog",
									"icon"		=> "edit",
								"type" => "heading");
		
		
		$of_options[] = array( "name" => "Enter your blog page title",
							"desc" => "",
							"id" => "blog_title_breadcrumb",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Enable author bio on single posts page?",
							"desc" => "Select if you want to enable author bio on single posts page",
							"id" => "enable_author_bio",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable);		
							
		$of_options[] = array( "name" => "Blog Posts Animations",
							"desc" => "Select blog posts appear animations",
							"id" => "blog_posts_animations",
							"std" => "",
							"type" => "select",
							"options"=>$animations);
		
		$of_options[] = array( "name" => "Blog Posts Animations Delay",
							"desc" => "Select blog posts appear animations delay",
							"id" => "blog_posts_animations_delay",
							"std" => "",
							"type" => "text");
		

		$of_options[] = array( "name" => "Social",
								"icon"		=> "facebook",
							"type" => "heading");

		$of_options[] = array( "name" => "Facebook URL",
							"desc" => "Add facebook url here.",
							"id" => "facebook",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Twitter URL",
							"desc" => "Add twitter url here.",
							"id" => "twitter",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Google URL",
							"desc" => "Add google url here.",
							"id" => "google",
							"std" => "",
							"type" => "text");	

		$of_options[] = array( "name" => "Linkedin",
							"desc" => "Add Linkedin url here.",
							"id" => "linkedin",
							"std" => "",
							"type" => "text");	
							
		$of_options[] = array( "name" => "RSS URL",
							"desc" => "Add external RSS URL, like Feedburner, etc.",
							"id" => "rss",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Dribbble URL",
							"desc" => "Add Dribbble url here.",
							"id" => "dribbble",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Youtube URL",
							"desc" => "Add Youtube url here.",
							"id" => "youtube",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Tumblr URL",
							"desc" => "Add Tumblr url here.",
							"id" => "tumblr",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Pinterest URL",
							"desc" => "Add Pinterest url here.",
							"id" => "pinterest",
							"std" => "",
							"type" => "text");		
							
		$of_options[] = array( "name" => "Dropbox URL",
							"desc" => "Add Dropbox url here.",
							"id" => "dropbox",
							"std" => "",
							"type" => "text");
		
		$of_options[] = array( "name" => "Contact",
									"icon"		=> "envelope",
								"type" => "heading");		
		
		$of_options[] = array( "name" => "Google Maps For Contact Page",
							"desc" => "Insert Google Maps URL here.",
							"id" => "google_maps",
							"std" => "",
							"type" => "textarea");
		
		$of_options[] = array( "name" => "Enable info widget on contact form?",
							"desc" => "Select if you want to display the info widget on contact form",
							"id" => "enable_info_widget",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable);

		$of_options[] = array( "name" => "Widget Phone Text",
							"desc" => "",
							"id" => "info_phone_text",
							"std" => "",
							"type" => "text");		
		
		$of_options[] = array( "name" => "Widget Phone Number",
							"desc" => "",
							"id" => "info_phone_number",
							"std" => "",
							"type" => "text");		
		
		$of_options[] = array( "name" => "Widget Address text",
							"desc" => "",
							"id" => "info_address_text",
							"std" => "",
							"type" => "text");		
		
		$of_options[] = array( "name" => "Widget Address",
							"desc" => "",
							"id" => "info_address",
							"std" => "",
							"type" => "text");		
		
		$of_options[] = array( "name" => "Widget Email text",
							"desc" => "",
							"id" => "info_email_text",
							"std" => "",
							"type" => "text");	
		
		$of_options[] = array( "name" => "Widget Email",
							"desc" => "",
							"id" => "info_email",
							"std" => "",
							"type" => "text");									
							
		

		$of_options[] = array( "name" => "Footer",
								"icon"		=> "bars",
							"type" => "heading");
							
		$of_options[] = array( "name" => "Footer logo",
							"desc" => "Upload the logo for your footer.",
							"id" => "footer_logo",
							"std" => "",
							"type" => "upload");
							
											
		$of_options[] = array( "name" => "Enable big phone number section",
							"desc" => "Enable big section phone number in the footer.",
							"id" => "enable_phone",
							"std" => "",
							"type" => "select",
							"options" => $enable_disable);
							
		$of_options[] = array( "name" => "Footer keep in touch text",
							"desc" => "Enter keep in touch text",
							"id" => "footer_keep_in_touch",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Footer big prefix phone number",
							"desc" => "Enter big prefix phone number",
							"id" => "footer_prefix_phone",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Footer big phone number",
							"desc" => "Enter big phone number",
							"id" => "footer_phone_number",
							"std" => "",
							"type" => "text");							
						
		/*$of_options[] = array( "name" => "Footer About Title.",
							"desc" => "You can change about footer title here.",
							"id" => "footer_about_title",
							"std" => "",
							"type" => "text");		

		$of_options[] = array( "name" => "Footer About Content",
							"desc" => "You can change the footer about content area.",
							"id" => "footer_about_content",
							"std" => "",
							"type" => "textarea");	

		$of_options[] = array( "name" => "Footer Location",
							"desc" => "",
							"id" => "footer_location",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Footer E-Mail Adress",
							"desc" => "",
							"id" => "footer_email",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Footer Phone Number",
							"desc" => "",
							"id" => "footer_phone",
							"std" => "",
							"type" => "text");						
		
		$of_options[] = array( "name" => "Footer Time",
							"desc" => "",
							"id" => "footer_time",
							"std" => "",
							"type" => "text"); */
							
		$of_options[] = array( "name" => "Footer Widget Animations",
							"desc" => "Select footer widgets appear animations",
							"id" => "footer_widgets_animations",
							"std" => "",
							"type" => "select",
							"options"=>$animations);
							
		$of_options[] = array( "name" => "Footer Copyright Text",
							"desc" => "Enter copyright text.",
							"id" => "footer_copyright",
							"std" => "",
							"type" => "text");
							
		$of_options[] = array( "name" => "Custom CSS",
								"icon"		=> "code",
							"type" => "heading");

		$of_options[] = array( "name" => "CSS Code",
							"desc" => "Add here your own custom CSS code for the theme.",
							"id" => "css_code",
							"std" => "",
							"type" => "textarea");

							
		$of_options[] = array( "name" => "Importer",
									"icon"		=> "cloud-upload",
								"type" => "heading");

		$of_options[] = array( "name" => "Import All Dummy Data Content at once",
								"desc" => "Import posts, pages, portfolio posts, sliders etc. This will make the theme look like in the live demo.<br/><br/><strong>Note:This process can take up to 1-3 mins. depending on your server performance. DO NOT PRESS Import button twice!</strong>",
								"id" => "import_all_dummy_data",
								"std" => admin_url('themes.php?page=optionsframework') . "&import_all_dummy_data=true",
								"btntext" => 'Import All Dummy Data',
								"type" => "button");
								
		$of_options[] = array( "name" => "Import Homepage 1 Only",
		"desc" => "This will import only Homepage 1. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_01",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_01=true",
		"btntext" => 'Import Homepage 1 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 2 Only",
		"desc" => "This will import only Homepage 2. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_02",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_02=true",
		"btntext" => 'Import Homepage 2 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 3 Only",
		"desc" => "This will import only Homepage 3. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_03",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_03=true",
		"btntext" => 'Import Homepage 3 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 4 Only",
		"desc" => "This will import only Homepage 4. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_04",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_04=true",
		"btntext" => 'Import Homepage 4 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 5 Only",
		"desc" => "This will import only Homepage 5. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_05",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_05=true",
		"btntext" => 'Import Homepage 5 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 6 Only",
		"desc" => "This will import only Homepage 6. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_06",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_06=true",
		"btntext" => 'Import Homepage 6 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 7 Only",
		"desc" => "This will import only Homepage 7. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_07",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_07=true",
		"btntext" => 'Import Homepage 7 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 8 Only",
		"desc" => "This will import only Homepage 8. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_08",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_08=true",
		"btntext" => 'Import Homepage 8 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 9 Only",
		"desc" => "This will import only Homepage 9. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_09",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_09=true",
		"btntext" => 'Import Homepage 9 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 10 Only",
		"desc" => "This will import only Homepage 10. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_10",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_10=true",
		"btntext" => 'Import Homepage 10 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 11 Only",
		"desc" => "This will import only Homepage 11. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_11",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_11=true",
		"btntext" => 'Import Homepage 11 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 12 Only",
		"desc" => "This will import only Homepage 12. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_12",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_12=true",
		"btntext" => 'Import Homepage 12 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 13 Only",
		"desc" => "This will import only Homepage 13. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_13",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_13=true",
		"btntext" => 'Import Homepage 13 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 14 Only",
		"desc" => "This will import only Homepage 14. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_14",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_14=true",
		"btntext" => 'Import Homepage 14 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 15 Only",
		"desc" => "This will import only Homepage 15. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_15",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_15=true",
		"btntext" => 'Import Homepage 15 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 16 Only",
		"desc" => "This will import only Homepage 16. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_16",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_16=true",
		"btntext" => 'Import Homepage 16 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 17 Only",
		"desc" => "This will import only Homepage 17. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_17",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_17=true",
		"btntext" => 'Import Homepage 17 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 18 Only",
		"desc" => "This will import only Homepage 18. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_18",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_18=true",
		"btntext" => 'Import Homepage 18 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 19 Only",
		"desc" => "This will import only Homepage 19. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_19",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_19=true",
		"btntext" => 'Import Homepage 19 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 20 Only",
		"desc" => "This will import only Homepage 20. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_20",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_20=true",
		"btntext" => 'Import Homepage 20 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 21 Only",
		"desc" => "This will import only Homepage 21. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_21",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_21=true",
		"btntext" => 'Import Homepage 21 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 22 Only",
		"desc" => "This will import only Homepage 22. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_22",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_22=true",
		"btntext" => 'Import Homepage 22 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 23 Only",
		"desc" => "This will import only Homepage 23. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_23",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_23=true",
		"btntext" => 'Import Homepage 23 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 24 Only",
		"desc" => "This will import only Homepage 24. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_24",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_24=true",
		"btntext" => 'Import Homepage 24 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 25 Only",
		"desc" => "This will import only Homepage 25. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_25",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_25=true",
		"btntext" => 'Import Homepage 25 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 26 Only",
		"desc" => "This will import only Homepage 26. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_26",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_26=true",
		"btntext" => 'Import Homepage 26 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 27 Only",
		"desc" => "This will import only Homepage 27. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_27",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_27=true",
		"btntext" => 'Import Homepage 27 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 28 Only",
		"desc" => "This will import only Homepage 28. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_28",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_28=true",
		"btntext" => 'Import Homepage 28 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 29 Only",
		"desc" => "This will import only Homepage 29. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_29",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_29=true",
		"btntext" => 'Import Homepage 29 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 30 Only",
		"desc" => "This will import only Homepage 30. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_30",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_30=true",
		"btntext" => 'Import Homepage 30 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 31 Only",
		"desc" => "This will import only Homepage 31. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_31",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_31=true",
		"btntext" => 'Import Homepage 31 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 32 Only",
		"desc" => "This will import only Homepage 32. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_32",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_32=true",
		"btntext" => 'Import Homepage 32 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 33 Only",
		"desc" => "This will import only Homepage 33. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_33",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_33=true",
		"btntext" => 'Import Homepage 33 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 34 Only",
		"desc" => "This will import only Homepage 34. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_34",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_34=true",
		"btntext" => 'Import Homepage 34 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 35 Only",
		"desc" => "This will import only Homepage 35. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_35",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_35=true",
		"btntext" => 'Import Homepage 35 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 36 Only",
		"desc" => "This will import only Homepage 36. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_36",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_36=true",
		"btntext" => 'Import Homepage 36 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 37 Only",
		"desc" => "This will import only Homepage 37. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_37",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_37=true",
		"btntext" => 'Import Homepage 37 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 38 Only",
		"desc" => "This will import only Homepage 38. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_38",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_38=true",
		"btntext" => 'Import Homepage 38 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 39 Only",
		"desc" => "This will import only Homepage 39. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_39",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_39=true",
		"btntext" => 'Import Homepage 39 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 40 Only",
		"desc" => "This will import only Homepage 40. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_40",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_40=true",
		"btntext" => 'Import Homepage 40 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 41 Only",
		"desc" => "This will import only Homepage 41. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_41",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_41=true",
		"btntext" => 'Import Homepage 41 Only',
		"type" => "button");

		$of_options[] = array( "name" => "Import Homepage 42 Only",
		"desc" => "This will import only Homepage 42. DO NOT PRESS Import button twice!",
		"id" => "import_homepage_42",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_homepage_42=true",
		"btntext" => 'Import Homepage 42 Only',
		"type" => "button");
		
		$of_options[] = array( "name" => "Import Features Pages Only",
		"desc" => "This will import only Features Pages as seen in the live demo. DO NOT PRESS Import button twice!",
		"id" => "import_features",
		"std" => admin_url('themes.php?page=optionsframework') . "&import_features=true",
		"btntext" => 'Import Features Pages Only',
		"type" => "button");

	}//End function: of_options()
}//End check if function exists: of_options()
?>