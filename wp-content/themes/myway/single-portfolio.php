<?php get_header(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			 <?php echo get_the_title(); ?>
		</div>
	</div>
</div>
</header><!--HEADER END-->
<?php
			  
			
			$categories = wp_get_object_terms( get_the_ID(), 'types');
			$separator = ' ';
			$output = '';
			$class = "";
			if($categories){
			foreach($categories as $category) {
			$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s",'myway' ), $category->name ) ) . '">'.$category->name.'</a>'.$separator;
			$class .= $category->slug." ";
				}
			}
?>	
<?php if (function_exists('get_field')) : /* We only display the single details if ACF functions exists - START */ ?>
<div class="port-single-body" id="single-projects"><!--PORTFOLIO SINGLE WRAP START-->
	
	<div class="container" id="project-single"><!--CONTAINER START-->
		
		<div id="carousel-details-holder" class="col-md-8 paddright0"><!--carousel-details-holder START-->
	
	<?php 	if ( 'video' == get_post_format() ) : //IF it's a video portfolio we display the videos instead of carousel?>	
	<?php //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>							

											<?php if (function_exists('get_field')) : //IF function get_field exists ?>
											
													<?php $video_id_youtube = get_field('video_id_youtube'); ?>

														<?php if ($video_id_youtube) : //IF it's youtube video ?>
												
															<div id="youtube_video">
																<div class="flex-video widescreen">
																	
																	<iframe width="100%" height="100%" src="http://www.youtube.com/embed/<?php echo $video_id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
																
																</div>
															</div>
																	
														
														<?php endif; //ENDIF it's youtube video ?>
														
														
													<?php $video_id_vimeo = get_field('video_id_vimeo'); ?>
													
														<?php if ($video_id_vimeo) : //IF it's vimeo video ?>
												
															<div id="vimeo_video">
																<div class="flex-video widescreen">
																	
																	<iframe src="//player.vimeo.com/video/<?php echo $video_id_vimeo; ?>?title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
																
																</div>
															</div>	
														
														<?php endif;  //ENDIF it's vimeo video?>
												
											<?php endif; //ENDIF function get_field exists ?>
<?php //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>

<?php else : //IF it's image portfolio we display the carousel  ?>

<?php //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>

	
		<div id="carousel" class="carousel slide-carousel" data-ride="carousel"><!--CAROUSEL START-->

			 <div class="carousel-inner"><!--CAROUSEL INNER START-->

				<?php $i=1; if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				
					<?php
					$portfolio_images = get_field('portfolio_images');
					if($portfolio_images): ?>
						<?php foreach($portfolio_images as $images): ?>
							 <div class="item <?php if ($i==1) echo 'active'; ?>">
								<?php echo '<img src="'.esc_url(aq_resize( $images['image'], 793, 600, true,true,true,true)).'" alt="'.esc_attr(get_the_title()).'"/>'; ?>
							</div>
						<?php $i++; endforeach; ?>
					<?php endif; ?>
								
				
					<?php endwhile; // end of the loop. ?>  
			</div><!--CAROUSEL INNER END-->
	

			<?php if (count($portfolio_images) > 1) : ?>
				<!-- Controls -->
				<div id="portfolio-single-carousel-controls">
					  <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
						<i class="pe-7f-angle-left"></i>
					  </a>
					  <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
						<i class="pe-7f-angle-right"></i>
					  </a>
				</div>
			<?php endif; ?>
		</div><!--CAROUSEL END-->
<?php //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// ?>
<?php endif; //end big IF it's a video ?>

		<div class="project_date_and_category"><!--project_date_and_category START-->
			<div class="title_desc_holder">
			  <h1><?php echo get_the_title(); ?></h1>
			  <div class="content_p"><?php the_content(); ?></div>
			</div>
			  <p class="project_date_cat_demo">
					<span class="text1"><i class="fa fa-calendar"></i></span>
					<span class="text2"><?php _e('Date Added','myway') ?></span> 
					<span class="text3"><?php echo the_time('d M Y'); ?></span> 
			  </p>
			  <p class="project_date_cat_demo">
					<span class="text1"><i class="fa fa-bars"></i></span>
					<span class="text2"><?php _e('Category','myway') ?></span> 
					<span class="text3"><?php echo esc_attr($categories[0]->name); ?></span> 
			  </p>
			  <?php $live_demo_text = get_field('live_demo'); if ($live_demo_text) : ?>
			  <a href="<?php echo esc_url($live_demo_text); ?>" target="_blank">
				  <p class="project_date_cat_demo">
						<span class="text1"><i class="fa fa-link"></i></span>
						<span class="text2"><?php _e('Live','myway') ?></span> 
						<span class="text3"><?php _e('Demo','myway') ?></a></span> 	
				  </p>
			  </a>
			  <?php endif; ?>
		</div><!--project_date_and_category END-->

	
	</div><!--carousel-details-holder END-->
	
	<div class="col-md-4"><!--COL-MD-4 START-->	
		
		<?php $values = get_field('extra_details'); if ($values) : ?>
		<div class="services_home2 portfolio_details"><!--SERVICES 2 START-->
		<h3 class="project_desc"><?php _e('Project Description','myway') ?><span class="dotcolor">.</span></h3>
			<?php 
				$values = get_field('extra_details');
				foreach ($values as $value) : ?>
						<div class="col-md-12 col-sm-6 col-xs-12"><!--COLS START-->
							<div class="box-services"><!--box-services START-->
								<span class="servicesnumber"><i class="fa <?php echo esc_attr($value['icon']);?>"></i></span> 
									<div class="services-box">
										<h2><?php echo esc_attr($value['heading_text']);?><span class="dotcolor">.</span></h2>
									</div><!--services-box-->
									<p><?php echo esc_attr($value['description']);?></p>
								<div class="line1"></div>
							</div><!--box-services END-->
						</div><!--COLS END-->	
				<?php endforeach; ?>	
		</div><!--SERVICES 2 END-->
		<?php endif; ?>
	
		<?php $values = get_field('extra_details_2'); if ($values) : ?>
		<div class="funny_facts"><!--FUNNY FACTS START-->
				<?php 
				$values2 = get_field('extra_details_2');
				foreach ($values2 as $value2) : ?>
				<div class="col-md-6 col-sm-4 col-xs-6"><!--col-md-6 START-->
					<div class="custom-boxes-facts">
						<div class="icon-facts"><i class="fa <?php echo esc_attr($value2['icon']);?>"></i>
							<div class="clear"></div>
						</div>
						<h2 class="number-facts counter"><?php echo esc_attr($value2['big_number']);?></h2>
						<p class="desc-facts"><?php echo esc_attr($value2['description']);?><span class="dotcolor">.</span> </p>
					</div>							
				</div><!--col-md-6 END-->
				<?php endforeach; ?>
		</div><!--FUNNY FACTS END-->
		<?php endif; ?>
		
	</div><!--COL-MD-4 END-->
	
 </div><!--CONTAINER END-->
	
</div><!--PORTFOLIO SINGLE WRAP END-->
<?php else : ?>
<h2><?php _e('It seems like you did not install ACF (Advanced Custom Fields Plugin). This plugin is required in order to display the portfolio single details.','myway'); ?></h2>
<?php endif; /* We only display the single details if ACF functions exists - END */ ?>
<?php get_footer(); ?>