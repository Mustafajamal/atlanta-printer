<?php get_header(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			<?php _e( 'Page not found', 'myway' ); ?>
		</div>
	</div>
</div>
</header><!--HEADER END-->
<div id="blog-body"><!--BLOG BODY START-->
		<div id="blog-wraper" class="container"><!--BLOG WRAPPER START-->
			<article id="post-404">
				<h1><?php _e( 'Whoopsie...It is the nasty', 'myway' ); ?></h1>
				<h1 class="not-found-404"><i class="icon-file-link"></i> <?php _e( '404 !', 'myway' ); ?></h1>
				<h2>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php _e( 'Return home?', 'myway' ); ?>"></a>
				</h2>
			</article>
		</div><!--BLOG WRAPPER END-->
</div><!--BLOG BODY END-->
<?php get_footer(); ?>