<?php 
	
	session_start();
	$path = $_SERVER['DOCUMENT_ROOT'];

	include_once $path . '/wp-config.php';
	
	function fetchaddress($address){
		global $wpdb;
		$results = $wpdb->get_results( "SELECT `StreetAddress` FROM `address_book` WHERE `user_id`='".$_SESSION['userid']."' AND `StreetAddress` LIKE '".$address."%'", ARRAY_A );
		echo json_encode($results);
		
	}

	function fetchAddressNames($org_name){
		global $wpdb;
		$results = $wpdb->get_results( "SELECT * FROM `address_book` WHERE `user_id`='".$_SESSION['userid']."' AND `Name` LIKE '".$org_name."%'", ARRAY_A );
		echo json_encode($results);
	}

	function fetchAddressData($add_id){
		global $wpdb;
		$results = $wpdb->get_row( "SELECT * FROM `address_book` WHERE `user_id`='".$_SESSION['userid']."' AND `add_id` = '".$add_id."%'");
		echo json_encode($results);
	}

	function usernameavalibel($username){
		global $wpdb;
		$results = $wpdb->get_results( "SELECT `user_name` FROM `signup_information` WHERE `user_name`='".$username."'", ARRAY_A );
		$rowCount = $wpdb->num_rows;
		
		if($rowCount>0){
		
			$msg="UserName Already Taken!!";
			echo json_encode($msg);
		
		}
		else{

			$msg="Avaliable";
			echo json_encode($msg);	
		
		}
	
	}
	

	if(isset($_POST['fetchaddress'])){

		$address=$_POST['address'];
		fetchaddress($address);
	}

	if(isset($_POST['fetchAddressNames'])){
		$org_name = $_POST['org_name'];
		fetchAddressNames($org_name);
	}

	if(isset($_POST['fetchAddressData'])){
		$add_id = $_POST['add_id'];
		fetchAddressData($add_id);
	}

	if(isset($_POST['usernameavalibel'])){
		$username=$_POST['username'];
		usernameavalibel($username);
	}

?>