<?php
/*
The footer template
*/
?>
<?php global $herowp_data; ?>
<footer><!--FOOTER START-->
	
	<?php if (!empty($herowp_data['enable_phone'])): ?>
		<?php if ($herowp_data['enable_phone'] == 'enable'): ?>
		<div class="big_phone_wrapper">
			<div class="container">
				<div class="col-md-12">
					<div class="big_phone">
						<p class="small_text"><?php if (!empty($herowp_data['footer_keep_in_touch'])) { echo esc_attr($herowp_data['footer_keep_in_touch']); }?></p>
						<p class="big_text">
							<strong><?php if (!empty($herowp_data['footer_prefix_phone'])) {echo esc_attr($herowp_data['footer_prefix_phone']); } ?></strong>
							<?php if (!empty($herowp_data['footer_phone_number'])) { echo esc_attr($herowp_data['footer_phone_number']); } ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
	
	<div class="footer-wrap"><!--FOOTER WRAP START-->
		<div class="container"><!--CONTAINER START-->
		
		<?php if(!empty($herowp_data['footer_logo'])): ?>
			<div class="left-logo">
				   <img class="footer-logo" src="<?php echo esc_url($herowp_data['footer_logo']);?>" alt="<?php the_title(); ?>" />   
			</div>
		 <?php endif; ?>

		<div class="footer-box col-md-4" <?php if (!empty($herowp_data['footer_widgets_animations'])) { if ($herowp_data['footer_widgets_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['footer_widgets_animations'].'" data-delay="200"'; }} ?>>
			<?php dynamic_sidebar('first_footer'); ?>
		</div>
		
		<div class="footer-box col-md-4" <?php if (!empty($herowp_data['footer_widgets_animations'])) { if ($herowp_data['footer_widgets_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['footer_widgets_animations'].'" data-delay="200"'; }} ?>>
			<?php dynamic_sidebar('second_footer'); ?>
		</div>
		
		<div class="footer-box col-md-4" <?php if (!empty($herowp_data['footer_widgets_animations'])) { if ($herowp_data['footer_widgets_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['footer_widgets_animations'].'" data-delay="200"'; }} ?>>
			<?php dynamic_sidebar('third_footer'); ?>
		</div>
		
	</div><!--CONTAINER END-->
</div><!--FOOTER WRAP END-->
	
</footer><!--FOOTER END-->
<div id="footer_copyright"><!--footer_copyright START-->
	
	<div class="container">
	
		
		
		<?php if(!empty($herowp_data['footer_copyright'])) : ?>
			<p><?php echo wp_kses_data($herowp_data['footer_copyright']); ?></p>
		<?php endif; ?>
		
		<div class="header-social footer-social"><!--SOCIAL START-->
						<ul>
							 <?php if(!empty($herowp_data['facebook'])) : ?>
								<li class="facebook"><a href="<?php echo esc_url($herowp_data['facebook']);?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
							 <?php endif; ?>
							 
							 <?php if(!empty($herowp_data['twitter'])): ?>
								<li class="twitter"><a href="<?php echo esc_url($herowp_data['twitter']);?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
							 <?php endif; ?>
							 
							 
							 <?php if(!empty($herowp_data['linkedin'])) : ?>
								<li class="linkedin"><a href="<?php echo esc_url($herowp_data['linkedin']);?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							 <?php endif; ?>
							 
							 							 
							 <?php if(!empty($herowp_data['youtube'])) : ?>
								<li class="youtube"><a href="<?php echo esc_url($herowp_data['youtube']);?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
							 <?php endif; ?>
							 
							 <						 
						</ul>
				</div><!--SOCIAL END--> 
	</div>
</div><!--footer_copyright END-->
</div><!--WRAP BOXED FULL END-->
<?php wp_footer(); ?>
</body>
</html>