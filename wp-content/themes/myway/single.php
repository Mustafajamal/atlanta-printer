<?php get_header();
wp_reset_postdata();
foreach((get_the_category()) as $category) {
    $category->cat_name . ' ';
	$category_name=$category->cat_name;
}
?>
<div class="shout-wrap"><!--SHOUT WRAP START-->
	<div class="container">
		<div class="col-lg-12">
			<div class="shout-box-title">
				<?php if (!empty($herowp_data['blog_title_breadcrumb'])){ echo esc_attr($herowp_data['blog_title_breadcrumb']); } ?><span class="dotcolor">.</span>
			</div>
		</div>
	</div>
</div><!--SHOUT WRAP END-->
</header><!--HEADER END-->
<div id="blog-body" class="blog_posts_block1"><!--BLOG BODY START-->
	<div id="blog-wraper" class="container"><!--BLOG WRAPPER START-->	
	  <div id="blog-grid-1-type-2" class="col-md-8"><!--BLOG GRID ONE TYPE TWO START-->		
		<div class="post-holder"><!--POST HOLDER START-->				
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
			<div class="col-md-12 nopadding blog-box" <?php if (!empty($herowp_data['blog_posts_animations'])){ if ($herowp_data['blog_posts_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['blog_posts_animations'].'" data-delay="0"'; }}?>><!--BLOG POST START-->
	
                            <section><!--section START-->
                                <div class="blog_img"><!--blog_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
												$id_img = get_post_thumbnail_id();
												$img_url = wp_get_attachment_url( $id_img,'full');
												$thumb = aq_resize($img_url, 783, 643, true, true,true,true); if ( !$thumb ) $thumb = $img_url;
										?>
												<a href="<?php the_permalink(); ?>">
										<?php
													echo '<img src="'.esc_url($thumb).'" alt="'.esc_attr(get_the_title()).'" class="img-responsive" />
												</a>';
											}
											else echo'<img src="'.esc_url('http://placehold.it/783x643/333333/ffffff').'" alt="'.esc_attr(get_the_title()).'">';
                                        ?>
											
									<div class="date_added_and_category"><!--date_added_and_category START-->
										<div class="round_date">
											<p><?php echo the_time('d'); ?></p>
											<span><?php echo the_time('M'); ?></span>
										</div>
										
										<div class="round_comments">
											<p><?php echo get_comments_number(); ?></p>
											<span><?php _e('comm','myway'); ?></span>
										</div>
										
										<div class="category">
											<p><?php echo $category_name; ?></p>
											<span><?php _e('Category','myway'); ?></span>
										</div>
								   </div><!--date_added_and_category END-->
                               </div><!--blog_img END-->
                                   
                               <h2><?php echo the_title(); ?><span class="dotcolor"> .</span></h2>
							   <div class="tagcloud-single"><?php the_tags('','',''); ?></div>
							   <div class="categories-single"><strong><?php _e('Posted in: ','myway'); ?></strong><?php the_category(' '); ?></div>
							   <?php the_content(); ?>
							   
						<?php if (!empty($herowp_data['enable_author_bio'])) : ?>
							<?php if ($herowp_data['enable_author_bio'] == 'enable'): ?>
								<div class="author-bio">
											<?php echo get_avatar( get_the_author_meta('email'), '90' ); ?>
											<div class="author-info">
												<h4 class="author-title"><?php _e('Post written by','myway'); ?><?php the_author_link(); ?></h3>
												<h6 class="author-title"><?php echo the_author_meta('user_firstname'); ?> <?php echo the_author_meta('user_lastname'); ?> // <a href="<?php the_author_meta('user_url');?>"><?php _e('Visit website','myway'); ?></a></h6>
												<p class="author-description"><?php the_author_meta('description'); ?></p>
												
												<ul class="author-icons">
													<?php 
	
														$google_profile = get_the_author_meta( 'google_profile' );
														if ( $google_profile && $google_profile != '' ) {
															echo '<li class="google"><a href="' . esc_url($google_profile) . '" rel="author"><i class="fa fa-google"></i></a></li>';
														}
														
														$twitter_profile = get_the_author_meta( 'twitter_profile' );
														if ( $twitter_profile && $twitter_profile != '' ) {
															echo '<li class="twitter"><a href="' . esc_url($twitter_profile) . '"><i class="fa fa-twitter"></i></a></li>';
														}
														
														$facebook_profile = get_the_author_meta( 'facebook_profile' );
														if ( $facebook_profile && $facebook_profile != '' ) {
															echo '<li class="facebook"><a href="' . esc_url($facebook_profile) . '"><i class="fa fa-facebook"></i></a></li>';
														}														
														
														$dribbble_profile = get_the_author_meta( 'dribbble_profile' );
														if ( $dribbble_profile && $dribbble_profile != '' ) {
															echo '<li class="dribbble"><a href="' . esc_url($dribbble_profile) . '"><i class="fa fa-dribbble"></i></a></li>';
														}
														
														$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
														if ( $linkedin_profile && $linkedin_profile != '' ) {
															echo '<li class="linkedin"><a href="' . esc_url($linkedin_profile) . '"><i class="fa fa-linkedin"></i></a></li>';
														}
													?>
												</ul>
											</div>
								<!--END .author-bio-->
								</div>
							<?php endif;?>
						<?php endif;?>	
								
								<!-- comments -->
								<div class="blog-comments">
									<?php comments_template( '', true );//'/comments-template.php'); ?>
								</div>
								<!-- /comments -->
								
								
								
                       </section><!--section END-->

			</div><!--BLOG POST END-->		
		</article>
		<!-- /article -->
						
	
	</div><!--POST HOLDER END-->
</div>
		<?php get_sidebar('my-bar'); ?>
</div><!--BLOG WRAPPER END-->
</div><!--BLOG BODY END-->
<?php get_footer(); ?>