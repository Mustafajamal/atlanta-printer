<?php 
/*
Template name: without Quote
*/
get_header();
?>
<?php herowp_output_custom_header_bg(); ?>
<?php herowp_output_custom_page_bg_color(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			 <?php echo get_the_title(); ?><span class="dotcolor">.</span>
		</div>
	</div>
</div>
</header><!--HEADER END-->
<div id="page-body">
	<div class="container">
		<div class="col-md-8">
			<div id="page-content">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<?php the_content(); ?>
				<?php endwhile; ?>

				<?php else: ?>

				<article>
					<h2><?php _e( 'Sorry, nothing to display.', 'myway' ); ?></h2>
				</article>
				<?php endif; ?>
				
			</div>			
		</div>

			<?php get_sidebar('my-bar'); ?>

	</div>
		
</div>	
<?php get_footer(); ?>
