<?php

	session_start();
	$path = $_SERVER['DOCUMENT_ROOT'];

	include_once $path . '/wp-config.php';

	function deleteAddress($add_ids){
		global $wpdb;
		$result = $wpdb->query("DELETE FROM `address_book` WHERE `add_id` IN ($add_ids)");

		echo "$result";
	}

	function exportTransaction(){
		global $wpdb;

		$transactions = $wpdb->get_results( "SELECT * FROM `transaction_details`", ARRAY_A );

		$counter = 0;
		$trans_headers = "";
		$trans_data = "";

		$file_name = "transaction_data.csv";
		$myfile = fopen($file_name, "w") or die("Unable to open file!");
		foreach ($transactions as $trans){
			foreach ($trans as $key => $value){
				if($counter == 0){
					$trans_headers = $trans_headers.$key."||";
				}
				$trans_data = $trans_data.$value."||";
			}
			$counter++;
			if($counter == 1){
				fputcsv($myfile, explode('||',$trans_headers), ",", '"', ",");
			}
			fputcsv($myfile, explode('||',$trans_data), ",", '"', ",");
			$trans_data = "";
		}
		fclose($myfile);

		header("Content-type: application/csv; charset=utf-8");
        header("Content-disposition: attachment; filename =\"" .$file_name. "\"");
        readfile($file_name);
        unlink($file_name);
        exit;
	}

	// Calling methods on the basis of params.
	if(isset($_POST['deleteAddress'])){
		$add_ids = $_POST['add_ids'];
		deleteAddress($add_ids);
	}

	// Calling methods on the basis of params.
	if(isset($_GET['exportTransaction'])){
		exportTransaction();
	}
?>