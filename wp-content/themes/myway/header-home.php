<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php global $herowp_data; ?>
<head>
 <meta charset="<?php bloginfo('charset'); ?>">
 <title><?php wp_title( '|', true, 'right' ); ?></title>
 <?php if (!empty ($herowp_data['favicon'])) : ?>
	 <?php if ($herowp_data['favicon']) : ?>
		<link href="<?php echo esc_url($herowp_data['favicon']); ?>" rel="icon" type="image/png">
	 <?php endif; ?>
 <?php endif; ?>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8">
 <meta name="viewport" content="width=device-width,initial-scale=1.0">
 <meta name="description" content="<?php bloginfo('description'); ?>">
 <?php require_once ('inc/custom-options.php'); /* A few options from Appearance -> Theme Options*/ ?>
 <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php require_once ('inc/front-pannel.php'); /*The front panel*/ ?>
<div <?php if (!empty($boxed_version)) {  echo $boxed_version; } ?> <?php if (!empty($boxed_version_panel)) { echo $boxed_version_panel;} ?>  id="wrap-home"><!--WRAP boxed/full START-->
<header class="home"><!--HEADER START-->
  
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114846220-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-114846220-1');
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->

	<div class="container"><!--CONTAINER HEADER START-->
		<div class="header-top-bar"></div>
		<nav class="header-menu col-md-12"><!--MENU START-->
			<?php if ( has_nav_menu( 'mega_main_sidebar_menu' )) : ?>
				<?php wp_nav_menu( array( "theme_location" => "mega_main_sidebar_menu" ) ); ?>
			<?php else : ?>
				<?php
					//if no menu is created we point the user to the menu page to create one
					$herowp_menu_url = '<a href="'.esc_url(admin_url('nav-menus.php')).'">here</a>';
					$herowp_logo_url = '<a href="'.esc_url(admin_url('themes.php?page=mmpm_options_mega_main_menu')).'">here</a>';
					printf('<p class="notice_header">'.__("Please create a menu %s and assign the Header Mega Menu location. To add a logo to your menu go %s", "myway").'</p>', $herowp_menu_url, $herowp_logo_url);
				?>
			<?php endif; ?>
		</nav><!--MENU END-->
  </div><!--CONTAINER HEADER END-->
</header><!--HEADER END-->
