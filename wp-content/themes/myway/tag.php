<?php get_header(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			<?php echo single_tag_title (__('Tag ','myway'), false); ?>
		</div>
	</div>
</div>
</header><!--HEADER END-->
<div id="blog-body" class="blog_posts_block1"><!--BLOG BODY START-->
	<div id="blog-wraper" class="container"><!--BLOG WRAPPER START-->	
		<div id="blog-grid-1-type-2" class="col-md-8"><!--BLOG GRID ONE TYPE TWO START-->
			<?php
			if (!empty($herowp_data['blog_posts_animations_delay'])){
				$i=$herowp_data['blog_posts_animations_delay'];
			}
			if (have_posts()): while (have_posts()) : the_post();
			?>
			
			
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
			<div class="col-md-12 nopadding blog-box" <?php if (!empty($herowp_data['blog_posts_animations']) && !empty($herowp_data['blog_posts_animations_delay']) ){ if ($herowp_data['blog_posts_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['blog_posts_animations'].'" data-delay="'.$i.'"'; }}?>><!--BLOG POST START-->

							
                            <section><!--section START-->
                                <div class="blog_img"><!--blog_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
												$id_img = get_post_thumbnail_id();
												$img_url = wp_get_attachment_url( $id_img,'full');
												$thumb = aq_resize($img_url, 783, 643, true, true,true,true); if ( !$thumb ) $thumb = $img_url;
										?>
												<a href="<?php the_permalink(); ?>">
										<?php
													echo '<img src="'.esc_url($thumb).'" alt="'.esc_attr(get_the_title()).'" class="img-responsive" />
												</a>';
											}
											else echo'<img src="'.esc_url('http://placehold.it/783x643/333333/ffffff').'" alt="'.esc_attr(get_the_title()).'">';
                                        ?>
										<a class="read_more" href="<?php the_permalink(); ?>"><?php _e('Read More','myway'); ?></a>	
                                </div><!--blog_img END-->
								

                               <div class="date_added_and_category"><!--date_added_and_category START-->
									<div class="round_date">
										<p><?php echo the_time('d'); ?></p>
										<span><?php echo the_time('M'); ?></span>
									</div>
									
									<div class="round_comments">
										<p><?php echo get_comments_number(); ?></p>
										<span><?php _e('comm','myway'); ?></span>
									</div>
                               </div><!--date_added_and_category END-->
                                        
                               <h2><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?><span class="dotcolor"> .</span></a></h2>
                               <p><?php echo excerpt(15); ?></p>

                        
                       </section><!--section END-->

		</div><!--BLOG POST END-->
	</article>
	<!-- /article -->

					<?php if (!empty($herowp_data['blog_posts_animations_delay'])){ $i=$i+$herowp_data['blog_posts_animations_delay']; } ?>
										
					<?php endwhile; ?> 
					
					<div class="col-md-12 nopadding">
						<?php herowp_numeric_posts_nav(); ?>
					</div>
					
					<?php endif; wp_reset_postdata(); ?>
					
					
		</div><!--BLOG ONE TYPE TWO END-->
		<?php get_sidebar(); ?>
	</div><!--BLOG WRAPPER END-->
</div><!--BLOG BODY END-->
<?php get_footer(); ?>