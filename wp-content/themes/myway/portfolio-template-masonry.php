<?php 
/*
Template Name: Portfolio Template Masonry
*/
get_header(); ?>
<?php herowp_output_custom_header_bg(); ?>
<?php herowp_output_custom_page_bg_color(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			 <?php echo get_the_title(); ?> 
		</div>
	</div>
</div>
</header><!--HEADER END-->
<?php
if (empty($herowp_data['portfolio_no'])) {$herowp_data['portfolio_no'] = 8 ; }
$term = get_query_var('type'); 
$tax = get_query_var('taxonomy'); 
$args=array('post_type'=> 'portfolio','post_status'=> 'publish', 'orderby'=> 'DESC', 'paged'=>$paged, 'posts_per_page'=>$herowp_data['portfolio_no']); 
$taxargs = array($tax=>$term);
if($term!='' && $tax!='') { $args = array_merge($args, $taxargs); }
query_posts($args); 
?>
<div class="projects-body-page" id="no-full-screen"><!--PROJECTS BODY START-->
	<div class="projects-filter-buttons global-buttons-center container padding30leftright"><!--PROJECTS FILTER START-->
 
						 
						 <ul class="project-filter">
											<?php 
												 $terms = get_terms("types");
												 $count = count($terms);
												 if ( $count > 0 ){
													 echo '
													 <li><a class="active view-all-button" data-filter="*" href="#"><span class="btn_text">'.__('ALL WORK','myway').'</span></a></li>';
													
													 foreach ( $terms as $term ) {
													 $term_id = $term->term_id;
														$term_meta = get_option( "taxonomy_$term_id" );
														
						echo '<li>
							<a data-filter=".' . esc_attr($term->slug).'" href="#" class="view-all-button"><span class="btn_text">' . esc_attr($term->name) .'</span></a>
						 </li>';
														
													 }
												 }
											?> 
						 </ul>

 </div><!--PROJECTS FILTER END-->

<div id="container-projects"><!--CONTAINER START-->
	<div class="portfolio_block1"><!--portfolio_block1 START-->
	<div class="projects" id="projects">
		<?php
			$masonry_sizes     = array('wide','regular','regular','regular','regular','wide_big_height','wide_big_height','regular','regular','regular','regular','wide');
			$placeholder_sizes = array('761x540','380x540','380x540','380x540','380x540','761x1080','761x1080','380x540','380x540','380x540','380x540','761x540');
			$width_sizes       = array('761','380','380','380','380','761','761','380','380','380','380','761');
			$height_sizes      = array('540','540','540','540','540','1080','1080','540','540','540','540','540');
			
			$i = 0;
			while ( have_posts()):the_post();
			$categories = wp_get_object_terms( get_the_ID(), 'types');
			$separator = ' ';
			$output = '';
			$class = "";
			if($categories){
			foreach($categories as $category) {
			$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s",'myway' ), $category->name ) ) . '">'.$category->name.'</a>'.$separator;
			$class .= $category->slug." ";
				}
			}
		?>	
		
		<?php
		
		//portfolio appear delay
		if (empty($herowp_data['portfolio_posts_animations_delay'])){
				$herowp_data['portfolio_posts_animations_delay']=0;
		}
		$j = $herowp_data['portfolio_posts_animations_delay'];
		
		//portfolio animations
		if (!empty($herowp_data['portfolio_posts_animations'])){
			if ($herowp_data['portfolio_posts_animations'] != 'No animations'){
				$herowp_data_animate='class="mix planning margintop0 not-animated" data-animate="'.$herowp_data['portfolio_posts_animations'].'" data-delay="'.$j.'"';
			}
			else{
				$herowp_data_animate='class="mix planning margintop0"';
			}
		}
		?>
		
		<div class="project-item <?php echo $class; echo $masonry_sizes[$i]; ?>" ><!--PORTFOLIO POST START-->
                            <section <?php if (!empty($herowp_data_animate)) { echo $herowp_data_animate; } ?>><!--section START-->
                                <div class="port_img overlay-image view-overlay"><!--port_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
                                            $id_img = get_post_thumbnail_id();
                                            $img_url = wp_get_attachment_url( $id_img,'full');
                                            $thumb = aq_resize($img_url, $width_sizes[$i], $height_sizes[$i], true, true, true, true); if ( !$thumb ) $thumb = $img_url;
                                        ?>
                                    
                                            <?php echo '<img src="'.esc_url($thumb).'" alt="'.esc_attr(get_the_title()).'" class="img-responsive" />';?>
                                            
                                        <?php }
                                        else {
                                       echo '<img src="'.esc_url('http://placehold.it/'.$placeholder_sizes[$i].'/333333/ffffff').'" alt="' . esc_attr( get_the_title() ) . '">';
                                            }
                                        ?>					
                                             <div class="mask"><!--MASK START-->
                        						<div class="port-zoom-link height200"><!--port-zoom-link START-->
                                                	    
														  <a href="<?php the_permalink() ?>"><i class="fa fa-link"></i></a>
														<h2><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?><span class="dotcolor"> .</span></a></h2>
														<p class="category"><?php echo esc_attr($categories[0]->name); ?></p>
													
                                                </div><!--port-zoom-link END-->
                   							 </div><!--MASK END-->
                                </div><!--port_img END-->
                       </section><!--section END-->
		</div><!--PORTFOLIO POST END--> 
	
		<?php 
		$j=$j+$herowp_data['portfolio_posts_animations_delay']; 
		$i = $i + 1 ; if ($i % 12 == 0) { $i = 0; } ; //we reset i and start parsing the array again
		?>
		<?php endwhile; ?>
	</div>
		
 </div><!--portfolio_block1 END-->
</div><!--CONTAINER END-->	
	
	<article class="container">
		<div class="pagination">
				<?php twentytwelve_content_nav(); ?>	
		</div>
	</article>
	
	<div class="container" id="additional_portfolio_text">
		<?php 
			wp_reset_query();	
			if (have_posts()) : while (have_posts()) : the_post();?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>

</div><!--PROJECTS BODY END-->
<?php get_footer(); ?>