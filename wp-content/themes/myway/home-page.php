<?php 

/*

Template Name: Home Page Template with Slider

*/

?>

<?php get_header('home'); ?>
<?php herowp_output_custom_page_bg_color(); ?>

<?php while (have_posts()) : the_post();  $post_id=get_the_ID(); ?>

	<?php the_content();?>

<?php endwhile; wp_reset_postdata(); ?>


<?php get_footer(); ?>