<?php
/*
The footer template
*/
?>
<?php global $herowp_data; ?>
<footer><!--FOOTER START-->
<style>
	.list-unstyled li a{font-size: 18px;}
	.shadowOutline {
  text-shadow: 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black, 0 0 4px black;
}
</style>
	<!-- <?php if (!empty($herowp_data['enable_phone'])): ?>
		<?php if ($herowp_data['enable_phone'] == 'enable'): ?>
		<div class="big_phone_wrapper">
			<div class="container">
				<div class="col-md-12">
					<div class="big_phone">
						<p class="small_text"><?php if (!empty($herowp_data['footer_keep_in_touch'])) { echo esc_attr($herowp_data['footer_keep_in_touch']); }?></p>
						<p class="big_text">
							<strong><?php if (!empty($herowp_data['footer_prefix_phone'])) {echo esc_attr($herowp_data['footer_prefix_phone']); } ?></strong>
							<?php if (!empty($herowp_data['footer_phone_number'])) { echo esc_attr($herowp_data['footer_phone_number']); } ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?> -->

	<div class="footer-wrap"><!--FOOTER WRAP START-->
		<div class="container"><!--CONTAINER START-->
		<div class="footer-box col-md-3" style="margin-top:30px;">
		<ul class="list-unstyled">
			<li><a href="<?=get_site_url()."/Acworth";?>" class="shadowOutline">Acworth</a></li>
			<li><a href="<?=get_site_url()."/Alpharetta";?>" class="shadowOutline">Alpharetta</a></li>
			<li><a href="<?=get_site_url()."/Atlanta";?>" class="shadowOutline">Atlanta</a></li>
			<li><a href="<?=get_site_url()."/Buckhead";?>" class="shadowOutline">Buckhead</a></li>
			<li><a href="<?=get_site_url()."/Chamblee";?>" class="shadowOutline">Chamblee</a></li>
			<li><a href="<?=get_site_url()."/CollegePark";?>" class="shadowOutline">College Park</a></li>
			<li><a href="<?=get_site_url()."/Dallas";?>" class="shadowOutline">Dallas</a></li>
		</ul>
		</div>
		<div class="footer-box col-md-3" style="margin-top:30px;">
		<ul class="list-unstyled">
			<li><a href="<?=get_site_url()."/Decatur";?>" class="shadowOutline">Decatur</a></li>
			<li><a href="<?=get_site_url()."/Doraville";?>" class="shadowOutline">Doraville</a></li>
			<li><a href="<?=get_site_url()."/Douglasville";?>" class="shadowOutline">Douglasville</a></li>
			<li><a href="<?=get_site_url()."/Dunwoody";?>" class="shadowOutline">Dunwoody</a></li>
			<li><a href="<?=get_site_url()."/Duluth";?>" class="shadowOutline">Duluth</a></li>
			<li><a href="<?=get_site_url()."/EastPoint";?>" class="shadowOutline">East Point</a></li>
			<li><a href="<?=get_site_url()."/Jonesboro";?>" class="shadowOutline">Jonesboro</a></li>
		</ul>
		</div>
		<div class="footer-box col-md-3" style="margin-top:30px;">
		<ul class="list-unstyled">
			<li><a href="<?=get_site_url()."/Kennesaw";?>" class="shadowOutline">Kennesaw</a></li>
			<li><a href="<?=get_site_url()."/Marietta";?>" class="shadowOutline">Marietta</a></li>
			<li><a href="<?=get_site_url()."/McDonough";?>" class="shadowOutline">McDonough</a></li>
			<li><a href="<?=get_site_url()."/Midtown";?>" class="shadowOutline">Midtown</a></li>
			<li><a href="<?=get_site_url()."/Newnan";?>" class="shadowOutline">Newnan</a></li>
			<li><a href="<?=get_site_url()."/Norcross";?>" class="shadowOutline">Norcross</a></li>
			<li><a href="<?=get_site_url()."/PeachtreeCity";?>" class="shadowOutline">Peachtree City</a></li>
		</ul>
		</div>
		<div class="footer-box col-md-3" style="margin-top:30px;">
		<ul class="list-unstyled">
			<li><a href="<?=get_site_url()."/Roswell";?>" class="shadowOutline">Roswell</a></li>
			<li><a href="<?=get_site_url()."/SandrySprings";?>" class="shadowOutline">Sandy Springs</a></li>
			<li><a href="<?=get_site_url()."/Smyrna";?>" class="shadowOutline">Smyrna</a></li>
			<li><a href="<?=get_site_url()."/Stockbridge";?>" class="shadowOutline">Stockbridge</a></li>
			<li><a href="<?=get_site_url()."/StoneMountain";?>" class="shadowOutline">Stone Mountain</a></li>
			<li><a href="<?=get_site_url()."/Tucker";?>" class="shadowOutline">Tucker</a></li>
		</ul>
		</div>
		<?php if(!empty($herowp_data['footer_logo'])): ?>
			<div class="left-logo">
				   <img class="footer-logo" src="<?php echo esc_url($herowp_data['footer_logo']);?>" alt="<?php the_title(); ?>" />
			</div>
		 <?php endif; ?>

		<div class="footer-box col-md-4" <?php if (!empty($herowp_data['footer_widgets_animations'])) { if ($herowp_data['footer_widgets_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['footer_widgets_animations'].'" data-delay="200"'; }} ?>>
			<?php dynamic_sidebar('first_footer'); ?>
		</div>

		<div class="footer-box col-md-4" <?php if (!empty($herowp_data['footer_widgets_animations'])) { if ($herowp_data['footer_widgets_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['footer_widgets_animations'].'" data-delay="200"'; }} ?>>
			<?php dynamic_sidebar('second_footer'); ?>
		</div>

		<div class="footer-box col-md-4" <?php if (!empty($herowp_data['footer_widgets_animations'])) { if ($herowp_data['footer_widgets_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['footer_widgets_animations'].'" data-delay="200"'; }} ?>>
			<?php dynamic_sidebar('third_footer'); ?>
		</div>

	</div><!--CONTAINER END-->
</div><!--FOOTER WRAP END-->

</footer><!--FOOTER END-->
<div id="footer_copyright"><!--footer_copyright START-->

	<div class="container">

		<div id="foot" style="float:right;">
		<a href="http://atlantaprintercouriers.com/work-with-us/" class="shadowOutline">Work with us</a>
		<span>|</span>
		<a href="http://atlantaprintercouriers.com/privacy-policy/" class="shadowOutline">Privacy Policy</a>
		<span>|</span>
		<a href="http://atlantaprintercouriers.com/termsconditions/" class="shadowOutline">Terms &amp; Conditions</a>
		<span>|</span>
		<a href="http://atlantaprintercouriers.com/missionstatement/" class="shadowOutline">Mission Statement</a>
		<span>|</span>
		<a href="http://atlantaprintercouriers.com/sitemap/" class="shadowOutline">Sitemap</a>
		<span>|</span>
		<a href="http://atlantaprintercouriers.com/services-6/" class="shadowOutline">Services</a>
		<span>|</span>
		<a href="http://atlantaprintercouriers.com/contact-us/" class="shadowOutline">Contact Us</a>
	</div>


		<?php if(!empty($herowp_data['footer_copyright'])) : ?>
			<p><?php echo wp_kses_data($herowp_data['footer_copyright']); ?></p>
		<?php endif; ?>

		<div class="header-social footer-social"><!--SOCIAL START-->
						<ul>
							 <?php if(!empty($herowp_data['facebook'])) : ?>
								<li class="facebook"><a href="<?php echo esc_url($herowp_data['facebook']);?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
							 <?php endif; ?>

							 <?php if(!empty($herowp_data['twitter'])): ?>
								<li class="twitter"><a href="<?php echo esc_url($herowp_data['twitter']);?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
							 <?php endif; ?>


							 <?php if(!empty($herowp_data['linkedin'])) : ?>
								<li class="linkedin"><a href="<?php echo esc_url($herowp_data['linkedin']);?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							 <?php endif; ?>


							 <?php if(!empty($herowp_data['youtube'])) : ?>
								<li class="youtube"><a href="<?php echo esc_url($herowp_data['youtube']);?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
							 <?php endif; ?>

							 <
						</ul>
				</div><!--SOCIAL END-->
	</div>

</div><!--footer_copyright END-->
</div><!--WRAP BOXED FULL END-->

<?php wp_footer(); ?>

<!-- Google Code for Remarketing Tag -->
<!---
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 817890062;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/817890062/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code for Remarketing Tag -->

</body>
</html>
