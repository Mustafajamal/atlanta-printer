<?php 

/*

Template Name: Home Page Template with Header

*/

?>
<?php get_header(); ?>
<?php herowp_output_custom_header_bg(); ?>
<?php herowp_output_custom_page_bg_color(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			 <?php echo get_the_title(); ?> 
		</div>
	</div>
</div>
</header><!--HEADER END-->

<?php while (have_posts()) : the_post();  $post_id=get_the_ID(); ?>

	<?php the_content();?>

<?php endwhile; ?>


<?php get_footer(); ?>