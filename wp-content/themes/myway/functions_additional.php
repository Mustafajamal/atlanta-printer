<?php

/*
=================================================================================================================
Theme core functions - DO NOT MODIFY THIS BY ANY MEANS!
=================================================================================================================
You have been warned! :-)
=================================================================================================================
*/

/*
=================================================================================================================
Let's start the fun!
=================================================================================================================
*/


/*
=================================================================================================================
Gobal variables
=================================================================================================================
*/
$herowp_theme_name = 'Myway';
global $herowp_theme_name;
/*
=================================================================================================================
HOOKS
=================================================================================================================
*/
require_once ('inc/widgets.php');
require_once ('inc/page-builder/aq-page-builder.php');
require_once ('inc/class-tgm-plugin-activation.php');
require_once ('inc/mega_main_menu/mega_main_menu.php');
require_once ('admin/index.php');


/*
=================================================================================================================
Function herowp_load_styles() used to enqueue the CSS files
=================================================================================================================
*/
function herowp_load_styles(){
	global $herowp_data;
	wp_enqueue_style( 'reset', get_template_directory_uri().'/css/reset.css', array(), false, 'all');
	wp_enqueue_style( 'style', get_template_directory_uri().'/style.css', array('bootstrap'), '1.0.0', 'all');
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.css', array(), '3.1.1', 'all');
	wp_enqueue_style( 'responsive', get_template_directory_uri().'/css/responsive.css', array('style'), '1.0.0', 'all');
	wp_enqueue_style( 'fontawesome', get_template_directory_uri().'/fonts/font-awesome/css/font-awesome.min.css', array('style'), '4.0.3', 'all');
	
	if (!empty($herowp_data['pannelshow'])){
		if($herowp_data['pannelshow']){
			wp_enqueue_style( 'colorpicker', get_template_directory_uri().'/admin/color-picker/colpick.css', array('style'), false, 'all');
		}
	}
	wp_enqueue_style( 'pe-7s-css', get_template_directory_uri().'/css/pe-icon-7-stroke.css', array('style'), false, 'all');
	wp_enqueue_style( 'pe-7s-css-filled', get_template_directory_uri().'/css/pe-icon-7-filled.css', array('style'), false, 'all');
	wp_enqueue_style( 'custom-font-icons-css', get_template_directory_uri().'/css/style-custom-font-icons.css', array('style'), false, 'all');
	wp_enqueue_style( 'touchtouch', get_template_directory_uri().'/css/touchTouch.css', array('style'), false, 'all');
	wp_enqueue_style( 'animate', get_template_directory_uri().'/css/animate.css', array('style'), false, 'all');
	wp_enqueue_style( 'YTPlayer', get_template_directory_uri().'/css/YTPlayer.css', array('style'), false, 'all');
	wp_enqueue_style( 'hover', get_template_directory_uri().'/css/hover-min.css', array('style'), false, 'all');
	wp_enqueue_style( 'herowpfont', get_template_directory_uri().'/inc/page-builder/assets/css/herowpfont.css', array('style'), false, 'all');
	wp_enqueue_style( 'lightbox', get_template_directory_uri().'/css/lightbox.css', array('style'), false, 'all');
	
	if (!empty($herowp_data['animate'])){
		if ($herowp_data['animate'] == 'disable'){
			wp_enqueue_style( 'not_animate', get_template_directory_uri().'/css/not-animate.css', array('style'), false, 'all');
		}
	}
}
add_action('wp_enqueue_scripts', 'herowp_load_styles');

/*
=================================================================================================================
Function herowp_load_scripts() used to enqueue the JS files
=================================================================================================================
*/
function herowp_load_scripts(){
	global $herowp_data;
	wp_enqueue_script( 'jquery', false, array(), null, true);
	wp_enqueue_script( 'jqueryui_js', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js', array(), '2.2', true);
	wp_enqueue_script( 'lightbox_js', get_template_directory_uri().'/js/lightbox.min.js', array(), '', true);
	wp_enqueue_script( 'html5lightbox_js', get_template_directory_uri().'/js/html5lightbox.js', array(), '', true);
	
	if (is_page_template ('portfolio-template-2.php') || is_page_template ('portfolio-template-3.php') || is_page_template ('portfolio-template-4.php') || is_page_template ('portfolio-template-masonry.php')){
		wp_enqueue_script( 'isotope_js', get_template_directory_uri().'/js/jquery.isotope.min.js', array(), '1.5.19', true);
		wp_enqueue_script( 'mixitup_js', get_template_directory_uri().'/js/jquery.mixitup.min.js', array(), '', true);
	}
	
	wp_enqueue_script( 'touchtouch_js', get_template_directory_uri().'/js/touchTouch.jquery.js', array(), '1.0', true);
	wp_enqueue_script( 'counterup_js', get_template_directory_uri().'/js/jquery.counterup.min.js', array(), '', true);
	wp_enqueue_script( 'waypoints_js', get_template_directory_uri().'/js/waypoints.min.js', array(), '', true);
	wp_enqueue_script( 'flexslider_js', get_template_directory_uri().'/js/jquery.flexslider.js', array(), '', true);
	wp_enqueue_script( 'modernizr_js', get_template_directory_uri().'/js/modernizr.js', array(), '', true);
	wp_enqueue_script( 'mousewheel_js', get_template_directory_uri().'/js/jquery.mousewheel.js', array(), '', true);
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri().'/js/bootstrap.min.js', array(), '', true);
	wp_enqueue_script( 'stellar_js', get_template_directory_uri().'/js/jquery.stellar.js', array(), '', true);
	wp_enqueue_script( 'parallax_js', get_template_directory_uri().'/js/jquery.parallax-1.1.3.js', array(), '', true);
	wp_enqueue_script( 'easing_js', get_template_directory_uri().'/js/jquery.easing.1.3.js', array(), '', true);
	wp_enqueue_script( 'appear_js', get_template_directory_uri().'/js/jquery.appear.js', array(), '', true);
	wp_enqueue_script( 'YTPlayer_js', get_template_directory_uri().'/js/jquery.mb.YTPlayer.js', array(), '', true);	
	wp_enqueue_script( 'asPieProgress_js', get_template_directory_uri().'/js/jquery-asPieProgress.js', array(), '', true);	
	wp_enqueue_script( 'script_js', get_template_directory_uri().'/js/script.js', array(), '', true);
	
	if (is_page_template ('portfolio-template-2.php') || is_page_template ('portfolio-template-3.php') || is_page_template ('portfolio-template-4.php')){
		wp_enqueue_script( 'script_isotope_js', get_template_directory_uri().'/js/script-isotope.js', array(), '', true);
	}
	
	if (is_page_template ('portfolio-template-masonry.php')){
		wp_enqueue_script( 'packery_masonry_js', get_template_directory_uri().'/js/packery-mode.pkgd.min.js', array(), '', true);
		wp_enqueue_script( 'script_masonry_js', get_template_directory_uri().'/js/script-masonry.js', array(), '', true);
	}
	
	if (!empty($herowp_data['pannelshow'])){
		if($herowp_data['pannelshow']){	
			wp_enqueue_script( 'colorpicker_js', get_template_directory_uri().'/admin/color-picker/colpick.js', array(), '', false);
		}
	}
	
	if (!empty($herowp_data['smooth_scroll'])){
		if ($herowp_data['smooth_scroll'] == 'enable'){
			wp_enqueue_script( 'smoothscroll_js', get_template_directory_uri().'/js/jquery.smoothscroll.js', array(), '', true);
		}
	}
	
	if (!empty($herowp_data['animate'])){
		if ($herowp_data['animate'] == 'enable'){
			wp_enqueue_script( 'script_animate_js', get_template_directory_uri().'/js/script-animate.js', array(), '', true);
		}
	}
}
add_action('wp_enqueue_scripts', 'herowp_load_scripts');

/*
=================================================================================================================
Function herowp_load_admin_styles() used to enqueue the CSS files in admin
=================================================================================================================
*/
function herowp_load_admin_styles(){
	wp_enqueue_style( 'fontawesome', get_template_directory_uri().'/fonts/font-awesome/css/font-awesome.min.css', NULL, 'all');
	wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/css/admin-styles.css', false, 'all' );
}
add_action( 'admin_enqueue_scripts', 'herowp_load_admin_styles' );


/*
=================================================================================================================
Function herowp_register_required_plugins() used to auto-install plugins on theme activation
=================================================================================================================
*/
function herowp_register_required_plugins() {
    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        array(
            'name'               => 'Revolution Slider', // The plugin name.
            'slug'               => 'revslider', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/inc/plugins/revslider.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),
		
		array(
            'name'               => 'Advanced Custom Fields', // The plugin name.
            'slug'               => 'advanced-custom-fields', // The plugin slug (typically the folder name).
            'source'             => get_stylesheet_directory() . '/inc/plugins/advanced-custom-fields.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),
		
		
		 array(
            'name'               => 'Shortcodes Ultimate', // The plugin name.
            'slug'               => 'shortcodes-ultimate', // The plugin slug (typically the folder name).
            'source'             => 'http://downloads.wordpress.org/plugin/shortcodes-ultimate.zip', // The plugin source.
            'required'           => false, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),
		 
		 
		  array(
            'name'               => 'Twitters Bootstrap Shortcodes Ultimate', // The plugin name.
            'slug'               => 'twitters-bootstrap-shortcodes-ultimate', // The plugin slug (typically the folder name).
            'source'             => 'http://downloads.wordpress.org/plugin/twitters-bootstrap-shortcodes-ultimate.1.0.4.zip', // The plugin source.
            'required'           => false, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),

		    array(
            'name'               => 'Custom Post Type UI', // The plugin name.
            'slug'               => 'custom-post-type-ui', // The plugin slug (typically the folder name).
            'source'             => 'http://downloads.wordpress.org/plugin/custom-post-type-ui.1.0.8.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),
		
			array(
            'name'               => 'Contact Form 7', // The plugin name.
            'slug'               => 'contact-form-7', // The plugin slug (typically the folder name).
            'source'             => 'http://downloads.wordpress.org/plugin/contact-form-7.4.1.2.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
            'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher.
            'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
            'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
            'external_url'       => '', // If set, overrides default API URL and points to an external URL.
        ),


    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
            'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'tgmpa' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'tgmpa' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'tgmpa' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'herowp_register_required_plugins' );

/*
=================================================================================================================
Theme support
=================================================================================================================
*/
if (!isset($content_width)){
	$content_width = 1140;
}

if (function_exists('add_theme_support')){
	// Add Menu Support
	add_theme_support('menus');
	// Add Thumbnail Theme Support
	add_theme_support('post-thumbnails');
	add_theme_support( 'post-formats', array( 'video') ); //video post format
	add_image_size('large', 700, '', true); // Large Thumbnail
	add_image_size('medium', 250, '', true); // Medium Thumbnail
	add_image_size('small', 120, '', true); // Small Thumbnail
	add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
	// Enables post and comment RSS feed links to head
	add_theme_support('automatic-feed-links');
	// Localisation Support
	load_theme_textdomain('myway', get_template_directory(). '/languages');
}

/*
=================================================================================================================
Navigation
=================================================================================================================
*/
function herowp_nav(){
	wp_nav_menu(
		array(
			'theme_location' => 'header-menu',
			'menu' => '',
			'container' => 'div',
			'container_class' => 'menu-{menu slug}-container',
			'container_id' => '',
			'menu_class' => 'menu',
			'menu_id' => '',
			'echo' => true,
			'fallback_cb' => 'wp_page_menu',
			'before' => '',
			'after' => '',
			'link_before' => '',
			'link_after' => '',
			'items_wrap' => '<ul>%3$s</ul>',
			'depth' => 0,
			'walker' => ''
			)
		);
}

/*
=================================================================================================================
Function herowp_register_menu() - register the menu in our theme
=================================================================================================================
*/
function herowp_register_menu(){
	register_nav_menus(array( // Using array to specify more menus if needed
		'mega_main_sidebar_menu' => __('Header Mega Menu', 'myway'), // Main Navigation
	));
}

/*
=================================================================================================================
Function herowp_wp_nav_menu_args() - Remove the <div> surrounding the dynamic navigation to cleanup markup
=================================================================================================================
*/
function herowp_wp_nav_menu_args($args = ''){
	$args['container'] = false;
	return $args;
}

/*
=================================================================================================================
Function herowp_admin_notice() - Display a success message after importing all dummy data
=================================================================================================================
*/
function herowp_admin_notice() {
	if( isset($_GET['import_status']) && $_GET['import_status'] == 'success' ) {
        echo '<div class="info"><p>';
        printf(__('Sucessfully imported dummy data!','myway'));
        echo "</p></div>";
	}
}
add_action('admin_notices', 'herowp_admin_notice');

/*
=================================================================================================================
Function herowp_add_slug_to_body_class() - Add page slug to body class, love this
=================================================================================================================
*/
function herowp_add_slug_to_body_class($classes){
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} 
	elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} 
	elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}
	return $classes;
}

/*
=================================================================================================================
Function herowp_widgets_init() - Register widget locations
=================================================================================================================
*/
function herowp_widgets_init() {
	
	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '<span class="dotcolor">.</span></h3>',
	) );	
	register_sidebar( array(
		'name' => 'First Footer Widget Area',
		'id' => 'first_footer',
		'before_widget' => '<div id="%1$s" class=" %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '<span class="dotcolor">.</span></h3></div>					  
						  <div class="footer-box-line2"></div>
		',
	) );	
	register_sidebar( array(
		'name' => 'Second Footer Widget Area',
		'id' => 'second_footer',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '<span class="dotcolor">.</span></h3></div>
						  
						  <div class="footer-box-line2"></div>
		',
	) );	
	
	register_sidebar( array(
		'name' => 'Third Footer Widget Area',
		'id' => 'third_footer',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="title"><h3>',
		'after_title' => '<span class="dotcolor">.</span></h3></div>
						  
						  <div class="footer-box-line2"></div>
		',
	) );	
}
add_action( 'widgets_init', 'herowp_widgets_init' );

/*
=================================================================================================================
Function herowp_remove_recent_comments_style() - Remove wp_head() injected Recent Comment styles
=================================================================================================================
*/
function herowp_remove_recent_comments_style(){
	global $wp_widget_factory;
	remove_action('wp_head', array(
		$wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
		'recent_comments_style'
	));
}

/*
=================================================================================================================
Function herowp_tag_cloud_same_size() - set tags to same font size no matter how many times is being used
=================================================================================================================
*/
function herowp_tag_cloud_same_size($in){
	return 'smallest=12&largest=12&number=25&orderby=name&unit=px';
}
add_filter( 'widget_tag_cloud_args', 'herowp_tag_cloud_same_size' );

/*
=================================================================================================================
Function excerpt() - limit excerpt to how many characters it's set
=================================================================================================================
*/
function excerpt($limit) {
 $excerpt = explode(' ', get_the_excerpt(), $limit);
 if (count($excerpt)>=$limit) {
 array_pop($excerpt);
 $excerpt = implode(" ",$excerpt).'...';
 } else {
 $excerpt = implode(" ",$excerpt);
 }
 $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
 return $excerpt;
}

/*
=================================================================================================================
Function custom_excerpt_length() - another custom excerpt length
=================================================================================================================
*/
function custom_excerpt_length($text) {
	global $post;
	if ( '' == $text ) {
		$text = get_the_content('');
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$text = strip_tags($text);
		$excerpt_length = 90;
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			array_push($words, '...');
			$text = implode(' ', $words);
		}
	}
	return $text;
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_excerpt_length');

/*
=================================================================================================================
Function herowp_numeric_posts_nav() - custom pagination
=================================================================================================================
*/
function herowp_numeric_posts_nav() {
	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="pagination_navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link('&laquo;') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>...</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>...</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('&raquo;') );

	echo '</ul></div>' . "\n";

}

/*
=================================================================================================================
Function herowp_style_remove() - Remove 'text/css' from our enqueued stylesheet
=================================================================================================================
*/
function herowp_style_remove($tag){
	return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

/*
=================================================================================================================
Function herowp_remove_thumbnail_dimensions() - Remove thumbnail width and height image attributes
=================================================================================================================
*/
function herowp_remove_thumbnail_dimensions( $html ){
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}

/*
=================================================================================================================
Function herowp_gravatar() - Custom Gravatar in Settings > Discussion
=================================================================================================================
*/
function herowp_gravatar ($avatar_defaults){
	$myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
	$avatar_defaults[$myavatar] = "Custom Gravatar";
	return $avatar_defaults;
}

/*
=================================================================================================================
Function herowp_gravatar() - Threaded Comments
=================================================================================================================
*/
function herowp_enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
			wp_enqueue_script('comment-reply');
		}
	}
}

/*
=================================================================================================================
Actions & Filters
=================================================================================================================
*/

// Add Actions
add_action('get_header', 'herowp_enable_threaded_comments'); // Enable Threaded Comments
add_action('init', 'herowp_register_menu'); // Register menu
add_action('widgets_init', 'herowp_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()


// Add Filters
add_filter('avatar_defaults', 'herowp_gravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'herowp_add_slug_to_body_class'); // Add slug to body class
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'herowp_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('style_loader_tag', 'herowp_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'herowp_remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'herowp_remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether



/*
=================================================================================================================
Aqua Resize Function - resize images on the fly
=================================================================================================================
*/
if(!class_exists('Aq_Resize')) {
 class Aq_Resize
 {
 /**
 * The singleton instance
 */
 static private $instance = null;
 /**
 * No initialization allowed
 */
 private function __construct() {}
 /**
 * No cloning allowed
 */
 private function __clone() {}
 /**
 * For your custom default usage you may want to initialize an Aq_Resize object by yourself and then have own defaults
 */
 static public function getInstance() {
 if(self::$instance == null) {
 self::$instance = new self;
 }
 return self::$instance;
 }
 /**
 * Run, forest.
 */
 public function process( $url, $width = null, $height = null, $crop = null, $single = true, $upscale = false ) {
 // Validate inputs.
 if ( ! $url || ( ! $width && ! $height ) ) return false;
 // Caipt'n, ready to hook.
 if ( true === $upscale ) add_filter( 'image_resize_dimensions', array($this, 'aq_upscale'), 10, 6 );
 // Define upload path & dir.
 $upload_info = wp_upload_dir();
 $upload_dir = $upload_info['basedir'];
 $upload_url = $upload_info['baseurl'];
 
 $http_prefix = "http://";
 $https_prefix = "https://";
 
 /* if the $url scheme differs from $upload_url scheme, make them match 
 if the schemes differe, images don't show up. */
 if(!strncmp($url,$https_prefix,strlen($https_prefix))){ //if url begins with https:// make $upload_url begin with https:// as well
 $upload_url = str_replace($http_prefix,$https_prefix,$upload_url);
 }
 elseif(!strncmp($url,$http_prefix,strlen($http_prefix))){ //if url begins with http:// make $upload_url begin with http:// as well
 $upload_url = str_replace($https_prefix,$http_prefix,$upload_url); 
 }
 
 // Check if $img_url is local.
 if ( false === strpos( $url, $upload_url ) ) return false;
 // Define path of image.
 $rel_path = str_replace( $upload_url, '', $url );
 $img_path = $upload_dir . $rel_path;
 // Check if img path exists, and is an image indeed.
 if ( ! file_exists( $img_path ) or ! getimagesize( $img_path ) ) return false;
 // Get image info.
 $info = pathinfo( $img_path );
 $ext = $info['extension'];
 list( $orig_w, $orig_h ) = getimagesize( $img_path );
 // Get image size after cropping.
 $dims = image_resize_dimensions( $orig_w, $orig_h, $width, $height, $crop );
 $dst_w = $dims[4];
 $dst_h = $dims[5];
 // Return the original image only if it exactly fits the needed measures.
 if ( ! $dims && ( ( ( null === $height && $orig_w == $width ) xor ( null === $width && $orig_h == $height ) ) xor ( $height == $orig_h && $width == $orig_w ) ) ) {
 $img_url = $url;
 $dst_w = $orig_w;
 $dst_h = $orig_h;
 } else {
 // Use this to check if cropped image already exists, so we can return that instead.
 $suffix = "{$dst_w}x{$dst_h}";
 $dst_rel_path = str_replace( '.' . $ext, '', $rel_path );
 $destfilename = "{$upload_dir}{$dst_rel_path}-{$suffix}.{$ext}";
 if ( ! $dims || ( true == $crop && false == $upscale && ( $dst_w < $width || $dst_h < $height ) ) ) {
 // Can't resize, so return false saying that the action to do could not be processed as planned.
 return false;
 }
 // Else check if cache exists.
 elseif ( file_exists( $destfilename ) && getimagesize( $destfilename ) ) {
 $img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
 }
 // Else, we resize the image and return the new resized image url.
 else {
 $editor = wp_get_image_editor( $img_path );
 if ( is_wp_error( $editor ) || is_wp_error( $editor->resize( $width, $height, $crop ) ) )
 return false;
 $resized_file = $editor->save();
 if ( ! is_wp_error( $resized_file ) ) {
 $resized_rel_path = str_replace( $upload_dir, '', $resized_file['path'] );
 $img_url = $upload_url . $resized_rel_path;
 } else {
 return false;
 }
 }
 }
 // Okay, leave the ship.
 if ( true === $upscale ) remove_filter( 'image_resize_dimensions', array( $this, 'aq_upscale' ) );
 // Return the output.
 if ( $single ) {
 // str return.
 $image = $img_url;
 } else {
 // array return.
 $image = array (
 0 => $img_url,
 1 => $dst_w,
 2 => $dst_h
 );
 }
 return $image;
 }
 /**
 * Callback to overwrite WP computing of thumbnail measures
 */
 function aq_upscale( $default, $orig_w, $orig_h, $dest_w, $dest_h, $crop ) {
 if ( ! $crop ) return null; // Let the wordpress default function handle this.
 // Here is the point we allow to use larger image size than the original one.
 $aspect_ratio = $orig_w / $orig_h;
 $new_w = $dest_w;
 $new_h = $dest_h;
 if ( ! $new_w ) {
 $new_w = intval( $new_h * $aspect_ratio );
 }
 if ( ! $new_h ) {
 $new_h = intval( $new_w / $aspect_ratio );
 }
 $size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );
 $crop_w = round( $new_w / $size_ratio );
 $crop_h = round( $new_h / $size_ratio );
 $s_x = floor( ( $orig_w - $crop_w ) / 2 );
 $s_y = floor( ( $orig_h - $crop_h ) / 2 );
 return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
 }
 }
}
if(!function_exists('aq_resize')) {
 /**
 * This is just a tiny wrapper function for the class above so that there is no
 * need to change any code in your own WP themes. Usage is still the same :)
 */
 function aq_resize( $url, $width = null, $height = null, $crop = null, $single = true, $upscale = false ) {
 $aq_resize = Aq_Resize::getInstance();
 return $aq_resize->process( $url, $width, $height, $crop, $single, $upscale );
 }
}
/*
=================================================================================================================
Function title_excerpt() - custom title excerpt
=================================================================================================================
*/
function title_excerpt( $length ) {
	$excerpt = explode( ' ', get_the_title(), $length );
	if ( count( $excerpt ) >= $length ) {
		array_pop( $excerpt );
		$excerpt = implode( " ", $excerpt );
	} else {
		$excerpt = implode( " ", $excerpt );
	}
	$excerpt = preg_replace( '`\[[^\]]*\]`', '', $excerpt );
	return $excerpt.'';
}

/*
=================================================================================================================
Function get_breadcrumb_navigation() - custom bradrcum navigation
=================================================================================================================
*/
function get_breadcrumb_navigation() {
	$delimiter = '';
	$before = '';
	$after = '';
	$before2 = '';
	$after2 = '';
	$before3 = '<span>';
	$after3 = '</span>';
	echo '<ul>';
	global $post;
	$homeLink = home_url();
	echo $before2.'<a href="' . $homeLink . '">HOMEPAGE</a> '. $after2;
	if ( is_category() ) {
		global $wp_query;
		$cat_obj = $wp_query->get_queried_object();
		$thisCat = $cat_obj->term_id;
		$thisCat = get_category($thisCat);
		$parentCat = get_category($thisCat->parent);
		if ($thisCat->parent != 0) echo $before.''.(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' ')).$after;
		echo $before3 . '' . single_cat_title('', false) . '' . $after3;
	} 
	elseif ( is_day() ) {
		echo $before . '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ' . $after;
		echo $before . '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ' . $after;
		echo $before3 . 'Archive by date "' . get_the_time('d') . '"' . $after3;
	} 
	elseif ( is_month() ) {
		echo $before . '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ' . $after;
		echo $before3 . 'Archive by month "' . get_the_time('F') . '"' . $after3;
	} 
	elseif ( is_year() ) {
		echo $before3 . 'Archive by year "' . get_the_time('Y') . '"' . $after3;
	} 
	elseif ( is_single() && !is_attachment() ) {
		if ( get_post_type() != 'post' ) {
			$post_type = get_post_type_object(get_post_type());
			$slug = $post_type->rewrite;
			echo $delimiter . ' ';
			echo $before3 .title_excerpt(50) . $after3;
		} 
		else {
			$cat = get_the_category(); $cat = $cat[0];
			echo $before .' ' . get_category_parents($cat, TRUE, ' ' . $delimiter . ' ') . ' '. $after;
			echo $before3 . '' . title_excerpt(50) . '' . $after3;
		}
	} 
	elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
		$post_type = get_post_type_object(get_post_type());
		echo $before3 . $post_type->labels->singular_name . $after3;
	} 
	elseif ( is_attachment() ) {
		$parent_id = $post->post_parent;
		$breadcrumbs = array();
		while ($parent_id) {
			$page = get_page($parent_id);
			$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
			$parent_id = $page->post_parent;
		}
		$breadcrumbs = array_reverse($breadcrumbs);
		foreach ($breadcrumbs as $crumb) echo ' ' . $crumb . ' ' . $delimiter . ' ';
		echo $before3 . '' . title_excerpt(50) . '' . $after3;
	} 
	elseif ( is_page() && !$post->post_parent ) {
		echo $before3 . '' . title_excerpt(50). '' . $after3;
	} 
	elseif ( is_page() && $post->post_parent ) {
		$parent_id = $post->post_parent;
		$breadcrumbs = array();
		while ($parent_id) {
			$page = get_page($parent_id);
			$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
			$parent_id = $page->post_parent;
		}
		$breadcrumbs = array_reverse($breadcrumbs);
		foreach ($breadcrumbs as $crumb) echo ' ' . $crumb . ' ' . $delimiter . ' ';
		echo $before3 . '' . title_excerpt(50) . '' . $after3;
	} 
	elseif ( is_search() ) {
		echo $before3 . 'Search results for "' . get_search_query() . '"' . $after3;
	} 
	elseif ( is_tag() ) {
		echo $before3 . 'Archive by tag "' . single_tag_title('', false) . '"' . $after3;
	} 
	elseif ( is_author() ) {
		global $author;
		$userdata = get_userdata($author);
		echo $before3 . 'Articles posted by "' . $userdata->display_name . '"' . $after3;
	} 
	elseif ( is_404() ) {
		echo $before3 . '' . 'Error 404' . '' . $after3;
	}
	if ( get_query_var('paged') ) {
		if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
		echo $before3 . 'Page ' . get_query_var('paged') . ' ' . $after3;
		if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
	}
echo '</ul>';
}


/*
=================================================================================================================
Function hex2rgba() - convert hex 2 rgba
=================================================================================================================
*/
function hex2rgba($hex, $alpha = 1){
	$hex = str_replace('#', '', $hex);
	$r = $g = $b = 0;

	switch(strlen($hex)){
		case 3:
			list($r, $g, $b) = str_split($hex);
			$r = hexdec($r.$r);
			$g = hexdec($g.$g);
			$b = hexdec($b.$b);
			break;
		case 6:
			list($r1, $r2, $g1, $g2, $b1, $b2) = str_split($hex);
			$r = hexdec($r1.$r2);
			$g = hexdec($g1.$g2);
			$b = hexdec($b1.$b2);
			break;
		default:
			break;
	}

	return 'rgba('.$r.', '.$g.', '.$b.', '.$alpha.')';
}

/*
=================================================================================================================
Function twentytwelve_content_nav() - Displays navigation to next/previous pages when applicable.
=================================================================================================================
*/
if ( ! function_exists( 'twentytwelve_content_nav' ) ) :
function twentytwelve_content_nav() {
	global $wp_query;
	if ( $wp_query->max_num_pages > 1 ) : ?>
		<?php previous_posts_link( '<i class="fa fa-angle-left"></i>'.__( '', 'myway' ) ); ?>
		<?php next_posts_link( '<i class="fa fa-angle-right"></i>'.__( '', 'myway' ) ); ?>
	<?php endif;
}
endif;

/*
=================================================================================================================
Function herowp_comment() - Template for comments and pingbacks.
=================================================================================================================
*/
if ( ! function_exists( 'herowp_comment' ) ) :
function herowp_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'myway' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'myway' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<!-- comment level 1 -->
	<div class="comment-container">
		<div class="comment-author"><?php echo get_avatar( $comment, 64 ); ?><?php comment_author_link(); ?></div>
		<div class="date_reply_holder">
			<div class="comment-date"><?php comment_date("d M Y"); ?></div>
			<div class="comment-reply"><?php echo comment_reply_link(array('depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=>'Reply Comment')); ?></div>
		</div>
		<div class="the-comment"><?php comment_text(); ?></div>
	</div>
	<?php
			break;
	endswitch;
}
endif;


/*
=================================================================================================================
Dummy data importer
=================================================================================================================
*/
$importer = get_template_directory() . '/inc/plugins/importer/importer.php';
include $importer;
add_action("after_switch_theme", "herowp_importer_theme_options");

/*
=================================================================================================================
Import mega menu and it's settings
=================================================================================================================
*/
add_action("after_switch_theme", "herowp_importer_main_mega_menu");


/*
=================================================================================================================
Import custom post types and taxonomies via Custom post types UI plugin
=================================================================================================================
*/
$herowp_file_activate_cptui = ABSPATH . 'wp-content/plugins/custom-post-type-ui/custom-post-type-ui.php';
function herowp_activate_custom_post_types(){
	herowp_importer_custom_post_types();
}
register_activation_hook( $herowp_file_activate_cptui, "herowp_activate_custom_post_types" );

/*
=================================================================================================================
Register portfolio custom fields options
=================================================================================================================
*/
function herowp_add_acf_fields(){
	if(function_exists("register_field_group")){
		register_field_group(array (
		  'id' => '542dcb08a31cb',
		  'title' => 'Portfolio Extra Details',
		  'fields' => 
		  array (
			0 => 
			array (
			  'key' => 'field_542adc8ceb001',
			  'label' => 'Extra details ',
			  'name' => 'extra_details',
			  'type' => 'repeater',
			  'instructions' => 'Add extra details to your portfolio',
			  'required' => '0',
			  'sub_fields' => 
			  array (
				0 => 
				array (
				  'key' => 'field_542adc8ceb07e',
				  'label' => 'Font awesome Icon code http://fortawesome.github.io/Font-Awesome/icons/',
				  'name' => 'icon',
				  'type' => 'text',
				  'default_value' => 'fa-laptop',
				  'formatting' => 'html',
				  'order_no' => 0,
				),
				1 => 
				array (
				  'key' => 'field_542adc8ceb0d9',
				  'label' => 'Heading text',
				  'name' => 'heading_text',
				  'type' => 'text',
				  'default_value' => 'A cool title',
				  'formatting' => 'html',
				  'order_no' => 1,
				),
				2 => 
				array (
				  'key' => 'field_542adc8ceb13e',
				  'label' => 'Description',
				  'name' => 'description',
				  'type' => 'text',
				  'default_value' => 'A cool description',
				  'formatting' => 'html',
				  'order_no' => 2,
				),
			  ),
			  'row_limit' => '',
			  'layout' => 'table',
			  'button_label' => '+Add New',
			  'order_no' => 0,
			),
		  ),
		  'location' => 
		  array (
			'rules' => 
			array (
			  0 => 
			  array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'portfolio',
				'order_no' => 0,
			  ),
			),
			'allorany' => 'all',
		  ),
		  'options' => 
		  array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
			),
		  ),
		  'menu_order' => 0,
		));
		register_field_group(array (
		  'id' => '542dcb08a3610',
		  'title' => 'Portfolio Images',
		  'fields' => 
		  array (
			0 => 
			array (
			  'label' => 'Add Images to portfolio',
			  'name' => 'portfolio_images',
			  'type' => 'repeater',
			  'instructions' => '',
			  'required' => '0',
			  'sub_fields' => 
			  array (
				0 => 
				array (
				  'label' => 'Image',
				  'name' => 'image',
				  'type' => 'image',
				  'save_format' => 'url',
				  'preview_size' => 'medium',
				  'key' => 'field_542af0689b540',
				  'order_no' => 0,
				),
			  ),
			  'row_limit' => '',
			  'layout' => 'table',
			  'button_label' => '+Add New Image',
			  'key' => 'field_542af0689ad6e',
			  'order_no' => 0,
			),
		  ),
		  'location' => 
		  array (
			'rules' => 
			array (
			  0 => 
			  array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'portfolio',
				'order_no' => 0,
			  ),
			),
			'allorany' => 'all',
		  ),
		  'options' => 
		  array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
			),
		  ),
		  'menu_order' => 0,
		));
		register_field_group(array (
		  'id' => '542dcb08a396f',
		  'title' => 'Portfolio Extra Details #2',
		  'fields' => 
		  array (
			0 => 
			array (
			  'key' => 'field_542b03666858b',
			  'label' => 'Extra details',
			  'name' => 'extra_details_2',
			  'type' => 'repeater',
			  'instructions' => '',
			  'required' => '0',
			  'sub_fields' => 
			  array (
				0 => 
				array (
				  'key' => 'field_542b0366685f2',
				  'label' => 'Font awesome Icon code - http://fortawesome.github.io/Font-Awesome/icons/',
				  'name' => 'icon',
				  'type' => 'text',
				  'default_value' => 'fa-cloud',
				  'formatting' => 'html',
				  'order_no' => 0,
				),
				1 => 
				array (
				  'key' => 'field_542b03666864a',
				  'label' => 'Big number',
				  'name' => 'big_number',
				  'type' => 'text',
				  'default_value' => '159',
				  'formatting' => 'html',
				  'order_no' => 1,
				),
				2 => 
				array (
				  'key' => 'field_542b0366686a3',
				  'label' => 'Description',
				  'name' => 'description',
				  'type' => 'text',
				  'default_value' => 'Days of work',
				  'formatting' => 'html',
				  'order_no' => 2,
				),
			  ),
			  'row_limit' => '',
			  'layout' => 'table',
			  'button_label' => 'Add Row',
			  'order_no' => 0,
			),
		  ),
		  'location' => 
		  array (
			'rules' => 
			array (
			  0 => 
			  array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'portfolio',
				'order_no' => 0,
			  ),
			),
			'allorany' => 'all',
		  ),
		  'options' => 
		  array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
			),
		  ),
		  'menu_order' => 0,
		));
		register_field_group(array (
		  'id' => '542dcb08a3d1e',
		  'title' => 'Live demo Url',
		  'fields' => 
		  array (
			0 => 
			array (
			  'label' => 'Enter Live Demo Url',
			  'name' => 'live_demo',
			  'type' => 'text',
			  'instructions' => '',
			  'required' => '0',
			  'default_value' => '',
			  'formatting' => 'html',
			  'key' => 'field_542b15bc1069b',
			  'order_no' => 0,
			),
		  ),
		  'location' => 
		  array (
			'rules' => 
			array (
			  0 => 
			  array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'portfolio',
				'order_no' => 0,
			  ),
			),
			'allorany' => 'all',
		  ),
		  'options' => 
		  array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
			),
		  ),
		  'menu_order' => 0,
		));
	}
}
add_action( 'after_setup_theme', 'herowp_add_acf_fields' );

/*
=================================================================================================================
Function herowp_add_video_portfolio() register youtube and vimeo portfolio options
=================================================================================================================
*/
function herowp_add_video_portfolio(){
	if(function_exists("register_field_group")){
		register_field_group(array (
					  'id' => '5435933b220de',
					  'title' => 'Youtube Video Id',
					  'fields' => 
					  array (
						0 => 
						array (
						  'key' => 'field_54353dfb4a561',
						  'label' => 'Youtube Video ID',
						  'name' => 'video_id_youtube',
						  'type' => 'text',
						  'instructions' => 'Enter Youtube video ID. This will replace the featured image with a video. Please not you can not use both at once (Youtube and Vimeo)',
						  'required' => '0',
						  'default_value' => '',
						  'formatting' => 'html',
						  'order_no' => 0,
						),
					  ),
					  'location' => 
					  array (
						'rules' => 
						array (
						  0 => 
						  array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'portfolio',
							'order_no' => 0,
						  ),
						),
						'allorany' => 'all',
					  ),
					  'options' => 
					  array (
						'position' => 'normal',
						'layout' => 'default',
						'hide_on_screen' => 
						array (
						),
					  ),
					  'menu_order' => 0,
					));
					register_field_group(array (
					  'id' => '5435933b223e4',
					  'title' => 'Vimeo Video ID',
					  'fields' => 
					  array (
						0 => 
						array (
						  'label' => 'Vimeo Video ID',
						  'name' => 'video_id_vimeo',
						  'type' => 'text',
						  'instructions' => 'Enter Vimeo video ID. This will replace the featured image with a video. Please not you can not use both at once (Vimeo and Youtube)',
						  'required' => '0',
						  'default_value' => '',
						  'formatting' => 'html',
						  'key' => 'field_543540746167d',
						  'order_no' => 0,
						),
					  ),
					  'location' => 
					  array (
						'rules' => 
						array (
						  0 => 
						  array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'portfolio',
							'order_no' => 0,
						  ),
						),
						'allorany' => 'all',
					  ),
					  'options' => 
					  array (
						'position' => 'normal',
						'layout' => 'default',
						'hide_on_screen' => 
						array (
						),
					  ),
					  'menu_order' => 0,
					));
	}
}
add_action( 'after_setup_theme', 'herowp_add_video_portfolio' );

/*
=================================================================================================================
Extra options on the user profile page
=================================================================================================================
*/
function herowp_add_to_author_profile( $contactmethods ) {
	
	$contactmethods['google_profile'] = 'Google Profile URL';
	$contactmethods['twitter_profile'] = 'Twitter Profile URL';
	$contactmethods['facebook_profile'] = 'Facebook Profile URL';
	$contactmethods['dribbble_profile'] = 'Dribbble Profile URL';
	$contactmethods['linkedin_profile'] = 'Linkedin Profile URL';
	
	return $contactmethods;
}
add_filter( 'user_contactmethods', 'herowp_add_to_author_profile', 10, 1);
/*
=================================================================================================================
Register custom header BG image option for pages
=================================================================================================================
*/
function herowp_add_acf_fields_header_bg(){
	if(function_exists("register_field_group"))
		{
		register_field_group(array (
		  'id' => '551ed1aa501d1',
		  'title' => 'Header custom background image',
		  'fields' => 
		  array (
			0 => 
			array (
			  'key' => 'field_551eb1810094d',
			  'label' => 'Header custom background image',
			  'name' => 'header_background_image',
			  'type' => 'image',
			  'instructions' => 'Add a specific background image for this page header. <br/> <i>Note!</i> For Wordpress default pages such as archives, tag, categories etc this option will not work.',
			  'required' => '0',
			  'save_format' => 'url',
			  'preview_size' => 'thumbnail',
			  'order_no' => 0,
			),
		  ),
		  'location' => 
		  array (
			'rules' => 
			array (
			  0 => 
			  array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
				'order_no' => 0,
			  ),
			),
			'allorany' => 'all',
		  ),
		  'options' => 
		  array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => 
			array (
			),
		  ),
		  'menu_order' => 0,
		));
		}
}
add_action( 'after_setup_theme', 'herowp_add_acf_fields_header_bg' );
/*
=================================================================================================================
Register custom page BG option for pages
=================================================================================================================
*/
function herowp_add_acf_fields_custom_page_bg(){
if(function_exists("register_field_group"))
{
register_field_group(array (
  'id' => '553f88352ca1f',
  'title' => 'Custom Page Background Color',
  'fields' => 
  array (
    0 => 
    array (
      'key' => 'field_553f81dce8a40',
      'label' => 'Main background color',
      'name' => 'main_background_color',
      'type' => 'color_picker',
      'instructions' => 'Add a custom background color for this page. Please note that this will override the Appearance -> Theme Options color.
Note! For Wordpress default pages such as archives, tag, categories etc this option will not work.',
      'required' => '0',
      'order_no' => 0,
    ),
  ),
  'location' => 
  array (
    'rules' => 
    array (
      0 => 
      array (
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
        'order_no' => 0,
      ),
    ),
    'allorany' => 'all',
  ),
  'options' => 
  array (
    'position' => 'normal',
    'layout' => 'default',
    'hide_on_screen' => 
    array (
    ),
  ),
  'menu_order' => 0,
));
}
}
add_action( 'after_setup_theme', 'herowp_add_acf_fields_custom_page_bg' );
/*
=================================================================================================================
Function herowp_remove_rev_notice() remove revolution slider activation notice
=================================================================================================================
*/
if(function_exists( 'set_revslider_as_theme' )){	
	function herowp_remove_rev_notice() {
		set_revslider_as_theme();
	}
add_action( 'init', 'herowp_remove_rev_notice' );
}

/*
=================================================================================================================
WP Link Pages
=================================================================================================================
*/
$defaults = array(
		'before'           => '<p>' . __( 'Pages:','myway' ),
		'after'            => '</p>',
		'link_before'      => '',
		'link_after'       => '',
		'next_or_number'   => 'number',
		'separator'        => ' ',
		'nextpagelink'     => __( 'Next page','myway' ),
		'previouspagelink' => __( 'Previous page','myway' ),
		'pagelink'         => '%',
		'echo'             => 1
	);
 
wp_link_pages( $defaults );

/*
=================================================================================================================
Home Title
=================================================================================================================
*/
function herowp_custom_title( $title ){
	global $herowp_data;
	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
		if (!empty($herowp_data['home_title'])){
			return  $herowp_data['home_title']. ' | ' . get_bloginfo( 'description' );
		}
		else{
			return __( 'Home', 'myway' ) . ' | ' . get_bloginfo( 'description' );
		}
	}
	else{
		return $title.get_bloginfo( 'description' );
	}
		return $title;
}
add_filter( 'wp_title', 'herowp_custom_title' );

/*
=================================================================================================================
herowp_output_custom_header_bg() Add custom CSS code for a specific header background image, selected by user
=================================================================================================================
*/
function herowp_output_custom_header_bg(){
	if (function_exists('get_field')){
		global $herowp_custom_header_bg;
		$herowp_custom_header_bg = get_field('header_background_image');
		function herowp_add_custom_header_bg(){
			global $herowp_custom_header_bg;
			if ($herowp_custom_header_bg){
				$output = '<style type="text/css">header{ background:url('.$herowp_custom_header_bg.')top center no-repeat !important; }</style>';
			}
			else{
				$output = '';
			}
			echo $output;
		}
	add_action('wp_footer','herowp_add_custom_header_bg');
	}
}

/*
=================================================================================================================
herowp_output_custom_page_bg_color() Add custom color for a specific homepage
=================================================================================================================
*/
function herowp_output_custom_page_bg_color(){
	if (function_exists('get_field')){
		global $herowp_custom_color_page_bg;
		$herowp_custom_color_page_bg = get_field('main_background_color');
		function herowp_add_custom_page_bg_color(){
			global $herowp_custom_color_page_bg;
			if ($herowp_custom_color_page_bg != '#ffffff' && !empty($herowp_custom_color_page_bg) || !isset($herowp_custom_color_page_bg) ){
				$output = '
<style type="text/css" media="all">
.header-phone span, .rev-sp1, .rev-sp2, .shout-box-title span, span.title-sp, .home-post-info, .home-post-info a, .team-social i:hover, .widget_tw > div.twit-icon > i, .footer-newsletter span, .well-boxes h2, .well-feat h2, .well h2, .steps-left span, a.featured-button:hover, .area-3-info-box i, .sidebar-posts h2 a:hover, a.cat-link, .widget-box a:hover, .services-thumb i, #post-area-3 a:hover, .projects-title h2 span, .dotcolor,.comment-date i, #post-area-2 a:hover, .icon_style_1,.info-header-data i,.description_and_list i, .tp-caption.very_big_white_6 i, .view-overlay .port-zoom-link .fa.fa-link,.sidebar-posts i,.services_home3 .servicesnumber,.testimonials .carousel-control,.blog_posts_block1 p.author_and_category a,.services_home5 .icon_service,.pricing_tables_holder h2 a:hover,.showcase-info h2,.offer-info h2 span,.offer-price span.new,.blog_posts_block1 p.author_and_category,.quote_block h1 i,.icon-facts i,.services_home5 a.read-more,.services_home09 .box-services:hover .servicesnumber,.services_home09 .box-services:hover .servicesnumber-image,.tp-caption.white_big_2_uppercase_def_color,.blog_posts_block2 .category i,.services_home4 a.read-more,.tp-caption.default_color_80px,.tp-caption.default_color_80px,.accordion-toggle:hover i,.accordion-toggle:hover span,a.default_button_outline:hover,.services_home08 .box-services:hover a.read-more,.portfolio_block3 .maginfy i,.portfolio_block3 p.category i,.blog_posts_block3 .category i,.blog_posts_block3 h2 a:hover,span.defaultcolor,.offer-info h2 span,.offer-price span.new{
	color: '.$herowp_custom_color_page_bg.' !important;
}
		
	.tp-caption.big_white ul li.nr-1 span, span.zoom-bg, .subscribe-submit, #footer-body > div.footer-social-body > div > a > i.fa.fa-rss:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-facebook:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-twitter:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-dribbble:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-youtube:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-tumblr:hover, .testimonial_quote, .about-title-line, .fa.fa-angle-left, .fa.fa-angle-right, .port-detail-box img, .projectdetails .flex-next, .projectdetails .flex-prev, .box-title-line, .ui-tabs-active.ui-state-active, .accordion-group.active .icon, .accordion-heading .accordion-toggle:hover .icon, #time, .search-submit, .tagcloud a:hover, .contact-facebook i, .contact-twitter i, .contact-google i, .author-fa, .port-img-overlay, .title-underline,.blog-comments p.form-submit input[name="submit"], .post-tags a:hover, .myway, .myway_small, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.default_dropdown .mega_dropdown > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.default_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.default_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.multicolumn_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.multicolumn_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li:hover > .processed_image, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li > .processed_image:hover, .main-content .left-container .contact-form .submit, .main-content .left-container .contact-form .submit:hover, a.button, a.button-boxes, a.button-boxes:hover, a.button:hover, .myway, .myway_small, .myway_small_left, .services_home4 .servicesnumber, .separator-line .round-icon-heading, .slider_base_bg_color, .portfolio_block_full_screen .mix:hover,  .services_home1 .box-services:hover, .blog_posts_block1 .round_date,.blog_posts_block1 .blog-posts:hover span.url_post,.pricing_tables_holder h2 a,.pricing_tables_holder .price_circle,.showcase-info h3,.services_home10 .servicesnumber,.portfolio_block1 .separator,.services_home .servicesnumber,.team-boxes .separator,.footer-box-content p span,form#searchform input[type="submit"],#main-contact .wpcf7 input[type="submit"],.overlay-image2,.portfolio_block_full_screen h2,.services_home5 .servicesnumber,.tp-caption.white_slim_1,.tp-caption.white_big_slim_2,.services_home09 .box-services:hover .counter_nr,.blog_posts_block2 .date_holder,a.default_button,.services_home08 a.read-more,a.default_button2,.portfolio_block3 .read-more,.blog_posts_block3 .comments,.blog_posts_block1 #nothing_found i,.offer-info-bottom p,.tp-caption.white_halfslim_32px_bg, .white_big_3_bg, .white_120px_base_bg {
	background: '.$herowp_custom_color_page_bg.' !important;
}

 #mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.submenu_full_width.drop_to_center > .mega_dropdown, 
#mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.drop_to_right > .mega_dropdown, .mega_dropdown{
border-top:solid 2px #ff6131 '.$herowp_custom_color_page_bg.' !important;
}	

.well:hover,.steps-box:hover,.box-button:hover {
	border-bottom: 2px solid '.$herowp_custom_color_page_bg.' !important;
}
	
.main-content .left-container .contact-form .submit {
	border-bottom: 4px solid #dbdbdb;
}
	
.port-title{
	border-bottom: 1px solid '.$herowp_custom_color_page_bg.';
}

#mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.submenu_full_width.drop_to_center > .mega_dropdown, #mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.drop_to_right > .mega_dropdown, .mega_dropdown{
	border-top:solid 2px '.$herowp_custom_color_page_bg.' !important;
}
	
.contact-form h2 {
	border-bottom: 8px solid '.$herowp_custom_color_page_bg.' !important;
}

.tp-caption.very_big_white:after, .static_content .view-all-button.bubble-bottom:before{
border-color: '.$herowp_custom_color_page_bg.' transparent transparent transparent !important;
}

.separator-line .round-icon-heading {
box-shadow: 0px 0px 0px 2px '.$herowp_custom_color_page_bg.';
}
.team-wrap .view-all-button.bubble-bottom:before,.portfolio_block_full_screen .view-all-button.bubble-bottom:before, .portfolio_block1 .view-all-button.bubble-bottom:before,.blog_posts_block1 .view-all-button.bubble-bottom:before,.offer-wrap .view-all-button.bubble-bottom:before{
border-color: '.$herowp_custom_color_page_bg.' transparent transparent transparent;
}
.projects-filter-buttons .view-all-button.bubble-bottom:before {
border-color: '.$herowp_custom_color_page_bg.' transparent transparent transparent;
}
form#searchform input[type="submit"],.offer-price a:hover{
border: solid 1px  '.$herowp_custom_color_page_bg.';
}
.offer-price a:hover {
border: solid 1px  '.$herowp_custom_color_page_bg.';
color:'.$herowp_custom_color_page_bg.';
}
form#searchform input[type="submit"]:hover {
background: #FFF !important;
border: solid 1px  '.$herowp_custom_color_page_bg.';
}
.blog-comments p.form-submit input[name="submit"],
#main-contact .wpcf7 input[type="submit"],
.services_home09 .box-services:hover .servicesnumber,
.services_home09 .box-services:hover .servicesnumber-image,
a.default_button,
.accordion-toggle:hover,
a.default_button_outline:hover,
.services_home08 a.read-more {
border: solid 1px  '.$herowp_custom_color_page_bg.' !important;
}
.services_home10 .servicesnumber {
  box-shadow: 0 0 0 4px '.$herowp_custom_color_page_bg.';
  border: solid 4px '.$herowp_custom_color_page_bg.';
}
.accordion-toggle:hover i{
  border-right: solid 1px '.$herowp_custom_color_page_bg.' !important;
}
#blog-grid-1-type-2 .sticky section{
border-bottom:solid 3px '.$herowp_custom_color_page_bg.' !important;
}
a.default_button_outline2:hover{
color: '.$herowp_custom_color_page_bg.' !important;
border: solid 1px '.$herowp_custom_color_page_bg.' !important;
}
a.default_button2{
background: '.$herowp_custom_color_page_bg.' !important;
border:solid 1px '.$herowp_custom_color_page_bg.' !important;
}
.services_home10 .box-services:hover .servicesnumber{
border:solid 4px #FFF !important;
}
.blog-comments p.form-submit input[name="submit"]:hover,
#main-contact .wpcf7 input[type="submit"]:hover {
background: #FFF !important;
border:solid 1px #DDD !important;
}
.portfolio_block3 .read-more:hover,
.portfolio_block3 .maginfy:hover{
background:#333 !important;
}

.portfolio_block3 .read-more:hover i,
.portfolio_block3 .maginfy:hover i{
color:#fff !important;
}
.services_home1 .box-services:hover{
-webkit-background-clip: padding-box !important;
background-clip: padding-box !important;
}

.title-heading h1 span, .myway_thin,.blog_posts_block1 .round_date,.blog_posts_block1 .round_comments {
-webkit-background-clip: padding-box !important;
background-clip: padding-box !important;
}
.view-all-button span.icon i{
color:#FFF;
}
.services_home08 a.read-more:hover,.services_home08 .box-services:hover a.read-more{
	background:none !important;
}
.pricing_tables_holder h2 a:hover{
background:#FFF !important;
}
a.default_button:hover, a.default_button2:hover {
  color: #333 !important;
  background: #fff !important;
  border: solid 1px #fff !important;
}
.services_home5 .servicesnumber {
  border-bottom: solid 5px '.hex2rgba($herowp_custom_color_page_bg ,$alpha = .35).' !important;
    background-clip: padding-box !important;
  -moz-background-clip: padding-box !important;
}
</style>
				';
			}
			else{
				$output = '';
			}
			echo $output;
		}
		if ($herowp_custom_color_page_bg != '#ffffff'){
				add_action('wp_footer','herowp_add_custom_page_bg_color',100);
		}
	}
}

/*
=================================================================================================================
herowp_register_help_menu_page() - adds a page with theme help and videos
=================================================================================================================
*/
/*function herowp_register_help_menu_page() {
	global $herowp_theme_name;
	add_menu_page( ''.$herowp_theme_name.' Help Videos', ''.$herowp_theme_name.' Help Videos', 'manage_options', 'herowp_add_help_videos', 'herowp_add_help_videos', 'dashicons-sos', '25.890' );
}
add_action( 'admin_menu', 'herowp_register_help_menu_page' ); */

/*
=================================================================================================================
herowp_add_help_videos() - generate the help page
=================================================================================================================
*/
/*function herowp_add_help_videos() {
	echo 'Videos here!';
}*/
function quote_view(){
	
		session_start();

		if(isset($_SESSION['userid'])) { 

			// var_dump($_SESSION['userid']);
			?>
			<script type = 'text/javascript'>

				   
				 
				jQuery(document).ready(function($) {

					// $('#org_phone').on("keypress",function() {
				    
				 //    alert("hi");
				 //    var length=$(this).val().length;
				 //    if(length==3){

				 //     $('#org_phone').val($(this).val()+"-"); 
				 //    }
				 //    else if(length==7){

				 //     $('#org_phone').val($(this).val()+"-"); 
				 //    }else if (length==12){

				 //     return false;
				 //    }
				 //   });

				 	function fetchAddressNames(org_name, target_field) {
						$.ajax({
							url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/ajaxfetchdata.php',
							type: 'post',
							dataType: 'json',
							data: {'org_name':org_name,'fetchAddressNames':'fetchAddressNames'},
							success:function(data){
								jQuery('#'+target_field).empty();
								jQuery('#'+target_field).show();
								jQuery.each(data,function(index,key){
									var row="<li class='list-group-item addressItem' data-add_id="+key.add_id+"><i class='fa fa-map' aria-hidden='true'></i>"+key.Name+"</li>";
									jQuery(row).appendTo('#'+target_field);
								});
							}
						});
				 	}

					$('#org_name').on('input',function(){
						var org_name=$(this).val();
						fetchAddressNames(org_name, "pick_up");
					});
					$('#org_name_add').on('input',function(){
						var org_name=$(this).val();
						fetchAddressNames(org_name, "pick_up_add");
					});

					jQuery(".addressName").on('click','.addressItem',function(){
						var addId = $(this).data("add_id");
						var parent_field = $(this).closest(".addressName").prop("id");
						jQuery('.addressName').empty();
						jQuery(".addressName").hide();
						$.ajax({
							url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/ajaxfetchdata.php',
							type: 'post',
							dataType: 'json',
							data: {'add_id':addId,'fetchAddressData':'fetchAddressData'},
							success:function(data){
								if(parent_field == "pick_up"){
									$('#org_name').val(data.Name);
									$('#org_phone').val(data.Phone);
									$('#addres1').val(data.StreetAddress);
									$('#org_state').val(data.State);
									// $('#org_add_2').val(data.Name);

									$('#pickupcounty').val(data.County);
									$('#pickupcounty').trigger('change');
									setTimeout(function() {
										$('#pickupcity').val(data.City);
										$('#pickupcity').trigger('change');
									}, 1000);
									setTimeout(function() {
										$('#placefrom').val(data.ZipCode);
									}, 1000);
								}
								if(parent_field == "pick_up_add"){
									$('#org_name_add').val(data.Name);
									$('#org_phone_add').val(data.Phone);
									$('#addres1_add').val(data.StreetAddress);
									$('#org_state_add').val(data.State);
									// $('#org_add_2').val(data.Name);

									$('#pickupcounty_add').val(data.County);
									$('#pickupcounty_add').trigger('change');
									setTimeout(function() {
										$('#pickupcity_add').val(data.City);
										$('#pickupcity_add').trigger('change');
									}, 1000);
									setTimeout(function() {
										$('#placefrom_add').val(data.ZipCode);
									}, 1000);
								}
								if(parent_field == "dest_add"){
									$('#des_name').val(data.Name);
									$('#des_phone').val(data.Phone);
									$('#des_add_1').val(data.StreetAddress);
									$('#des_state').val(data.State);
									// $('#des_add_2').val(data.Name);

									$('#dropoffcounty').val(data.County);
									$('#dropoffcounty').trigger('change');
									setTimeout(function() {
										$('#dropoffCity').val(data.City);
										$('#dropoffCity').trigger('change');
									}, 1000);
									setTimeout(function() {
										$('#placeto').val(data.ZipCode);
									}, 1000);
								}
								if(parent_field == "dest_add_add"){
									$('#des_name_add').val(data.Name);
									$('#des_phone_add').val(data.Phone);
									$('#des_add_1_add').val(data.StreetAddress);
									$('#des_state_add').val(data.State);
									// $('#des_add_2').val(data.Name);

									$('#dropoffcounty_add').val(data.County);
									$('#dropoffcounty_add').trigger('change');
									setTimeout(function() {
										$('#dropoffCity_add').val(data.City);
										$('#dropoffCity_add').trigger('change');
									}, 1000);
									setTimeout(function() {
										$('#placeto_add').val(data.ZipCode);
									}, 1000);
								}
							}
						});
					});

					$('#des_name').on('input',function(){
						var org_name=$(this).val();
						fetchAddressNames(org_name, "dest_add");
					});
					$('#des_name_add').on('input',function(){
						var org_name=$(this).val();
						fetchAddressNames(org_name, "dest_add_add");
					});

					$('#addres1').on('input',function(){
						 //alert('hi i am cleciked');
						var address=$(this).val();
						
						if(address.length>=2){
							
							$.ajax({
								url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/ajaxfetchdata.php',
								type: 'post',
								dataType: 'json',
								data: {'address':address,'fetchaddress':'fetchaddress'},
								success:function(data){
									jQuery('.adress1ul').empty();
		         					 jQuery('.adress1div').show();
									jQuery.each(data,function(index,key){

									var row="<li class='list-group-item adress1list' data-codefr="+key.StreetAddress+"><i class='fa fa-map' aria-hidden='true'></i>"+key.StreetAddress+"</li>";
    			 					jQuery(row).appendTo('.adress1ul');
								});
								}
							});								
						}
					});
					jQuery(".adress1ul").on('click','.adress1list',function(){

			    		
			    		var text=jQuery(this).text();
			    		jQuery('.adres1').val(text);
			    		jQuery(".adress1ul").hide();
			    	});

					$('#des_add_1').on('input',function(){
						var address=$(this).val();
						if(address.length>=2){
							
							$.ajax({
								url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/ajaxfetchdata.php',
								type: 'post',
								dataType: 'json',
								data: {'address':address,'fetchaddress':'fetchaddress'},
								success:function(data){
									jQuery('.desadress1ul').empty();
		         					 jQuery('.desadress1div').show();
									jQuery.each(data,function(index,key){
									var row="<li class='list-group-item desadress1list' data-codefr="+key.StreetAddress+"><i class='fa fa-map' aria-hidden='true'></i>"+key.StreetAddress+"</li>";
    			 					jQuery(row).appendTo('.desadress1ul');
								});
								}
							});								
						}
					});
					jQuery(".desadress1ul").on('click','.desadress1list',function(){

			    		
			    		var text=jQuery(this).text();
			    		jQuery('.des_add_1').val(text);
			    		jQuery(".desadress1ul").hide();
			    	});

					$(".additional_delivery").hide();
					$('#add_delivery_issue').on('click',function(){
						if ($(this).prop('checked')==true){ 
							$(".additional_delivery").show();
						}
						else{
							$(".additional_delivery").hide();
						}
					});

               }); 
			</script>

		<?php } 

			
			
		?>
	<style>
	.errorinput{
		border-color: red !important;
	}
	.hover_mydiv{
	color: black !important;
    width: 500px;
    height: auto;
    z-index: 9999;
     display: none; 
    font-size: 19px;
    position: absolute;
       background-color: rgb(124, 200, 228);
    border: 1px solid #00AEEF;
    border-radius: 10px;
    padding: 10px 15px;
}
#hover_div ul li
{
	    list-style-type: square;
    padding: 10px;
}
	#help_img:hover #hover_div {
    display: block !important;
}
.small{
	margin: 0px !important;
}
	</style>
	
	<div class="row">
		<form action="<?=get_site_url()."/paypal/payment.php"?>" method="POST" enctype="multipart/form-data">
		<div class="col-md-12">  
			    <ul class="nav nav-tabs" style="color:black !important;">
			      <li id="info" class="active"><a style="color:black !important;" href="#">Information</a></li>
			      <li id="other"><a style="color:black !important;" href="#">Other Details</a></li>
			    </ul>
			 
			
			<div id="information">
				<div class="col-lg-6">
					<h3>Pickup Details</h3>
				
				<table class="table ">
					<tr>
						<td colspan="2">Name<span class="required" aria-required="true">*</span><br/>
						<input type="text" name="org_name" required id="org_name" class="form-control" value="<?php if(isset($_SESSION['temp_data']['org_name'])){echo $_SESSION['temp_data']['org_name'];} ?>">
							<ul class="list-group addressName" id="pick_up" style="display:none; margin: 0px 0 0 0;">
							</ul>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							Phone<span class="required" aria-required="true">*</span><br/>
							<input type="text" name="org_phone" required id="org_phone" class="phone_us form-control" placeholder="123-456-7890" maxlength="12" value="<?php if(isset($_SESSION['temp_data']['org_phone'])){echo $_SESSION['temp_data']['org_phone'];}?>">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							Pickup Address:<span class="required" aria-required="true">*</span><br/>
							<!--<input type="text" name="org_add_1" id="org_add_1" class="form-control" placeholder="Address Line 1-->
							<input id="addres1" type="text" required autocomplete="off" name="org_add_1" class="col-md-12 form-control adres1" placeholder="Search cities..." value="<?php if(isset($_SESSION['temp_data']['org_add_1'])){echo $_SESSION['temp_data']['org_add_1'];}?>">
							<div class="a">
                                    <ul class="list-group adress1ul adress1div" id="show_my_fly" style="display:none; margin: 0px 0 0 0;">

                                        
                                    </ul>
                              </div>
							
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="org_add_2" id="org_add_2" placeholder="Address Line 2" class="form-control" value="<?php if(isset($_SESSION['temp_data']['org_add_2'])){echo $_SESSION['temp_data']['org_add_2'];}?>"></td>
					</tr>
					<tr>
	        
						<td><input type="text" name="org_state" id="org_state" placeholder="State" class="form-control" value="<?php if(isset($_SESSION['temp_data']['org_state'])){echo $_SESSION['temp_data']['org_state'];}?>"></td>

						<td><select class="form-control" required id="pickupcounty" style="text-transform: capitalize;" name="org_county" >

								<?php 
								 if(isset($_SESSION['temp_data']['org_county'])){

								 echo	"<option value=".$_SESSION['temp_data']['org_county']." selected=''>".$_SESSION['temp_data']['org_county']."</option>";

								}else{

								
								?>
								<option value="" selected="" disabled="">Choose Your County</option>
								 <option value="Carroll">Carroll</option>
								 <option value="Cherokee">Cherokee</option>
								 <option value="Clayton">Clayton</option>
								 <option value="Cobb">Cobb</option>
								 <option value="Coweta">Coweta</option>
								 <option value="Dekalb">Dekalb</option>
								 <option value="Douglas">Douglas</option>
								 <option value="Fayett">Fayett</option>
								 <option value="Forsyth">Forsyth</option>
								 <option value="Fulton">Fulton</option>
								 <option value="Hall">Hall</option>
								 <option value="Henry">Henry</option>
								 <option value="Gwinnett">Gwinnett</option>
								 <option value="Rockdale">Rockdale</option>
								<?php }?> 
							</select></td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="form-control" required id="pickupcity" style="text-transform: capitalize;" name="org_city">
								<?php 
								 if(isset($_SESSION['temp_data']['org_city'])){

								 echo	"<option value=".$_SESSION['temp_data']['org_city']." selected=''>".$_SESSION['temp_data']['org_city']."</option>";

								}else{

								 echo "<option value='' selected='' disabled=''>Choose Your City</option>";	
								}
								?>
								 
								 
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="placefrom" required id="placefrom" class="form-control">

								
								<?php 
								 if(isset($_SESSION['temp_data']['placefrom'])){

								 echo	"<option value=".$_SESSION['temp_data']['placefrom']." selected=''>".$_SESSION['temp_data']['placefrom']."</option>";

								}else{

								 echo "<option value='' selected='' disabled=''>Choose Your Zipcode</option>";	
								}
								?>

							</select>
						</td>
					</tr>
				</table>

				</div>
				<div class="col-lg-6" >
					<h3>Destination Details</h3>
				<table class="table">
					<tr>
						<td colspan="2">Name<span class="required" aria-required="true">*</span><br/>
						<input type="text" name="des_name" required id="des_name" class="form-control" value="<?php if(isset($_SESSION['temp_data']['des_name'])){echo $_SESSION['temp_data']['des_name'];}?>">
							<ul class="list-group addressName" id="dest_add" style="display:none;">
							</ul>
						</td>
					</tr>
					<tr>
						<td colspan="2"> 
							Phone<span class="required" aria-required="true">*</span><br/>
							<input type="text" maxlength="12" required name="des_phone" id="des_phone" placeholder="123-456-7890" class="phone_us form-control" value="<?php if(isset($_SESSION['temp_data']['des_phone'])){echo $_SESSION['temp_data']['des_phone'];}?>">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							destination  Address:<span class="required" aria-required="true">*</span><br/>
							<input type="text" name="des_add_1" required id="des_add_1" class="form-control" placeholder="Address Line 1" value="<?php if(isset($_SESSION['temp_data']['des_add_1'])){echo $_SESSION['temp_data']['des_add_1'];}?>">
							<div class="a">
                                    <ul class="list-group desadress1ul desadress1div" id="show_my_fly" style="display:none;margin: 0px 0 0 0;">
                                        
                                    </ul>
                              </div>
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="des_add_2" id="des_add_2" placeholder="Address Line 2" class="form-control" value="<?php if(isset($_SESSION['temp_data']['des_add_2'])){echo $_SESSION['temp_data']['des_add_2'];}?>"></td>
					</tr>
					<tr>
						<td><input type="text" name="des_state" required id="des_state" placeholder="State" class="form-control" value="<?php if(isset($_SESSION['temp_data']['des_state'])){echo $_SESSION['temp_data']['des_state'];}?>"></td>
						<td>
						<select class="form-control" name="des_county" required id="dropoffcounty" style="text-transform: capitalize;" >
								
								<?php 
								 if(isset($_SESSION['temp_data']['des_county'])){

								 echo	"<option value=".$_SESSION['temp_data']['des_county']." selected=''>".$_SESSION['temp_data']['des_county']."</option>";

								}else{

								?>

								<option value="" selected="" disabled="">Choose Your County</option>
								<option value="Carroll">Carroll</option>
								 <option value="Cherokee">Cherokee</option>
								 <option value="Clayton">Clayton</option>
								 <option value="Cobb">Cobb</option>
								 <option value="Coweta">Coweta</option>
								 <option value="Dekalb">Dekalb</option>
								 <option value="Douglas">Douglas</option>
								 <option value="Fayett">Fayett</option>
								 <option value="Forsyth">Forsyth</option>
								 <option value="Fulton">Fulton</option>
								 <option value="Hall">Hall</option>
								 <option value="Henry">Henry</option>
								 <option value="Gwinnett">Gwinnett</option>
								 <option value="Rockdale">Rockdale</option>
								 <?php }?> 
							</select></td>
					</tr>
					<td colspan="2">
							<select class="form-control" id="dropoffCity" required style="text-transform: capitalize;" name="des_City">
								
								<?php 
								 if(isset($_SESSION['temp_data']['des_City'])){

								 echo	"<option value=".$_SESSION['temp_data']['des_City']." selected=''>".$_SESSION['temp_data']['des_City']."</option>";

								}else{

								 echo "<option value='' selected='' disabled=''>Choose Your City</option>";	
								}
								?>
									
							</select>
						</td>
					<tr>
						<td colspan="2">
							<select name="placeto" id="placeto" required class="form-control">
								
								 <?php 
								 if(isset($_SESSION['temp_data']['placeto'])){

								 echo	"<option value=".$_SESSION['temp_data']['placeto']." selected=''>".$_SESSION['temp_data']['placeto']."</option>";

								}else{

								 echo "<option value='' selected='' disabled=''>Zip Code</option>";	
								}
								?>
								

								
							</select>
						</td>
					</tr>
				</table>

				</div>
				<div>
					<input type="checkbox" id="add_delivery_issue">Do you have additional delivery?
				</div>
<!-- Additional delivery -->
				<div class="col-lg-6 additional_delivery" style="visibility: none;">				
				<table class="table ">
					<tr>
						<td colspan="2">Name<span class="required" aria-required="true">*</span><br/>
						<input type="text" name="org_name_add" required id="org_name_add" class="form-control" value="<?php if(isset($_SESSION['temp_data']['org_name_add'])){echo $_SESSION['temp_data']['org_name_add'];} ?>">
							<ul class="list-group addressName" id="pick_up_add" style="display:none; margin: 0px 0 0 0;">
							</ul>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							Phone<span class="required" aria-required="true">*</span><br/>
							<input type="text" name="org_phone_add" required id="org_phone_add" class="phone_us form-control" placeholder="123-456-7890" maxlength="12" value="<?php if(isset($_SESSION['temp_data']['org_phone_add'])){echo $_SESSION['temp_data']['org_phone_add'];}?>">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							Pickup Address:<span class="required" aria-required="true">*</span><br/>
							<!--<input type="text" name="org_add_1_add" id="org_add_1_add" class="form-control" placeholder="Address Line 1-->
							<input id="addres1_add" type="text" required autocomplete="off" name="org_add_1_add" class="col-md-12 form-control adres1" placeholder="Search cities..." value="<?php if(isset($_SESSION['temp_data']['org_add_1_add'])){echo $_SESSION['temp_data']['org_add_1_add'];}?>">
							<div class="a">
                                    <ul class="list-group adress1ul adress1div" id="show_my_fly" style="display:none; margin: 0px 0 0 0;">

                                        
                                    </ul>
                              </div>
							
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" name="org_add_2_add" id="org_add_2_add" placeholder="Address Line 2" class="form-control" value="<?php if(isset($_SESSION['temp_data']['org_add_2_add'])){echo $_SESSION['temp_data']['org_add_2_add'];}?>"></td>
					</tr>
					<tr>
	        
						<td><input type="text" name="org_state_add" id="org_state_add" placeholder="State" class="form-control" value="<?php if(isset($_SESSION['temp_data']['org_state_add'])){echo $_SESSION['temp_data']['org_state_add'];}?>"></td>

						<td><select class="form-control" required id="pickupcounty_add" style="text-transform: capitalize;" name="org_county_add" >

								<?php 
								 if(isset($_SESSION['temp_data']['org_county_add'])){

								 echo	"<option value=".$_SESSION['temp_data']['org_county_add']." selected=''>".$_SESSION['temp_data']['org_county_add']."</option>";

								}else{

								
								?>
								<option value="" selected="" disabled="">Choose Your County</option>
								 <option value="Carroll">Carroll</option>
								 <option value="Cherokee">Cherokee</option>
								 <option value="Clayton">Clayton</option>
								 <option value="Cobb">Cobb</option>
								 <option value="Coweta">Coweta</option>
								 <option value="Dekalb">Dekalb</option>
								 <option value="Douglas">Douglas</option>
								 <option value="Fayett">Fayett</option>
								 <option value="Forsyth">Forsyth</option>
								 <option value="Fulton">Fulton</option>
								 <option value="Hall">Hall</option>
								 <option value="Henry">Henry</option>
								 <option value="Gwinnett">Gwinnett</option>
								 <option value="Rockdale">Rockdale</option>
								<?php }?> 
							</select></td>
					</tr>
					<tr>
						<td colspan="2">
							<select class="form-control" required id="pickupcity_add" style="text-transform: capitalize;" name="org_city_add">
								<?php 
								 if(isset($_SESSION['temp_data']['org_city_add'])){

								 echo	"<option value=".$_SESSION['temp_data']['org_city_add']." selected=''>".$_SESSION['temp_data']['org_city_add']."</option>";

								}else{

								 echo "<option value='' selected='' disabled=''>Choose Your City</option>";	
								}
								?>
								 
								 
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<select name="placefrom_add" required id="placefrom_add" class="form-control">

								
								<?php 
								 if(isset($_SESSION['temp_data']['placefrom_add'])){

								 echo	"<option value=".$_SESSION['temp_data']['placefrom_add']." selected=''>".$_SESSION['temp_data']['placefrom_add']."</option>";

								}else{

								 echo "<option value='' selected='' disabled=''>Choose Your Zipcode</option>";	
								}
								?>

							</select>
						</td>
					</tr>
				</table>

				</div>
				<div class="col-lg-6 additional_delivery" style="visibility: none;" >
					<table class="table">
						<tr>
							<td colspan="2">Name<span class="required" aria-required="true">*</span><br/>
							<input type="text" name="des_name_add" required id="des_name_add" class="form-control" value="<?php if(isset($_SESSION['temp_data']['des_name_add'])){echo $_SESSION['temp_data']['des_name_add'];}?>">
								<ul class="list-group addressName" id="dest_add_add" style="display:none;">
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="2"> 
								Phone<span class="required" aria-required="true">*</span><br/>
								<input type="text" maxlength="12" required name="des_phone_add" id="des_phone_add" placeholder="123-456-7890" class="phone_us form-control" value="<?php if(isset($_SESSION['temp_data']['des_phone_add'])){echo $_SESSION['temp_data']['des_phone_add'];}?>">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								destination  Address:<span class="required" aria-required="true">*</span><br/>
								<input type="text" name="des_add_1_add" required id="des_add_1_add" class="form-control" placeholder="Address Line 1" value="<?php if(isset($_SESSION['temp_data']['des_add_1_add'])){echo $_SESSION['temp_data']['des_add_1_add'];}?>">
								<div class="a">
	                                    <ul class="list-group desadress1ul desadress1div" id="show_my_fly" style="display:none;margin: 0px 0 0 0;">
	                                        
	                                    </ul>
	                              </div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><input type="text" name="des_add_2_add" id="des_add_2_add" placeholder="Address Line 2" class="form-control" value="<?php if(isset($_SESSION['temp_data']['des_add_2_add'])){echo $_SESSION['temp_data']['des_add_2_add'];}?>"></td>
						</tr>
						<tr>
							<td><input type="text" name="des_state_add" required id="des_state_add" placeholder="State" class="form-control" value="<?php if(isset($_SESSION['temp_data']['des_state_add'])){echo $_SESSION['temp_data']['des_state_add'];}?>"></td>
							<td>
							<select class="form-control" name="des_county_add" required id="dropoffcounty_add" style="text-transform: capitalize;" >
									
									<?php 
									 if(isset($_SESSION['temp_data']['des_county_add'])){

									 echo	"<option value=".$_SESSION['temp_data']['des_county_add']." selected=''>".$_SESSION['temp_data']['des_county_add']."</option>";

									}else{

									?>
									<option value="" selected="" disabled="">Choose Your County</option>
									<option value="Carroll">Carroll</option>
									 <option value="Cherokee">Cherokee</option>
									 <option value="Clayton">Clayton</option>
									 <option value="Cobb">Cobb</option>
									 <option value="Coweta">Coweta</option>
									 <option value="Dekalb">Dekalb</option>
									 <option value="Douglas">Douglas</option>
									 <option value="Fayett">Fayett</option>
									 <option value="Forsyth">Forsyth</option>
									 <option value="Fulton">Fulton</option>
									 <option value="Hall">Hall</option>
									 <option value="Henry">Henry</option>
									 <option value="Gwinnett">Gwinnett</option>
									 <option value="Rockdale">Rockdale</option>
									 <?php }?> 
								</select></td>
						</tr>
						<td colspan="2">
								<select class="form-control" id="dropoffCity_add" required style="text-transform: capitalize;" name="des_City_add">
									
									<?php 
									 if(isset($_SESSION['temp_data']['des_City_add'])){

									 echo	"<option value=".$_SESSION['temp_data']['des_City_add']." selected=''>".$_SESSION['temp_data']['des_City_add']."</option>";

									}else{

									 echo "<option value='' selected='' disabled=''>Choose Your City</option>";	
									}
									?>
										
								</select>
							</td>
						<tr>
							<td colspan="2">
								<select name="placeto_add" id="placeto_add" required class="form-control">
									
									 <?php 
									 if(isset($_SESSION['temp_data']['placeto_add'])){

									 echo	"<option value=".$_SESSION['temp_data']['placeto_add']." selected=''>".$_SESSION['temp_data']['placeto_add']."</option>";

									}else{

									 echo "<option value='' selected='' disabled=''>Zip Code</option>";	
									}
									?>
								</select>
							</td>
						</tr>
					</table>

				</div>
				<button class="btn btn-info" type="button" id="shownext" style="box-shadow: 5px 7px 6px #ccc;"> NEXT</button>
				<div style="display:none;" class="btn btn-info" id="check">check</div>
			</div>
		</div>

			<div id="other_info" class="col-md-12" style="display: none;">
				<div>
				<h3>Other Details</h3>
					<table class="table">
						<tr >
							<td colspan="2">Vehicle<span class="required" aria-required="true">*</span><br/>
							 <select name="travel_vehical" id="travel_vehical" class="form-control" style="width:85%; float:left;" required >
							  <?php 
								 if(isset($_SESSION['temp_data']['travel_vehical'])){

								 echo	"<option value=".$_SESSION['temp_data']['travel_vehical']." selected=''>".$_SESSION['temp_data']['travel_vehical']."</option>";

								}else{

								 
								?>

							  <option value="" selected="" disabled="">Choose Your vehicle</option>
							  <option value="car">Car</option>
							  <option value="cargo-van">Cargo Van</option>
							  <option value="strt-truck">Truck</option>
							  <?php }?>
							</select>
							<a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 6%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Vehicles: car used for 10-15 boxes of average size (12x12x12 or approximate). Cargo van is either transit or van and can take approximately 15-40 boxes (of average size) or more. Truck is used for bigger items and deliveries</li>
								  </ul>
							</div>
					          </a>
							<input type="text" readonly="" class="form-control small" id="truckCharge" name="truckCharge" style="width: 8%;" value="<?php if(isset($_SESSION['temp_data']['truckCharge'])){echo $_SESSION['temp_data']['truckCharge'];}?>">
							</td>
						</tr>
						<tr>
							<?php
							if (isset($_SESSION['userid'])) {
							?>
							<td >
								Choose your PDF (Max size: 17MB) <span class="required" aria-required="true">*</span><br/>
								<input type="file" name="fileAttachment" class="form-control">
							</td>
							<td>
							Select type 
							<select Name="type_PDF" class="form-control">
							<option value="" selected="" disabled="">Select Type here</option>
							<option value="banner">banner</option>
							<option value="booklet">booklet</option>
							<option value="brochures">brochures</option>
							<option value="Others">business cards</option>
							<option value="flyers">flyers</option>
							<option value="pamplet">pamplet</option>
							<option value="sticker">sticker</option>
							<option value="trophy">trophy</option>
							<option value="t-shirts">t-shirts</option>
							<option value="Others">Others</option>
							</select>
							
							</td>
							<?php } ?>
							
						</tr>
						<tr>
							<td>Boxes<span class="required" aria-required="true">*</span><br/> <input type="text" name="box" id="box" class="form-control" placeholder="1" required style="width: 70%; float:left;" value="<?php if(isset($_SESSION['temp_data']['box'])){echo $_SESSION['temp_data']['box'];}?>">
								<a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 13%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Boxes: Total number of boxes (12x12x12 or approximate)  the driver will pick up. Should this be a smaller package like 1-10 business card boxes (500 cards), please enter 1. But detail by email, the total delivery size and weight</li>
								  </ul>
							</div>
					          </a>
							<input type="text" id="boxspan" class="form-control small" readonly="" name="boxspan" style="width: 15%;" value="<?php if(isset($_SESSION['temp_data']['boxspan'])){echo $_SESSION['temp_data']['boxspan'];}?>"></td>

							<td>
								Total Weight<span class="required" aria-required="true">*</span><br/> <input type="text" name="total_weight" id="total_weight" class="form-control" placeholder="30 lbs" style="width: 70%;float:left;" value="<?php if(isset($_SESSION['temp_data']['total_weight'])){echo $_SESSION['temp_data']['total_weight'];}?>">
								<a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 13%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Total Weight: calculate/approximate the overall weight of all the items being couriered</li>
								  </ul>
							</div>
					          </a>
								<input type="text" id="weightspan" class="form-control small"  readonly="" name="weightspan" style="width: 15%;" value="<?php if(isset($_SESSION['temp_data']['weightspan'])){echo $_SESSION['temp_data']['weightspan'];}?>">
							</td>
						</tr>
						<tr>
							<td colspan="2">
							
							Type<span class="required" aria-required="true">*</span>
							
							<br/>
					          <select name="delivery_ser" id="delivery_ser" class="form-control" style="width: 82%;float: left;" required="" >
					             <?php 
								 if(isset($_SESSION['temp_data']['delivery_ser'])){

								 echo	"<option value=".$_SESSION['temp_data']['delivery_ser']." selected=''>".$_SESSION['temp_data']['delivery_ser']."</option>";

								}else{

								 
								?>

					             <option value="" selected="" disabled="">Choose Your Service</option>
					             <option value="direct">Direct</option>
					             <option value="rush">Rush</option>
					             <option value="regular">Regular</option>
					             <?php }?>
					          </select>
					          <a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 6%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Regular service: packages will be delivered same day by end of day if picked up before noon or delivered outside of business hours if pre-arranged approximately with 3.5 hours of pick up.</li>
								
								
								  <li>Rush service: packages will be delivered same day by end of day or outside of business hours if pre-arranged approximately with 1.5 hours of pick up.</li>
								
								  <li>Direct service: packages will be delivered same day by end of day or outside of business hours if pre-arranged and picked up within 30 minutes (to 1 hour)	</li>
								  </ul>
							</div>
					          </a>
							
							<input type="text" class="form-control small" readonly=""  id="span_delivery_ser" style="width: 11%;" name="span_delivery_ser" value="<?php if(isset($_SESSION['temp_data']['span_delivery_ser'])){echo $_SESSION['temp_data']['span_delivery_ser'];}?>"></td>

						</tr>
						<tr>
							<td>
							Pick Up Date<span class="required" aria-required="true">*</span><br/> 
							<?php if(isset($_SESSION['temp_data']['pickdate'])){
							
							echo "<div class='form-group' style='float:left; width:85%;'>".
						            "<div class='input-group ' >".
						                "<input class='form-control datetimepicker11' name='pickdate' value='".$_SESSION['temp_data']['pickdate']."'  />".
						                "<span class='input-group-addon'>".
						                   "<span class='glyphicon glyphicon-calendar'></span>".
						                "</span>".
						            "</div>".
						        "</div>";
							 } else { 
							
								echo "<div class='form-group' style='float:left; width:85%;'>".
						            "<div class='input-group' >".
						                "<input class='form-control datetimepicker11' name='pickdate'   />".
						                "<span class='input-group-addon'>".
						                   "<span class='glyphicon glyphicon-calendar'></span>".
						                "</span>".
						            "</div>".
						        "</div>";
								
								}?>
							<a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 13%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Regular service: packages will be delivered same day by end of day if picked up before noon or delivered outside of business hours if pre-arranged approximately with 3.5 hours of pick up.</li>
								  </ul>
							</div>
					          </a>
							</td>
							<td>
							Ready Time<span class="required" aria-required="true">*</span><br/> 
							<input id="timepicker1" type="text" name="time" id="total_weight" class="form-control" style="float:left; width:85%;" value="<?php if(isset($_SESSION['temp_data']['time'])){echo $_SESSION['temp_data']['time'];}?>">
							<a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 13%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Regular service: packages will be delivered same day by end of day if picked up before noon or delivered outside of business hours if pre-arranged approximately with 3.5 hours of pick up.</li>
								  </ul>
							</div>
					          </a>
							</td>
						</tr>
						<tr>
							<td>
								Waiting Time<span class="required" aria-required="true">*</span><br/> <input type="text" name="waiting_time" id="waiting_time" class="form-control" placeholder="In Min" style="width: 70%;float:left;" value="<?php if(isset($_SESSION['temp_data']['waiting_time'])){echo $_SESSION['temp_data']['waiting_time'];}?>">
								<a id="help_img">
					          <img src="https://atlantaprintercouriers.com/wp-content/themes/myway/images/helpimage.jpg" style="width: 13%;padding-left: 10px;">
					          <div id="hover_div" class="hover_mydiv">
					          <ul>
								<li>Regular service: packages will be delivered same day by end of day if picked up before noon or delivered outside of business hours if pre-arranged approximately with 3.5 hours of pick up.</li>
								  </ul>
							</div>
					          </a>
								<input type="text" id="latespan" class="form-control small"  readonly="" name="latespan"  style="width: 15%;" value="<?php if(isset($_SESSION['temp_data']['latespan'])){echo $_SESSION['temp_data']['latespan'];}?>">
							</td>
							<td>
								Total Amount <input name="total_amount" type="text" id="total_amount"  class="form-control" readonly="" required value="<?php if(isset($_SESSION['temp_data']['total_amount'])){echo $_SESSION['temp_data']['total_amount'];}?>"/>
								<small id="discounttxt"></small>

								<input type="hidden" id="mileage" name="mileage" value="<?php if(isset($_SESSION['temp_data']['mileage'])){echo $_SESSION['temp_data']['mileage'];}?>">
							</td>
						</tr>
						<tr></tr>
						
						<tr >
							<td colspan="2">Coupon Code<br/>
							 <input type="text" name="couponcode" id="couponcode" class="form-control" onkeydown="upperCaseF(this)" style="width:85%; float:left;">
							 <button class="btn btn-info" type="button" id="valcoupon" style="box-shadow: 5px 7px 6px #ccc;position: absolute;z-index: 30;">Validate</button>
							</td>
						</tr>
						
					</table>

					<!-- Additional delivery -->
					<!-- Additional delivery -->

					
					<button type="submit" name="paypalsubmit" ><img alt="" src="<?=get_template_directory_uri ()."/images/Buy-Now.png"?>" style="width:225px;height:87px;"></button>

      
    			
					<!--
					<button style="box-shadow: 5px 7px 6px #ccc;"  class="btn btn-success" id="bokingg">
						Get Quote
					</button> -->

				</div>
			</div>

			
			<input type="hidden" name="cp" value="0">
			</form>	
		</div>

<script>

function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}


<?php

$ss88_coupons = json_decode(get_option('ss88_coupons'), true);
echo 'var couponarray = ' . json_encode(array_values($ss88_coupons));

?>

var couponused = '';
var oldtotal;

jQuery('#valcoupon').click(function(e) {
	
	e.preventDefault();
	
	if(couponused!='')
	{
		jQuery('#total_amount').val(oldtotal);
	}
	
	if(searchTheArray(jQuery('#couponcode').val(), couponarray) !== -1)
	{		
		// Coupon Valid
		oldtotal = jQuery('#total_amount').val();
		var arraywhere = searchTheArray(jQuery('#couponcode').val(), couponarray);
		var totalAmount = oldtotal;
		var percentage = couponarray[arraywhere].value;
		jQuery('input[name=cp]').val(percentage);
		var discount = ( (percentage * totalAmount) / 100 );
		console.log(arraywhere + ' ' + totalAmount + ' ' + percentage + ' ' + discount);
		alert('You just saved $' + discount.toFixed(2) + '!');
		jQuery('#total_amount').val( (totalAmount - discount).toFixed(2) );
		jQuery('#discounttxt').html('This includes a discount of $' + discount.toFixed(2) + '.');
		couponused = jQuery('#couponcode').val();
	}
	else
	{
		alert( 'The coupon is not valid. Please try again.' );
	}
	
});

function searchTheArray(what, arrayc)
{
	var theIndex = -1;
	for (var i = 0, len = arrayc.length; i < len; i++) {
		if (arrayc[i].code == what) {
			theIndex = i;
			break;
		}
	}
	return(theIndex);
}
</script>
		
<?php
	if(isset($_SESSION['temp_data'])){

		unset($_SESSION['temp_data']);
		unset($_SESSION['redirect']);	
	}
	}
	add_action("quote_hook","quote_view",10);
function quote_script(){

	?>
	<script>
		
		var swift=function(){

	var  savedata=function(data){


		jQuery.ajax({
		     url: 'swift.php',
		     type: 'POST',
		     dataType: 'json',
		     data: {action: 'booking',pickup:data.pickup,pickupaddress:data.pickupaddress,drop:data.drop,dropaddress:data.dropaddress,item:data.item},
		     success:function(data){
		     	
		     	
		     		alert("Booking Hase Been Done");
		      		//location.reload();
		     	
		      
		     }
		});
	}
	jQuery(function() {
   
    //jQuery('.phone_us').mask('000-000-0000');
    
  });
	var get_cityarray=function(data){
		
		jQuery.ajax({
				     url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
				     type: 'POST',
				     dataType: 'json',
				     data: {action:'citycode',pickup:data},
				     success:function(data){

				      jQuery('#pickupcity option:not(:first)').remove();
				      jQuery.each(data,function(index, el) {
				      	
						var row="<option value="+index+">"+el+"</option>";

						jQuery(row).appendTo('#pickupcity');	

				      });

                jQuery('#pickupcity').val("");
				     }
				});
	}
	var get_ziparray=function(data){
		
		jQuery.ajax({
				     url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
				     type: 'POST',
				     dataType: 'json',
				     data: {action:'zipcode','pickup':data},
				     success:function(data){

				      jQuery('#placefrom :not(:first)').remove();
				      jQuery.each(data,function(index, el) {
				      	
						var row="<option value="+el+">"+el+"</option>";

						jQuery(row).appendTo('#placefrom');	

				      });

				     }
				});
	}

	var get_dropziparray=function(data){
		
		jQuery.ajax({
				     url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
				     type: 'POST',
				     dataType: 'json',
				     data: {action:'zipcode','pickup':data},
				     success:function(data){
				     	
				      jQuery('#placeto :not(:first)').remove();
				      jQuery.each(data,function(index, el) {
				      	
						var row="<option value="+el+">"+el+"</option>";

						jQuery(row).appendTo('#placeto');	

				      });

				     }
				});
	}
	var get_dropcityarray=function(data){
		
		jQuery.ajax({
				     url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
				     type: 'POST',
				     dataType: 'json',
				     data: {action:'citycode',pickup:data},
				     success:function(data){
				     	
				      jQuery('#dropoffCity option:not(:first)').remove();
				      jQuery.each(data,function(index, el) {
				      	
						var row="<option value="+index+">"+el+"</option>";

						jQuery(row).appendTo('#dropoffCity');


				      });

              jQuery("#dropoffCity").val("");
				     }
				});
	}

	var getsaveobj=function(){

       	var pickup={};
       	var pickupaddress={};
       	var drop={};
       	var dropaddress={};   
		var useless={};
		var item={};    	 
		var data=[];
		var check=[];
    	  if(jQuery('#org_name').val()!=''){

           pickup.Name = jQuery.trim(jQuery('#org_name').val());
    	  
    	  }      
          if(jQuery('#org_phone').val()!=''){
           
           pickup.Phone = jQuery.trim(jQuery('#org_phone').val());
          
          }

          if(jQuery("#org_telephone").val()!=''){

           // pickup.Phone = jQuery.trim(jQuery("#org_telephone").val());
          
          }

          if(jQuery("#org_state").val()!=''){
           
           pickupaddress.StateProvince = jQuery.trim(jQuery("#org_state").val());
          
          }

          
          if(jQuery('#org_country').val()!=''){

           pickupaddress.Country = jQuery.trim(jQuery('#org_country').val());
          
          }

          if(jQuery('#org_add_1').val()!=''){
           
           pickup.Address = jQuery.trim(jQuery('#org_add_1').val());

          }

          if(jQuery('#org_add_2').val()!=''){
           
           pickup.Address = jQuery.trim(jQuery('#org_add_2').val());
          
          }
          
          if(jQuery('#placefrom').val()!=''){

           pickupaddress.Postcode = jQuery.trim(jQuery('#placefrom').val());

          }
          

          if(jQuery('#des_name').val()!=''){

           drop.Name = jQuery.trim(jQuery('#des_name').val());

          }
          
          if(jQuery('#des_phone').val()!=''){

           drop.Phone = jQuery.trim(jQuery('#des_phone').val());

          }
          
          if(jQuery('#des_telephone').val()!=''){

           drop.Phone = jQuery.trim(jQuery('#des_telephone').val());

          }
          
          if(jQuery('#des_state').val()!=''){

           dropaddress.StateProvince = jQuery.trim(jQuery('#des_state').val());

          }
          
          if(jQuery('#des_country').val()!=''){

           dropaddress.Country = jQuery.trim(jQuery('#des_country').val());

          }
          
          if(jQuery('#des_add_1').val()!=''){

           drop.Address = jQuery.trim(jQuery('#des_add_1').val());

          }
          
          if(jQuery('#des_add_2').val()!=''){

           drop.Address = jQuery.trim(jQuery('#des_add_2').val());

          }
          
          if(jQuery('#placeto').val()!=''){

           dropaddress.Postcode = jQuery.trim(jQuery('#placeto').val());

          }
          
          if(jQuery('#travel_vehical').val()!=''){

           useless.travel_vehical = jQuery.trim(jQuery('#travel_vehical').val());

          }
          
          if(jQuery('#box').val()!=''){

           item.Quantity = jQuery.trim(jQuery('#box').val());

          }
          
          if(jQuery('#total_weight').val()!=''){

           item.Quantity = jQuery.trim(jQuery('#total_weight').val());

          }
          
          if(jQuery('#delivery_ser').val()!=''){

           useless.delivery_ser = jQuery.trim(jQuery('#delivery_ser').val());

          }
          
          if(jQuery('#waiting_time').val()!=''){

           useless.waiting_time = jQuery.trim(jQuery('#waiting_time').val());

          }
          
          if(jQuery('#truckCharge').val()!=''){

           useless.truckCharge = jQuery.trim(jQuery('#truckCharge').val());

          }
          
          if(jQuery('#boxspan').val()!=''){

           useless.boxspan = jQuery.trim(jQuery('#boxspan').val());

          }
          
          if(jQuery('#weightspan').val()!=''){

           useless.weightspan = jQuery.trim(jQuery('#weightspan').val());

          }
          
          if(jQuery('#latespan').val()!=''){

           useless.latespan = jQuery.trim(jQuery('#latespan').val());

          }
          if(jQuery('#total_amount').val()!=''){

           item.price = jQuery.trim(jQuery('#total_amount').val());

          }

   		


   		data['pickup']  = pickup;
       	data['pickupaddress'] = pickupaddress;
       	data['drop'] = drop;
       	data['dropaddress'] = dropaddress;
		data['useless'] = useless;
		data['item'] = item;


   		
   			console.log(data);
		return data;
	}
  var total_amount=function()
  {
  	var total=0;
  	var latespan=jQuery("#latespan").val();
  	var span_delivery_ser=jQuery("#span_delivery_ser").val();
  	var weightspan=jQuery("#weightspan").val();
  	var boxspan=jQuery("#boxspan").val();
  	var truckCharge=jQuery("#truckCharge").val();
  	totala = Number(latespan)+Number(span_delivery_ser)+Number(weightspan)+Number(boxspan)+Number(truckCharge);
  	total=Math.round(totala);
  	jQuery("#total_amount").val((total).toFixed(2));
  	
  }    
  var validation=function(){
        var flag=false;
		jQuery(".errorinput").removeClass('errorinput');

		if(jQuery('#org_name').val()==''){
			
			jQuery( "#org_name" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#org_phone').val()==''){
			
			jQuery( "#org_phone" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#org_telephone').val()==''){
			
			jQuery( "#org_telephone" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#org_state').val()==''){
			
			jQuery( "#org_state" ).addClass( "errorinput" );

		 	flag=true;
		}
		
		if(jQuery('#org_country').val()==''){
			
			jQuery( "#org_country" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#org_add_1').val()==''){
			
			jQuery( "#org_add_1" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#placefrom').val()==''){
			
			jQuery( "#placefrom" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#des_name').val()==''){
			
			jQuery( "#des_name" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#des_phone').val()==''){
			
			jQuery( "#des_phone" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#des_telephone').val()==''){
			
			jQuery( "#des_telephone" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#des_state').val()==''){
			
			jQuery( "#des_state" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#des_country').val()==''){
			
			jQuery( "#des_country" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#des_add_1').val()==''){
			
			jQuery( "#des_add_1" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#pickupcounty').val()==null){
			
			jQuery( "#pickupcounty" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#pickupcity').val()==null){
			
			jQuery( "#pickupcity" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#placefrom').val()==null){
			
			jQuery( "#placefrom" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#dropoffcounty').val()==null){
			
			jQuery( "#dropoffcounty" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#dropoffCity').val()==null){
			
			jQuery( "#dropoffCity" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#placeto').val()==null){
			
			jQuery( "#placeto" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#addres1').val()==''){
			
			jQuery( "#addres1" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}


/////// Additional delivery
	if (jQuery("#add_delivery_issue").prop('checked')==true){ 
		if(jQuery('#org_name_add').val()==''){
			
			jQuery( "#org_name_add" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#org_phone_add').val()==''){
			
			jQuery( "#org_phone_add" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#org_state_add').val()==''){
			
			jQuery( "#org_state_add" ).addClass( "errorinput" );

		 	flag=true;
		}
		
		if(jQuery('#org_county_add').val()==''){
			
			jQuery( "#org_county_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#org_add_1_add').val()==''){
			
			jQuery( "#org_add_1_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#placefrom_add').val()==''){
			
			jQuery( "#placefrom_add" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#des_name_add').val()==''){
			
			jQuery( "#des_name_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#des_phone_add').val()==''){
			
			jQuery( "#des_phone_add" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#des_state_add').val()==''){
			
			jQuery( "#des_state_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#des_county_add').val()==''){
			
			jQuery( "#des_county_add" ).addClass( "errorinput" );
		 
		 	flag=true;
		}
		
		if(jQuery('#des_add_1_add').val()==''){
			
			jQuery( "#des_add_1_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		
		if(jQuery('#pickupcounty_add').val()==null){
			
			jQuery( "#pickupcounty_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#pickupcity_add').val()==null){
			
			jQuery( "#pickupcity_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#placefrom_add').val()==null){
			
			jQuery( "#placefrom_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#dropoffcounty_add').val()==null){
			
			jQuery( "#dropoffcounty_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#dropoffCity_add').val()==null){
			
			jQuery( "#dropoffCity_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#placeto_add').val()==null){
			
			jQuery( "#placeto_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
		if(jQuery('#addres1_add').val()==''){
			
			jQuery( "#addres1_add" ).addClass( "errorinput" );
		 	
		 	flag=true;
		}
	}

		// if(jQuery('#travel_vehical').val()==''){
			
		// 	jQuery( "#travel_vehical" ).addClass( "errorinput" );
		 
		//  	flag=true;
		// }
		
		// if(jQuery('#box').val()==''){
			
		// 	jQuery( "#box" ).addClass( "errorinput" );
		 	
		//  	flag=true;
		// }
		// if(jQuery('#total_weight').val()==''){
			
		// 	jQuery( "#total_weight" ).addClass( "errorinput" );
		 	
		//  	flag=true;
		// }
		// if(jQuery('#delivery_ser').val()==''){
			
		// 	jQuery( "#delivery_ser" ).addClass( "errorinput" );
		 	
		//  	flag=true;
		// }
		// if(jQuery('#waiting_time').val()==''){
			
		// 	jQuery( "#waiting_time" ).addClass( "errorinput" );
		 	
		//  	flag=true;
		// }

		return flag;
    
    }

		
		var service=function(zipcode,srvice,placefrom){

		    jQuery.ajax({
		     url: 'array.php',
		     type: 'POST',
		     dataType: 'JSON',
		     data: {action: 'service',type:srvice,dropoff:zipcode,pickup:placefrom},
		     success:function(data){
		      	
		      //alert(data.rate);
		      	
		      	if(data.type=="Atlanta"){

		      		Atlantaprice(data,srvice);
		      	
		     	}

		     	if(data.type=="1.5"){

		      		price(data,srvice);
		      	
		     	}

		     	if(data.type=="1.6"){

		      		price(data,srvice);
		      	
		     	}

		     	if(data.type=="1.7"){

		      		price(data,srvice);
		      	
		     	}
		     	

		     }
		    });

		}

		
		var checkit=function(placeto,placefrom,service){

		    jQuery.ajax({
		     url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
		     type: 'POST',
		     dataType: 'JSON',
		     data: {action: 'checkit',placeto:placeto,placefrom:placefrom},
		     success:function(data){
		     	
		     	jQuery("#mileage").val(data['miles']);

		     	// alert(data);
		     	if(data.rate==='15'){

		     		Atlantaprice(data,service);

		     	}
		     	if(data.rate==='1.5'){

		     		price(data,service);

		     	}

		     	if(data.rate==='1.6'){

		     		price(data,service);
		     	}
		     	if(data.rate==='1.7'){

		     		price(data,service);
		     	}

		     	
		     	if(data==="emailme"){

		     	alert("This delivery is outside of our service area, please email us for a custom quote at info@atlantaprintercouriers.com");
		     	
          window.location.replace("https://atlantaprintercouriers.com/cp/");
          	

		     	}

		     }
		    });

		}
		

	var Atlantaprice=function(data,srvice){

				var rate=data['rate'];
		     	 	var total_rate=0;

		     	 if(srvice==="direct"){
		     	 	total_rate=(Number(rate)*2.5).toFixed(2);
		     	 	
		     	 }
		     	 if(srvice==="rush"){
		     	 	total_rate=(Number(rate)*1.75).toFixed(2);
		     	 	
		     	 }

		     	 if(srvice==="regular"){
		     	 	total_rate=(Number(rate)*1).toFixed(2);
		     	 	
		     	 }
		     	 var rate=data['rate'];	
           jQuery('#span_delivery_ser').val(total_rate);	
		     		total_amount();
	}

	var price=function(data,srvice){

				var rate=data['rate'];
		     	 	miles=data['miles'].split('mi');

		     	 	var total_rate=0;

		     	 if(srvice==="direct"){
		     	 	total_rate=(Number(rate)*2.5).toFixed(2);
		     	 	
		     	 }
		     	 if(srvice==="rush"){
		     	 	total_rate=(Number(rate)*1.75).toFixed(2);
		     	 	
		     	 }

		     	 if(srvice==="regular"){
		     	 	total_rate=(Number(rate)*1).toFixed(2);
		     	 	
		     	 }
		     	 var rate=data['rate'];
		     	 jQuery('#span_delivery_ser').val((miles[0]*total_rate).toFixed(2));	
		     		total_amount();
	}	

return{

	init:function(){

		this.bindUI();
	},
	bindUI:function(){
		
		var self=this;
		jQuery("#info").click(function(){
				jQuery("#information").show();
				jQuery("#other_info").hide();
				jQuery("#devilory").hide();
				jQuery( "#info" ).addClass( "active" );
				jQuery( "#other" ).removeClass( "active" );
				jQuery( "#book" ).removeClass( "active" );
			});
			jQuery("#other").click(function(){
				// var status=validation();
				// //alert(status);
				// if(status!=true)
				// {
				jQuery("#other_info").show();
				jQuery("#information").hide();
				jQuery("#devilory").hide();
				jQuery( "#info" ).removeClass( "active" );
				jQuery( "#other" ).addClass( "active" );
				jQuery( "#book" ).removeClass( "active" );
				//}
			});
			jQuery("#book").click(function(){
				jQuery("#devilory").show();
				jQuery("#other_info").hide();
				jQuery("#information").hide();
				jQuery( "#info" ).removeClass( "active" );
				jQuery( "#other" ).removeClass( "active" );
				jQuery( "#book" ).addClass( "active" );
			});
			
			jQuery("#shownext").click(function(){
				var status=validation();
				//alert(status);
				if(status!=true)
				{
				jQuery("#other_info").show();
				jQuery("#information").hide();
				jQuery("#devilory").hide();
				jQuery( "#info" ).removeClass( "active" );
				jQuery( "#other" ).addClass( "active" );
				jQuery( "#book" ).removeClass( "active" );
				}
				
			});
			jQuery("#shownextlevel").click(function(){
				jQuery("#devilory").show();
				jQuery("#other_info").hide();
				jQuery("#information").hide();
				jQuery( "#info" ).removeClass( "active" );
				jQuery( "#other" ).removeClass( "active" );
				jQuery( "#book" ).addClass( "active" );
			});
			jQuery('#travel_vehical').on('change',function(){
				var type=jQuery(this).val();
				if (type=='strt-truck') {
					jQuery('#truckCharge').val("20");	
				}
				else if(type=='cargo-van'){
					jQuery('#truckCharge').val("10");	
				}
				else{

					jQuery('#truckCharge').val("");	
				}
				
				if(type=='car' && jQuery('#box').val()>=50){

					alert("a car is for 10-15 boxes of average size.  Please ensure you select the right vehicle type for your packages");
					jQuery('#box').val("");
		    	jQuery("#travel_vehical").val("");
		    	jQuery("#travel_vehical").focus();
				}

				total_amount();
			});
			jQuery('#box').on('input',function(){

		    var box=jQuery(this).val();
		    if(box>3 && box<50){
		     amount=(box-3)*2.5;
		     jQuery('#boxspan').val((amount).toFixed(2));

		    }
		    else
		    {
				 jQuery('#boxspan').val('');		    	
		    }
		    if(box>=50 && jQuery('#travel_vehical').val()=="car"){

		    	alert("a car is for 10-15 boxes of average size.  Please ensure you select the right vehicle type for your packages");
		    	jQuery('#box').val("");
		    	jQuery("#travel_vehical").val("");
		    	jQuery("#travel_vehical").focus();
		    }
		    total_amount();
		 
		   });

		   jQuery('#total_weight').on('input',function(){

		     var total_weight=jQuery(this).val();

		     if(total_weight>30){

		      weight=(total_weight-30)*0.15;
		      jQuery('#weightspan').val((weight).toFixed(2)); 
		      
		     }
		     else
		     {
		     	jQuery('#weightspan').val('');
		     }
		     total_amount();

		   });

		   jQuery('#waiting_time').on('input',function(){

		     var waiting_time=jQuery(this).val();

		     if(waiting_time>15){

		      latecharge=(waiting_time-15)*0.25;
		      jQuery('#latespan').val((latecharge).toFixed(2)); 
		      total_amount();
		     }
		     else
		     {
		     	jQuery('#latespan').val('');
		     }
		     total_amount();
		   });


		  
		    jQuery('#pickupcounty').on('change',function(){

		    	var pickUpzipcode=jQuery(this).val();
		    	get_cityarray(pickUpzipcode);

		    });
		    jQuery('#pickupcity').on('change',function(){

		    	var pickUpzipcode=jQuery(this).val();
		    	get_ziparray(pickUpzipcode);

		    });
		    jQuery('#pickupcounty_add').on('change',function(){

		    	var pickUpzipcode=jQuery(this).val();
		    	get_cityarray(pickUpzipcode);

		    });
		    jQuery('#pickupcity_add').on('change',function(){

		    	var pickUpzipcode=jQuery(this).val();
		    	get_ziparray(pickUpzipcode);

		    });

		    jQuery('#dropoffcounty').on('change',function(){

		    	var dropoffzipcode=jQuery(this).val();
		    	get_dropcityarray(dropoffzipcode);
		    
		    });
		    jQuery('#dropoffCity').on('change',function(){

		    	var dropoffzipcode=jQuery(this).val();
		    	// alert(dropoffzipcode);
		    	get_dropziparray(dropoffzipcode);
		    
		    });
		    jQuery('#dropoffcounty_add').on('change',function(){

		    	var dropoffzipcode=jQuery(this).val();
		    	get_dropcityarray(dropoffzipcode);
		    
		    });
		    jQuery('#dropoffCity_add').on('change',function(){

		    	var dropoffzipcode=jQuery(this).val();
		    	// alert(dropoffzipcode);
		    	get_dropziparray(dropoffzipcode);
		    
		    });

		    jQuery('#boxspan').on('input',function(){

			jQuery('#boxspan').val("");
			var box=jQuery('#box').val();
		    
		     if(box>3){
			     
			     amount=(box-3)*2.5;
			     jQuery('#boxspan').val(amount);
				
			}
		
		});

        jQuery('#weightspan').on('input',function(){

			jQuery('#weightspan').val("");
			
		    
		     var total_weight=jQuery("#total_weight").val();

		     if(total_weight>30){

		      weight=(total_weight-30)*0.15;
		      jQuery('#weightspan').val(weight); 
		      
		     }
		
		});

		jQuery('#span_delivery_ser').on('input',function(){

			jQuery('#span_delivery_ser').val("");
			
		    var placeto=jQuery("#placeto").val();
	        var placefrom=jQuery('#placefrom').val();
	        var srvice=jQuery("#delivery_ser").val();
	        checkit(placeto,placefrom,srvice); 
			      
			     
		
		});   

		jQuery('#latespan').on('input',function(){

			jQuery('#latespan').val("");
			
		    var waiting_time=jQuery("#waiting_time").val();

		     if(waiting_time>15){

		      latecharge=(waiting_time-15)*0.25;
		      jQuery('#latespan').val(latecharge); 
		      
		     }      
			     
		
		});


            
			jQuery(".again").on('click',function(){

				jQuery(this).addClass('date');
				var dateToday = new Date();
				jQuery('.again').datetimepicker({
        			minDate: dateToday
            	});
			});

            //jQuery('#timepicker1').timepicker();
       		
   //     		var dateToday = new Date();
   //     		var month = dateToday.getMonth()+1;
			// var day = dateToday.getDate();

			// var output='<?php //echo Date('m/d/Y');?>'


		    jQuery('#check').on('click',function(){

		    	var placeto=jQuery("#placeto").val();
		   		var placefrom=jQuery('#placefrom').val();

		   		checkit(placeto,placefrom);
		    
		    });
		    

		   jQuery('#delivery_ser').on('change',function(){
		    	var placeto=jQuery("#placeto").val();
        		var placefrom=jQuery('#placefrom').val();
        		var srvice=jQuery(this).val();
        		checkit(placeto,placefrom,srvice);
          // service(placeto,srvice,placefrom);
		   });
		 
		   jQuery('#bokingg').on('click',function(){

		   		self.initsaveobj();
		   		

		   });

	},
	initsaveobj:function(){

		var data=getsaveobj();
		var errorcheak=validation();
		if(!errorcheak){

			savedata(data);
		}
		else{

			alert('some error occour');
		}
	}

};
};
var swif=new swift();
swif.init();
	</script>

<?php	
	
	}	
add_action("wp_footer","quote_script",10);
function signinscript()
{
	?>
		<script>
				jQuery('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var jQuerythis = jQuery(this),
      label = jQuerythis.prev('label');

	  if (e.type === 'keyup') {
			if (jQuerythis.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( jQuerythis.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( jQuerythis.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( jQuerythis.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

jQuery('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  jQuery(this).parent().addClass('active');
  jQuery(this).parent().siblings().removeClass('active');
  
  target = jQuery(this).attr('href');

  jQuery('.tab-content > div').not(target).hide();
  
  jQuery(target).fadeIn(600);
  
});

function validatefileds(){

	 var flag=false;
	 var usersignup=jQuery.trim(jQuery('#usersignup').val());
	 var passwordsignup=jQuery.trim(jQuery('#passwordsignup').val());
	 var repasswordsignup=jQuery.trim(jQuery('#repasswordsignup').val());

	 if(usersignup==""){

	 	flag=true;
	 }
	 if(passwordsignup==""){

	 	flag=true;
	 }
	 if(repasswordsignup==""){

	 	flag=true;
	 }

	 return flag;
}
	jQuery('#get_strt_forget').on('click',function(){
		
		var userforget=jQuery.trim(jQuery('#userforget').val());
		var emailforget=jQuery.trim(jQuery('#emailforget').val());
		if(userforget!="" && emailforget!=""){
			
			jQuery('#forget').hide();
			jQuery('#get_strt_forget_div').show();
		}
		else{

			alert("Fill the Date correctly!!");
		}

	});
jQuery('#get_strt').click(function(){
  
  	var password=jQuery('#passwordsignup').val();
	var repassword=jQuery('#repasswordsignup').val();

	var error=validatefileds();
	if(!error){

		if(password==repassword){
		
		 var usersignup=jQuery('#usersignup').val();
		 var passwordsignup=jQuery('#passwordsignup').val();
		 var repasswordsignup=jQuery('#repasswordsignup').val();
		  jQuery('#user_name').val(usersignup);
		  jQuery('#password').val(passwordsignup);
		  jQuery('#re_password').val(repasswordsignup);
		  jQuery("#part1").hide();
		  jQuery("#part2").show(600);
		}
	else{

		jQuery('#password_error').text("Mis match of password !!");
		alert("not same !!!!");
		}
	}
	else{

		alert("fill the data correctly");
	}

	

  
 
  
});
jQuery('#usersignup').blur(function(event) {
	
	 
	var username=jQuery(this).val();
	if(username!=""){
		jQuery('#showcheckdiv').show();
		jQuery.ajax({
			url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/ajaxfetchdata.php',
			type: 'POST',
			dataType: 'json',
			data: {'username': username,'usernameavalibel':'usernameavalibel'},
			success:function(data){

				if(data=="UserName Already Taken!!"){

					jQuery('#usersignup').val("");
					jQuery( "#usersignup" ).focus();
					jQuery('#showcheckdiv').hide();
					jQuery('#useravalibel').text(data);
					jQuery('#useravalibel').css({
						color: 'red',
						'font-size': '18px'
					});
				}
				else{

					jQuery('#showcheckdiv').hide();
					jQuery('#useravalibel').text(data);
					jQuery('#useravalibel').css({
						color: 'Green',
						'font-size': '18px'
					});	
				}
			}
		});
	}
});


		</script>
	<?php
}
add_action("wp_footer","signinscript",10);
function sign_inpage(){
	session_start();

	?>
	<style>
	input,textarea,select{
  font-size: 22px;
  display: block;
  width: 100%;
  height: 100%;
  padding: 5px 10px;
  background: none;
  color: #a0b3b0 !important;
  background-image: none;
  border: 1px solid #a0b3b0;
  color: #ffffff;
  border-radius: 0;
  -webkit-transition: border-color .25s ease, box-shadow .25s ease;
  transition: border-color .25s ease, box-shadow .25s ease;
}
select:focus{
  outline: 0;
  border-color: #1ab188;
}
select option{
	background: #2a3843;
	color: #a0b3b0 !important;
  background-image: none;
}
	</style>
	&nbsp;
<?php 


if(isset($_POST['login'])){
	
	session_start();

	$username=$_POST['user_name'];
	$password=$_POST['paswrd'];
	global $wpdb;
	$results = $wpdb->get_results( "SELECT `user_id`,`user_name` FROM `signup_information` WHERE `user_name`='".$username."' AND password='".$password."'", ARRAY_A );
	$rowCount = $wpdb->num_rows;
	if($rowCount>0){
		$_SESSION['username']=$username;
		$_SESSION['userid']=$results[0]['user_id'];

			if(isset($_SESSION['redirect'])){

				header("Location: https://atlantaprintercouriers.com/quote/#");
				  exit;					
			}
			else{

				 header("Location: https://atlantaprintercouriers.com/dashboard/");
				  exit;

			}
	}
	
	else{

		$msg="UserName Or Password is Invalide!!";

	}
	



}?>

<div class="form">
<ul class="tab-group">
 	<li class="tab active"><a href="#login">Log In</a></li>
 	<li class="tab "><a href="#signup">Sign Up</a></li>
</ul>
<div class="tab-content" style="color:white;">
<div id="login">
<h1 style="color: #1ab188;">Welcome Back!</h1>
<form action="https://atlantaprintercouriers.com/signin/" method="post">
<div class="field-wrap"><label>User Name<span class="req">*</span></label><input autocomplete="off" required="" type="text" name="user_name" value=""/></div>
<div class="field-wrap"><label>Password<span class="req">*</span></label><input autocomplete="off" required="" type="password" name="paswrd" value="" /></div>
<div><p style="font-size: 22px;text-align: center;color: red;"><?php if (isset($msg)) {echo $msg;}?></p></div>

<p class="forgot"><a href="https://atlantaprintercouriers.com/forgot-password/">Forgot Password?</a></p>
<button class="button button-block" name="login">Log In</button>
</form></div>
<div id="signup">
<h1 style="color: #1ab188;">Sign Up for Free</h1>

<div id="part1">
<div class="field-wrap"><label>User Name<span class="req">*</span>
</label><input autocomplete="off" required="" type="text" value="" name="user_name" id="usersignup"/></div>
<div><span id="useravalibel"></span></div>
<div class="top-row">
<div class="field-wrap"><label>Password<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="password" type="password" id="passwordsignup"/></div>
<div class="field-wrap"><label>ReEnter Password<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="re_password" type="password" id="repasswordsignup"/></div>

</div>
<div><span id="password_error" style="color: red;font-size: 18px;"></span></div>
<button class="button button-block" id="get_strt"  type="button">Get Started</button>
</div>

<form action="<?= site_url()."/signup/";?>" method="post" name="signup">
<div id="part2" style="display:none;">
<input type="hidden" name="user_name"   id="user_name"   >
<input type="hidden" name="password"    id="password"    >
<input type="hidden" name="re_password" id="re_password" >
<div class="field-wrap">
<label><span class="req"></span></label><select name="question">
	<option>Password Hint Question <span class="req">*</span></option>
	<option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
	<option value="What is the name of the city
in which you were born?">What is the name of the city
in which you were born?</option>
	<option value="What is your pet's name?">What is your pet's name? </option>
</select>
</div>
<div class="field-wrap"><label>Password Hint Answer<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="pass_ans" type="text" /></div>
<div class="top-row">
<div class="field-wrap"><label>First Name<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="fname" type="text" /></div>
<div class="field-wrap"><label>Last Name<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="lname" type="text" /></div>
</div>
<div class="field-wrap"><label>Company Name<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="Company" type="text" /></div>
<div class="field-wrap"><label>Address 1 <span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="addres1" type="text" /></div>
<div class="field-wrap"><label>Address 2<span class="req">optional</span>
</label><input autocomplete="off" value="" name="adress2" type="text" /></div>
<div class="top-row" style="display:none;">
<div class="field-wrap"><label>State<span class="req">*</span>
</label><input autocomplete="off" value="" name="room" type="text" /></div>
<div class="field-wrap"><label>Country<span class="req">*</span>
</label><input autocomplete="off" value="" name="city" type="text" /></div>
</div>
<div class="top-row">
<div class="field-wrap"><label>
</label><select name="State" size="1"> 
	        <option value="" selected="">State<span class="req">*</span></option><option value="AK">AK</option><option value="AL">AL</option><option value="AR">AR</option><option value="AZ">AZ</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DC">DC</option><option value="DE">DE</option><option value="FL">FL</option><option value="GA">GA</option><option value="HI">HI</option><option value="IA">IA</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="MA">MA</option><option value="MD">MD</option><option value="ME">ME</option><option value="MI">MI</option><option value="MN">MN</option><option value="MO">MO</option><option value="MS">MS</option><option value="MT">MT</option><option value="NC">NC</option><option value="ND">ND</option><option value="NE">NE</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NV">NV</option><option value="NY">NY</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VA">VA</option><option value="VT">VT</option><option value="WA">WA</option><option value="WI">WI</option><option value="WV">WV</option><option value="WY">WY</option>
	      </select></div>
<div class="field-wrap"><label>
</label><select name="Country" size="1"> 
	        <option value="" selected="">Select Country<span class="req">*</span></option><option value="USA">USA</option><option value="Argentina">Argentina</option><option value="Brazil">Brazil</option><option value="Canada">Canada</option><option value="China">China</option><option value="Finland">Finland</option><option value="France">France</option><option value="Germany">Germany</option><option value="Hong Kong">Hong Kong</option><option value="India">India</option><option value="Italy">Italy</option><option value="Japan">Japan</option><option value="Mexico">Mexico</option><option value="Norway">Norway</option><option value="Portugal">Portugal</option><option value="Spain">Spain</option><option value="UK">UK</option>
	      </select></div>
</div>
<div class="top-row">
<div class="field-wrap"><label>Zip code<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="zip" type="number" /></div>
<div class="field-wrap"><label>Phone<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="phone" type="number" />
<!--<input type="text" maxlength="12" required="" name="phone" id="des_phone" placeholder="123-456-7890" class="phone_us form-control" value="">--></div>
</div> 
<div class="field-wrap"><label>Email<span class="req">*</span>
</label><input autocomplete="off" required="" value="" name="email" type="email" /></div>
<button class="button button-block" id="" type="submit" name="btn">Get Register </button>
</div>

</form></div>
</div>
<div id="showcheckdiv" style="display: none;position: absolute;left: 377px;top: 373px;background: black;opacity: 0.4;width: 598px;height: 516px;">
	<img src="<?=get_template_directory_uri ()."/images/checking.gif"?>" style="position:relative;top: 96px;left: 167px;">
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!--<script src="js/index.js"></script>//-->

&nbsp;
	<?php
}
add_filter("sign_inpage","sign_inpage",10);
function dashBoard(){
	
	session_start();

	if(isset($_POST['logout'])){
		unset($_SESSION['username']);
		unset($_SESSION['userid']);
		session_destroy();
		header("Location: https://atlantaprintercouriers.com");
		exit;
	}

	if(isset($_SESSION['username'])){
			
			if(isset($_POST['saveaddress'])){
			
				$_SESSION['userid'];
					global $wpdb;
		$GLOBALS['wpdb']->query("INSERT INTO `address_book` (`Name`,`StreetAddress`,`City`,`State`,`ZipCode`,`County`,`Phone`,`Email`,`user_id`)VALUES ('".$_POST['Name']."','".$_POST['StreetAddress']."','".$_POST['City']."','".$_POST['State']."','".$_POST['ZipCode']."','".$_POST['County']."','".$_POST['Phone']."','".$_POST['Email']."','".$_SESSION['userid']."')");

	}	
	?>

	<div class="subnavbar">
<div class="subnavbar-inner">
<div class="container">
	<nav class="navbar" style="color:black;">
		<div class="container-fluid">
				<div id="myNavbar">
					<ul class="nav nav-tabs">
		 				<li class="active">
		 					<a style="color: black !important;font-size: 18px;" href="#dashboard" data-toggle="tab">Dash Board</a>
		 				</li>
		 				<li>
		 					<a style="color: black !important;font-size: 18px;" href="#myhistory" data-toggle="tab">History</a>
		 				</li>
				        <li style="float:right; "><form action="https://atlantaprintercouriers.com/dashboard/" method="post">
		 						<button class="btn btn-info" name="logout" style="box-shadow: 5px 7px 6px #ccc;" type="submit"><span class="glyphicon glyphicon-log-out"></span>log out</button>
		 				</form></li>
		 				<li style="color: black !important;font-size: 18px;float: right;margin: 5px 42px;">Welcome : <?php echo $_SESSION['username']; ?></li>

				    </ul>
				</div>
		</div>
	</nav>
</div>
<!-- /container -->

</div>
<!-- /subnavbar-inner -->

</div>
<!-- /subnavbar -->
<div class="tab-content ">
  <div class="tab-pane active" id="dashboard">
		<div class="row" style="position: relative;top: -16px;left: 20px;">
			<a href="https://atlantaprintercouriers.com/quote/"><button type=button class="btn btn-info" style="box-shadow: 5px 7px 6px #ccc;">schedule a delivery</button></a>
		</div> 
			<div class="main">
				<div class="main-inner">
					<div class="container">
						<div class="row">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="col-md-3">
										Address Book
									</div>
									<div class="col-md-9">
										<div class="pull-right">
											<button type="button" id="delete_address" class="btn btn-info" data-toggle="modal">
												<span class="glyphicon glyphicon-minus"></span>
											</button>
											<button type="button" class="btn btn-info" data-toggle="modal" data-target="#addadressModal" data-toggle="modal" data-target="#myModal">
												<span class="glyphicon glyphicon-plus"></span>
											</button>
										</div>
									</div>
									<br/>
								</div>
							<div class="panel-body">
								<table class="table">
									<tr>
										<th class="hidden-xs" width="50">
											<input id="selectAllAddress" type="checkbox" aria-label="X">
										</th>
										<th> Name</th>
										<th> Street</th>
										<th> City</th>
										<th> State</th>
										<th> ZipCode #</th>
										<th> County</th>
										<th> Phone #</th>
										<th> Email</th>
									</tr>
							<?php 
								global $wpdb;
									$results = $wpdb->get_results( "SELECT `add_id`, `Name`,`StreetAddress`,`City`,`State`,`ZipCode`,`County`,`Phone`,`Email` FROM `address_book` WHERE `user_id`='".$_SESSION['userid']."'", ARRAY_A );
										$rowCount = $wpdb->num_rows;
										if($rowCount>0){

											foreach ($results as $key => $value) {
										
											echo"<tr>".
													"<td>
														<input type='checkbox' value='$value[add_id]' name='addressCheck'>
													</td>".
													"<td>".$value['Name']."</td>".
													"<td>".$value['StreetAddress']."</td>".
													"<td>".$value['City']."</td>".
													"<td>".$value['State']."</td>".
													"<td>".$value['ZipCode']."</td>".
													"<td>".$value['County']."</td>".
													"<td>".$value['Phone']."</td>".
													"<td>".$value['Email']."</td>".
												"</tr>";

											}
										}
										else{

											echo "<tr><td colspan='9' style='text-align: center;'>No Address is Saved Yet </td></tr>";
										}
							?>
							
						</table>
					</div>
				</div>
				
			</div>
  		</div>

</div>
</div>
</div>
<div class="tab-pane" id="myhistory">
	<?php if ($_SESSION['userid'] == "8") {?>
		<div id="export_history" style="position: relative;top: -16px;left: 20px;" class="row">
			<button style="box-shadow: 5px 7px 6px #ccc;" class="btn btn-info" type="button">Export history</button>
		</div>
	<?php } ?>
    <div class="row">
	<div class="col-md-12">
		<table class="table table-striped">
			<thead>
				<tr id="transaction_header">
					<th>Transaction ID</th>
					<?php if ($_SESSION['userid'] == "8") { ?>
					<th>User name</th>
					<?php } ?>
					<th>Company</th>
					<th>Pick up address</th>
					<th>Pick up State</th>
					<th>Pick up City</th>
					<th>Drop off address</th>
					<th>Drop off State</th>
					<th>Drop off City</th>
					<th>Pick Up Date</th>
					<th>Amount</th>
					<th>Mileage</th>
				</tr>
			</thead>
			<tbody id="transaction_data">
			
			<?php 
			// var_dump($_SESSION['username']);
				global $wpdb;
				if ($_SESSION['userid'] == "8") {
					$results = $wpdb->get_results( "SELECT * FROM `transaction_details`", ARRAY_A );
				}
				else{
					$results = $wpdb->get_results( "SELECT `transaction_id`, `org_name`, `org_add_1`,`org_add_2`,`org_state`,`org_city`,`des_add_1`,`des_add_2`,`des_state`,`des_City`,`total_amount`,`pickdate`, `mileage` FROM `transaction_details` WHERE `username`='".$_SESSION['username']."'", ARRAY_A );
				}	

						$rowCount = $wpdb->num_rows;
						if($rowCount>0){

							foreach ($results as $key => $value) {
						
							echo"<tr>".
									"<td>".$value['transaction_id']."</td>";
							if ($_SESSION['userid'] == "8") {
								echo "<td>".$value['username']."</td>";
							}
							echo    "<td>".$value['org_name']."</td>".
									"<td>".$value['org_add_1']." ".$value['org_add_2']."</td>".
									"<td>".$value['org_state']."</td>".
									"<td>".$value['org_city']."</td>".
									"<td>".$value['des_add_1']." ".$value['des_add_2']."</td>".
									"<td>".$value['des_state']."</td>".
									"<td>".$value['des_City']."</td>".
									"<td>".$value['pickdate']."</td>".
									"<td>".$value['total_amount']."</td>".
									"<td>".$value['mileage']."</td>".
								"</tr>";

							}
						}
						else{

							echo "<tr><td colspan='12' style='text-align: center;'>No Record Found </td></tr>";
						}
				?>
				</tbody>
		</table>
	</div>
</div>
</div>
<!-- /main-inner -->

</div>

<script type="text/javascript">

jQuery(document).ready(function(){

	var get_cityarray=function(data){
		jQuery.ajax({
			url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
			type: 'POST',
			dataType: 'json',
			data: {action:'citycode',pickup:data},
			success:function(data){
				jQuery('#add_city option:not(:first)').remove();
				jQuery.each(data,function(index, el) {
					var row="<option value="+index+">"+el+"</option>";
					jQuery(row).appendTo('#add_city');	
				});

				jQuery('#add_city').val("");
			}
		});
	}
	var get_ziparray=function(data){
		jQuery.ajax({
			url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/myarray.php',
			type: 'POST',
			dataType: 'json',
			data: {action:'zipcode','pickup':data},
			success:function(data){
				jQuery('#ZipCode :not(:first)').remove();
				jQuery.each(data,function(index, el) {
					var row="<option value="+el+">"+el+"</option>";
					jQuery(row).appendTo('#ZipCode');	
				});
			}
		});
	}

	jQuery('#add_county').on('change',function(){
		get_cityarray(jQuery(this).val());
	});
	jQuery('#add_city').on('change',function(){
		get_ziparray(jQuery(this).val());
	});

});

</script>

<div id="addadressModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content" style="position: relative;top: 150px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Address</h4>
      </div>
      <form action="https://atlantaprintercouriers.com/dashboard/" method="post">
      <div class="modal-body">
   <div class="row">
    <div class="col-md-6">
     <lable>Name</lable>
     <input type="text" required="" name="Name" class="form-control" placeholder="Place Your Comapny Name Here">
    </div>
   </div>
   <div class="row">
    <div class="col-md-12">
     <lable>Street Address</lable>
     <input type="text" required="" name="StreetAddress" class="form-control" placeholder="Place Your Street Address here ">
    </div>
   </div>
   <div class="row">
    <div class="col-md-6">
     <lable>County</lable>
     <select type="text" required="" id="add_county" name="County" class="form-control" placeholder="Place Your County here">
		<option value="" selected="" disabled="">Choose Your County</option>
		<option value="Carroll">Carroll</option>
		<option value="Cherokee">Cherokee</option>
		<option value="Clayton">Clayton</option>
		<option value="Cobb">Cobb</option>
		<option value="Coweta">Coweta</option>
		<option value="Dekalb">Dekalb</option>
		<option value="Douglas">Douglas</option>
		<option value="Fayett">Fayett</option>
		<option value="Forsyth">Forsyth</option>
		<option value="Fulton">Fulton</option>
		<option value="Hall">Hall</option>
		<option value="Henry">Henry</option>
		<option value="Gwinnett">Gwinnett</option>
		<option value="Rockdale">Rockdale</option>
     </select>
    </div>
    <div class="col-md-6">
     <lable>State</lable>
     <select name="State" required="" class="form-control" placeholder="Place Your State here"><option value="" selected="" disabled="">State</option><option value="AK">AK</option><option value="AL">AL</option><option value="AR">AR</option><option value="AZ">AZ</option><option value="CA">CA</option><option value="CO">CO</option><option value="CT">CT</option><option value="DC">DC</option><option value="DE">DE</option><option value="FL">FL</option><option value="GA">GA</option><option value="HI">HI</option><option value="IA">IA</option><option value="ID">ID</option><option value="IL">IL</option><option value="IN">IN</option><option value="KS">KS</option><option value="KY">KY</option><option value="LA">LA</option><option value="MA">MA</option><option value="MD">MD</option><option value="ME">ME</option><option value="MI">MI</option><option value="MN">MN</option><option value="MO">MO</option><option value="MS">MS</option><option value="MT">MT</option><option value="NC">NC</option><option value="ND">ND</option><option value="NE">NE</option><option value="NH">NH</option><option value="NJ">NJ</option><option value="NM">NM</option><option value="NV">NV</option><option value="NY">NY</option><option value="OH">OH</option><option value="OK">OK</option><option value="OR">OR</option><option value="PA">PA</option><option value="RI">RI</option><option value="SC">SC</option><option value="SD">SD</option><option value="TN">TN</option><option value="TX">TX</option><option value="UT">UT</option><option value="VA">VA</option><option value="VT">VT</option><option value="WA">WA</option><option value="WI">WI</option><option value="WV">WV</option><option value="WY">WY</option></select>
    </div>
   </div>
   <div class="row">
    <div class="col-md-6">
     <lable>City</lable>
		<select class="form-control" required="" id="add_city" placeholder="Place Your City here" style="text-transform: capitalize;" name="City">
			<option value='' selected='' disabled=''>Choose Your City</option>
		</select>
    </div>
    <div class="col-md-6">
     <lable>Phone</lable>
     <input type="text" required="" name="Phone" class="form-control" placeholder="Place Your Phone Number here">
    </div>
   </div>
   <div class="row">
    <div class="col-md-6">
     <lable>Zip Code</lable>
		<select name="ZipCode" required id="ZipCode" class="form-control" placeholder="Place Your ZipCode here">
			<option value='' selected='' disabled=''>Choose Your Zipcode</option>
		</select>
    </div>
    <div class="col-md-6">
     <lable>Email</lable>
     <input type="text" required="" name="Email" class="form-control" placeholder="Place Your Email Address here">
    </div>
   </div>
      </div>
      <div class="modal-footer">
        <div class="row">
         <div class="col-md-4 col-md-offset-8">
          <div class="col-md-6">
           <button type="submit" class="btn btn-success" name="saveaddress"><span class="glyphicon glyphicon-floppy-saved"></span>Save</button>
  
          </div>
          <div class="col-md-6">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
          </div>
         </div>
        </div>
      </form>
    </div>

  </div>
</div>
</div>

	<script>
		jQuery("#delete_address").on("click", function(){
			var add_id_list = [];
			jQuery('input[name=addressCheck]').each(function() {
				if (jQuery(this).is(':checked')) {
					var add_id = jQuery(this).val();
					add_id_list.push(add_id);
				}
			});
			if (add_id_list.length > 0){
				var is_confirmed = confirm("Do you want to delete all selected records?", "Yes", "No");
				if(is_confirmed){
					jQuery.ajax({
						url: 'https://atlantaprintercouriers.com/wp-content/themes/myway/ajax_hits.php',
						type: 'post',
						dataType: 'json',
						data: {'add_ids':add_id_list.toString(), 'deleteAddress':'deleteAddress'},
						success:function(data){
							window.location = "https://atlantaprintercouriers.com/dashboard";
						}
					});
				}
			}
		});
		jQuery("#selectAllAddress").on("click", function(){
			if (jQuery("#selectAllAddress").is(':checked')) {
				jQuery('input[name=addressCheck]').each(function () {
					jQuery(this).prop("checked", true)
				});
			} else {
				jQuery('input[name=addressCheck]').each(function () {
					jQuery(this).prop("checked", false)
				});	
			}
		});

		jQuery("#export_history").on("click", function(){
			var downloadUrl = "https://atlantaprintercouriers.com/wp-content/themes/myway/ajax_hits.php?exportTransaction=exportTransaction";
			window.open(downloadUrl);
		});

	</script>

	<?php		
	
	}
	
	else{
		
		 
		header("Location: https://atlantaprintercouriers.com/signin/");		
	
		}
	}
?>
	

	<?php

add_action("dash_board","dashBoard",10);

function forget_password_contents(){

	?>
		<style>
	select{
  font-size: 22px;
  display: block;
  width: 100%;
  height: 100%;
  padding: 5px 10px;
  background: none;
  color: #a0b3b0 !important;
  background-image: none;
  border: 1px solid #a0b3b0;
  color: #ffffff;
  border-radius: 0;
  -webkit-transition: border-color .25s ease, box-shadow .25s ease;
  transition: border-color .25s ease, box-shadow .25s ease;
}
select:focus{
  outline: 0;
  border-color: #1ab188;
}
button, input, select, textarea {
	height: 42px !important; 
	}
	</style>
		&nbsp;
<div class="form" id="logindiv">
<div class="tab-content" style="color:white;">
<div id="forget_main" style="display: block !important;">
<h1>Change Your Password</h1>
<div id="forget">
<form action="" method="post">
<div class="field-wrap"><label>User Name<span class="req">*</span>
</label><input id="userforget" autocomplete="off" name="user_name" required="" type="text" value="" /></div>
<div class="field-wrap"><label>Email<span class="req">*</span>
</label><input id="emailforget" autocomplete="off" name="email" required="" type="text" value="" /></div>
<button id="get_strt_forget" class="button button-block" type="button">Get Started</button>

</div>

<div id="get_strt_forget_div" style="display: none;">
<div class="field-wrap"><label></label>
<select required name="question">
<option>Password Hint Question</option>
<option value="What is your mother's maiden name?">What is your mother's maiden name?</option>
<option value="What is the name of the city in which you were born?">What is the name of the city</option>
<option value="What is the name of the city in which you were born?">in which you were born?</option>
<option value="What is your pet's name?">What is your pet's name?</option>
</select></div>
<div class="field-wrap"><label>Password Hint Answer<span class="req">*</span>
</label><input autocomplete="off" name="pass_ans" required="" type="text" value="" /></div>
<button id="" class="button button-block" name="btnforget" type="submit">Get Register </button>

</div>
</form></div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="js/index.js"></script>

	<?php
}

function reset_pasword_form($msg){
	?>
	<div class="form">
	   <div class="tab-content" style="color: white;">
	      <div id="signup" style="display: block !important;">
	         <h1>Reset Your Password</h1>
	         <div id="part3">
	            <form action="" method="post">
	            <div class="field-wrap"><label>New Password<span class="req">*</span>
	               </label><input id="newpass" autocomplete="off" name="new_pass" required="" type="password" value="" />
	            </div>
	            <div class="field-wrap"><label>Retype Password<span class="req">*</span>
	               </label><input id="repass" autocomplete="off" name="renew_pass" required="" type="password" value="" />
	            </div>
	            <div><span style="color: red;font-size: 18px;"><?php  if(isset($msg)){ echo $msg ;}?></span></div>
	            <button class="button button-block" name="re_pass" type="submit">Reset Password</button>
	         	</form>
	         </div>
	      </div>
	   </div>
	</div>
	<?php
} 

function forget_password(){
	
		session_start();

		 
		if(isset($_POST['re_pass'])){

			//var_dump($_POST);
			if($_POST['new_pass']==$_POST['renew_pass']){

				$GLOBALS['wpdb']->query("UPDATE `signup_information` SET `password`='".$_POST['new_pass']."'
					WHERE `user_id`='".$_SESSION['forgetid']."'");
					unset($_SESSION['forgetid']);
	
			}else{

				$msg="Mis Match of Password !!! Retype Password Again ";
				reset_pasword_form($msg);	
			}	
					 
			
		}

		else if(isset($_POST['btnforget'])){

			
			global $wpdb;
			$results = $wpdb->get_results( "SELECT `user_id` FROM `signup_information` WHERE `user_name`='".$_POST['user_name']."' AND email='".$_POST['email']."' AND question='".$_POST['question']."' AND pass_ans='".$_POST['pass_ans']."'", ARRAY_A );
			
			$rowCount = $wpdb->num_rows;
			
			if($rowCount>0){
				
					
					$_SESSION['forgetid']=$results[0]['user_id'];
							
						reset_pasword_form($msg);			
					}
			else{

				echo "Some Error occur!";

					forget_password_contents();
				
				}
			
			}
			else{

					forget_password_contents();
			}	
}
add_filter("forget_password","forget_password",10);
function sign_incss()
{
	session_start();
	if(isset($_SESSION['username']))
	{
		?>
		<style>
			#mega_main_menu.first-lvl-align-right > .menu_holder > .menu_inner > ul> #menu-item-2028
			{
				display: none;
			}
			
		</style>
		<?php
	}
	else
	{
		?>
		<style>
			
			#mega_main_menu.first-lvl-align-right > .menu_holder > .menu_inner > ul> #menu-item-2049
			{
				display: none;
			}
		</style>
		<?php 
	}
	?>
	<link rel="stylesheet" href="https://atlantaprintercouriers.com/wp-content/themes/myway/css/signin_css.css">
	<link href="https://atlantaprintercouriers.com/wp-content/themes/myway/css/dashstyle.css" rel="stylesheet">
	<link href="https://atlantaprintercouriers.com/wp-content/themes/myway/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="https://atlantaprintercouriers.com/wp-content/themes/myway/css/bootstrap-timepicker.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://atlantaprintercouriers.com/wp-content/themes/myway/css/bootstrap-datetimepicker.css">
	<style>
		#mega_main_menu.first-lvl-align-right > .menu_holder > .menu_inner > ul> #menu-item-2028 , #menu-item-2049
	{
		background-color: green !important;
		border-radius: 10px;
	}
	#mega_main_menu.first-lvl-align-right > .menu_holder > .menu_inner > ul> li > .item_link > span > .link_text
	{
		font-size: 18px !important;
		font-weight: bold !important;
	}
	.bootstrap-timepicker-widget table td input {
		width: 52px;
		color: black !important;
	}
	input,textarea{color: black !important;}
	</style>

	<script type='text/javascript' src='https://atlantaprintercouriers.com/wp-content/themes/myway/js/bootstrap-timepicker.min.js'></script>
	<script type='text/javascript' src='https://atlantaprintercouriers.com/wp-content/themes/myway/js/jquery.mockjax.js'></script>
	<script type='text/javascript' src='https://atlantaprintercouriers.com/wp-content/themes/myway/js/typehead.js'></script>
	<script type="text/javascript" src="https://atlantaprintercouriers.com/wp-content/themes/myway/js/moment-with-locales.js" ></script>
	<script type="text/javascript" src="https://atlantaprintercouriers.com/wp-content/themes/myway/js/bootstrap-datetimepicker.js" ></script>
	
	<script type='text/javascript' src='https://atlantaprintercouriers.com/wp-content/themes/myway/js/jquery.mask.min.js'></script>

	<?php
}
add_filter("wp_head","sign_incss",10);
add_filter('the_content', 'remove_bad_br_tags');
function remove_bad_br_tags($content) {
	$content = str_ireplace("<label><br />", "<label><br />", $content);
	return $content;
}
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
function insertdata(){
	if(isset($_POST['btn'])){

		unset($_POST['btn']);
		global $wpdb;
		$abc=$GLOBALS['wpdb']->query("INSERT INTO `signup_information` (`user_name`,`password`,`question`,`pass_ans`,`fname`,`lname`,`Company`,`addres1`,`adress2`,`room`,`city`,`State`,`Country`,`zip`,`phone`,`email`)VALUES ('".$_POST['user_name']."','".$_POST['password']."','".$_POST['question']."','".$_POST['pass_ans']."','".$_POST['fname']."','".$_POST['lname']."','".$_POST['Company']."','".$_POST['addres1']."','".$_POST['adress2']."','".$_POST['room']."','".$_POST['city']."','".$_POST['State']."','".$_POST['Country']."','".$_POST['zip']."','".$_POST['phone']."','".$_POST['email']."')");
		 		
	 		echo "<h2 style='color: green;'>You Have been Successfully sign up your Account</h2>";	
			    $to =get_option('admin_email');
				$subject = 'News letter For Coupons';
				$message = $_SERVER['REMOTE_ADDR']." has requested you for coupons ?? his/her email id is ".$_POST['email'];
			 
			 wp_mail( $to, $subject, $message, $headers );
		} 	
	else{

		 
		wp_redirect( home_url() );
	}
}
add_action('myinsert','insertdata',10);

