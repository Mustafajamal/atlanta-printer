<?php 
/*
Template Name: sitemap 
*/
get_header();
?>
<?php herowp_output_custom_header_bg(); ?>
<?php herowp_output_custom_page_bg_color(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			 <?php echo get_the_title(); ?><span class="dotcolor">.</span>
		</div>
	</div>
</div>
</header><!--HEADER END-->
<div id="main-contact" style="padding-bottom:0px;"><!--main-contact START-->

	<?php if(!empty($herowp_data['google_maps'])): ?>
		<div class="contact-map">
			<?php $allowed_html_tags = array(
					'iframe' => array(
						'src' => array(),
						'width' => array(),
						'height' => array(),
						'frameborder' => array(),
						'style' => array(),
					),
				);
			echo wp_kses($herowp_data['google_maps'],$allowed_html_tags); ?>
		</div>
	<?php endif;?>



										
</div><!--main-contact END-->


<?php get_footer(); ?>