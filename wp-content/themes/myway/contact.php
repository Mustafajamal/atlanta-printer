<?php 
/*
Template Name: Contact Us Template
*/
get_header();
?>
<?php herowp_output_custom_header_bg(); ?>
<?php herowp_output_custom_page_bg_color(); ?>
<div class="shout-wrap">
	<div class="shout-box container">
		<div class="shout-box-title">
			 <?php echo get_the_title(); ?><span class="dotcolor">.</span>
		</div>
	</div>
</div>
</header><!--HEADER END-->
<div id="main-contact"><!--main-contact START-->

	<?php if(!empty($herowp_data['google_maps'])): ?>
		<div class="contact-map">
			<?php $allowed_html_tags = array(
					'iframe' => array(
						'src' => array(),
						'width' => array(),
						'height' => array(),
						'frameborder' => array(),
						'style' => array(),
					),
				);
			echo wp_kses($herowp_data['google_maps'],$allowed_html_tags); ?>
		</div>
	<?php endif;?>

	<div class="container"><!--CONTAINER START-->
		<?php 
		if (have_posts()) : while (have_posts()) : the_post(); 
		?>
	
	
	
	<div class="col-md-8"><!--CONTACT FORM START-->
		<div id="contact-form">
			<h3><?php _e('Leave a reply','myway'); ?></h3>
			<?php the_content();?>
		</div>
	</div><!--CONTACT FORM END-->
	
	<?php if (!empty($herowp_data['enable_info_widget'])) : ?>
		<?php if ($herowp_data['enable_info_widget'] == 'enable'): ?>
		<div class="col-md-4"><!--SIDEBAR START-->
			<a href="<?=get_site_url ()."/signin";?>"><img src="<?=get_template_directory_uri()."/images/bannersave.png";?>" style="width:350px;"></a>
			<h3 class="contact-info" style="border-bottom: 2px dashed black;">Quick quote</h3>
				<p style="margin-top: 19px;font-size: 15px;">Quickly get a Atlanta printer courier quote for your item</p>
				<a href="<?=get_site_url ()."/signin";?>"><button class="btn btn_success" style="background-color: #EC008C;color: white;margin: 20px 0 0 24%;box-shadow: 5px 7px 6px #ccc;">Get a Quick Quote now</button></a>
				<h3 class="contact-info" style="border-bottom: 2px dashed black;">Track Item</h3>
      <input type="text" value="" name="" style="margin-top: 20px;background-color: #555;color: white;border-radius: 10px;" placeholder="Track ID ">
    <p style="margin-top: 19px;font-size: 15px;">Quickly Track an item by intering above the courier order tracking number</p>
    <button class="btn btn_success" style="background-color: #EC008C;color: white;margin: 20px 0 0 32%;box-shadow: 5px 7px 6px #ccc;">Start Tracking</button>

			<h3 class="contact-info"><?php _e('Where to find us','myway'); ?></h3>
			<div id="contact-info"><!--CONTACT INFO START-->
				<div class="info"><!--INFO START-->
					<div class="icon-holder"><i class="icon-telephone-2"></i></div>
					<div class="details-holder">
						<p class="detail1"><?php if(!empty($herowp_data['info_phone_text'])) { echo esc_attr($herowp_data['info_phone_text']); } ?></p>
						<p class="detail2"><?php if(!empty($herowp_data['info_phone_number'])) { echo esc_attr($herowp_data['info_phone_number']); } ?></p>
					</div>
				</div><!--INFO END-->
				<div class="info"><!--INFO START-->
					<div class="icon-holder"><i class="icon-map2"></i></div>
					<div class="details-holder">
						<p class="detail1"><?php if(!empty($herowp_data['info_address_text'])) { echo esc_attr($herowp_data['info_address_text']); } ?></p>
						<p class="detail2"><?php if(!empty($herowp_data['info_address'])) { echo esc_attr($herowp_data['info_address']); } ?></p>
					</div>
				</div><!--INFO END-->
				<div class="info last"><!--INFO START-->
					<div class="icon-holder"><i class="icon-envelope"></i></div>
					<div class="details-holder">
						<p class="detail1"><?php if(!empty($herowp_data['info_email_text'])) { echo esc_attr($herowp_data['info_email_text']); } ?></p>
						<p class="detail2"><?php if(!empty($herowp_data['info_email'])) { echo esc_attr($herowp_data['info_email']); } ?></p>
					</div>
				</div><!--INFO END-->
			</div><!--CONTACT INFO END-->
		</div><!--SIDEBAR END-->
		<?php endif;?>
	<?php endif;?>
		
	</div><!--CONTAINER END-->

	<?php endwhile;endif;?>

										
</div><!--main-contact END-->


<?php get_footer(); ?>