<!-- sidebar -->
<?php if (is_page()){
	$id_page = 'id="page-sidebar"';
}
else{
	$id_page = '';
}
?>
<div class="col-md-4" <?php echo $id_page; ?>>
  <div class="sidebar">
	<div class="sidebar-widget">
		<?php dynamic_sidebar('sidebar');?>
	</div>	
  </div>
</div>
<!-- /sidebar -->
