<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
			<div class="col-md-12 nopadding blog-box" <?php if (!empty($herowp_data['blog_posts_animations']) && !empty($herowp_data['blog_posts_animations_delay']) ){ if ($herowp_data['blog_posts_animations'] != 'No animations') { echo 'data-animate="'.$herowp_data['blog_posts_animations'].'" data-delay="'.$i.'"'; }}?>><!--BLOG POST START-->

							
                            <section><!--section START-->
                                <div class="blog_img"><!--blog_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
												$id_img = get_post_thumbnail_id();
												$img_url = wp_get_attachment_url( $id_img,'full');
												$thumb = aq_resize($img_url, 783, 643, true, true,true,true); if ( !$thumb ) $thumb = $img_url;
										?>
												<a href="<?php the_permalink(); ?>">
										<?php
													echo '<img src="'.esc_url($thumb).'" alt="'.esc_attr(get_the_title()).'" class="img-responsive" />
												</a>';
											}
											else echo'<img src="'.esc_url('http://placehold.it/783x643/333333/ffffff').'" alt="'.esc_attr(get_the_title()).'">';
                                        ?>
										<a class="read_more" href="<?php the_permalink(); ?>"><?php _e('Read More','myway'); ?></a>	
                                </div><!--blog_img END-->
								

                               <div class="date_added_and_category"><!--date_added_and_category START-->
									<div class="round_date">
										<p><?php echo the_time('d'); ?></p>
										<span><?php echo the_time('M'); ?></span>
									</div>
									
									<div class="round_comments">
										<p><?php echo get_comments_number(); ?></p>
										<span><?php _e('comm','myway'); ?></span>
									</div>
                               </div><!--date_added_and_category END-->
                                        
                               <h2><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?><span class="dotcolor"> .</span></a></h2>
                               <p><?php echo excerpt(15); ?></p>

                        
                       </section><!--section END-->

		</div><!--BLOG POST END-->
	</article>
	<!-- /article -->

<?php endif; ?>
