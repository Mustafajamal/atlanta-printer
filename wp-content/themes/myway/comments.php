<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to a function which is
 * located in the functions.php file.
 */
?>

<div class="comment-list">
	<h2 class="article-title"><?php comments_number( __('NO COMMENTS','myway'), __('1 AWESOME COMMENT','myway'), __('% AWESOME COMMENTS','myway') ); ?></h2>
	<div class="title-underline"></div>

	<div id="comments">
	<?php if ( post_password_required() ) : ?>
		<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'myway' ); ?></p>
	</div><!-- #comments -->
	<?php
			/* Stop the rest of comments.php from being processed,
			 * but don't kill the script entirely -- we still have
			 * to fully load the template.
			 */
			return;
		endif;
	?>

	<?php // You can start editing here -- including this comment! ?>

	<?php if ( have_comments() ) : ?>
		<h2 id="comments-title">
			<?php
				/*printf( _n( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'andreas' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );*/
			?>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'myway' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'myway' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'myway' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<ul class="level-1">
			<?php
				/* Loop through and list the comments. Tell wp_list_comments()
				 * to use andreas_comment() to format the comments.
				 * If you want to overload this in a child theme then you can
				 * define andreas_comment() and that will be used instead.
				 * See andreas() in andreeas/functions.php for more.
				 */
				wp_list_comments( array( 'callback' => 'herowp_comment' ) );
				
			?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below">
			<h1 class="assistive-text"><?php _e( 'Comment navigation', 'myway' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'myway' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'myway' ) ); ?></div>
		</nav>
		<?php endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a little note, shall we?
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Comments are closed.' , 'myway' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>

<div class="comment-form">
	<span class="article-title"><?php _e( '' , 'myway' ); ?></span>
	<div class="title-underline"></div>
<?php	
	if (!isset($aria_req))  $aria_req='';

	$args = array(
  'id_form'           => 'commentform',
  'id_submit'         => 'send-comment-button',
  'title_reply'       => '<div class="title comm"><div class="icon"></div><h2>'.__( 'DARE TO LEAVE A REPLY?').'</h2><div class="clear"></div></div>',
  'title_reply_to'    => '<div class="icon"></div>'.__( 'Leave a reply %s' ),
  'cancel_reply_link' => __( 'Cancel Reply','myway' ),
  'label_submit'      => __( 'Send message','myway' ),

  
    'fields' => apply_filters( 'comment_form_default_fields', array(

    'author' =>'
<div class="inputs-container">
	<div class="col-md-6 nopadding">
	
		<div class="inputs"><input type="text" class="field name" id="author" name="author" placeholder="Name" value="' . esc_attr( $commenter['comment_author'] ). $aria_req . '" />
			<!--<i class="fa fa-user"></i>-->
		</div>',
		  
		'email' =>
		  '<div class="inputs">
			<input class="field email" id="email" name="email" type="text" placeholder="Email" value="' . esc_attr(  $commenter['comment_author_email'] ). $aria_req . '" />
			<!--<i class="fa fa-envelope-o"></i>-->
		  </div>',

		'url' =>
		  '<div class="inputs">
				<input class="field web" id="url" name="url" type="text" placeholder="Website" value="' . esc_attr( $commenter['comment_author_url'] ) .
		  '" />
				<!--<i class="fa fa-retweet"></i>-->
				</div>
				
	</div>'
    )
  ),
   
  'comment_field' =>  '
  <div class="col-md-6 nopadding" id="reply-comment-area">
 
		<label for="comment">' . _e( '', 'myway' ) .'</label>
		<textarea id="comment" name="comment" aria-required="true" placeholder="Reply">' .'</textarea>
		<!--<i class="fa fa-pencil-square-o"></i>-->
  </div>
  
</div>',
  'must_log_in' => '<p class="must-log-in">' .
    sprintf(
      __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
      wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
    ) . '</p>',
		
	 'comment_notes_after' => '',
	 'comment_notes_before' => '',
	
  'logged_in_as' => '<p class="logged-in-as">' .
    sprintf(
    '<div class="col-md-4 nopadding"><div class="reply_left"><div class="reply_login">'.__( 'Logged in as %2$s','myway').'<a href="%3$s" title="Log out of this account">'.__('Log out?','myway').'</a></div></div></div>',
      admin_url( 'profile.php' ),
      $user_identity,
      wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
    ) . '</p>',
);

?>
	<?php comment_form($args); ?>
	</div>
</div>
 <?php if ( !is_user_logged_in() ) { echo "</div>"; } ?> 
