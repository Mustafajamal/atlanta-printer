<?php
function herowp_output_base_color_css_by_cookies(){
if (isset ($_COOKIE['colorpickercolor'])) {
$herowp_data['color_theme'] = $_COOKIE['colorpickercolor'];
?>
<style type="text/css" media="all">
.header-phone span, .rev-sp1, .rev-sp2, .shout-box-title span, span.title-sp, .home-post-info, .home-post-info a, .team-social i:hover, .widget_tw > div.twit-icon > i, .footer-newsletter span, .well-boxes h2, .well-feat h2, .well h2, .steps-left span, a.featured-button:hover, .area-3-info-box i, .sidebar-posts h2 a:hover, a.cat-link, .widget-box a:hover, .services-thumb i, #post-area-3 a:hover, .projects-title h2 span, .dotcolor,.comment-date i, #post-area-2 a:hover, .icon_style_1,.info-header-data i, .description_and_list i, .tp-caption.very_big_white_6 i, .view-overlay .port-zoom-link .fa.fa-link,.sidebar-posts i,.services_home3 .servicesnumber,.testimonials .carousel-control,.blog_posts_block1 p.author_and_category a,.services_home5 .icon_service,.pricing_tables_holder h2 a:hover,.showcase-info h2,.offer-info h2 span,.offer-price span.new,.blog_posts_block1 p.author_and_category,.quote_block h1 i,.icon-facts i,.services_home5 a.read-more,.services_home09 .box-services:hover .servicesnumber,.services_home09 .box-services:hover .servicesnumber-image,.tp-caption.white_big_2_uppercase_def_color,.blog_posts_block2 .category i,.services_home4 a.read-more,.tp-caption.default_color_80px,.tp-caption.default_color_80px,.accordion-toggle:hover i,.accordion-toggle:hover span,a.default_button_outline:hover,.services_home08 .box-services:hover a.read-more,.portfolio_block3 .maginfy i,.portfolio_block3 p.category i,.blog_posts_block3 .category i,.blog_posts_block3 h2 a:hover,span.defaultcolor,.offer-info h2 span,.offer-price span.new{
	color: <?php echo $herowp_data['color_theme'];?> !important;
}
		
	.tp-caption.big_white ul li.nr-1 span, span.zoom-bg, .subscribe-submit, #footer-body > div.footer-social-body > div > a > i.fa.fa-rss:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-facebook:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-twitter:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-dribbble:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-youtube:hover, #footer-body > div.footer-social-body > div > a > i.fa.fa-tumblr:hover, .testimonial_quote, .about-title-line, .fa.fa-angle-left, .fa.fa-angle-right, .port-detail-box img, .projectdetails .flex-next, .projectdetails .flex-prev, .box-title-line, .ui-tabs-active.ui-state-active, .accordion-group.active .icon, .accordion-heading .accordion-toggle:hover .icon, #time, .search-submit, .tagcloud a:hover, .contact-facebook i, .contact-twitter i, .contact-google i, .author-fa, .port-img-overlay, .title-underline,.blog-comments p.form-submit input[name="submit"], .post-tags a:hover, .myway, .myway_small, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li.current-menu-ancestor > .item_link, #mega_main_menu.mega_main_sidebar_menu > .menu_holder > .menu_inner > ul > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.default_dropdown .mega_dropdown > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.default_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.default_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.multicolumn_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.multicolumn_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li:hover > .processed_image, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li:hover > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li > .item_link:hover, #mega_main_menu.mega_main_sidebar_menu ul li.grid_dropdown .mega_dropdown > li.current-menu-item > .item_link, #mega_main_menu.mega_main_sidebar_menu ul li.post_type_dropdown .mega_dropdown > li > .processed_image:hover, .main-content .left-container .contact-form .submit, .main-content .left-container .contact-form .submit:hover, a.button, a.button-boxes, a.button-boxes:hover, a.button:hover, .myway, .myway_small, .myway_small_left, .services_home4 .servicesnumber, .separator-line .round-icon-heading, .slider_base_bg_color, .portfolio_block_full_screen .mix:hover,  .services_home1 .box-services:hover, .blog_posts_block1 .round_date,.blog_posts_block1 .blog-posts:hover span.url_post,.pricing_tables_holder h2 a,.pricing_tables_holder .price_circle,.showcase-info h3,.services_home10 .servicesnumber,.portfolio_block1 .separator,.services_home .servicesnumber,.team-boxes .separator,.footer-box-content p span,form#searchform input[type="submit"],#main-contact .wpcf7 input[type="submit"],.overlay-image2,.portfolio_block_full_screen h2,.services_home5 .servicesnumber,.tp-caption.white_slim_1,.tp-caption.white_big_slim_2,.services_home09 .box-services:hover .counter_nr,.blog_posts_block2 .date_holder,a.default_button,.services_home08 a.read-more,a.default_button2,.portfolio_block3 .read-more,.blog_posts_block3 .comments,.blog_posts_block1,.tp-caption.white_big_3_bg  #nothing_found i,.offer-info-bottom p,.tp-caption.white_halfslim_32px_bg,.white_120px_base_bg{
	background: <?php echo $herowp_data['color_theme'];?> !important;
}

 #mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.submenu_full_width.drop_to_center > .mega_dropdown, 
#mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.drop_to_right > .mega_dropdown, .mega_dropdown{
border-top:solid 2px #ff6131 <?php echo $herowp_data['color_theme'];?> !important;
}	

.well:hover,.steps-box:hover,.box-button:hover {
	border-bottom: 2px solid <?php echo $herowp_data['color_theme'];?> !important;
}
	
.main-content .left-container .contact-form .submit {
	border-bottom: 4px solid #dbdbdb;
}
	
.port-title{
	border-bottom: 1px solid <?php echo $herowp_data['color_theme'];?>;
}

#mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.submenu_full_width.drop_to_center > .mega_dropdown, #mega_main_menu > .menu_holder > .menu_inner > ul > li.multicolumn_dropdown.drop_to_right > .mega_dropdown, .mega_dropdown{
	border-top:solid 2px <?php echo $herowp_data['color_theme'];?> !important;
}
	
.contact-form h2 {
	border-bottom: 8px solid <?php echo $herowp_data['color_theme'];?> !important;
}

.tp-caption.very_big_white:after, .static_content .view-all-button.bubble-bottom:before{
border-color: <?php echo $herowp_data['color_theme'];?> transparent transparent transparent !important;
}

.separator-line .round-icon-heading {
box-shadow: 0px 0px 0px 2px <?php echo $herowp_data['color_theme'];?>;
}
.team-wrap .view-all-button.bubble-bottom:before,.portfolio_block_full_screen .view-all-button.bubble-bottom:before, .portfolio_block1 .view-all-button.bubble-bottom:before,.blog_posts_block1 .view-all-button.bubble-bottom:before,.offer-wrap .view-all-button.bubble-bottom:before{
border-color: <?php echo $herowp_data['color_theme'];?> transparent transparent transparent;
}
.projects-filter-buttons .view-all-button.bubble-bottom:before {
border-color: <?php echo $herowp_data['color_theme'];?> transparent transparent transparent;
}
form#searchform input[type="submit"],.offer-price a:hover{
border: solid 1px  <?php echo $herowp_data['color_theme'];?>;
}
.offer-price a:hover {
border: solid 1px  <?php echo $herowp_data['color_theme'];?>;
color:<?php echo $herowp_data['color_theme'];?>;
}
form#searchform input[type="submit"]:hover {
background: #FFF !important;
border: solid 1px  <?php echo $herowp_data['color_theme'];?>;
}
.blog-comments p.form-submit input[name="submit"],
#main-contact .wpcf7 input[type="submit"],
.services_home09 .box-services:hover .servicesnumber,
.services_home09 .box-services:hover .servicesnumber-image,
a.default_button,
.accordion-toggle:hover,
a.default_button_outline:hover,
.services_home08 a.read-more {
border: solid 1px  <?php echo $herowp_data['color_theme'];?> !important;
}
.services_home10 .servicesnumber {
  box-shadow: 0 0 0 4px <?php echo $herowp_data['color_theme'];?>;
  border: solid 4px <?php echo $herowp_data['color_theme'];?>;
}
.accordion-toggle:hover i{
  border-right: solid 1px <?php echo $herowp_data['color_theme'];?> !important;
}
#blog-grid-1-type-2 .sticky section{
border-bottom:solid 3px <?php echo $herowp_data['color_theme'];?> !important;
}
a.default_button_outline2:hover{
color: <?php echo $herowp_data['color_theme'];?> !important;
border: solid 1px <?php echo $herowp_data['color_theme'];?> !important;
}
a.default_button2{
background: <?php echo $herowp_data['color_theme'];?> !important;
border:solid 1px <?php echo $herowp_data['color_theme'];?> !important;
}
.services_home10 .box-services:hover .servicesnumber{
border:solid 4px #FFF !important;
}
.blog-comments p.form-submit input[name="submit"]:hover,
#main-contact .wpcf7 input[type="submit"]:hover {
background: #FFF !important;
border:solid 1px #DDD !important;
}
.portfolio_block3 .read-more:hover,
.portfolio_block3 .maginfy:hover{
background:#333 !important;
}

.portfolio_block3 .read-more:hover i,
.portfolio_block3 .maginfy:hover i{
color:#fff !important;
}
.services_home1 .box-services:hover{
-webkit-background-clip: padding-box !important; /* for Safari */
background-clip: padding-box !important; /* for IE9+, Firefox 4+, Opera, Chrome */
}
.title-heading h1 span, .myway_thin,.blog_posts_block1 .round_date,.blog_posts_block1 .round_comments {
-webkit-background-clip: padding-box !important;
background-clip: padding-box !important;
}
.view-all-button span.icon i{
color:#FFF;
}
.services_home08 a.read-more:hover,.services_home08 .box-services:hover a.read-more{
	background:none !important;
}
.pricing_tables_holder h2 a:hover{
background:#FFF !important;
}
a.default_button:hover, a.default_button2:hover {
  color: #333 !important;
  background: #fff !important;
  border: solid 1px #fff !important;
}
.services_home5 .servicesnumber {
  border-bottom: solid 5px <?php echo hex2rgba($herowp_data['color_theme'] ,$alpha = .35); ?> !important;
    background-clip: padding-box !important;
  -moz-background-clip: padding-box !important;
}
</style>
<?php }
}
 ?>
