
<?php 
global $herowp_data; { ?>

<div class="options-panel options-shown">
    <div class="options-toggle">
        <i class="fa fa-cog"></i>
    </div>
    <form method="post" class="layout-style" action="#">
        <h4>Layout Style</h4>
        <select name="layout-style" id="layout-style">
            <option value="wide">Wide</option>
            <option value="boxed">Boxed</option>
        </select>
  
	<h4>Theme Color</h4>
	<p class="desc-note-panel"><strong>Note:</strong> Please be aware that some colors may not be changing, because they are overridden by the page builder inline styles. However they can be changed with ease in your theme.</p>
	<div id="picker"></div>
	<input type="hidden" id="colorpickercolor" name="colorpickercolor" value="color-picker2">
	
<input type="hidden" name="option-hidden" value="xfgoption">
<button type="submit" class="option-button">Submit changes</button>


	  </form>
	
	
</div>

<script type="text/javascript">
function setCookie(c_name,value,exdays)
{
		var exdate=new Date();
		
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value+";path=/";
		
}
jQuery('#picker').colpick({
	flat: true,
	layout:'hex',
	submit:0,
	colorScheme:'dark',
	onChange:function(hsb,hex,rgb,el,bySetColor) {
		jQuery("#colorpickercolor").val('#'+hex);
		setCookie('colorpickercolor','#'+hex,7);
		// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
		if(!bySetColor) jQuery("#colorpickercolor").val(hex);
	}
}).keyup(function(){
	jQuery(this).colpickSetColor(this.value);
});

    /* Javascript for presentation panel */
    /* You may delete this */

    jQuery(document).ready(function() {
        var isHidden = false;

        jQuery('#layout-style option:first-child').attr('selected', true);
        jQuery('#header-style option:first-child').attr('selected', true);
        jQuery('#header-type option:first-child').attr('selected', true);

        jQuery('.options-toggle').on('click', function() {

            if (isHidden) {
                jQuery('.options-panel').removeClass('options-hidden');
                jQuery('.options-panel').addClass('options-shown');

                isHidden = false;
            }
            else {
                jQuery('.options-panel').removeClass('options-shown');
                jQuery('.options-panel').addClass('options-hidden');

                isHidden = true;
            }
        });

        var colorArray = [];

        jQuery('#header-style').change(function() {
            var value = jQuery(this).val();
            var header = jQuery('header');

            if (value == 'corporate') {


                header.addClass('corporate-header');
                jQuery('#header-navigation ul').children('li').each(function() {
                    colorArray.push(jQuery(this).css('backgroundColor'));
                    jQuery(this).css('backgroundColor', '#ffffff');
                });
            }
            else {
                header.removeClass('corporate-header');

                jQuery('#header-navigation ul').children('li').each(function(index, value) {
                    jQuery(this).css('backgroundColor', colorArray[index]);
                });
            }
        });

        jQuery('#header-type').change(function() {
            var value = jQuery(this).val();
            var header = jQuery('#header-container');

            if (value == 'normal') {
                header.removeClass('navbar-fixed-top');
                header.addClass('navbar-normal-top');
            }
            else {
                header.removeClass('navbar-normal-top');
                header.addClass('navbar-fixed-top');
            }
        });

        jQuery('#layout-style').change(function() {
            var value = jQuery(this).val();
            var layout = jQuery('#main');

            if (value == 'boxed') {
                layout.removeClass('wide');
                layout.addClass('boxed');
            }
            else {
                layout.removeClass('boxed');
                layout.addClass('wide');
            }
        });

        jQuery('.pattern-sample').on('click', function() {
            var patternFile = jQuery(this).attr('data-sample');

            jQuery('#body').css({
                backgroundImage: 'url(' + patternFile + ')'
            });
        });
    });
</script>

<?php } ?>