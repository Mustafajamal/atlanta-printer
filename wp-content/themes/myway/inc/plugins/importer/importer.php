<?php
defined( 'ABSPATH' ) or die( 'You cannot access this script directly' );
global $wpdb;

// Hook importer into admin init
add_action( 'admin_init', 'herowp_importer' );
function herowp_importer() {
    global $wpdb;

    if ( current_user_can( 'manage_options' )) {
		
		$numbers = array("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42");
			
			foreach ($numbers as $number){

				// We import only specific homepage
				if( isset( $_GET['import_homepage_'.$number.''] ) ){
					herowp_importer_wp();
					herowp_importer_revolution_slider();
					herowp_importer_widgets();
					herowp_importer_menus();
					
					// Set reading options
					$homepage = get_page_by_title( 'Homepage '.$number.'' );
					$posts_page = get_page_by_title( 'Blog' );
					if($homepage->ID && $posts_page->ID) {
						update_option('show_on_front', 'page');
						update_option('page_on_front', $homepage->ID); // Front Page
						update_option('page_for_posts', $posts_page->ID); // Blog Page
					}
					wp_redirect( admin_url( 'themes.php?page=optionsframework&import_status=success#of-option-general' ) );
				}
				
		}

		// We import only features pages
		if( isset( $_GET['import_features'] ) )
		{
			//we import all dummy data including posts, sliders etc...
			herowp_importer_wp();
			herowp_importer_menus();
			wp_redirect( admin_url( 'themes.php?page=optionsframework&import_status=success#of-option-general' ) );
		}		
		
		
		// We import all dummy data
		if( isset( $_GET['import_all_dummy_data'] ) )
		{
			//we import all dummy data including posts, sliders etc...
			herowp_importer_wp();
			herowp_importer_revolution_slider();
			herowp_importer_menus();
			herowp_importer_widgets();
			
			// Set reading options
            $homepage = get_page_by_title( 'Homepage' );
            $posts_page = get_page_by_title( 'Blog' );
            if($homepage->ID && $posts_page->ID) {
                update_option('show_on_front', 'page');
                update_option('page_on_front', $homepage->ID); // Front Page
                update_option('page_for_posts', $posts_page->ID); // Blog Page
            }
			
			wp_redirect( admin_url( 'themes.php?page=optionsframework&import_status=success#of-option-general' ) );
		}
		
		
		
	}
} 
	

function herowp_importer_wp(){
	global $wpdb;
		
				if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true); // we are loading importers

				if ( ! class_exists( 'WP_Importer' ) ) { // if main importer class doesn't exist
					$wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
					include $wp_importer;
				}

				if ( ! class_exists('WP_Import') ) { // if WP importer doesn't exist
					$wp_import = get_template_directory() . '/inc/plugins/importer/wordpress-importer.php';
					include $wp_import;
				}

				if ( class_exists( 'WP_Importer' ) && class_exists( 'WP_Import' ) ) { // check for main import class and wp import class
		
				$numbers = array("01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42");
				
				foreach ($numbers as $number){
				
					if( isset( $_GET['import_all_dummy_data'] ) ){
						$xml_file = 'myway.xml.gz';
					}
					
					elseif( isset( $_GET['import_homepage_'.$number.''] ) ){
						$xml_file = 'homepage'.$number.'.gz';
					}					
					
					elseif( isset( $_GET['import_features'] ) ){
						$xml_file = 'features.gz';
					}
				
				}
				
						$importer = new WP_Import();

						$theme_xml = get_template_directory() . '/inc/dummy-data/xml/'.$xml_file.'';
						$theme_xml_clean = trim(preg_replace('/\s+/', ' ', $theme_xml)); //importer is buggy if has the XML file contain strings with new lines
						$importer->fetch_attachments = false;
						ob_start();
						$importer->import($theme_xml_clean);
						ob_end_clean();
				}
	
					
}

function herowp_importer_custom_post_types(){
	global $wpdb;
	
	$data = stripslashes_deep(trim('{"portfolio":{"name":"portfolio","label":"Portfolio","singular_label":"Portfolio","description":"Add your portfolio","public":"true","show_ui":"true","has_archive":"false","has_archive_string":"","exclude_from_search":"true","capability_type":"post","hierarchical":"false","rewrite":"true","rewrite_slug":"","rewrite_withfront":"true","query_var":"true","menu_position":"","show_in_menu":"true","show_in_menu_string":"","menu_icon":null,"supports":["title","editor","thumbnail","post-formats"],"taxonomies":[],"labels":{"menu_name":"","all_items":"","add_new":"","add_new_item":"","edit":"","edit_item":"","new_item":"","view":"","view_item":"","search_items":"","not_found":"","not_found_in_trash":"","parent":""}}}'));
	$decode = json_decode( $data, true );
	update_option( 'cptui_post_types', $decode );
		
	$data_tax = stripslashes_deep(trim('{"types":{"name":"types","label":"Types","singular_label":"Type","hierarchical":"false","show_ui":"true","query_var":"true","query_var_slug":"","rewrite":"true","rewrite_slug":"","rewrite_withfront":"1","rewrite_hierarchical":"0","show_admin_column":"false","labels":{"menu_name":"","all_items":"","edit_item":"","view_item":"","update_item":"","add_new_item":"","new_item_name":"","parent_item":"","parent_item_colon":"","search_items":"","popular_items":"","separate_items_with_commas":"","add_or_remove_items":"","choose_from_most_used":"","not_found":""},"object_types":["portfolio"]}}'));
	$decode_tax = json_decode( $data_tax, true );
	update_option( 'cptui_taxonomies', $decode_tax );
}

function herowp_importer_main_mega_menu(){
	global $wpdb;
			if (!defined('MMPM_OPTIONS_DB_NAME')) { define( 'MMPM_OPTIONS_DB_NAME', ''); }
				$dummy_data='{"last_modified":"1412246672","mega_menu_locations":["is_checkbox","mega_main_sidebar_menu"],"mega_main_sidebar_menu_included_components":["is_checkbox","company_logo","woo_cart"],"mega_main_sidebar_menu_first_level_item_height":"50","mega_main_sidebar_menu_primary_style":"flat","mega_main_sidebar_menu_first_level_button_height":"20","mega_main_sidebar_menu_first_level_item_align":"right","mega_main_sidebar_menu_first_level_icons_position":"left","mega_main_sidebar_menu_first_level_separator":"none","mega_main_sidebar_menu_corners_rounding":"0","mega_main_sidebar_menu_dropdowns_animation":"anim_3","mega_main_sidebar_menu_mobile_minimized":["is_checkbox","true"],"mega_main_sidebar_menu_mobile_label":"Menu Responsive","mega_main_sidebar_menu_direction":"horizontal","mega_main_sidebar_menu_first_level_item_height_sticky":"48","mega_main_sidebar_menu_sticky_status":["is_checkbox","true"],"mega_main_sidebar_menu_sticky_offset":"100","logo_src":"http:\/\/herowp.com\/demos\/myway\/wp-content\/uploads\/2014\/08\/logo.png","logo_height":"99","mega_main_sidebar_menu_menu_bg_gradient":{"color1":"rgba(255,255,255,1)","start":"0","color2":"rgba(255,255,255,1)","end":"100","orientation":"top"},"mega_main_sidebar_menu_menu_bg_image":{"background_image":"","background_repeat":"repeat","background_attachment":"scroll","background_position":"center","background_size":"auto"},"mega_main_sidebar_menu_menu_first_level_link_font":{"font_family":"Inherit","font_size":"14","font_weight":"400"},"mega_main_sidebar_menu_menu_first_level_link_color":"rgba(79,79,79,1)","mega_main_sidebar_menu_menu_first_level_icon_font":{"font_size":"15"},"mega_main_sidebar_menu_menu_first_level_link_bg":{"color1":"rgba(255,255,255,0)","start":"0","color2":"rgba(255,255,255,0)","end":"100","orientation":"top"},"mega_main_sidebar_menu_menu_first_level_link_color_hover":"#f8f8f8","mega_main_sidebar_menu_menu_first_level_link_bg_hover":{"color1":"#ff6131","start":"0","color2":"#ff6131","end":"100","orientation":"top"},"mega_main_sidebar_menu_menu_search_bg":"#ff6131","mega_main_sidebar_menu_menu_search_color":"#f8f8f8","mega_main_sidebar_menu_menu_dropdown_wrapper_gradient":{"color1":"#ffffff","start":"0","color2":"#ffffff","end":"100","orientation":"top"},"mega_main_sidebar_menu_menu_dropdown_link_font":{"font_family":"Inherit","font_size":"14","font_weight":"400"},"mega_main_sidebar_menu_menu_dropdown_link_color":"rgba(138,138,138,1)","mega_main_sidebar_menu_menu_dropdown_icon_font":{"font_size":"12"},"mega_main_sidebar_menu_menu_dropdown_link_bg":{"color1":"rgba(255,255,255,0)","start":"0","color2":"rgba(255,255,255,0)","end":"100","orientation":"top"},"mega_main_sidebar_menu_menu_dropdown_link_border_color":"#f0f0f0","mega_main_sidebar_menu_menu_dropdown_link_color_hover":"#f8f8f8","mega_main_sidebar_menu_menu_dropdown_link_bg_hover":{"color1":"#ff6131","start":"0","color2":"#ff6131","end":"100","orientation":"top"},"mega_main_sidebar_menu_menu_dropdown_plain_text_color":"#333333","set_of_google_fonts":["0"],"additional_styles_presets":["0"],"set_of_custom_icons":["1",{"custom_icon":"http:\/\/herowp.com\/demos\/myway\/wp-content\/plugins\/mega_main_menu\/src\/img\/megamain-logo-120x120.png"}],"custom_css":"","responsive_styles":["is_checkbox","true"],"coercive_styles":["is_checkbox"],"indefinite_location_mode":["is_checkbox"],"number_of_widgets":"1","language_direction":"ltr","item_descr":["is_checkbox"],"item_style":["is_checkbox"],"item_visibility":["is_checkbox"],"item_icon":["is_checkbox"],"disable_text":["is_checkbox"],"disable_link":["is_checkbox"],"submenu_type":["is_checkbox"],"submenu_drops_side":["is_checkbox"],"submenu_columns":["is_checkbox"],"submenu_enable_full_width":["is_checkbox"],"submenu_bg_image":["is_checkbox"]}';
				$options_backup = json_decode( $dummy_data, true );
				update_option( MMPM_OPTIONS_DB_NAME, $options_backup );
				
}	


function herowp_importer_theme_options(){
	global $wpdb;
			$theme_options = unserialize(base64_decode('YTo0OTp7aTowO2I6MDtzOjE1OiJoZWFkZXJfYmdfaW1hZ2UiO3M6MDoiIjtzOjIxOiJpbXBvcnRfYWxsX2R1bW15X2RhdGEiO3M6Nzk6IltzaXRlX3VybF0vd3AtYWRtaW4vdGhlbWVzLnBocD9wYWdlPW9wdGlvbnNmcmFtZXdvcmsmaW1wb3J0X2FsbF9kdW1teV9kYXRhPXRydWUiO3M6OToic21vZl9pbml0IjtzOjMxOiJXZWQsIDIyIEFwciAyMDE1IDE0OjA4OjA0ICswMDAwIjtzOjEzOiJzbW9vdGhfc2Nyb2xsIjtzOjc6ImRpc2FibGUiO3M6NzoiYW5pbWF0ZSI7czo2OiJlbmFibGUiO3M6MTc6ImhlYWRpbmdfZm9udF9mYWNlIjtzOjE5OiJTZWxlY3QgRGVzaXJlZCBGb250IjtzOjE5OiJoZWFkaW5nX2ZvbnRfd2VpZ2h0IjtzOjM1OiJTZWxlY3QgRm9udCBXZWlnaHQgRm9yIEhlYWRpbmcgRm9udCI7czoxNDoiYm9keV9mb250X2ZhY2UiO3M6MTk6IlNlbGVjdCBEZXNpcmVkIEZvbnQiO3M6MTY6ImJvZHlfZm9udF93ZWlnaHQiO3M6MzU6IlNlbGVjdCBGb250IFdlaWdodCBGb3IgSGVhZGluZyBGb250IjtzOjEyOiJwb3J0Zm9saW9fbm8iO3M6MToiOCI7czoyNjoicG9ydGZvbGlvX3Bvc3RzX2FuaW1hdGlvbnMiO3M6ODoiZmFkZUluVXAiO3M6MzI6InBvcnRmb2xpb19wb3N0c19hbmltYXRpb25zX2RlbGF5IjtzOjM6IjEwMCI7czoyMToiYmxvZ190aXRsZV9icmVhZGNydW1iIjtzOjg6Ik91ciBibG9nIjtzOjIxOiJibG9nX3Bvc3RzX2FuaW1hdGlvbnMiO3M6Njoiem9vbUluIjtzOjI3OiJibG9nX3Bvc3RzX2FuaW1hdGlvbnNfZGVsYXkiO3M6MzoiMTAwIjtzOjg6ImZhY2Vib29rIjtzOjE6IiMiO3M6NzoidHdpdHRlciI7czoxOiIjIjtzOjY6Imdvb2dsZSI7czoxOiIjIjtzOjg6ImxpbmtlZGluIjtzOjE6IiMiO3M6MzoicnNzIjtzOjE6IiMiO3M6ODoiZHJpYmJibGUiO3M6MToiIyI7czo3OiJ5b3V0dWJlIjtzOjE6IiMiO3M6NjoidHVtYmxyIjtzOjE6IiMiO3M6OToicGludGVyZXN0IjtzOjE6IiMiO3M6NzoiZHJvcGJveCI7czoxOiIjIjtzOjExOiJnb29nbGVfbWFwcyI7czozMzQ6IjxpZnJhbWUgc3JjPSJodHRwczovL3d3dy5nb29nbGUuY29tL21hcHMvZW1iZWQ/cGI9ITFtMTghMW0xMiExbTMhMWQ3Nzk0NC40MTg3MTYwODE3NyEyZDQuODk4NjE2NjQ5OTk5OTk5ITNkNTIuMzc0NzE1NzUhMm0zITFmMCEyZjAhM2YwITNtMiExaTEwMjQhMmk3NjghNGYxMy4xITNtMyExbTIhMXMweDQ3YzYzZmI1OTQ5YTc3NTUlM0EweDY2MDBmZDRjYjdjMGFmOGQhMnNBbXN0ZXJkYW0lMkMrT2xhbmRhITVlMCEzbTIhMXNybyEyc3JvITR2MTQxMjMzNTU4NDMxNCIgd2lkdGg9IjEwMCUiIGhlaWdodD0iNDUwIiBmcmFtZWJvcmRlcj0iMCIgc3R5bGU9ImJvcmRlcjowIj48L2lmcmFtZT4iO3M6MTg6ImVuYWJsZV9pbmZvX3dpZGdldCI7czo2OiJlbmFibGUiO3M6MTU6ImluZm9fcGhvbmVfdGV4dCI7czoxODoiQ2FsbCB1cyAgdG9sbCBmcmVlIjtzOjE3OiJpbmZvX3Bob25lX251bWJlciI7czoxNjoiMDA0OS0xMjMtNDU2LTc4OSI7czoxNzoiaW5mb19hZGRyZXNzX3RleHQiO3M6MjA6IndlIGhhdmUgdGhpcyBhZGRyZXNzIjtzOjEyOiJpbmZvX2FkZHJlc3MiO3M6MTQ6IkFtc3RlcmRhbSAxMDI0IjtzOjE1OiJpbmZvX2VtYWlsX3RleHQiO3M6NToiRW1haWwiO3M6MTA6ImluZm9fZW1haWwiO3M6MTY6ImNvb2xAZXhhbXBsZS5jb20iO3M6MTE6ImZvb3Rlcl9sb2dvIjtzOjY1OiJodHRwOi8vaGVyb3dwLmNvbS9kZW1vcy9teXdheS93cC1jb250ZW50L3VwbG9hZHMvMjAxNC8wOC9sb2dvLnBuZyI7czoxMjoiZW5hYmxlX3Bob25lIjtzOjY6ImVuYWJsZSI7czoyMDoiZm9vdGVyX2tlZXBfaW5fdG91Y2giO3M6MjA6Ildhbm5hIHN0YWkgaW4gdG91Y2g/IjtzOjE5OiJmb290ZXJfcHJlZml4X3Bob25lIjtzOjQ6IjAwNDkiO3M6MTk6ImZvb3Rlcl9waG9uZV9udW1iZXIiO3M6MTI6Ii0xMjMtNDU2LTc4OSI7czoxODoiZm9vdGVyX2Fib3V0X3RpdGxlIjtzOjg6IkFib3V0IHVzIjtzOjIwOiJmb290ZXJfYWJvdXRfY29udGVudCI7czoxMzM6IkNoaWEgZGlyZWN0IHRyYWRlIFZIUyBjcmVkLiBSYXcgZGVuaW0gY29vbCBQQlImQiBHb2RhcmQsIG9yZ2FuaWMgWE9YTy4gPGJyLz4gPGJyLz4NCg0KRm9vZCB0cnVjayA8c3Bhbj5tZWdnaW5nczwvc3Bhbj4gYmVhcmQga2l0c2NoLiAiO3M6MTU6ImZvb3Rlcl9sb2NhdGlvbiI7czoyNDoiU2FsdCBMYWtlIENpdHksIFVUIDg3MjM0IjtzOjEyOiJmb290ZXJfZW1haWwiO3M6MTk6ImF3ZXNvbWVAZXhhbXBsZS5jb20iO3M6MTI6ImZvb3Rlcl9waG9uZSI7czoxNDoiKDg3MSkgMzIxLTY1NjciO3M6MTE6ImZvb3Rlcl90aW1lIjtzOjI3OiJNb25kYXktRnJpZGF5IDA4OjAwIC0gMTg6MDAiO3M6MjU6ImZvb3Rlcl93aWRnZXRzX2FuaW1hdGlvbnMiO3M6MTM6Ik5vIGFuaW1hdGlvbnMiO3M6MTY6ImZvb3Rlcl9jb3B5cmlnaHQiO3M6NzU6IkNvcHlyaWdodCAyMDE0IDxzdHJvbmc+TVlXQVk8L3N0cm9uZz4gV29yZHByZXNzIFRoZW1lLiBBbGwgcmlnaHRzIHJlc2VydmVkLiI7czoxODoibmF2X21lbnVfbG9jYXRpb25zIjthOjE6e3M6MjI6Im1lZ2FfbWFpbl9zaWRlYmFyX21lbnUiO3M6MjoiMTciO31zOjE3OiJlbmFibGVfYXV0aG9yX2JpbyI7czo2OiJlbmFibGUiO30=')); 
			of_save_options($theme_options);
}






function herowp_importer_revolution_slider(){
	global $wpdb;
	
			if (class_exists('RevSliderAdmin')) {
                $rev_directory = get_template_directory() . '/inc/dummy-data/revolution/'; // revslider data dir

				if (isset( $_GET['import_all_dummy_data'] ) ){
			    foreach( glob( $rev_directory . '*.zip' ) as $filename ) { // get all files from revsliders data dir
						$filename = basename($filename);
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
					}
                }	
				
				elseif( isset( $_GET['import_homepage_01'] ) ){
						$filename = 'slider2.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_02'] ) ){
						$filename = 'slider1.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_03'] ) ){
						$filename = 'slider3.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_04'] ) ){
						$filename = 'slider5.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_05'] ) ){
						$filename = 'slider4.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_06'] ) ){
						$filename = 'slider6.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_07'] ) ){
						$filename = 'slider7.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_08'] ) ){
						$filename = 'slider8.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_09'] ) ){
						$filename = 'slider9.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_10'] ) ){
						$filename = 'slider10.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_11'] ) ){
						$filename = 'slider11.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_12'] ) ){
						$filename = 'slider12.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_13'] ) ){
						$filename = 'slider13.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_14'] ) ){
						$filename = 'slider14.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_15'] ) ){
						$filename = 'slider16.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_16'] ) ){
						$filename = 'slider17.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_17'] ) ){
						$filename = 'slider18.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_18'] ) ){
						$filename = 'slider19.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_19'] ) ){
						$filename = 'slider20.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_20'] ) ){
						$filename = 'slider21.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_21'] ) ){
						$filename = 'slider22.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_22'] ) ){
						$filename = 'slider23.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_23'] ) ){
						$filename = 'slider24.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_24'] ) ){
						$filename = 'slider25.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_25'] ) ){
						$filename = 'slider26.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_26'] ) ){
						$filename = 'slider27.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_27'] ) ){
						$filename = 'slider28.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_28'] ) ){
						$filename = 'slider29.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_29'] ) ){
						$filename = 'slider30.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_30'] ) ){
						$filename = 'slider31.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_31'] ) ){
						$filename = 'slider32.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_32'] ) ){
						$filename = 'slider33.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_33'] ) ){
						$filename = 'slider34.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_34'] ) ){
						$filename = 'slider35.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_35'] ) ){
						$filename = 'slider36.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_36'] ) ){
						$filename = 'slider37.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_37'] ) ){
						$filename = 'slider38.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_38'] ) ){
						$filename = 'slider39.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_39'] ) ){
						$filename = 'slider40.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_40'] ) ){
						$filename = 'slider41.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_41'] ) ){
						$filename = 'slider42.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}
				
				elseif( isset( $_GET['import_homepage_42'] ) ){
						$filename = 'slider43.zip';
						$rev_files[] = get_template_directory() . '/inc/dummy-data/revolution/' . $filename ;
				}


                foreach( $rev_files as $rev_file ) { 

                        $filepath = $rev_file;

                        //check if zip file or fallback to old, if zip, check if all files exist
                        $zip = new ZipArchive;
                        $importZip = $zip->open($filepath, ZIPARCHIVE::CREATE);

                        if($importZip === true){ //true or integer. If integer, its not a correct zip file

                            //check if files all exist in zip
                            $slider_export = $zip->getStream('slider_export.txt');
                            $custom_animations = $zip->getStream('custom_animations.txt');
                            $dynamic_captions = $zip->getStream('dynamic-captions.css');
                            $static_captions = $zip->getStream('static-captions.css');

                            $content = '';
                            $animations = '';
                            $dynamic = '';
                            $static = '';

                            while (!feof($slider_export)) $content .= fread($slider_export, 1024);
                            if($custom_animations){ while (!feof($custom_animations)) $animations .= fread($custom_animations, 1024); }
                            if($dynamic_captions){ while (!feof($dynamic_captions)) $dynamic .= fread($dynamic_captions, 1024); }
                            if($static_captions){ while (!feof($static_captions)) $static .= fread($static_captions, 1024); }

                            fclose($slider_export);
                            if($custom_animations){ fclose($custom_animations); }
                            if($dynamic_captions){ fclose($dynamic_captions); }
                            if($static_captions){ fclose($static_captions); }

                            //check for images!

                        }else{ //check if fallback
                            //get content array
                            $content = @file_get_contents($filepath);
                        }

                        if($importZip === true){ //we have a zip
                            $db = new UniteDBRev();

                            //update/insert custom animations
                            $animations = @unserialize($animations);
                            if(!empty($animations)){
                                foreach($animations as $key => $animation){ //$animation['id'], $animation['handle'], $animation['params']
                                    $exist = $db->fetch(GlobalsRevSlider::$table_layer_anims, "handle = '".$animation['handle']."'");
                                    if(!empty($exist)){ //update the animation, get the ID
                                        if($updateAnim == "true"){ //overwrite animation if exists
                                            $arrUpdate = array();
                                            $arrUpdate['params'] = stripslashes(json_encode(str_replace("'", '"', $animation['params'])));
                                            $db->update(GlobalsRevSlider::$table_layer_anims, $arrUpdate, array('handle' => $animation['handle']));

                                            $id = $exist['0']['id'];
                                        }else{ //insert with new handle
                                            $arrInsert = array();
                                            $arrInsert["handle"] = 'copy_'.$animation['handle'];
                                            $arrInsert["params"] = stripslashes(json_encode(str_replace("'", '"', $animation['params'])));

                                            $id = $db->insert(GlobalsRevSlider::$table_layer_anims, $arrInsert);
                                        }
                                    }else{ //insert the animation, get the ID
                                        $arrInsert = array();
                                        $arrInsert["handle"] = $animation['handle'];
                                        $arrInsert["params"] = stripslashes(json_encode(str_replace("'", '"', $animation['params'])));

                                        $id = $db->insert(GlobalsRevSlider::$table_layer_anims, $arrInsert);
                                    }

                                    //and set the current customin-oldID and customout-oldID in slider params to new ID from $id
                                    $content = str_replace(array('customin-'.$animation['id'], 'customout-'.$animation['id']), array('customin-'.$id, 'customout-'.$id), $content);
                                }
                                
                            }

                            //overwrite/append static-captions.css
                            if(!empty($static)){
                                if($updateStatic == "true"){ //overwrite file
                                    RevOperations::updateStaticCss($static);
                                }else{ //append
                                    $static_cur = RevOperations::getStaticCss();
                                    $static = $static_cur."\n".$static;
                                    RevOperations::updateStaticCss($static);
                                }
                            }
                            //overwrite/create dynamic-captions.css
                            //parse css to classes
                            $dynamicCss = UniteCssParserRev::parseCssToArray($dynamic);

                            if(is_array($dynamicCss) && $dynamicCss !== false && count($dynamicCss) > 0){
                                foreach($dynamicCss as $class => $styles){
                                    //check if static style or dynamic style
                                    $class = trim($class);

                                    if((strpos($class, ':hover') === false && strpos($class, ':') !== false) || //before, after
                                        strpos($class," ") !== false || // .tp-caption.imageclass img or .tp-caption .imageclass or .tp-caption.imageclass .img
                                        strpos($class,".tp-caption") === false || // everything that is not tp-caption
                                        (strpos($class,".") === false || strpos($class,"#") !== false) || // no class -> #ID or img
                                        strpos($class,">") !== false){ //.tp-caption>.imageclass or .tp-caption.imageclass>img or .tp-caption.imageclass .img
                                        continue;
                                    }

                                    //is a dynamic style
                                    if(strpos($class, ':hover') !== false){
                                        $class = trim(str_replace(':hover', '', $class));
                                        $arrInsert = array();
                                        $arrInsert["hover"] = json_encode($styles);
                                        $arrInsert["settings"] = json_encode(array('hover' => 'true'));
                                    }else{
                                        $arrInsert = array();
                                        $arrInsert["params"] = json_encode($styles);
                                    }
                                    //check if class exists
                                    $result = $db->fetch(GlobalsRevSlider::$table_css, "handle = '".$class."'");

                                    if(!empty($result)){ //update
                                        $db->update(GlobalsRevSlider::$table_css, $arrInsert, array('handle' => $class));
                                    }else{ //insert
                                        $arrInsert["handle"] = $class;
                                        $db->insert(GlobalsRevSlider::$table_css, $arrInsert);
                                    }
                                }
                               
                            }
                        }

                        $content = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $content); //clear errors in string

                        $arrSlider = @unserialize($content);
                        $sliderParams = $arrSlider["params"];

                        if(isset($sliderParams["background_image"]))
                            $sliderParams["background_image"] = UniteFunctionsWPRev::getImageUrlFromPath($sliderParams["background_image"]);

                        $json_params = json_encode($sliderParams);

                        //new slider
                        $arrInsert = array();
                        $arrInsert["params"] = $json_params;
                        $arrInsert["title"] = UniteFunctionsRev::getVal($sliderParams, "title","Slider1");
                        $arrInsert["alias"] = UniteFunctionsRev::getVal($sliderParams, "alias","slider1");
                        $sliderID = $wpdb->insert(GlobalsRevSlider::$table_sliders,$arrInsert);
                        $sliderID = $wpdb->insert_id;

                        //-------- Slides Handle -----------

                        //create all slides
                        $arrSlides = $arrSlider["slides"];

                        $alreadyImported = array();

                        foreach($arrSlides as $slide){

                            $params = $slide["params"];
                            $layers = $slide["layers"];

                            //convert params images:
                            if(isset($params["image"])){
                                //import if exists in zip folder
                                if(trim($params["image"]) !== ''){
                                    if($importZip === true){ //we have a zip, check if exists
                                        $image = $zip->getStream('images/'.$params["image"]);
                                        if(!$image){
                                            echo $params["image"].' not found!<br>';
                                        }else{
                                            if(!isset($alreadyImported['zip://'.$filepath."#".'images/'.$params["image"]])){
                                                $importImage = UniteFunctionsWPRev::import_media('zip://'.$filepath."#".'images/'.$params["image"], $sliderParams["alias"].'/');

                                                if($importImage !== false){
                                                    $alreadyImported['zip://'.$filepath."#".'images/'.$params["image"]] = $importImage['path'];

                                                    $params["image"] = $importImage['path'];
                                                }
                                            }else{
                                                $params["image"] = $alreadyImported['zip://'.$filepath."#".'images/'.$params["image"]];
                                            }
                                        }
                                    }
                                }
                                $params["image"] = UniteFunctionsWPRev::getImageUrlFromPath($params["image"]);
                            }

                            //convert layers images:
                            foreach($layers as $key=>$layer){
                                if(isset($layer["image_url"])){
                                    //import if exists in zip folder
                                    if(trim($layer["image_url"]) !== ''){
                                        if($importZip === true){ //we have a zip, check if exists
                                            $image_url = $zip->getStream('images/'.$layer["image_url"]);
                                            if(!$image_url){
                                                echo $layer["image_url"].' not found!<br>';
                                            }else{
                                                if(!isset($alreadyImported['zip://'.$filepath."#".'images/'.$layer["image_url"]])){
                                                    $importImage = UniteFunctionsWPRev::import_media('zip://'.$filepath."#".'images/'.$layer["image_url"], $sliderParams["alias"].'/');

                                                    if($importImage !== false){
                                                        $alreadyImported['zip://'.$filepath."#".'images/'.$layer["image_url"]] = $importImage['path'];

                                                        $layer["image_url"] = $importImage['path'];
                                                    }
                                                }else{
                                                    $layer["image_url"] = $alreadyImported['zip://'.$filepath."#".'images/'.$layer["image_url"]];
                                                }
                                            }
                                        }
                                    }
                                    $layer["image_url"] = UniteFunctionsWPRev::getImageUrlFromPath($layer["image_url"]);
                                    $layers[$key] = $layer;
                                }
                            }

                            //create new slide
                            $arrCreate = array();
                            $arrCreate["slider_id"] = $sliderID;
                            $arrCreate["slide_order"] = $slide["slide_order"];
                            $arrCreate["layers"] = json_encode($layers);
                            $arrCreate["params"] = json_encode($params);

                            $wpdb->insert(GlobalsRevSlider::$table_slides,$arrCreate);
                        //}
                    }
                }
			}	
}
	

function herowp_importer_menus(){
global $wpdb;
           
		   $locations = get_theme_mod( 'nav_menu_locations' ); // registered menu locations in theme
            $menus = wp_get_nav_menus(); // registered menus

			if($menus)
			{
				foreach($menus as $menu) {
                    if( $menu->name == 'Header Menu' ) {
                        $locations['mega_main_sidebar_menu'] = $menu->term_id;
						}                  
				}
			}
            set_theme_mod( 'nav_menu_locations', $locations ); // set menus to locations
}

function herowp_importer_widgets(){
global $wpdb;
// Add data to widgets
            $widgets_json = get_template_directory_uri() . '/inc/dummy-data/widgets/widget_data.json'; // widgets data file
            $widgets_json = wp_remote_get( $widgets_json );
            $widget_data = $widgets_json['body'];
            $import_widgets = herowp_import_widget_dummy_data( $widget_data );
}

// Parsing Widgets
// Thanks to http://wordpress.org/plugins/widget-settings-importexport/
function herowp_import_widget_dummy_data( $widget_data ) {
    $json_data = $widget_data;
    $json_data = json_decode( $json_data, true );

    $sidebar_data = $json_data[0];
    $widget_data = $json_data[1];

    foreach ( $widget_data as $widget_data_title => $widget_data_value ) {
        $widgets[ $widget_data_title ] = '';
        foreach( $widget_data_value as $widget_data_key => $widget_data_array ) {
            if( is_int( $widget_data_key ) ) {
                $widgets[$widget_data_title][$widget_data_key] = 'on';
            }
        }
    }
    unset($widgets[""]);

    foreach ( $sidebar_data as $title => $sidebar ) {
        $count = count( $sidebar );
        for ( $i = 0; $i < $count; $i++ ) {
            $widget = array( );
            $widget['type'] = trim( substr( $sidebar[$i], 0, strrpos( $sidebar[$i], '-' ) ) );
            $widget['type-index'] = trim( substr( $sidebar[$i], strrpos( $sidebar[$i], '-' ) + 1 ) );
            if ( !isset( $widgets[$widget['type']][$widget['type-index']] ) ) {
                unset( $sidebar_data[$title][$i] );
            }
        }
        $sidebar_data[$title] = array_values( $sidebar_data[$title] );
    }

    foreach ( $widgets as $widget_title => $widget_value ) {
        foreach ( $widget_value as $widget_key => $widget_value ) {
            $widgets[$widget_title][$widget_key] = $widget_data[$widget_title][$widget_key];
        }
    }

    $sidebar_data = array( array_filter( $sidebar_data ), $widgets );

    herowp_parse_dummy_data( $sidebar_data );
}

function herowp_parse_dummy_data( $import_array ) {
    global $wp_registered_sidebars;
    $sidebars_data = $import_array[0];
    $widget_data = $import_array[1];
    $current_sidebars = get_option( 'sidebars_widgets' );
    $new_widgets = array( );

    foreach ( $sidebars_data as $import_sidebar => $import_widgets ) :

        foreach ( $import_widgets as $import_widget ) :
            //if the sidebar exists
            if ( isset( $wp_registered_sidebars[$import_sidebar] ) ) :
                $title = trim( substr( $import_widget, 0, strrpos( $import_widget, '-' ) ) );
                $index = trim( substr( $import_widget, strrpos( $import_widget, '-' ) + 1 ) );
                $current_widget_data = get_option( 'widget_' . $title );
                $new_widget_name = herowp_new_widget_name_get( $title, $index );
                $new_index = trim( substr( $new_widget_name, strrpos( $new_widget_name, '-' ) + 1 ) );

                if ( !empty( $new_widgets[ $title ] ) && is_array( $new_widgets[$title] ) ) {
                    while ( array_key_exists( $new_index, $new_widgets[$title] ) ) {
                        $new_index++;
                    }
                }
                $current_sidebars[$import_sidebar][] = $title . '-' . $new_index;
                if ( array_key_exists( $title, $new_widgets ) ) {
                    $new_widgets[$title][$new_index] = $widget_data[$title][$index];
                    $multiwidget = $new_widgets[$title]['_multiwidget'];
                    unset( $new_widgets[$title]['_multiwidget'] );
                    $new_widgets[$title]['_multiwidget'] = $multiwidget;
                } else {
                    $current_widget_data[$new_index] = $widget_data[$title][$index];
                    $current_multiwidget = $current_widget_data['_multiwidget'];
                    $new_multiwidget = isset($widget_data[$title]['_multiwidget']) ? $widget_data[$title]['_multiwidget'] : false;
                    $multiwidget = ($current_multiwidget != $new_multiwidget) ? $current_multiwidget : 1;
                    unset( $current_widget_data['_multiwidget'] );
                    $current_widget_data['_multiwidget'] = $multiwidget;
                    $new_widgets[$title] = $current_widget_data;
                }

            endif;
        endforeach;
    endforeach;

    if ( isset( $new_widgets ) && isset( $current_sidebars ) ) {
        update_option( 'sidebars_widgets', $current_sidebars );

        foreach ( $new_widgets as $title => $content )
            update_option( 'widget_' . $title, $content );

        return true;
    }

    return false;
}

function herowp_new_widget_name_get( $widget_name, $widget_index ) {
    $current_sidebars = get_option( 'sidebars_widgets' );
    $all_widget_array = array( );
    foreach ( $current_sidebars as $sidebar => $widgets ) {
        if ( !empty( $widgets ) && is_array( $widgets ) && $sidebar != 'wp_inactive_widgets' ) {
            foreach ( $widgets as $widget ) {
                $all_widget_array[] = $widget;
            }
        }
    }
    while ( in_array( $widget_name . '-' . $widget_index, $all_widget_array ) ) {
        $widget_index++;
    }
    $new_widget_name = $widget_name . '-' . $widget_index;
    return $new_widget_name;
}