<?php
/**
 * @package MegaMain
 * @subpackage MegaMain
 * @since mm 1.0
 */
	if( is_admin() ) {
		include_once( 'functions.php' );
		include_once( 'backup_settings.php' );
	}


	/* 
	 * Functions get array of the all setting from DB and create file to download.
	 */
	if ( isset( $_GET['mmpm_page'] ) && !empty( $_GET['mmpm_page'] ) ) {
		if ( $_GET['mmpm_page'] == 'phpinfo' ) {
			// Urge file to download instead of opening in the browser window.
			echo phpinfo();
			die();
		}
	}

?>