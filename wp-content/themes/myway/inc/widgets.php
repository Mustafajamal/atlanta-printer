<?php
/*
=================================================================================================================
This file holds all of the theme custom widgets
=================================================================================================================
*/


/*
=================================================================================================================
Twitter widget
=================================================================================================================
*/

add_action( 'widgets_init', 'herowp_load_twitter_Widget' );
function herowp_load_twitter_Widget() {
	register_widget( 'herowp_twitter_Widget' );
}
	
class herowp_twitter_Widget extends WP_Widget {

	function herowp_twitter_Widget() {
	global $herowp_theme_name;
	/* Widget settings. */
	$widget_ops = array( 'classname' => 'widget_tw', 'description' => '' );
	 
	/* Widget control settings. */
	$control_ops = array( 'id_base' => 'tw-widget' );
	 
	/* Create the widget. */
	$this->WP_Widget( 'tw-widget', ''.$herowp_theme_name.' Twitter Feed', $widget_ops, $control_ops );
	}
	 
	function widget( $args, $instance ) {
	extract( $args );
	require_once ('TwitterAPIExchange.php');
	
	if (
            ! empty( $instance['twitter'] )
            && ! empty( $instance['twitter_consumer_key'] )
            && ! empty( $instance['twitter_consumer_secret'] )
            && ! empty( $instance['twitter_oauth_access_token'] )
            && ! empty( $instance['twitter_oauth_access_token_secret'] )
        ) {
		$settings = array(
				'oauth_access_token' => $instance['twitter_oauth_access_token'],
				'oauth_access_token_secret' => $instance['twitter_oauth_access_token_secret'],
				'consumer_key' => $instance['twitter_consumer_key'],
				'consumer_secret' => $instance['twitter_consumer_secret']
			);

		/** URL for REST request, see: https://dev.twitter.com/docs/api/1.1/ **/

			$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
			$getfield = '?screen_name='.$instance['twitter'].'&count='.$instance['number_of_tweets'].'';
			//$getfield = '?screen_name='.$instance['twitter'].'&count=1';
			$requestMethod = 'GET';
			$twitter = new TwitterAPIExchange($settings);
			$vars = json_decode($twitter->setGetfield($getfield)
             ->buildOauth($url, $requestMethod)
             ->performRequest());
			$text_tw = '';
			if (is_array($vars))
			{
				foreach ($vars as $list) 
				{
					$date = (strtotime($list->created_at));
					$datee = date("F j, Y", $date); 
					$text_tw .= ' <li>'.$list->text.'<br><span>'.$datee.'</span><span class="icon"></span></li>';
				}
			}
		else {
		$counter = 0;
		}
	}
	echo $before_widget;
	echo '<div class="footer-box-title">
				<h3>'.$instance['title'].'<span class="dotcolor">.</span></h3>
				
				<div class="footer-box-line2"></div>
			</div>
			<div class="clear"></div>
			
	
		<ul class="tweets">
				'.$text_tw.'                   
		</ul>
	';
                                 	
	echo $after_widget;
	}
	 
	function update( $new_instance, $old_instance ) {
	$instance = $old_instance;
	 
	/* Strip tags (if needed) and update the widget settings. */
		$instance['title']                             = strip_tags(esc_attr( $new_instance['title']));
		$instance['twitter']                           = strip_tags(esc_attr($new_instance['twitter']));
		$instance['twitter_oauth_access_token']        = strip_tags(esc_attr($new_instance['twitter_oauth_access_token']));
		$instance['twitter_oauth_access_token_secret'] = strip_tags(esc_attr($new_instance['twitter_oauth_access_token_secret']));
		$instance['twitter_consumer_key']              = strip_tags(esc_attr($new_instance['twitter_consumer_key']));
		$instance['twitter_consumer_secret']           = strip_tags(esc_attr($new_instance['twitter_consumer_secret']));
		$instance['number_of_tweets']                  = strip_tags(absint($new_instance['number_of_tweets']));
	return $instance;
	}
	 
	function form( $instance ) {
		
		$title                              = isset( $instance['title']) ? strip_tags(esc_attr( $instance['title'] )) : 'Flickr feed';
		$twitter                            = isset( $instance['twitter']) ? strip_tags(esc_attr( $instance['twitter'] )) : 'hero_wp';
		$twitter_oauth_access_token         = isset( $instance['twitter_oauth_access_token']) ? strip_tags(esc_attr( $instance['twitter_oauth_access_token'] )) : '1967467555-OmR4wsSk8IZrBRaoi5EmOtZbb0NtFbpHUPnl7VG';
		$twitter_oauth_access_token_secret  = isset( $instance['twitter_oauth_access_token_secret']) ? strip_tags(esc_attr( $instance['twitter_oauth_access_token_secret'] )) : 'y2Gp4mKVly7Gs0YirV8ps5i7X9AU3ux1KAjoSJg0kFx03';
		$twitter_consumer_key               = isset( $instance['twitter_consumer_key']) ? strip_tags(esc_attr( $instance['twitter_consumer_key'] )) : 'YxJYC6pgBbUkH3HGmF9dg';
		$twitter_consumer_secret            = isset( $instance['twitter_consumer_secret']) ? strip_tags(esc_attr( $instance['twitter_consumer_secret'] )) : 'hGcmZL8SUcdZc16fKJ2KVe8UsHxsjImQ8xXVSLWc6R8';
		$number_of_tweets                   = isset( $instance['number_of_tweets'] ) ? strip_tags(absint( $instance['number_of_tweets'] )) : 5;

	?>
	<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Title', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($title); ?>" />
		</p>	
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>"><?php _e('Twitter Username:', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter' )); ?>" value="<?php echo esc_attr($twitter); ?>" />
		</p>		
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter_oauth_access_token' )); ?>"><?php _e('Twitter Oauth Access Token:', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'twitter_oauth_access_token' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter_oauth_access_token' )); ?>" value="<?php echo esc_attr($twitter_oauth_access_token); ?>" />
		</p>	
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter_oauth_access_token_secret' )); ?>"><?php _e('Twitter Oauth Access Token Secret:', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'twitter_oauth_access_token_secret' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter_oauth_access_token_secret' )); ?>" value="<?php echo esc_attr($twitter_oauth_access_token_secret); ?>" />
		</p>	
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter_consumer_key' )); ?>"><?php _e('Twitter Consumer Key:', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'twitter_consumer_key' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter_consumer_key' )); ?>" value="<?php echo esc_attr($twitter_consumer_key); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter_consumer_secret' )); ?>"><?php _e('Twitter Consumer Secret:', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'twitter_consumer_secret' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter_consumer_secret' )); ?>" value="<?php echo esc_attr($twitter_consumer_secret); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'number_of_tweets' )); ?>"><?php _e('Number of tweets:', 'myway'); ?><br/></label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'number_of_tweets' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'number_of_tweets' )); ?>" value="<?php echo esc_attr($number_of_tweets); ?>" />
		</p>
	<?php
	}

}

/*
=================================================================================================================
Flickr widget
=================================================================================================================
*/
add_action('widgets_init', create_function('', 'return register_widget("herowp_flickr_widget");'));
class herowp_flickr_widget extends WP_Widget {
    /** constructor */
    function herowp_flickr_widget() {
		global $herowp_theme_name;
		$widget_ops = array('classname' => 'herowp_flickr_widget', 'description' => 'Fliker' );
		$this->WP_Widget('herowp_flickr_widget',''.$herowp_theme_name.' Flickr Widget', $widget_ops);	
    }

    function widget($args, $instance) {	
	
		global $shortname;	
        extract( $args );
		$title     = strip_tags(esc_attr($instance['title']));
		$user_name = strip_tags(esc_attr($instance['user_name']));							
		$num_posts = strip_tags(absint($instance['num_posts']));		
		//$social_network = esc_attr($instance['social_network']);
		if(empty($num_posts)){$num_posts = 4;}
        ?>
            <?php echo $before_widget; ?>
                <?php if ( $title ) ; 
					
					$jsonn = file_get_contents('https://api.flickr.com/services/rest/?method=flickr.people.findByUsername&username='.$user_name.'&format=json&api_key=85145f20ba1864d8ff559a3971a0a033&nojsoncallback=1');
					preg_match_all('!\d+!', $jsonn, $matches);
				    $nsid = $matches['0']['0'].'@N'.$matches['0']['1'];
					
					$jsonns = file_get_contents('https://api.flickr.com/services/rest/?method=flickr.photos.search&user_id='.$nsid.'&format=json&api_key=85145f20ba1864d8ff559a3971a0a033&per_page='.$num_posts.'&page=1&extras=url_sq&nojsoncallback=1');
					$allphotos = json_decode($jsonns);
					echo '<div class="footer-box-title">
							<h3>'.esc_attr($instance['title']).'<span class="dotcolor">.</span></h3>
							<div class="footer-box-line2"></div>
						</div>
					<div class="clear"></div>
					<ul class="flickr">';
						
						foreach($allphotos->photos as $photos)
						{	
							if (is_array($photos)){
								foreach($photos as $phot)
								{	
									echo '<li><a class="photo" href="'.esc_url('http://www.flickr.com/photos/'.$nsid.'/').'" target="_blank"><img src="'.esc_url(str_replace("_s.jpg","_n.jpg",$phot->url_sq)).'" alt="" /></a></li>';	
								}
							}
						}
					
					echo '</ul>';
					?>

			<?php echo $after_widget; ?>
            <?php
    }
    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
		$instance = $old_instance;
		$instance['title'] 		    = strip_tags(esc_attr($new_instance['title']));
		$instance['user_name'] 	    = strip_tags( esc_attr($new_instance['user_name']));
		$instance['num_posts']      = absint( esc_attr($new_instance['num_posts']));
		//$instance['thumb_width']    = absint( esc_attr($new_instance['thumb_width']));
		//$instance['thumb_height']   = absint( esc_attr($new_instance['thumb_height']));
		//$instance['social_network']	= strip_tags( esc_attr($new_instance['title']));
        return $instance;
    }
    /** @see WP_Widget::form */
    function form($instance) {
		$title	        = isset( $instance['title']) ? strip_tags(esc_attr( $instance['title'] )) : 'Flickr feed';
		$user_name	    = isset( $instance['user_name']) ? strip_tags(esc_attr( $instance['user_name'] )) : 'johndoee2015';
		$num_posts      = isset( $instance['num_posts'] ) ? strip_tags(absint( $instance['num_posts'] )) : 1;
		//$social_network	= isset( $instance['social_network']) ? esc_attr( $instance['social_network'] ) : 'Flickr';
		//$thumb_width    = isset( $instance['thumb_width'] ) ? absint( $instance['thumb_width'] ) : '168';		
		//$thumb_height   = isset( $instance['thumb_height'] ) ? absint( $instance['thumb_height'] ) : '102';		
     ?>
         
		 <p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:','myway'); ?> <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php if(isset($title)){echo esc_attr($title);} else { echo 'Flickr feed';}  ?>" /></label></p>
         
		 <p><label for="<?php echo esc_attr($this->get_field_id('user_name')); ?>"><?php _e('Username:','myway'); ?> <input class="widefat" id="<?php echo esc_attr($this->get_field_id('user_name')); ?>" name="<?php echo esc_attr($this->get_field_name('user_name')); ?>" type="text" value="<?php if(isset($user_name)){echo esc_attr($user_name);} else { echo 'johndoee2015';} ?>" /></label></p>
         
		 <p><label for="<?php echo esc_attr($this->get_field_id('num_posts')); ?>"><?php _e('Number Of Images: eg:4','myway'); ?> <input class="widefat" id="<?php echo esc_attr($this->get_field_id('num_posts')); ?>" name="<?php echo esc_attr($this->get_field_name('num_posts')); ?>" type="text" value="<?php if(isset($num_posts)){echo esc_attr($num_posts);} ?>" /></label></p>		
		 

        <?php 
    }
}

/*
=================================================================================================================
Recent Posts Widget
=================================================================================================================
*/
add_action( 'widgets_init', 'herowp_recent_posts_widget' );
function herowp_recent_posts_widget() {
	register_widget('herowp_recent_posts_widget');
}

class herowp_recent_posts_widget extends WP_Widget {
	function herowp_recent_posts_widget() {
		global $herowp_theme_name;
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'herowp_recent_posts_widget', 'description' => 'Display Recent Posts in a nice style' );
		 
		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'herowp_recent_posts_widget' );
		 
		/* Create the widget. */
		$this->WP_Widget( 'herowp_recent_posts_widget', ''.$herowp_theme_name.' Recent Posts', $widget_ops, $control_ops );
	}
	 
	function widget( $args, $instance ) {
		extract( $args );
		$post_no = strip_tags(absint($instance['post_no'])) ;
		$title   = strip_tags(esc_attr($instance['title'])) ;
	?>
	
<?php $the_query = new WP_Query( 'showposts='.$post_no.' '); ?>
		<h3><?php echo $title; ?><span class="dotcolor">.</span></h3>
		
			<div class="footer-box-line2"></div>
		<div class="sidebar-posts-body">
			<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
				<div class="sidebar-posts-box">
					<div class="sidebar-post-thumb">
						<?php 
							if ( has_post_thumbnail() ) { 
							$id_img = get_post_thumbnail_id();
							$img_url = wp_get_attachment_url( $id_img,'full');
							$thumb = aq_resize($img_url, 100, 100, true); if ( !$thumb ) $thumb = $img_url;
						?>
						<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'myway' ), the_title_attribute('echo=0' ) ); ?>">
							<?php echo '<img src="'.esc_url($thumb).'" alt="'.esc_attr(get_the_title()).'" class="categ-thumb"/>';?>
						</a>	
						<?php }  
						else {
						echo '<img src="'.esc_url('http://dummyimage.com/100x100/e3e3e3/6b6b6b.png').'" width="100" height="100" alt="' . esc_attr( get_the_title() ) . '">';
							}
						?>					
					</div>
					<div class="sidebar-posts">
						<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?><span class="dotcolor">.</span></a></h2>
						<p><span class="comments_sidebar"><i class="fa fa-comment-o"></i><?php echo get_comments_number(); ?> <?php _e('Comments','myway'); ?></span>  <i class="fa fa-calendar-o"></i><?php echo the_time('d M Y'); ?></p>
					</div>
				</div>
			<?php endwhile;?>	
		</div>
		
<?php	
}
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		 
		/* Strip tags (if needed) and update the widget settings. */
		$instance['title']   =   strip_tags(esc_attr($new_instance['title']));
		$instance['post_no'] =   strip_tags(absint($new_instance['post_no']));
		return $instance;
	}
	 
	function form( $instance ) {
		
		/* Set up some default widget settings. */
		$title	  = isset($instance['title']) ? strip_tags(esc_attr($instance['title'])) : 'Latest news';
		$post_no  = isset($instance['post_no']) ? strip_tags(absint( $instance['post_no'])) : 2;
	

		?>
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Title','myway'); ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($title); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'post_no' )); ?>"><?php _e('Number of posts','myway'); ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'post_no' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'post_no' )); ?>" value="<?php echo esc_attr($post_no); ?>">
		</p>
		
	<?php
	}
}

/*
=================================================================================================================
Categories Widget
=================================================================================================================
*/
add_action( 'widgets_init', 'herowp_categories_widget' );
function herowp_categories_widget() {
	register_widget('herowp_categories_widget');
}

class herowp_categories_widget extends WP_Widget {
	function herowp_categories_widget() {
		global $herowp_theme_name;
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'herowp_categories_widget', 'description' => 'Display Categories with style' );
		 
		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'herowp_categories_widget' );
		 
		/* Create the widget. */
		$this->WP_Widget( 'herowp_categories_widget', ''.$herowp_theme_name.' Categories', $widget_ops, $control_ops );
	}
	 
	function widget( $args, $instance ) {
		extract( $args );
		$title    = strip_tags(esc_attr($instance['title'])) ;
		$cat_link = strip_tags(esc_attr($instance['cat_link'])) ;
	?>
	
<h3><?php echo $title; ?><span class="dotcolor">.</span></h3>

<div class="footer-box-line2"></div>
<?php
	$args = array(
	  'orderby' => 'name',
	  'order' => 'ASC'
	  );
	$categories = get_categories($args);
	echo'<div class="sidebar_cat_list">';
	  foreach($categories as $category) { 
		echo '<div class="sidebar_cat"><a href="' .esc_url(get_category_link( $category->term_id )) . '" class="cat-name" title="' . sprintf( __( "View all posts in %s",'myway' ), esc_attr($category->name)) . '" ' . '>' . esc_attr($category->name).'</a> <a href="' . esc_url(get_category_link( $category->term_id )). '" class="cat-link" title="">'.$cat_link.'</a></div>';
		} 
	echo '</div>';
?>			
<?php	
}
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		 
		/* Strip tags (if needed) and update the widget settings. */
		$instance['title']    =  strip_tags(esc_attr($new_instance['title']));
		$instance['cat_link'] =  strip_tags(esc_attr($new_instance['cat_link']));
		return $instance;
	}
	 
	function form( $instance ) {
		/* Set up some default widget settings. */
		$defaults = array( 'content' => '');
		$instance = wp_parse_args( $instance, $defaults );

		$title	  = isset($instance['title']) ? strip_tags(esc_attr($instance['title'])) : 'Categories';
		$cat_link = isset($instance['cat_link']) ? strip_tags(esc_attr($instance['cat_link'])) : 'Browse All';
		
		?>
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Widget Title','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($title); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'cat_link' )); ?>"><?php _e('Category link name for browsing','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'cat_link' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'cat_link' )); ?>" value="<?php echo esc_attr($cat_link); ?>">
		</p>
	
	<?php
	}
}


/*
=================================================================================================================
Contact widget
=================================================================================================================
*/
add_action( 'widgets_init', 'herowp_contact_widget' );
function herowp_contact_widget() {
	register_widget('herowp_contact_widget');
}

class herowp_contact_widget extends WP_Widget {
	function herowp_contact_widget() {
		global $herowp_theme_name;
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'herowp_contact_widget', 'description' => 'Contact form widget.' );
		 
		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'herowp_contact_widget' );
		 
		/* Create the widget. */
		$this->WP_Widget( 'herowp_contact_widget', ''.$herowp_theme_name.' Contact Widget', $widget_ops, $control_ops );
	}
	 
	function widget( $args, $instance ) {
		
		extract( $args );
		
		
		echo '<div class="widget-contact-form">';
		echo '<h3>'.esc_attr($instance['title']).'<span class="dotcolor">.</span></h3>
		<div class="footer-box-line2"></div>';
		echo '<div class="widget-contact-ct"><p>';
		if($instance['content_desc']) {
			echo ' '.esc_attr($instance['content_desc']).''; 
		} 
		echo '</p></div>';
		echo '<div class="widget-ct-social"><div class="wid-title-line"></div><div class="wid-title"><h3>'.esc_attr($instance['social_title']).'</h3></div><div class="clear"></div>';
		echo '<div class="widget-box"><div class="contact-facebook"><i class="fa fa-facebook"></i><a href="'.esc_url($instance['social_facebook']).'">'.__('Follow us on facebook','myway').'</a></div>';
		echo '<div class="contact-twitter"><i class="fa fa-twitter"></i><a href="'.esc_url($instance['social_twitter']).'">'.__('Follow us on twitter','myway').'</a></div>';
		echo '<div class="contact-google"><i class="fa fa-google-plus"></i><a href="'.esc_url($instance['social_google']).'">'.__('Get in touch with us by Google','myway').'</a></div></div>';
		echo '</div></div>';
	}
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		 
		/* Strip tags (if needed) and update the widget settings. */
		$instance['title']           =  strip_tags(esc_attr($new_instance['title']));
		$instance['content_desc']    =  strip_tags(esc_attr($new_instance['content_desc']));
		$instance['social_title']    =  strip_tags(esc_attr($new_instance['social_title']));
		$instance['social_facebook'] =  strip_tags(esc_attr($new_instance['social_facebook']));
		$instance['social_twitter']  =  strip_tags(esc_attr($new_instance['social_twitter']));
		$instance['social_google']   =  strip_tags(esc_attr($new_instance['social_google']));
		
		
		return $instance;
	}
	 
	function form( $instance ) {
		
		$defaults = array( 'content' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		
		/* Set up some default widget settings. */
		$title	         = isset( $instance['title']) ? strip_tags(esc_attr( $instance['title'] )) : 'Get in touch';
		$content_desc	 = isset( $instance['content_desc']) ? strip_tags(esc_attr( $instance['content_desc'] )) : 'Content here';
		$social_title	 = isset( $instance['social_title']) ? strip_tags(esc_attr( $instance['social_title'] )) : 'Be social';
		$social_facebook = isset( $instance['social_facebook']) ? strip_tags(esc_attr( $instance['social_facebook'] )) : '#';
		$social_twitter	 = isset( $instance['social_twitter']) ? strip_tags(esc_attr( $instance['social_twitter'] )) : '#';
		$social_google	 = isset( $instance['social_google']) ? strip_tags(esc_attr( $instance['social_google'] )) : '#';
		
		?>
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Title','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" value="<?php echo esc_attr($title); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'content_desc' )); ?>"><?php _e('Content','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'content_desc' )); ?>" value="<?php echo esc_attr($content_desc); ?>" name="<?php echo esc_attr($this->get_field_name( 'content_desc' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'social_title' )); ?>"><?php _e('Social Title','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'social_title' )); ?>" value="<?php echo esc_attr($social_title); ?>" name="<?php echo esc_attr($this->get_field_name( 'social_title' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'social_facebook' )); ?>"><?php _e('Facebook URL (do not forget http://)','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'social_facebook' )); ?>" value="<?php echo esc_url($social_facebook); ?>" name="<?php echo esc_attr($this->get_field_name( 'social_facebook' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'social_twitter' )); ?>"><?php _e('Twitter URL (do not forget http://)','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'social_twitter' )); ?>" value="<?php echo esc_url($social_twitter); ?>" name="<?php echo esc_attr($this->get_field_name( 'social_twitter' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'social_google' )); ?>"><?php _e('Google URL (do not forget http://)','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'social_google' )); ?>" value="<?php echo esc_url($social_google); ?>" name="<?php echo esc_attr($this->get_field_name( 'social_google' )); ?>">
		</p>
	<?php
	}
}



/*
=================================================================================================================
About us widget
=================================================================================================================
*/
add_action( 'widgets_init', 'herowp_about_us_widget' );
function herowp_about_us_widget() {
	register_widget('herowp_about_us_widget');
}

class herowp_about_us_widget extends WP_Widget {
	function herowp_about_us_widget() {
		global $herowp_theme_name;
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'herowp_about_us_widget', 'description' => 'Add a fancy description and details of your company.' );
		 
		/* Widget control settings. */
		$control_ops = array( 'id_base' => 'herowp_about_us_widget' );
		 
		/* Create the widget. */
		$this->WP_Widget( 'herowp_about_us_widget', ''.$herowp_theme_name.' About Us Widget', $widget_ops, $control_ops );
	}
	 
	function widget( $args, $instance ) {
		extract( $args ); ?>

		<div class="footer-box-title"><!--FOOTER BOX TITLE START-->
				<?php if(!empty($instance['title'])) : ?>
					<h3><?php echo esc_attr($instance['title']); ?><span class="dotcolor">.</span></h3>
				<?php endif; ?>
			<div class="footer-box-line2"></div>
		</div><!--FOOTER BOX TITLE END-->
			
			<div class="footer-box-content"><!--FOOTER BOX CONTENT START-->	
				
				<?php if(!empty($instance['about_us_content'])) : ?>
					<p><?php echo html_entity_decode(esc_attr($instance['about_us_content']));?></p>
				<?php endif; ?>
				
				<ul>
					
					<?php if(!empty($instance['about_us_location'])) : ?>
						<li><i class="fa fa-map-marker"></i><?php echo esc_attr($instance['about_us_location']);?></li>
					<?php endif; ?>		
					
					<?php if(!empty($instance['about_us_email'])) : ?>	
						<li><i class="fa fa-envelope"></i><?php echo esc_attr($instance['about_us_email']);?></li>
					<?php endif; ?>		
					
					<?php if(!empty($instance['about_us_phone'])): ?>	
						<li><i class="fa fa-mobile"></i><?php echo esc_attr($instance['about_us_phone']);?></li>
					<?php endif; ?>		
					
					<?php if(!empty($instance['about_us_time'])) : ?>
						<li><i class="fa fa-clock-o"></i><?php echo esc_attr($instance['about_us_time']);?></li>
					<?php endif; ?>	
					
				</ul>
		</div><!--FOOTER BOX CONTENT END-->

	
	<?php }
	 
	function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		 
		/* Strip tags (if needed) and update the widget settings. */
		$instance['title']              =  strip_tags(esc_attr($new_instance['title']));
		$instance['about_us_content']   =  htmlentities(esc_attr($new_instance['about_us_content']));
		$instance['about_us_location']  =  strip_tags(esc_attr($new_instance['about_us_location']));
		$instance['about_us_email']     =  strip_tags(esc_attr($new_instance['about_us_email']));
		$instance['about_us_phone']     =  strip_tags(esc_attr($new_instance['about_us_phone']));
		$instance['about_us_time']      =  strip_tags(esc_attr($new_instance['about_us_time']));
		
		
		return $instance;
	}
	 
	function form( $instance ) {

		$defaults = array( 'content' => '');
		$instance = wp_parse_args( (array) $instance, $defaults ); 
	
		/* Set up some default widget settings. */
		$title	              = isset( $instance['title']) ? strip_tags(esc_attr( $instance['title'] )) : 'About us';
		$about_us_content     = isset( $instance['about_us_content']) ? htmlentities(esc_attr($instance['about_us_content'])) : htmlentities(esc_attr('Chia direct trade VHS cred. Raw denim cool PBR&B Godard, organic XOXO. <br/><br/>Food truck <span>meggings</span> beard kitsch.'));
		$about_us_location	  = isset( $instance['about_us_location']) ? strip_tags(esc_attr( $instance['about_us_location'] )) : 'Salt Lake City, UT 87234';
		$about_us_email       = isset( $instance['about_us_email']) ? strip_tags(esc_attr( $instance['about_us_email'] )) : 'awesome@example.com';
		$about_us_phone	      = isset( $instance['about_us_phone']) ? strip_tags(esc_attr( $instance['about_us_phone'] )) : '(871) 321-6567';
		$about_us_time	      = isset( $instance['about_us_time']) ? strip_tags(esc_attr( $instance['about_us_time'] )) : 'Monday-Friday 08:00 - 18:00';
		
		?>
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Title','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" value="<?php echo esc_attr($title); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'about_us_content' )); ?>"><?php _e('About text','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'about_us_content' )); ?>" value="<?php echo htmlentities(esc_attr($about_us_content)); ?>" name="<?php echo esc_attr($this->get_field_name( 'about_us_content' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'about_us_location' )); ?>"><?php _e('Location','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'about_us_location' )); ?>" value="<?php echo esc_attr($about_us_location); ?>" name="<?php echo esc_attr($this->get_field_name( 'about_us_location' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'about_us_email' )); ?>"><?php _e('Email)','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'about_us_email' )); ?>" value="<?php echo esc_attr($about_us_email); ?>" name="<?php echo $this->get_field_name( 'about_us_email' ); ?>">
		</p>
		
		<p>
		<label for="<?php echo esc_attr($this->get_field_id( 'about_us_phone' )); ?>"><?php _e('Phone','myway') ?></label>
		<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'about_us_phone' )); ?>" value="<?php echo esc_attr($about_us_phone); ?>" name="<?php echo esc_attr($this->get_field_name( 'about_us_phone' )); ?>">
		</p>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'about_us_time' ); ?>"><?php _e('Schedule','myway') ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'about_us_time' ); ?>" value="<?php echo $about_us_time; ?>" name="<?php echo $this->get_field_name( 'about_us_time' ); ?>">
		</p>
	<?php
	}
}