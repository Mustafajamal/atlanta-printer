<?php
global $herowp_data;
/*
=================================================================================================================
Custom container width
=================================================================================================================
*/
$plus_30_px=30;
if (!empty($herowp_data['container_width'])){
	$container_width=$herowp_data['container_width'];
	$container_plus=$container_width+$plus_30_px;
	if($herowp_data['container_width']) echo'
		<style type="text/css">
			@media (min-width: '.$container_plus.'px) {
				.container {
					 width:'.esc_attr($herowp_data['container_width']).'px !important;
					}
				}
			</style>';
}
/*
=================================================================================================================
Let's apply a border radius on boxed version
=================================================================================================================
		
if($herowp_data['layoutboxed2']) echo'
	<style type="text/css">
		header{
			-webkit-border-top-left-radius: 5px !important;
			-webkit-border-top-right-radius: 5px !important;
			-moz-border-radius-topleft: 5px !important;
			-moz-border-radius-topright: 5px !important;
			border-top-left-radius: 5px !important;
			border-top-right-radius: 5px !important;
		}
	</style>';
*/	
/*
=================================================================================================================
Header background image
=================================================================================================================
*/
if (!empty($herowp_data['header_bg_image'])){
	function herowp_header_bg(){
		global $herowp_data;
		if($herowp_data['header_bg_image']){ echo '
			<style type="text/css">
				header {
					background:url('.esc_url($herowp_data['header_bg_image']).') top center no-repeat;
				}
			</style>';
		}
	}
	add_action('wp_footer','herowp_header_bg');
}
/*
=================================================================================================================
Option for logo height
=================================================================================================================
*/
if (!empty($herowp_data['logo_height'])){       
	if($herowp_data['logo_height']){ echo '
		<style type="text/css">
			#mega_main_menu .nav_logo .logo_link img{
				height:'.$herowp_data['logo_height'].'px !important;
			}
		</style>';
	}
}

/*
=================================================================================================================
Option for breadcrumb
=================================================================================================================
*/
if (!empty($herowp_data['shoutbox_enable'])){      
	if ($herowp_data['shoutbox_enable'] == 'disable'){ echo '
		<style type="text/css">
			.shout-box-title{
				display:none;
			}
		</style>';
	}
}
/*
=================================================================================================================
Option for breadcrumb font-size
=================================================================================================================
*/
if (!empty($herowp_data['shoutbox_font_size'])){
	function herowp_shoutbox_font_size(){
		global $herowp_data;
		if ($herowp_data['shoutbox_font_size']){ echo '
			<style type="text/css">
				.shout-box-title{
					font-size:'.$herowp_data['shoutbox_font_size'].'px;
					letter-spacing:-1px;
				}
			</style>';
		}
	}
	add_action('wp_footer','herowp_shoutbox_font_size');	
}

/*
=================================================================================================================
Option for sticky menu BG and opacity
=================================================================================================================
*/
if (!empty($herowp_data['sticky_menu_bg_color']) || !empty($herowp_data['sticky_menu_bg_opacity'])) {       
	
	function herowp_add_sticky_menu_bg(){
		global $herowp_data;
		if ($herowp_data['sticky_menu_bg_opacity']){
			$herowp_data['sticky_menu_bg_opacity'] = $herowp_data['sticky_menu_bg_opacity'];
		}
		else{
			$herowp_data['sticky_menu_bg_opacity'] = '.9';
		}
		
		if ($herowp_data['sticky_menu_bg_color']){
			$herowp_data['sticky_menu_bg_color'] = $herowp_data['sticky_menu_bg_color'];
		}
		else{
			$herowp_data['sticky_menu_bg_color'] = '0,0,0,';
		}
		
		if( $herowp_data['sticky_menu_bg_color'] || $herowp_data['sticky_menu_bg_opacity'] ){ echo '
			<style type="text/css">
				.sticky_container {
					background: '.hex2rgba($herowp_data['sticky_menu_bg_color'] ,$alpha = $herowp_data['sticky_menu_bg_opacity']).' !important;
				}
			</style>';
		}
	}
	add_action ('wp_footer','herowp_add_sticky_menu_bg');
}

/*
=================================================================================================================
Header breadcrumb background image
=================================================================================================================
	
if($herowp_data['breadcrumb_bg_image']){ echo '
	<style type="text/css">
		.shout-wrap {
			background: url('.esc_url($herowp_data['breadcrumb_bg_image']).') top center no-repeat !important;
		}
	</style>';
}
*/				
/*
=================================================================================================================
Option for background change
=================================================================================================================
*/
if (!empty($herowp_data['background_bg'])){       
	if($herowp_data['background_bg']){ echo '
		<style type="text/css">
			body {
				background:url('.esc_url($herowp_data['background_bg']).') !important;
				background-attachment:fixed !important;
				background-color: transparent !important;
				background-size: cover !important;
				background-position: 50% 0% !important;
				background-repeat: repeat repeat !important;
			}
		</style>';
	}
}

/*
=================================================================================================================
Option for layout boxed
=================================================================================================================
*/
if (!empty($herowp_data['layoutboxed2'])){
	if($herowp_data['layoutboxed2']=='1'){
		$boxed_version= 'class="container container_boxed main"';
	}		
	elseif($herowp_data['layoutboxed2']=='0'){
		$boxed_version= '';
	}
}
/*
=================================================================================================================
Option for custom CSS
=================================================================================================================
*/
if (!empty($herowp_data['css_code'])){
	if($herowp_data['css_code']) { 
		load_template(get_template_directory() . '/inc/custom.css.php') ;
	}
}
if (isset($_SESSION['colorpickercolor'])){ 
	load_template(get_template_directory() . '/inc/custom.css.php') ;
}

/*
=================================================================================================================
Google Analytics Code
=================================================================================================================
*/
/*if (!empty($herowp_data['google_analytics'])){	
	echo $herowp_data['google_analytics'];
}
	
/*
=================================================================================================================
Options for custom typography
=================================================================================================================
*/	
load_template(get_template_directory() . '/inc/custom.typography.css.php') ;

/*
=================================================================================================================
Include the file for custom color scheme - from left panel
=================================================================================================================
*/
if (isset ($_COOKIE['colorpickercolor'])) {
	require_once(get_template_directory() . '/inc/custom.colorpik.css.php');
	add_action('wp_footer','herowp_output_base_color_css_by_cookies');
}

/*
=================================================================================================================
Include the file for custom color scheme
=================================================================================================================
*/
if(!empty($herowp_data['color_theme'])){	
	if($herowp_data['color_theme']) {
		require_once(get_template_directory() . '/inc/custom.color.css.php');
		add_action ('wp_footer','herowp_output_base_color_css');
	}
}
?>