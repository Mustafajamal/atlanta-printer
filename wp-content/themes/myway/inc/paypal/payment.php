<?php 

	
	// var_dump($_POST['total_amount']);die();
	use PayPal\Api\Payer;
	use PayPal\Api\Details;
	use PayPal\Api\Amount;
	use PayPal\Api\Transaction;
	use PayPal\Api\Payment;
	use PayPal\Api\RedirectUrls;
	
	// echo get_template_directory_uri ()."/inc/paypal/payment.php";die();
	require __DIR__."/src/start.php";	

	$payer   		= new Payer();
	$details 		= new Details();
	$amount  		= new Amount();
	$transaction 	= new Transaction();
	$payment 		= new Payment();
	$redirectUrls 	= new RedirectUrls();


	$payer->setPaymentMethod('paypal');

	$details->setShipping('0.00')
			->setTax('0.00')
			->setSubtotal($_POST['total_amount']);
	
	$amount->setCurrency('USD')
			->setTotal($_POST['total_amount'])
			->setDetails($details);
	
	$transaction->setAmount($amount)
				->setDescription('Membership');
	
	$payment->setIntent('Sale')
			->setPayer($payer)
			->setTransactions([$transaction]);
	
	$redirectUrls->setReturnUrl('https://atlantaprintercouriers.com/wp-content/themes/myway/inc/paypal/paypalpayment/pay.php?approved=true')
				 ->setCancelUrl('https://atlantaprintercouriers.com/wp-content/themes/myway/inc/paypal/paypalpayment/pay.php?approved=false');

	$payment->setRedirectUrls($redirectUrls);			 	
	
	try{

		$payment->create($api);
	}
	catch(PayPalConnectionException $e){

		header('location: ../paypalpayment/error.php');
	}

	foreach ($payment->getLinks() as $link ) {
		
		if($link->getRel()=='approval_url'){
			$redirectUrls=$link->getHref();
		}
	}
	header('location:'.$redirectUrls);
	// var_dump();
?>