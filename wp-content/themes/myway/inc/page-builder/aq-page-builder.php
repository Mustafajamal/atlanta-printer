<?php
/** 

Author: Syamil MJ
Author URI: http://aquagraphite.com

*/
 
/**
 * Copyright (c) 2013 Syamil MJ. All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */


 
//definitions
$path=get_template_directory().'/inc/page-builder/';
$dir=get_template_directory_uri().'/inc/page-builder/';
if(!defined('AQPB_VERSION')) define( 'AQPB_VERSION', '1.1.2' );
if(!defined('AQPB_PATH')) define( 'AQPB_PATH', $path );
if(!defined('AQPB_DIR')) define( 'AQPB_DIR', $dir);


//required functions & classes
require_once(AQPB_PATH . 'functions/aqpb_config.php');
require_once(AQPB_PATH . 'functions/aqpb_blocks.php');
require_once(AQPB_PATH . 'classes/class-aq-page-builder.php');
require_once(AQPB_PATH . 'classes/class-aq-block.php');
require_once(AQPB_PATH . 'functions/aqpb_functions.php');

//used blocks
require_once(AQPB_PATH . 'blocks/aq-column-block.php');
require_once(AQPB_PATH . 'blocks/aq-column-parallax-block.php');
require_once(AQPB_PATH . 'blocks/aq-column-video-block.php');
require_once(AQPB_PATH . 'blocks/aq-heading.php'); 
require_once(AQPB_PATH . 'blocks/aq-slider-shortcode.php');
require_once(AQPB_PATH . 'blocks/aq-image.php');
require_once(AQPB_PATH . 'blocks/aq-quote.php');
require_once(AQPB_PATH . 'blocks/aq-button.php');
require_once(AQPB_PATH . 'blocks/aq-funny-facts.php');
require_once(AQPB_PATH . 'blocks/aq-services.php'); 
require_once(AQPB_PATH . 'blocks/aq-services-2.php');
require_once(AQPB_PATH . 'blocks/aq-services-04.php');
require_once(AQPB_PATH . 'blocks/aq-services-05.php');
require_once(AQPB_PATH . 'blocks/aq-services-3.php');
require_once(AQPB_PATH . 'blocks/aq-services-07.php');
require_once(AQPB_PATH . 'blocks/aq-services-08.php');
require_once(AQPB_PATH . 'blocks/aq-services-09.php');
require_once(AQPB_PATH . 'blocks/aq-services-10.php');
require_once(AQPB_PATH . 'blocks/aq-services-11.php');
require_once(AQPB_PATH . 'blocks/aq-portfolio.php');
require_once(AQPB_PATH . 'blocks/aq-portfolio-2.php');
require_once(AQPB_PATH . 'blocks/aq-portfolio-3.php');
require_once(AQPB_PATH . 'blocks/aq-promo.php');
require_once(AQPB_PATH . 'blocks/aq-team.php');
require_once(AQPB_PATH . 'blocks/aq-blog.php'); 
require_once(AQPB_PATH . 'blocks/aq-blog-02.php'); 
require_once(AQPB_PATH . 'blocks/aq-blog-03.php'); 
require_once(AQPB_PATH . 'blocks/aq-pricing-tables.php'); 
require_once(AQPB_PATH . 'blocks/aq-shortcode.php'); 
require_once(AQPB_PATH . 'blocks/aq-accordion.php');
require_once(AQPB_PATH . 'blocks/aq-progress-bars.php');
require_once(AQPB_PATH . 'blocks/aq-editor-block.php');
require_once(AQPB_PATH . 'blocks/aq-blank.php'); 


//register used blocks
aq_register_block('AQ_Column_Block');
aq_register_block('AQ_Column_Parallax_Block');
aq_register_block('AQ_Column_Video_Block');
aq_register_block('AQ_Heading');
aq_register_block('AQ_Slider_Shortcode');
aq_register_block('AQ_Image_Block');
aq_register_block('AQ_Quote_Block');
aq_register_block('AQ_Button');
aq_register_block('AQ_Funny_Facts');
aq_register_block('AQ_Box_Services');
aq_register_block('AQ_Box_Services_2');
aq_register_block('AQ_Box_Services_3');
aq_register_block('AQ_Box_Services_4');
aq_register_block('AQ_Box_Services_5');
aq_register_block('AQ_Box_Services_07');
aq_register_block('AQ_Box_Services_08');
aq_register_block('AQ_Box_Services_09');
aq_register_block('AQ_Box_Services_10');
aq_register_block('AQ_Box_Services_11');
aq_register_block('AQ_Portfolio_Block');
aq_register_block('AQ_Portfolio_Block2');
aq_register_block('AQ_Portfolio_Block3');
aq_register_block('AQ_Promo');
aq_register_block('AQ_Team_Block');
aq_register_block('AQ_Blog_Posts_Block');
aq_register_block('AQ_Blog_Posts_Block2');
aq_register_block('AQ_Blog_Posts_Block3');
aq_register_block('AQ_Pricing_Tables');
aq_register_block('AQ_Shortcode');
aq_register_block('AQ_Box_Accordion');
aq_register_block('AQ_Progress_Bars');
aq_register_block('AQ_Editor_Block');
aq_register_block('AQ_Blank');


//fire up page builder
$aqpb_config = aq_page_builder_config();
$aq_page_builder = new AQ_Page_Builder($aqpb_config);
if(!is_network_admin()) $aq_page_builder->init();
