<?php
/**
 * Aqua Page Builder functions
 *
 * This holds the external functions which can be used by the theme
 * Requires the AQ_Page_Builder class
 *
 * @todo - multicheck, image checkbox, better colorpicker
**/

if(class_exists('AQ_Page_Builder')) {
	
	/** 
	 * Core functions
	*******************/
	 
	/* Register a block */
	function aq_register_block($block_class) {
		global $aq_registered_blocks;
		$aq_registered_blocks[strtolower($block_class)] = new $block_class;
	}
	
	/** Un-register a block **/
	function aq_unregister_block($block_class) {
		global $aq_registered_blocks;
		$block_class = strtolower($block_class);
		foreach($aq_registered_blocks as $block) {
			if($block->id_base == $block_class) unset($aq_registered_blocks[$block_class]);
		}
	}
	
	/** Get list of all blocks **/
	function aq_get_blocks($template_id) {
		global $aq_page_builder;
		$blocks = $aq_page_builder->get_blocks($template_id);
		
		return $blocks;
	}
	
	/** 
	 * Form Field Helper functions
	 *
	 * Provides some default fields for use in the blocks
	********************************************************/
	
	/* Input field - Options: $size = min, small, full */
	function aq_field_input($field_id, $block_id, $input, $size = 'full', $type = 'text') {
		$output = '<input type="'.$type.'" id="'. $block_id .'_'.$field_id.'" class="input-'.$size.'" value="'.$input.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
		
		return $output;
	}	
	
	
	/* Input field - Options: $size = min, small, full */
	function aq_field_input_hidden($field_id, $block_id, $input, $type = 'hidden') {
		$output = '<input type="'.$type.'" id="'. $block_id .'_'.$field_id.'" value="'.$input.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
		
		return $output;
	}
	
	/* Input field - Font Awesome */
	function aq_field_input_fontawesome($field_id, $block_id, $input, $size = 'full', $type = 'text') {
		$output = '<input type="'.$type.'" id="'. $block_id .'_'.$field_id.'" class="input-'.$size.' awesome-fonts-input" value="'.$input.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
		
		return $output;
	}
	
	/* Textarea field */
	function aq_field_textarea($field_id, $block_id, $text, $size = 'full') {
		$output = '<textarea id="'. $block_id .'_'.$field_id.'" class="textarea-'.$size.'" name="aq_blocks['.$block_id.']['.$field_id.']" rows="5">'.$text.'</textarea>';
		
		return $output;
	}
	
	/* Textarea field for responsive properties*/
	function herowp_textarea_responsive($field_id, $block_id, $text,$placeholder) {
		$output = '<textarea id="'. $block_id .'_'.$field_id.'" class="textarea_responsive" placeholder="'.$placeholder.'" name="aq_blocks['.$block_id.']['.$field_id.']" rows="5">'.$text.'</textarea>';
		
		return $output;
	}
	
	
	/* Select field */
	function aq_field_select($field_id, $block_id, $options, $selected) {
		$options = is_array($options) ? $options : array();
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
		foreach($options as $key=>$value) {
			$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
		}
		$output .= '</select>';
		
		return $output;
	}
	
	/* Multiselect field */
	function aq_field_multiselect($field_id, $block_id, $options, $selected_keys = array()) {
		$output = '<select id="'. $block_id .'_'.$field_id.'" multiple="multiple" class="select of-input" name="aq_blocks['.$block_id.']['.$field_id.'][]">';
		foreach ($options as $key => $option) {
			$selected = (is_array($selected_keys) && in_array($key, $selected_keys)) ? $selected = 'selected="selected"' : '';			
			$output .= '<option id="'. $block_id .'_'.$field_id.'_'. $key .'" value="'.$key.'" '. $selected .' />'.$option.'</option>';
		}
		$output .= '</select>';
		
		return $output;
	}
	
	/* Color picker field */
	function aq_field_color_picker($field_id, $block_id, $color, $default = '') {
		$output = '<div class="aqpb-color-picker">';
			$output .= '<input type="text" id="'. $block_id .'_'.$field_id.'" class="input-color-picker" value="'. $color .'" name="aq_blocks['.$block_id.']['.$field_id.']" data-default-color="'. $default .'"/>';
		$output .= '</div>';
		
		return $output;
	}
	
	/* Single Checkbox */
	function aq_field_checkbox($field_id, $block_id, $check) {
		$output = '<input type="hidden" name="aq_blocks['.$block_id.']['.$field_id.']" value="0" />';
		$output .= '<input type="checkbox" id="'. $block_id .'_'.$field_id.'" class="input-checkbox" name="aq_blocks['.$block_id.']['.$field_id.']" '. checked( 1, $check, false ) .' value="1"/>';
		
		return $output;
	}
	
	/* Multi Checkbox */
	function aq_field_multicheck($field_id, $block_id, $fields = array(), $selected = array()) {
	
	}
	
	/* Media Uploader */
	function aq_field_upload($field_id, $block_id, $media, $media_type = 'image') {
		$output = '<input type="text" id="'. $block_id .'_'.$field_id.'" class="input-float input-upload" value="'.$media.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
		$output .= '<a href="#" class="aq_upload_button" rel="'.$media_type.'">Upload</a><p></p>';
		
		return $output;
	}
	
	/** 
	 * Misc Helper Functions
	**************************/
	
	/** Get column width
	 * @parameters - $size (column size), $grid (grid size e.g 940), $margin
	 */
	function aq_get_column_width($size, $grid = 940, $margin = 20) {
		
		$columns = range(1,12);
		$widths = array();
		foreach($columns as $column) {
			$width = (( $grid + $margin ) / 12 * $column) - $margin;
			$width = round($width);
			$widths[$column] = $width;
		}
		
		$column_id = absint(preg_replace("/[^0-9]/", '', $size));
		$column_width = $widths[$column_id];
		return $column_width;
	}
	
	/** Recursive sanitize
	 * For those complex multidim arrays 
	 * Has impact on server load on template save, so use only where necessary 
	 */
	function aq_recursive_sanitize($value) {
		if(is_array($value)) {
			$value = array_map('aq_recursive_sanitize', $value);
		} else {
			$value = htmlspecialchars(stripslashes($value));
		}
		return $value;
	}
	/** 
	 * Generate list of icons
	 * Currently 1075 icons
	 */
	function herowp_font_icon(){
	
		$font_awesome_icons=array("icon-aa","icon-add-userstar","icon-add","icon-add-bag","icon-add-basket","icon-add-cart","icon-add-file","icon-address-book","icon-address-book2","icon-add-spaceafterparagraph","icon-add-spacebeforeparagraph","icon-add-user","icon-add-window","icon-administrator","icon-aerobics","icon-aerobics-2","icon-aerobics-3","icon-affiliate","icon-air-balloon","icon-airship","icon-alarm","icon-alarm-clock","icon-alarm-clock2","icon-alien","icon-alien-2","icon-aligator","icon-align-center","icon-align-justifyall","icon-align-justifycenter","icon-align-justifyleft","icon-align-justifyright","icon-align-left","icon-align-right","icon-alpha","icon-ambulance","icon-amx","icon-anchor","icon-anchor-2","icon-angel-smiley","icon-angry","icon-apple","icon-apple-bite","icon-approved-window","icon-aquarius","icon-aquarius-2","icon-archery","icon-archery-2","icon-argentina","icon-aries","icon-aries-2","icon-army-key","icon-arrow-around","icon-arrow-back-3","icon-arrow-back","icon-arrow-back2","icon-arrow-barrier","icon-arrow-circle","icon-arrow-down","icon-arrow-down2","icon-arrow-down3","icon-arrow-downincircle","icon-arrow-fork","icon-arrow-forward","icon-arrow-forward2","icon-arrow-from","icon-arrow-inside","icon-arrow-inside45","icon-arrow-insidegap","icon-arrow-insidegap45","icon-arrow-into","icon-arrow-join","icon-arrow-junction","icon-arrow-left","icon-arrow-left2","icon-arrow-leftincircle","icon-arrow-loop","icon-arrow-mix","icon-arrow-next","icon-arrow-outleft","icon-arrow-outright","icon-arrow-outside","icon-arrow-outside45","icon-arrow-outsidegap","icon-arrow-outsidegap45","icon-arrow-over","icon-arrow-refresh","icon-arrow-refresh2","icon-arrow-right","icon-arrow-right2","icon-arrow-rightincircle","icon-arrow-shuffle","icon-arrow-squiggly","icon-arrow-through","icon-arrow-to","icon-arrow-turnleft","icon-arrow-up","icon-arrow-up2","icon-arrow-up3","icon-arrow-upincircle","icon-arrow-xleft","icon-arrow-xright","icon-assistant","icon-astronaut","icon-atm","icon-atom","icon-at-sign","icon-audio","icon-auto-flash","icon-autumn","icon-a-z","icon-baby","icon-baby-clothes","icon-baby-clothes2","icon-baby-cry","icon-background","icon-back-media","icon-back-music","icon-bacteria","icon-bag","icon-bag-coins","icon-bag-items","icon-bag-quantity","icon-bakelite","icon-ballet-shoes","icon-balloon","icon-banana","icon-band-aid","icon-bank","icon-bar-chart","icon-bar-chart2","icon-bar-chart3","icon-bar-chart4","icon-bar-chart5","icon-barricade","icon-barricade-2","icon-baseball","icon-basket-ball","icon-basket-coins","icon-basket-items","icon-basket-quantity","icon-bat","icon-bat-2","icon-bathrobe","icon-batman-mask","icon-battery-0","icon-battery-25","icon-battery-50","icon-battery-75","icon-battery-100","icon-battery-charge","icon-bear","icon-beard","icon-beard-3","icon-bee","icon-beer","icon-beer-glass","icon-bell","icon-bell-2","icon-belt","icon-belt-2","icon-belt-3","icon-berlin-tower","icon-beta","icon-bicycle","icon-bicycle-2","icon-bicycle-3","icon-big-bang","icon-big-data","icon-bike-helmet","icon-bikini","icon-bilk-bottle2","icon-binocular","icon-bio-hazard","icon-biotech","icon-bird","icon-bird-deliveringletter","icon-birthday-cake","icon-bisexual","icon-bishop","icon-bitcoin","icon-blackboard","icon-black-cat","icon-block-cloud","icon-block-window","icon-blood","icon-blouse","icon-blueprint","icon-board","icon-bodybuilding","icon-bold-text","icon-bones","icon-book","icon-bookmark","icon-books","icon-books-2","icon-boom","icon-boot","icon-boot-2","icon-bottom-totop","icon-bow","icon-bow-2","icon-bow-3","icon-bow-4","icon-bow-5","icon-bow-6","icon-bowling","icon-bowling-2","icon-box","icon-box-close","icon-box-open","icon-box-withfolders","icon-boy","icon-bra","icon-brain","icon-brain-2","icon-brain-3","icon-brazil","icon-bread","icon-bread-2","icon-bridge","icon-broke-link2","icon-broken-link","icon-broom","icon-brush","icon-bucket","icon-bug","icon-building","icon-bulleted-list","icon-bus-2","icon-business-man","icon-business-manwoman","icon-business-mens","icon-business-woman","icon-butterfly","icon-button","icon-cable-car","icon-cake","icon-calculator","icon-calculator-2","icon-calculator-3","icon-calendar","icon-calendar-2","icon-calendar-3","icon-calendar-4","icon-calendar-clock","icon-camel","icon-camera","icon-airbrush","icon-angel","icon-arrow-cross","icon-arrow-merge","icon-arrow-turnright","icon-back","icon-bar-code","icon-beard-2","icon-billing","icon-bone","icon-box-full","icon-bus","icon-camera-2","icon-camera-3","icon-camera-4","icon-camera-5","icon-camera-back","icon-can","icon-can-2","icon-canada","icon-cancer","icon-cancer-2","icon-cancer-3","icon-candle","icon-candy","icon-candy-cane","icon-cannon","icon-cap","icon-cap-2","icon-cap-3","icon-capricorn","icon-capricorn-2","icon-cap-smiley","icon-car","icon-car-2","icon-car-3","icon-car-coins","icon-cardigan","icon-cardiovascular","icon-car-items","icon-cart-quantity","icon-car-wheel","icon-casette-tape","icon-cash-register","icon-cash-register2","icon-castle","icon-cat","icon-cathedral","icon-cauldron","icon-cd","icon-cd-2","icon-cd-cover","icon-cello","icon-celsius","icon-chacked-flag","icon-chair","icon-charger","icon-check","icon-check-2","icon-checked-user","icon-checkmate","icon-checkout","icon-checkout-bag","icon-checkout-basket","icon-cheese","icon-cheetah","icon-chef","icon-chef-hat","icon-chef-hat2","icon-chemical","icon-chemical-2","icon-chemical-3","icon-chemical-4","icon-chemical-5","icon-chess","icon-chess-board","icon-chicken","icon-chile","icon-chimney","icon-china","icon-chinese-temple","icon-chip","icon-chopsticks","icon-chopsticks-2","icon-christmas","icon-christmas-ball","icon-christmas-bell","icon-christmas-candle","icon-christmas-hat","icon-christmas-sleigh","icon-christmas-snowman","icon-christmas-sock","icon-christmas-tree","icon-chrysler-building","icon-cinema","icon-circular-point","icon-city-hall","icon-clamp","icon-clapperboard-close","icon-clapperboard-open","icon-claps","icon-clef","icon-clinic","icon-clock","icon-clock-2","icon-clock-3","icon-clock-4","icon-clock-back","icon-clock-forward","icon-close","icon-close-window","icon-clothing-store","icon-cloud","icon-cloud-","icon-cloud--","icon-cloud-camera","icon-cloud-computer","icon-cloud-email","icon-cloud-hail","icon-cloud-laptop","icon-cloud-lock","icon-cloud-moon","icon-cloud-music","icon-cloud-picture","icon-cloud-rain","icon-cloud-remove","icon-clouds","icon-cloud-secure","icon-cloud-settings","icon-cloud-smartphone","icon-cloud-snow","icon-cloud-sun","icon-clouds-weather","icon-cloud-tablet","icon-cloud-video","icon-cloud-weather","icon-clown","icon-cmyk","icon-coat","icon-cocktail","icon-coconut","icon-code-window","icon-coding","icon-coffee","icon-coffee-2","icon-coffee-bean","icon-coffee-machine","icon-coffee-togo","icon-coffin","icon-coin","icon-coins","icon-coins-2","icon-coins-3","icon-colombia","icon-colosseum","icon-column","icon-column-2","icon-column-3","icon-comb","icon-comb-2","icon-communication-tower","icon-communication-tower2","icon-compass","icon-compass-2","icon-compass-3","icon-compass-4","icon-compass-rose","icon-computer","icon-computer-2","icon-computer-3","icon-computer-secure","icon-conference","icon-confused","icon-conservation","icon-consulting","icon-contrast","icon-control","icon-control-2","icon-cookie-man","icon-cookies","icon-cool","icon-cool-guy","icon-copyright","icon-costume","icon-couple-sign","icon-cow","icon-cpu","icon-crane","icon-cranium","icon-credit-card","icon-credit-card2","icon-credit-card3","icon-cricket","icon-criminal","icon-croissant","icon-crop-2","icon-crop-3","icon-crown","icon-crown-2","icon-crying","icon-cube-molecule","icon-cube-molecule2","icon-cupcake","icon-cursor","icon-cursor-click","icon-cursor-click2","icon-cursor-move","icon-cursor-move2","icon-cursor-select","icon-dam","icon-danemark","icon-danger","icon-danger-2","icon-dashboard","icon-data","icon-data-backup","icon-data-block","icon-data-center","icon-data-clock","icon-data-cloud","icon-data-compress","icon-data-copy","icon-data-download","icon-data-financial","icon-data-key","icon-data-lock","icon-data-network","icon-data-password","icon-data-power","icon-data-refresh","icon-data-save","icon-data-search","icon-data-security","icon-data-settings","icon-data-sharing","icon-data-shield","icon-data-signal","icon-data-storage","icon-data-stream","icon-data-transfer","icon-data-unlock","icon-data-upload","icon-data-yes","icon-david-star","icon-daylight","icon-death","icon-dec","icon-decrase-inedit","icon-deer","icon-deer-2","icon-delete-file","icon-delete-window","icon-depression","icon-device-syncwithcloud","icon-d-eyeglasses","icon-d-eyeglasses2","icon-diamond","icon-dice","icon-dice-2","icon-digital-drawing","icon-dinosaur","icon-diploma","icon-diploma-2","icon-direction-east","icon-direction-north","icon-direction-south","icon-direction-west","icon-director","icon-disk","icon-dj","icon-dna","icon-dna-2","icon-dna-helix","icon-doctor","icon-dog","icon-dollar","icon-dollar-sign","icon-dollar-sign2","icon-dolphin","icon-domino","icon-door","icon-door-hanger","icon-double-circle","icon-double-tap","icon-doughnut","icon-dove","icon-down","icon-down-2","icon-down-3","icon-down-4","icon-download","icon-download-2","icon-download-fromcloud","icon-download-window","icon-downward","icon-drag","icon-drag-down","icon-drag-left","icon-drag-right","icon-drag-up","icon-dress","icon-drill","icon-drill-2","icon-drop","icon-drum","icon-dry","icon-duck","icon-dumbbell","icon-duplicate-layer","icon-duplicate-window","icon-dvd","icon-eagle","icon-ear","icon-earphones","icon-earphones-2","icon-eci-icon","icon-edit","icon-edit-map","icon-eggs","icon-egypt","icon-eifel-tower","icon-eject","icon-eject-2","icon-elbow","icon-el-castillo","icon-electric-guitar","icon-electricity","icon-elephant","icon-embassy","icon-empire-statebuilding","icon-empty-box","icon-end","icon-end-2","icon-endways","icon-engineering","icon-envelope","icon-envelope-2","icon-environmental","icon-environmental-2","icon-environmental-3","icon-equalizer","icon-eraser","icon-eraser-2","icon-eraser-3","icon-error-404window","icon-euro","icon-euro-sign","icon-euro-sign2","icon-evil","icon-explode","icon-eye","icon-eye-2","icon-eye-blind","icon-eyebrow","icon-eyebrow-2","icon-eyebrow-3","icon-eyeglasses-smiley","icon-eyeglasses-smiley2","icon-eye-invisible","icon-eye-scan","icon-eye-visible","icon-face-style","icon-face-style2","icon-face-style3","icon-face-style4","icon-face-style5","icon-face-style6","icon-factory","icon-factory-2","icon-fahrenheit","icon-family-sign","icon-fan","icon-farmer","icon-fashion","icon-favorite-window","icon-fax","icon-feather","icon-female","icon-female-2","icon-female-sign","icon-file","icon-file-block","icon-file-bookmark","icon-file-chart","icon-file-clipboard","icon-file-clipboardfiletext","icon-file-clipboardtextimage","icon-file-cloud","icon-file-copy","icon-file-copy2","icon-file-csv","icon-file-download","icon-file-edit","icon-file-excel","icon-file-favorite","icon-file-fire","icon-file-graph","icon-file-hide","icon-file-horizontal","icon-file-horizontaltext","icon-file-html","icon-file-jpg","icon-file-link","icon-file-loading","icon-file-lock","icon-file-love","icon-file-music","icon-file-network","icon-file-pictures","icon-file-pie","icon-file-presentation","icon-file-refresh","icon-files","icon-file-search","icon-file-settings","icon-file-share","icon-file-textimage","icon-file-trash","icon-file-txt","icon-file-upload","icon-file-video","icon-file-word","icon-file-zip","icon-film","icon-film-board","icon-film-cartridge","icon-film-strip","icon-film-video","icon-filter","icon-filter-2","icon-financial","icon-find-user","icon-finger","icon-finger-dragfoursides","icon-finger-dragtwosides","icon-fingerprint","icon-finger-print","icon-fingerprint-2","icon-fire-flame","icon-fire-flame2","icon-fire-hydrant","icon-fire-staion","icon-firewall","icon-first","icon-first-aid","icon-fish","icon-fish-food","icon-fit-to","icon-fit-to2","icon-five-fingers","icon-five-fingersdrag","icon-five-fingersdrag2","icon-five-fingerstouch","icon-flag","icon-flag-2","icon-flag-3","icon-flag-4","icon-flag-5","icon-flag-6","icon-flamingo","icon-flash","icon-flash-2","icon-flashlight","icon-flash-video","icon-flask","icon-flask-2","icon-flick","icon-flowerpot","icon-fluorescent","icon-fog-day","icon-fog-night","icon-folder","icon-folder-add","icon-folder-archive","icon-folder-binder","icon-folder-binder2","icon-folder-block","icon-folder-bookmark","icon-folder-close","icon-folder-cloud","icon-folder-delete","icon-folder-download","icon-folder-edit","icon-folder-favorite","icon-folder-fire","icon-folder-hide","icon-folder-link","icon-folder-loading","icon-folder-lock","icon-folder-love","icon-folder-music","icon-folder-network","icon-folder-open","icon-folder-open2","icon-folder-organizing","icon-folder-pictures","icon-folder-refresh","icon-folder-remove-","icon-folders","icon-folder-search","icon-folder-settings","icon-folder-share","icon-folder-trash","icon-folder-upload","icon-folder-video","icon-folder-withdocument","icon-folder-zip","icon-font-color","icon-font-name","icon-font-size","icon-font-style","icon-font-stylesubscript","icon-font-stylesuperscript","icon-font-window","icon-foot","icon-foot-2","icon-football","icon-football-2","icon-footprint","icon-footprint-2","icon-footprint-3","icon-forest","icon-fork","icon-formula","icon-forward","icon-fountain-pen","icon-four-fingers","icon-four-fingersdrag","icon-four-fingersdrag2","icon-four-fingerstouch","icon-fox","icon-frankenstein","icon-french-fries","icon-frog","icon-fruits","icon-fuel","icon-full-bag","icon-full-basket","icon-full-cart","icon-full-moon","icon-full-screen","icon-full-screen2","icon-full-view","icon-full-view2","icon-full-viewwindow","icon-function","icon-funky","icon-funny-bicycle","icon-gamepad","icon-gamepad-2","icon-gas-pump","icon-gaugage","icon-gaugage-2","icon-gay","icon-gear","icon-gear-2","icon-gears","icon-gears-2","icon-geek","icon-geek-2","icon-gemini","icon-gemini-2","icon-genius","icon-gentleman","icon-geo","icon-geo-","icon-geo--","icon-geo2","icon-geo2-","icon-geo2--","icon-geo2-close","icon-geo2-love","icon-geo2-number","icon-geo2-star","icon-geo3","icon-geo3-","icon-geo3--","icon-geo3-close","icon-geo3-love","icon-geo3-number","icon-geo3-star","icon-geo-close","icon-geo-love","icon-geo-number","icon-geo-star","icon-gey","icon-gift-box","icon-giraffe","icon-girl","icon-glasses","icon-glasses-2","icon-glasses-3","icon-glass-water","icon-global-position","icon-globe","icon-globe-2","icon-gloves","icon-go-bottom","icon-goggles","icon-golf","icon-golf-2","icon-gopro","icon-gorilla","icon-go-top","icon-grave","icon-graveyard","icon-greece","icon-green-energy","icon-green-house","icon-guitar","icon-gun","icon-gun-2","icon-gun-3","icon-gymnastics","icon-hair","icon-hair-2","icon-hair-3","icon-hair-4","icon-half-moon","icon-halloween-halfmoon","icon-halloween-moon","icon-hamburger","icon-hammer","icon-hand","icon-hands","icon-handshake","icon-hand-touch","icon-hand-touch2","icon-hand-touchsmartphone","icon-hanger","icon-happy","icon-hat","icon-hat-2","icon-haunted-house","icon-hd","icon-hdd","icon-hd-video","icon-headphone","icon-headphones","icon-headset","icon-heart","icon-heart-2","icon-heels","icon-heels-2","icon-height-window","icon-helicopter","icon-helicopter-2","icon-helix-2","icon-hello","icon-helmet","icon-helmet-2","icon-helmet-3","icon-hipo","icon-hipster-glasses","icon-hipster-glasses2","icon-hipster-glasses3","icon-hipster-headphones","icon-hipster-men","icon-hipster-men2","icon-hipster-men3","icon-hipster-sunglasses","icon-hipster-sunglasses2","icon-hipster-sunglasses3","icon-hokey","icon-holly","icon-home","icon-home-2","icon-home-3","icon-home-4","icon-home-5","icon-home-window","icon-homosexual","icon-honey","icon-hong-kong","icon-hoodie","icon-horror","icon-horse","icon-hospital","icon-hospital-2","icon-host","icon-hot-dog","icon-hotel","icon-hour","icon-hub","icon-humor","icon-hurt","icon-ice-cream","icon-id-2","icon-id-3","icon-id-card","icon-idea","icon-idea-2","icon-idea-3","icon-idea-4","icon-idea-5","icon-identification-badge","icon-inbox","icon-inbox-empty","icon-inbox-forward","icon-inbox-full","icon-inbox-into","icon-inbox-out","icon-inbox-reply","icon-increase-inedit","icon-indent-firstline","icon-indent-leftmargin","icon-indent-rightmargin","icon-india","icon-information","icon-info-window","icon-inifity","icon-internet","icon-internet-2","icon-internet-smiley","icon-israel","icon-italic-text","icon-jacket","icon-jacket-2","icon-jamaica","icon-japan","icon-japanese-gate","icon-jeans","icon-jeep","icon-jeep-2","icon-jet","icon-joystick","icon-juice","icon-jump-rope","icon-kangoroo","icon-kenya","icon-key","icon-key-2","icon-key-3","icon-keyboard","icon-keyboard3","icon-key-lock","icon-keypad","icon-king","icon-king-2","icon-kiss","icon-knee","icon-knife","icon-knife-2","icon-knight","icon-koala","icon-korea","icon-lamp","icon-landscape","icon-landscape-2","icon-lantern","icon-laptop","icon-laptop-2","icon-laptop-3","icon-laptop-phone","icon-laptop-secure","icon-laptop-tablet","icon-laser","icon-last","icon-laughing","icon-layer-backward","icon-layer-forward","icon-leafs","icon-leafs-2","icon-leaning-tower","icon-left","icon-left-2","icon-left-3","icon-left-4","icon-left--right","icon-left--right3","icon-left-toright","icon-leg","icon-leg-2","icon-lego","icon-lemon","icon-len","icon-len-2","icon-len-3","icon-leo","icon-leo-2","icon-leopard","icon-lesbian","icon-lesbians","icon-letter-close","icon-letter-open","icon-letter-sent","icon-libra","icon-libra-2","icon-library","icon-library-2","icon-life-jacket","icon-life-safer","icon-light-bulb","icon-light-bulb2","icon-light-bulbleaf","icon-lighthouse","icon-line-chart","icon-line-chart2","icon-line-chart3","icon-line-chart4","icon-line-spacing","icon-line-spacingtext","icon-link","icon-link-2","icon-lion","icon-loading","icon-loading-2","icon-loading-3","icon-loading-window","icon-location","icon-location-2","icon-lock","icon-lock-2","icon-lock-3","icon-lock-user","icon-lock-window","icon-lollipop","icon-lollipop-2","icon-lollipop-3","icon-loop","icon-loud","icon-loudspeaker","icon-love","icon-love-2","icon-love-user","icon-love-window","icon-lowercase-text","icon-luggafe-front","icon-luggage-2","icon-macro","icon-magic-wand","icon-magnet","icon-magnifi-glass","icon-magnifi-glass-","icon-magnifi-glass2","icon-mail","icon-mail-2","icon-mail-3","icon-mail-add","icon-mail-attachement","icon-mail-block","icon-mailbox-empty","icon-mailbox-full","icon-mail-delete","icon-mail-favorite","icon-mail-forward","icon-mail-gallery","icon-mail-inbox","icon-mail-link","icon-mail-lock","icon-mail-love","icon-mail-money","icon-mail-open","icon-mail-outbox","icon-mail-password","icon-mail-photo","icon-mail-read","icon-mail-removex","icon-mail-reply","icon-mail-replyall","icon-mail-search","icon-mail-send","icon-mail-settings","icon-mail-unread","icon-mail-video","icon-mail-withatsign","icon-mail-withcursors","icon-male","icon-male-2","icon-malefemale","icon-male-sign","icon-management","icon-man-sign","icon-mans-underwear","icon-mans-underwear2","icon-map","icon-map2","icon-map-marker","icon-map-marker2","icon-map-marker3","icon-marker","icon-marker-2","icon-marker-3","icon-martini-glass","icon-mask","icon-master-card","icon-maximize","icon-maximize-window","icon-medal","icon-medal-2","icon-medal-3","icon-medical-sign","icon-medicine","icon-medicine-2","icon-medicine-3","icon-megaphone","icon-memory-card","icon-memory-card2","icon-memory-card3","icon-men","icon-menorah","icon-mens","icon-mexico","icon-mic","icon-microphone","icon-microphone-2","icon-microphone-3","icon-microphone-4","icon-microphone-5","icon-microphone-6","icon-microphone-7","icon-microscope","icon-milk-bottle","icon-mine","icon-minimize","icon-minimize-maximize-close-window","icon-minimize-window","icon-mirror","icon-mixer","icon-money","icon-money-2","icon-money-bag","icon-money-smiley","icon-monitor","icon-monitor-2","icon-monitor-3","icon-monitor-4","icon-monitor-5","icon-monitor-analytics","icon-monitoring","icon-monitor-laptop","icon-monitor-phone","icon-monitor-tablet","icon-monitor-vertical","icon-monkey","icon-monster","icon-morocco","icon-motorcycle","icon-mouse","icon-mouse-2","icon-mouse-3","icon-mouse-4","icon-mouse-pointer","icon-moustache-smiley","icon-movie","icon-movie-ticket","icon-mp3-file","icon-museum","icon-mushroom","icon-music-note","icon-music-note2","icon-music-note3","icon-mustache-2","icon-mustache-3","icon-mustache-4","icon-mustache-5","icon-mustache-6","icon-mustache-7","icon-mustache-8","icon-mute","icon-navigate-end","icon-navigation-leftwindow","icon-navigation-rightwindow","icon-navigat-start","icon-nepal","icon-network","icon-network-window","icon-neutron","icon-new-mail","icon-next","icon-next-3","icon-next-music","icon-no-battery","icon-no-drop","icon-no-flash","icon-noose","icon-normal-text","icon-no-smoking","icon-note","icon-notepad","icon-notepad-2","icon-nuclear","icon-numbering-list","icon-nurse","icon-office","icon-office-lamp","icon-old-clock","icon-old-radio","icon-old-sticky","icon-old-sticky2","icon-old-telephone","icon-old-tv","icon-on-air","icon-one-finger","icon-one-fingertouch","icon-one-window","icon-on-off","icon-on-off-2","icon-on-off-3","icon-open-banana","icon-open-book","icon-opera-house","icon-optimization","icon-ornament","icon-over-time","icon-over-time2","icon-owl","icon-pac-man","icon-paintbrush","icon-paint-brush","icon-paint-bucket","icon-palette","icon-palm-tree","icon-panda","icon-panorama","icon-pantheon","icon-pantone","icon-pants","icon-paper","icon-paper-plane","icon-password-2shopping","icon-password-field","icon-password-shopping","icon-pause","icon-pause-2","icon-paw","icon-pawn","icon-pen","icon-pen-2","icon-pen-3","icon-pen-4","icon-pen-5","icon-pen-6","icon-pencil","icon-pencil-ruler","icon-penguin","icon-pentagon","icon-petrol","icon-petronas-tower","icon-philipines","icon-phone","icon-phone-2","icon-phone-3","icon-phone-3g","icon-phone-4g","icon-phone-simcard","icon-phone-sms","icon-phone-wifi","icon-photo","icon-photo-2","icon-photo-3","icon-photo-album","icon-photo-album2","icon-photo-album3","icon-piano","icon-pie-chart","icon-pie-chart2","icon-pie-chart3","icon-pilates","icon-pilates-2","icon-pilates-3","icon-pilot","icon-pinch","icon-ping-pong","icon-pipe","icon-pipette","icon-piramids","icon-pisces","icon-pisces-2","icon-pizza","icon-pizza-slice","icon-plasmid","icon-plaster","icon-plastic-cupphone","icon-plastic-cupphone2","icon-plate","icon-plates","icon-play-music","icon-plug-in","icon-plug-in2","icon-pointer","icon-poland","icon-police","icon-police-man","icon-police-station","icon-police-woman","icon-polo-shirt","icon-portrait","icon-post-office","icon-post-sign","icon-post-sign2ways","icon-pound","icon-pound-sign","icon-pound-sign2","icon-power","icon-power-2","icon-power-3","icon-power-cable","icon-power-station","icon-prater","icon-present","icon-presents","icon-press","icon-preview","icon-previous","icon-profile","icon-project","icon-projector","icon-projector-2","icon-pulse","icon-pumpkin","icon-punk","icon-punker","icon-puzzle","icon-qr-code","icon-queen","icon-queen-2","icon-quill","icon-quill-2","icon-quill-3","icon-quotes","icon-quotes-2","icon-rainbow","icon-rainbow-2","icon-rain-drop","icon-ram","icon-razzor-blade","icon-receipt","icon-receipt-2","icon-receipt-3","icon-receipt-4","icon-record","icon-record-3","icon-record-music","icon-recycling","icon-recycling-2","icon-redirect","icon-redo","icon-reel","icon-reload","icon-reload-2","icon-reload-3","icon-remote-controll","icon-remote-controll2","icon-remove","icon-remove-bag","icon-remove-basket","icon-remove-cart","icon-remove-file","icon-remove-user","icon-remove-window","icon-rename","icon-repair","icon-repeat","icon-repeat-2","icon-repeat-3","icon-repeat-7","icon-reset","icon-resize","icon-restore-window","icon-retouching","icon-retro","icon-retro-camera","icon-retweet","icon-rewind","icon-rgb","icon-ribbon","icon-ribbon-2","icon-ribbon-3","icon-right","icon-right-2","icon-right-3","icon-right-4","icon-road-3","icon-robot","icon-robot-2","icon-rock-androll","icon-rocket","icon-roller","icon-roof","icon-rook","icon-rotate-gesture","icon-rotate-gesture2","icon-rotate-gesture3","icon-rotation","icon-rotation-390","icon-router","icon-router-2","icon-ruler","icon-ruler-2","icon-music-note4","icon-music-player","icon-mustache","icon-newspaper","icon-newspaper-2","icon-new-tab","icon-oil","icon-old-camera","icon-old-cassette","icon-orientation","icon-orientation-2","icon-orientation-3","icon-parasailing","icon-parrot","icon-password","icon-people-oncloud","icon-pepper","icon-pepper-withfire","icon-photos","icon-physics","icon-pi","icon-plane","icon-plane-2","icon-plant","icon-portugal","icon-post-mail","icon-post-mail2","icon-pricing","icon-printer","icon-professor","icon-radio","icon-radioactive","icon-rafting","icon-refinery","icon-refresh","icon-refresh-window","icon-repeat-4","icon-repeat-5","icon-repeat-6","icon-right-toleft","icon-road","icon-road-2","icon-running","icon-running-shoes","icon-safe-box","icon-safe-box2","icon-safety-pinclose","icon-safety-pinopen","icon-sagittarus","icon-sagittarus-2","icon-sailing-ship","icon-sand-watch","icon-sand-watch2","icon-santa-claus","icon-santa-claus2","icon-santa-onsled","icon-satelite","icon-satelite-2","icon-save","icon-save-window","icon-saw","icon-saxophone","icon-scale","icon-scarf","icon-scissor","icon-scooter","icon-scooter-front","icon-scorpio","icon-scorpio-2","icon-scotland","icon-screwdriver","icon-scroll","icon-scroller","icon-scroller-2","icon-scroll-fast","icon-sea-dog","icon-search-oncloud","icon-search-people","icon-secound","icon-secound2","icon-security-block","icon-security-bug","icon-security-camera","icon-security-check","icon-security-settings","icon-security-smiley","icon-securiy-remove","icon-seed","icon-selfie","icon-serbia","icon-server","icon-server-2","icon-servers","icon-settings-window","icon-sewing-machine","icon-sexual","icon-share","icon-share-oncloud","icon-share-window","icon-shark","icon-sheep","icon-sheriff-badge","icon-shield","icon-ship","icon-ship-2","icon-shirt","icon-shoes","icon-shoes-2","icon-shoes-3","icon-shop","icon-shop-2","icon-shop-3","icon-shop-4","icon-shopping-bag","icon-shopping-basket","icon-shopping-cart","icon-short-pants","icon-shovel","icon-shuffle","icon-shuffle-2","icon-shuffle-3","icon-shuffle-4","icon-shutter","icon-sidebar-window","icon-signal","icon-singapore","icon-skateboard","icon-skateboard-2","icon-skate-shoes","icon-skeleton","icon-ski","icon-skirt","icon-skull","icon-skydiving","icon-sled","icon-sled-withgifts","icon-sleeping","icon-sleet","icon-slippers","icon-smart","icon-smartphone","icon-smartphone-2","icon-smartphone-3","icon-smartphone-4","icon-smartphone-secure","icon-smile","icon-smoking-area","icon-smoking-pipe","icon-snake","icon-snorkel","icon-snow","icon-snow-2","icon-snow-dome","icon-snowflake","icon-snowflake-2","icon-snowflake-3","icon-snowflake-4","icon-snowman","icon-snow-storm","icon-soccer-ball","icon-soccer-shoes","icon-socks","icon-solar","icon-sound","icon-sound-wave","icon-soup","icon-south-africa","icon-space-needle","icon-spain","icon-spam-mail","icon-speach-bubble","icon-speach-bubble2","icon-speach-bubble3","icon-speach-bubble4","icon-speach-bubble5","icon-speach-bubble6","icon-speach-bubble7","icon-speach-bubble8","icon-speach-bubble9","icon-speach-bubble10","icon-speach-bubble11","icon-speach-bubble12","icon-speach-bubble13","icon-speach-bubbleasking","icon-speach-bubblecomic","icon-speach-bubblecomic2","icon-speach-bubblecomic3","icon-speach-bubblecomic4","icon-speach-bubbledialog","icon-speach-bubbles","icon-speak","icon-speak-2","icon-speaker","icon-speaker-2","icon-spell-check","icon-spell-checkabc","icon-spermium","icon-spider","icon-spiderweb","icon-split-foursquarewindow","icon-split-horizontal","icon-split-horizontal2window","icon-split-vertical","icon-split-vertical2","icon-split-window","icon-spoder","icon-spoon","icon-sport-mode","icon-sports-clothings1","icon-sports-clothings2","icon-sports-shirt","icon-spot","icon-spray","icon-spread","icon-spring","icon-spy","icon-squirrel","icon-ssl","icon-stamp","icon-stamp-2","icon-stapler","icon-star","icon-starfish","icon-start","icon-start-3","icon-star-track","icon-start-ways","icon-statistic","icon-st-basilscathedral","icon-stethoscope","icon-stop","icon-stop--2","icon-stop-music","icon-stopwatch","icon-stopwatch-2","icon-storm","icon-st-paulscathedral","icon-street-view","icon-street-view2","icon-strikethrough-text","icon-stroller","icon-structure","icon-student-female","icon-student-hat","icon-student-hat2","icon-student-male","icon-student-malefemale","icon-students","icon-studio-flash","icon-studio-lightbox","icon-suit","icon-suitcase","icon-sum","icon-sum-2","icon-summer","icon-sun","icon-sun-cloudyrain","icon-sunglasses","icon-sunglasses-2","icon-sunglasses-3","icon-sunglasses-smiley","icon-sunglasses-smiley2","icon-sunglasses-w","icon-sunglasses-w2","icon-sunglasses-w3","icon-sunrise","icon-sunset","icon-superman","icon-support","icon-surprise","icon-sushi","icon-sweden","icon-swimming","icon-swimming-short","icon-swimmwear","icon-switch","icon-switzerland","icon-sync","icon-sync-cloud","icon-synchronize","icon-synchronize-2","icon-tablet","icon-tablet-2","icon-tablet-3","icon-tablet-orientation","icon-tablet-phone","icon-tablet-secure","icon-tablet-vertical","icon-tactic","icon-tag","icon-tag-2","icon-tag-3","icon-tag-4","icon-tag-5","icon-taj-mahal","icon-talk-man","icon-tap","icon-target","icon-target-market","icon-taurus","icon-taurus-2","icon-taxi","icon-taxi-2","icon-taxi-sign","icon-teacher","icon-teapot","icon-teddy-bear","icon-tee-mug","icon-telephone","icon-telephone-2","icon-telescope","icon-temperature","icon-temperature-2","icon-temperature-3","icon-temple","icon-tennis","icon-tennis-ball","icon-tent","icon-testimonal","icon-test-tube","icon-test-tube2","icon-text-box","icon-text-effect","icon-text-highlightcolor","icon-text-paragraph","icon-thailand","icon-the-whitehouse","icon-this-sideup","icon-thread","icon-three-arrowfork","icon-three-fingers","icon-three-fingersdrag","icon-three-fingersdrag2","icon-three-fingerstouch","icon-thumb","icon-thumbs-downsmiley","icon-thumbs-upsmiley","icon-thunder","icon-thunderstorm","icon-ticket","icon-tie","icon-tie-2","icon-tie-3","icon-tie-4","icon-tiger","icon-time-backup","icon-time-bomb","icon-time-clock","icon-time-fire","icon-time-machine","icon-timer","icon-timer-2","icon-time-window","icon-to-bottom","icon-to-bottom2","icon-token-","icon-to-left","icon-tomato","icon-tongue","icon-tooth","icon-tooth-2","icon-top-tobottom","icon-to-right","icon-to-top","icon-to-top2","icon-touch-window","icon-tourch","icon-tower","icon-tower-2","icon-tower-bridge","icon-trace","icon-tractor","icon-traffic-light","icon-traffic-light2","icon-train","icon-train-2","icon-tram","icon-transform","icon-transform-2","icon-transform-3","icon-transform-4","icon-trash-withmen","icon-tree","icon-tree-2","icon-tree-3","icon-tree-4","icon-tree-5","icon-trekking","icon-triangle-arrowdown","icon-triangle-arrowleft","icon-triangle-arrowright","icon-triangle-arrowup","icon-tripod-2","icon-tripod-andvideo","icon-tripod-withcamera","icon-tripod-withgopro","icon-trophy","icon-trophy-2","icon-truck","icon-trumpet","icon-t-shirt","icon-turkey","icon-turn-down","icon-turn-down2","icon-turn-downfromleft","icon-turn-downfromright","icon-turn-left","icon-turn-left3","icon-turn-right","icon-turn-right3","icon-turn-up","icon-turn-up2","icon-turtle","icon-tuxedo","icon-tv","icon-twister","icon-two-fingers","icon-two-fingersdrag","icon-two-fingersdrag2","icon-two-fingersscroll","icon-two-fingerstouch","icon-two-windows","icon-type-pass","icon-ukraine","icon-umbrela","icon-umbrella-2","icon-umbrella-3","icon-under-linetext","icon-undo","icon-united-kingdom","icon-united-states","icon-university","icon-university-2","icon-unlock","icon-unlock-2","icon-unlock-3","icon-up","icon-up-2","icon-up-3","icon-up-4","icon-up--down","icon-up--down3","icon-upgrade","icon-upload","icon-upload-2","icon-upload-tocloud","icon-upload-window","icon-uppercase-text","icon-upward","icon-url-window","icon-usb","icon-usb-2","icon-usb-cable","icon-user","icon-vase","icon-vector","icon-vector-2","icon-vector-3","icon-vector-4","icon-vector-5","icon-venn-diagram","icon-vest","icon-vest-2","icon-video","icon-video-2","icon-video-3","icon-video-4","icon-video-5","icon-video-6","icon-video-gamecontroller","icon-video-len","icon-video-len2","icon-video-photographer","icon-video-tripod","icon-vietnam","icon-view-height","icon-view-width","icon-virgo","icon-virgo-2","icon-virus","icon-virus-2","icon-virus-3","icon-visa","icon-voice","icon-voicemail","icon-volleyball","icon-volume-down","icon-volume-up","icon-vpn","icon-wacom-tablet","icon-waiter","icon-walkie-talkie","icon-wallet","icon-wallet-2","icon-wallet-3","icon-warehouse","icon-warning-window","icon-watch","icon-watch-2","icon-watch-3","icon-wave","icon-wave-2","icon-webcam","icon-weight-lift","icon-wheelbarrow","icon-wheelchair","icon-width-window","icon-wifi","icon-wifi-2","icon-wifi-keyboard","icon-windmill","icon-window","icon-window-2","icon-windows","icon-windows-2","icon-windsock","icon-wind-turbine","icon-windy","icon-wine-bottle","icon-wine-glass","icon-wink","icon-winter","icon-winter-2","icon-wireless","icon-witch","icon-witch-hat","icon-wizard","icon-wolf","icon-womanman","icon-woman-sign","icon-womans-underwear","icon-womans-underwear2","icon-women","icon-wonder-woman","icon-worker","icon-worker-clothes","icon-wrap-text","icon-wreath","icon-wrench","icon-x-ray","icon-yacht","icon-yes","icon-ying-yang","icon-z-a","icon-zebra","icon-zombie","icon-zoom-gesture");
	    foreach($font_awesome_icons as $font_awesome_icon){
			$replaces=array('fa fa-','pe-7s-');
			$font_awesome_name=str_replace($replaces,'',$font_awesome_icon);
			echo '<li><i class="'.$font_awesome_icon.'"></i> <span></span></li> ';
        }
	}
	/** 
	 * Generate list of image icons
	 * Currently 903 icons
	 */
	function herowp_generate_image_icons(){
	
		$herowp_icons_images=array("3D-Glass-circle.png","3D-glass.png","AOL.png","Airplane-circle.png","Airplane.png","Amex.png","Apple-Store.png","Arrow-1-circle.png","Arrow-square.png","Arrow.png","Attach-circle.png","Attach.png","Attachment-square.png","Backpack-circle.png","Backpack-square.png","Backpack.png","Backward.png","Bag-1-circle.png","Bag-1-square.png","Bag-1.png","Bag-2-circle.png","Bag-2-square.png","Bag-2.png","Bag-3-square.png","Bagel-square.png","Bagel.png","Ball-Baseball-circle.png","Ball-Baseball.png","Ball-Basketball-circle.png","Ball-Basketball.png","Ball-Billard-circle.png","Ball-Billiard.png","Ball-Bowling.png","Ball-Football-circle.png","Ball-Football.png","Ball-Rugby-circle.png","Ball-Rugby.png","Ball-Soccer-circle.png","Ball-Soccer.png","Ball-Tennis-circle.png","Ball-Tennis.png","Baseball-square.png","Basket-1-circle.png","Basket-1.png","Basket-2-circle.png","Basket-2.png","Basket-3.png","Basketball-square.png","Beboo.png","Behance.png","Bike-circle.png","Bike-square.png","Bike.png","Billar-square.png","Blog.png","Bluetooth-circle.png","Bluetooth.png","Bookmark-circle.png","Bookmark-square.png","Bowling-square.png","Box-1-circle.png","Box-1-square.png","Box-1.png","Box-2-circle.png","Box-2-square.png","Box-2.png","Box-3-circle.png","Box-3.png","Bulb-circle.png","Bulb-square.png","Burger-square.png","Cake-circle.png","Cake-square.png","Cake.png","Calculator-circle.png","Calculator-square.png","Calculator.png","Calendar-1-circle.png","Calendar-1-square.png","Calendar-1.png","Calendar-2-circle.png","Calendar-2-square.png","Calendar-2.png","Camera-1-square.png","Camera-1.png","Camera-2-circle.png","Camera-2-square.png","Camera-2.png","Camera-Roll-square.png","Camera-circle.png","Car-circle.png","Car-square.png","Car.png","Cart-1-circle.png","Cart-1.png","Cart-2-circle.png","Cart-2.png","Cart-3.png","Case-circle.png","Cash-Register-square.png","Cash-Register.png","Charger-circle.png","Charger-square.png","Charger.png","Chart-1-circle.png","Chart-1-square.png","Chart-1.png","Chart-2-circle.png","Chart-2-square.png","Chart-2.png","Chart-3-circle.png","Chart-3-square.png","Chart-3.png","Chart-4-circle.png","Chart-4-square.png","Chart-4.png","Chart-5-circle.png","Chart-5-square.png","Chart-5.png","Chart-6-circle.png","Chart-6-square.png","Chart-6.png","Chart-7-circle.png","Chart-7-square.png","Chart-7.png","Chash-Register-circle.png","Check-circle.png","Check.png","Chip-Dollar-square.png","Chip-Euro-square.png","Clapper-circle.png","Clapper-square.png","Clapper.png","Clock-1-circle.png","Clock-1-square.png","Clock-1.png","Clock-2-circle.png","Clock-2-square.png","Clock-2.png","Clock-3-square.png","Clock-Tower-circle.png","Clock-Tower.png","Cloud-1-circle.png","Cloud-1-square.png","Cloud-1.png","Cloud-10-circle.png","Cloud-11-circle.png","Cloud-12-circle.png","Cloud-13-circle.png","Cloud-14-circle.png","Cloud-15-circle.png","Cloud-16-circle.png","Cloud-2-circle.png","Cloud-2-square.png","Cloud-2.png","Cloud-3-circle.png","Cloud-3-square.png","Cloud-3.png","Cloud-4-circle.png","Cloud-4-square.png","Cloud-4.png","Cloud-5-circle.png","Cloud-5-square.png","Cloud-5.png","Cloud-6-circle.png","Cloud-6-square.png","Cloud-6.png","Cloud-7-circle.png","Cloud-8-circle.png","Cloud-9-circle.png","Cloud-Forecast-1.png","Cloud-Forecast-10.png","Cloud-Forecast-11.png","Cloud-Forecast-12.png","Cloud-Forecast-13.png","Cloud-Forecast-14.png","Cloud-Forecast-15.png","Cloud-Forecast-16.png","Cloud-Forecast-2.png","Cloud-Forecast-3.png","Cloud-Forecast-4.png","Cloud-Forecast-5.png","Cloud-Forecast-6.png","Cloud-Forecast-7.png","Cloud-Forecast-8.png","Cloud-Forecast-9.png","Cocktail-square.png","Coctail-circle.png","Coctail.png","Coffee-1-circle.png","Coffee-2-circle.png","Coffee.png","Compass-circle.png","Compass-square.png","Compass.png","Credit-Card-circle.png","Credit-Card-square.png","Credit-Card.png","Crop-circle.png","Crop-square.png","Crop.png","Cross-circle.png","Cup-circle.png","Cup-square.png","Cutlery-2.png","Cutlery.png","Data-1-square.png","Data-1.png","Data-2-circle.png","Data-2-square.png","Data-2.png","Data-3-circle.png","Data-3-square.png","Data-3.png","Data-circle.png","Degree.png","Delicious.png","Devianart.png","Diamond-circle.png","Diamond-square.png","Diamond.png","Digg.png","Discover.png","Dollar-1-circle.png","Dollar-2-circle.png","Dollar-square.png","Dollar.png","Dont-Disturb-circle.png","Dont-Disturb-square.png","Dont-Disturb.png","Donut-1-circle.png","Donut-1.png","Donut-2-circle.png","Donut-square.png","Download-1-circle.png","Download-1-square.png","Download-1.png","Download-2-circle.png","Download-2-square.png","Download-2.png","Download-3-square.png","Download-4-square.png","Download-5-square.png","Dribbble.png","Drink-1.png","Drink-2.png","Drinking-1-square.png","Drinking-2-square.png","Drinking-3-square.png","Drinking-circle.png","Drops-square.png","Eiffel-circle.png","Eiffel-square.png","Eiffel.png","Eject-circle.png","Eject.png","Envelope-1-circle.png","Envelope-1-square.png","Envelope-2-circle.png","Envelope-2-square.png","Envelope-3-circle.png","Envelope-3-square.png","Envelope-4-circle.png","Envelope-4-square.png","Envelope-5-circle.png","Envelope-5-square.png","Equaliser-2-circle.png","Equaliser-circle.png","Equalizer-1.png","Equalizer-2.png","Euro-1-circle.png","Euro-2-circle.png","Euro-square.png","Euro.png","Eye-circle.png","Eye-square.png","Eye.png","Facebook.png","Fahrenheit-circle.png","Fahrenheit.png","File-1-circle.png","File-1-square.png","File-1.png","File-10-circle.png","File-10-square.png","File-10.png","File-11-circle.png","File-11-square.png","File-11.png","File-12-circle.png","File-12-square.png","File-12.png","File-13-circle.png","File-13-square.png","File-13.png","File-14-circle.png","File-14-square.png","File-14.png","File-15-circle.png","File-15-square.png","File-15.png","File-16-circle.png","File-16.png","File-17-circle.png","File-17-square.png","File-17.png","File-18-circle.png","File-18-square.png","File-18.png","File-19-circle.png","File-19-square.png","File-19.png","File-2-circle.png","File-2-square.png","File-2.png","File-20-circle.png","File-20-square.png","File-20.png","File-21-circle.png","File-21-square.png","File-21.png","File-22-circle.png","File-22.png","File-3-circle.png","File-3-square.png","File-3.png","File-4-circle.png","File-4-square.png","File-4.png","File-5-circle.png","File-5-square.png","File-5.png","File-6-circle.png","File-6-square.png","File-6.png","File-7-circle.png","File-7-square.png","File-7.png","File-8-circle.png","File-8-square.png","File-8.png","File-9-circle.png","File-9-square.png","File-9.png","Film-Roll-circle.png","Film-Strip-circle.png","Film-Strip-square.png","Film-Strip.png","Film.png","Fire-circle.png","Fire-square.png","Fire.png","Flag-circle.png","Flag-square.png","Flag.png","Flickr.png","Folder-1-circle.png","Folder-1-square.png","Folder-1.png","Folder-2-circle.png","Folder-2-square.png","Folder-2.png","Folder-3-circle.png","Folder-3-square.png","Folder-3.png","Folder-4-circle.png","Folder-4-square.png","Folder-4.png","Folder-5-circle.png","Folder-5-square.png","Folder-5.png","Folder-6-circle.png","Folder-6-square.png","Folder-6.png","Football-square.png","Forecast-1-square.png","Forecast-10-square.png","Forecast-11-square.png","Forecast-12-square.png","Forecast-13-square.png","Forecast-14-square.png","Forecast-15-square.png","Forecast-16-square.png","Forecast-17-square.png","Forecast-18-square.png","Forecast-2-square.png","Forecast-3-square.png","Forecast-4-square.png","Forecast-5-square.png","Forecast-6-square.png","Forecast-7-square.png","Forecast-8-square.png","Forecast-9-square.png","Fork-knife-square.png","Forrst.png","Forward-1-circle.png","Forward-2-circle.png","Forward.png","Friendfeed.png","Fries-circle.png","Fries-square.png","Fries.png","GPS-circle.png","GPS-square.png","GPS.png","Gift-Box-square.png","GitHub.png","Glass-1-circle.png","Glass-1-square.png","Glass-1.png","Glass-2-circle.png","Glass-2-square.png","Glass-2.png","Glass-3-circle.png","Glass-3-square.png","Glass-3.png","Globe-circle.png","Globe-square.png","Globe.png","Gong-circle.png","Gong-square.png","Gong.png","Google-Plus.png","Google-play.png","Groveshark.png","Guitar-circle.png","Guitar-square.png","Guitar.png","HTML-5.png","Hamburger-circle.png","Handset-circle.png","Handset-square.png","Handset.png","Hat-circle.png","Hat-square.png","Hat.png","Headset-1-circle.png","Headset-1-square.png","Headset-1.png","Headset-2-circle.png","Headset-2-square.png","Headset-2.png","Heart-square.png","Hearth.png","Home-circle.png","Home-square.png","Home.png","Hot-Dog-square.png","Hot-Dog.png","ID-circle.png","ID.png","Ice-Cream-1-circle.png","Ice-Cream-2-circle.png","Ice-Cream-2.png","Ice-Cream.png","IceCream-1-square.png","IceCream-2-square.png","Id-square.png","Inbox-1-circle.png","Inbox-1.png","Inbox-2-circle.png","Inbox-2.png","Inbox-3-circle.png","Inbox-3.png","Info-circle.png","Info.png","Instagram.png","Island-circle.png","Island-square.png","Island.png","Key-1-square.png","Key-2-circle.png","Key-2-square.png","Key-2.png","Key.png","Label-circle.png","Label-square.png","Label.png","Laptop-square.png","Laptop.png","Last-Fm.png","Lemon-circle.png","Lemon-square.png","Lemon.png","Lightning-2-circle.png","Lightning-2-square.png","Lightning-circle.png","Lightning-square.png","Lightning.png","Like-circle.png","Like-square.png","Link-1-circle.png","Link-1-square.png","Link-1.png","Link-2-circle.png","Link-2-square.png","Link-2.png","Linkedin.png","Loading-circle.png","Loading-square.png","Loading.png","Lock-1-circle.png","Lock-1-square.png","Lock-1.png","Lock-2-circle.png","Lock-2-square.png","Lock-2.png","Loop-circle.png","Luggage-circle.png","Macbook-circle.png","Maestro.png","Mag-square.png","Mag.png","Map-PIn-1.png","Map-PIn-2.png","Map-PIn-3.png","Map-PIn-4.png","Map-PIn-5.png","Map-Pin-1-circle.png","Map-Pin-2-circle.png","Map-Pin-3-circle.png","Map-Pin-4-circle.png","Map-Pin-5-circle.png","MapPin-1-square.png","MapPin-2-square.png","MapPin-3-square.png","MapPin-4-square.png","Master-Card.png","Medal-circle.png","Medal-square.png","Medal.png","Megaphone-1-circle.png","Megaphone-1-square.png","Megaphone-2-circle.png","Megaphone-2-square.png","Megaphone-2.png","Megaphone.png","Microphone-1-circle.png","Microphone-1-square.png","Microphone-1.png","Microphone-2-circle.png","Microphone-2-square.png","Microphone-2.png","Microphone-3-circle.png","Microphone-3-square.png","Microphone-3.png","Minus-circle.png","Money-1.png","Money-2.png","Moon-circle.png","Moon-square.png","Moon.png","Music-Eject-square.png","Music-Equaliser-square.png","Music-Forward-square.png","Music-Forward2-square.png","Music-Pause-square.png","Music-Play-square.png","Music-Setup-square.png","Music-Sound1-square.png","Music-Sound2-square.png","Music-Sound3-square.png","Music-Stop-square.png","Mustache-circle.png","Mustache.png","Myspace.png","Network-1-circle.png","Network-1-square.png","Network-1.png","Network-2-circle.png","Network-2-square.png","Network-2.png","Network-3-circle.png","Network-3-square.png","Network-3.png","New-Email-square.png","New-Mail-1-circle.png","New-Mail-2-circle.png","New-Mail.png","New-Mail2.png","New-Mail3.png","New-Mail4.png","New-circle.png","New-square.png","New.png","News.png","Newspaper-circle.png","Newsvine.png","Nota-1-square.png","Nota-1.png","Nota-2-circle.png","Nota-2-square.png","Nota-2.png","Nota-circle.png","On-Off-square.png","On-off-circle.png","On-off.png","Open-Closed-circle.png","Open-Closed-square.png","Open.png","Origami-box.png","Paint-Roll-circle.png","Paint-Roll.png","Paint-Roller-square.png","Paper-Airplane-circle.png","Paper-Airplane.png","Passport-circle.png","Passport-square.png","Passport.png","Pause-circle.png","Pause.png","PayPal.png","Pen-circle.png","Pen-square.png","Pen.png","People-1-circle.png","People-1-square.png","People-1.png","People-2-circle.png","People-2-square.png","People-2.png","People-3-circle.png","People-3-square.png","People-3.png","People-4-circle.png","People-5-circle.png","People-6-circle.png","Person-1-square.png","Person-1.png","Person-2-square.png","Person-2.png","Person-3-square.png","Person-3.png","Photo-1-circle.png","Photo-1-square.png","Photo-1.png","Photo-2-circle.png","Photo-2-square.png","Photo-2.png","Piano-circle.png","Piano-square.png","Piano.png","Picasa.png","Pie-Chart-1-circle.png","Pie-Chart-2-circle.png","Pie-Chart-2-square.png","Pie-Chart-square.png","PieChart-1.png","PieChart-2.png","Pinterest.png","Pizza-circle.png","Pizza-square.png","Pizza.png","Plane-2-square.png","Plane-square.png","Plate-circle.png","Plate-square.png","Play-circle.png","Play.png","Plus-circle.png","Popcorn-circle.png","Popcorn-square.png","Popcorn.png","Printer-Fax-circle.png","Printer-square.png","Printer.png","Pyramids-circle.png","Pyramids-square.png","Pyramids.png","Quora.png","RSS.png","Race-Flag-circle.png","Race-Flag.png","Radio-circle.png","Radio-square.png","Radio.png","Rain-circle.png","Rain.png","Refresh-2-square.png","Refresh-2.png","Refresh-circle.png","Refresh-square.png","Refresh.png","Roster-circle.png","Roster-square.png","Roster.png","Rugby-square.png","Satallite-1-circle.png","Satallite-2-circle.png","Satellite-1-square.png","Satellite-1.png","Satellite-2-square.png","Satellite-2.png","Sausage-circle.png","Scale-1-circle.png","Scale-1-square.png","Scale-1.png","Scale-2-circle.png","Scale-2-square.png","Scale-2.png","Scale-3-circle.png","Scale-3-square.png","Scale-3.png","Scale-4-circle.png","Scale-4-square.png","Scale-4.png","Scissors-circle.png","Scissors-square.png","Scissors.png","Scooter-circle.png","Scooter-square.png","Scooter.png","Screen-1-circle.png","Screen-1-square.png","Screen-2-circle.png","Screen-2-square.png","Screen-2.png","Screen-3.png","Screen-4.png","Screen.png","Send-1-circle.png","Send-2-circle.png","Send-3-circle.png","Sent-1.png","Sent-2.png","Sent-3.png","Setting-1-circle.png","Setting-1.png","Setting-2-circle.png","Setting-2.png","Setting-3-circle.png","Setting-3.png","Setup-1-square.png","Setup-2-square.png","Setup-3-square.png","Setup-4-square.png","Share-This.png","Ship-circle.png","Ship-square.png","Ship.png","ShoppingBasket-1-square.png","ShoppingBasket-2-square.png","ShoppingCart-1-square.png","ShoppingCart-2-square.png","Shuffle-2-square.png","Shuffle-circle.png","Shuffle-square.png","Shuffle.png","Skype.png","Smartphone-1.png","Smartphone-2.png","Smartphone-circle.png","Smartphone-square.png","Soccer-Ball-square.png","Soundcloud.png","Speaker-circle.png","Speaker-square.png","Speaker.png","Speech-Bubble-1-circle.png","Speech-Bubble-10-circle.png","Speech-Bubble-2-circle.png","Speech-Bubble-3-circle.png","Speech-Bubble-4-circle.png","Speech-Bubble-5-circle.png","Speech-Bubble-6-circle.png","Speech-Bubble-7-circle.png","Speech-Bubble-8-circle.png","Speech-Bubble-9-circle.png","SpeechBubble-1-square.png","SpeechBubble-1.png","SpeechBubble-10-square.png","SpeechBubble-10.png","SpeechBubble-11-square.png","SpeechBubble-2-square.png","SpeechBubble-2.png","SpeechBubble-3-square.png","SpeechBubble-3.png","SpeechBubble-4-square.png","SpeechBubble-4.png","SpeechBubble-5-square.png","SpeechBubble-5.png","SpeechBubble-6.png","SpeechBubble-7-square.png","SpeechBubble-7.png","SpeechBubble-8-square.png","SpeechBubble-8.png","SpeechBubble-9-square.png","SpeechBubble-9.png","Star-circle.png","Star-square.png","Star.png","Stop-circle.png","Stop.png","Stopwatch-circle.png","Stopwatch.png","Store-circle.png","Store-square.png","Store.png","Stumbleupon.png","Suitcase-2-square.png","Suitcase-square.png","Suitcase.png","Sun-Shade-square.png","Sun-circle.png","Sun-square.png","Sun.png","Sunbed-circle.png","Sunbed-square.png","Sunbed.png","Tablet-1.png","Tablet-2.png","Tablet-circle.png","Tablet-square.png","Tablet2-square.png","Target-circle.png","Target-square.png","Tennis-square.png","Tent-square.png","Tent.png","Test-Tube-circle.png","Test-Tube-square.png","Test-Tube.png","Thank-square.png","Thermometer-circle.png","Thermometer-square.png","Thermometer.png","Tools-circle.png","Tools.png","Tornado-circle.png","Tornado-square.png","Tornado.png","Towel-square.png","Train-circle.png","Train-square.png","Train.png","Trash-circle.png","Trash-square.png","Trash.png","Tumblr.png","Turntable-circle.png","Turntable-square.png","Turntable.png","Twitter-2.png","Twitter.png","Umbrella-1-circle.png","Umbrella-1.png","Umbrella-2-circle.png","Umbrella-2.png","Umbrella-3-circle.png","Umbrella-3.png","Umbrella-4-circle.png","Umbrella-4.png","Upload-1-circle.png","Upload-1-square.png","Upload-1.png","Upload-2-circle.png","Upload-2-square.png","Upload-2.png","Upload-3-square.png","Upload-4-square.png","Upload-5-square.png","Valise.png","Vimeo.png","Visa.png","Voice-Mail-circle.png","Voice-Mail.png","VoiceMaile-square.png","Volume-1-circle.png","Volume-1.png","Volume-2-circle.png","Volume-2.png","Volume-3-circle.png","Volume-3.png","Wallet.png","Wind-1-circle.png","Wind-2-circle.png","Wind-3-circle.png","Wind-4-circle.png","Wind-5-circle.png","Wind-square.png","Wind.png","Wireless-square.png","Wordpress.png","Yahoo.png","Youtube.png","Zerply.png","Zing.png","Zoom-In.png","Zoom-Out-circle.png","Zoom-Out-square.png","Zoom-Out.png","Zoom-in-circle.png","Zoom-in-square.png","degree-circle.png","hamburger.png","hearth-circle.png","info-square.png","key-1-circle.png","like.png","mag-circle.png","newspaper-square.png","target.png","tent-circle.png");
		foreach($herowp_icons_images as $herowp_icon_image){
			//echo '<li><i class="'.$herowp_icon_image.'"></i> <span></span></li> ';
			$replace_class = str_replace('.png','',$herowp_icon_image);
			$class_image = strtolower($replace_class);
			echo '<li><img class="'.$class_image.'" src="'.get_template_directory_uri().'/images/icons/'.$herowp_icon_image.'"></li> ';
        }
	}
	/** 
	 * Generate animations
	 * Currently 35 animations
	 */
	/*function herowp_animations2($field_id, $block_id, $options, $selected){
		$options=array(
		'0' => 'No animations',
		'bounce'=>'bounce', 
		'flash'=> 'flash',
		'pulse'=>'pulse', 
		'rubberBand'=> 'rubberBand',
		'shake'=>'shake', 
		'swing'=> 'swing',
		'tada'=>'tada', 
		'wobble'=> 'wobble',
		'bounceIn'=>'bounceIn', 
		'bounceInDown'=> 'bounceInDown',
		'bounceInLeft'=>'bounceInLeft', 
		'bounceInRight'=> 'bounceInRight',
		'bounceInUp'=>'bounceInUp', 		
		'fadeIn'=>'fadeIn', 
		'fadeInDown'=> 'fadeInDown',
		'fadeInDownBig'=>'fadeInDownBig', 
		'fadeInLeft'=> 'fadeInLeft',
		'fadeInLeftBig'=>'fadeInLeftBig', 
		'fadeInRight'=> 'fadeInRight',
		'fadeInRightBig'=>'fadeInRightBig', 
		'fadeInUp'=> 'fadeInUp',
		'fadeInUpBig'=>'fadeInUpBig', 
		'flip'=>'flip', 
		'flipInX'=> 'flipInX',
		'flipInY'=>'flipInY', 		
		'lightSpeedIn'=>'lightSpeedIn', 
		'rotateIn'=> 'rotateIn',
		'rotateInDownLeft'=>'rotateInDownLeft', 
		'rotateInDownRight'=> 'rotateInDownRight',
		'rotateInUpLeft'=>'rotateInUpLeft', 
		'rotateInUpRight'=> 'rotateInUpRight',
		'slideInDown'=>'slideInDown', 
		'slideInLeft'=> 'slideInLeft',
		'slideInRight'=>'slideInRight', 
		'rollIn'=>'rollIn' );
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
			foreach($options as $key=>$value) {
				$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
			}
		$output .= '</select>';
		return $output;
	}*/
	
	/** 
	 * Generate animations
	 * Currently 28 animations
	 */
	function herowp_animations($field_id, $block_id, $options, $selected){
		$options=array(
	'0' => 'No animations',
		'fadeIn'=>'fadeIn',
		'fadeInUp'=>'fadeInUp',
		'fadeInUpLarge'=>'fadeInUpLarge',
		'fadeInDown'=>'fadeInDown',
		'fadeInDownLarge'=>'fadeInDownLarge',
		'fadeInLeft'=>'fadeInLeft',
		'fadeInLeftLarge'=>'fadeInLeftLarge',
		'fadeInRight'=>'fadeInRight',
		'fadeInRightLarge'=>'fadeInRightLarge',
		'bounceIn'=>'bounceIn',
		'bounceInLarge'=>'bounceInLarge',
		'bounceInUp'=>'bounceInUp',
		'bounceInUpLarge'=>'bounceInUpLarge',
		'bounceInDown'=>'bounceInDown',
		'bounceInDownLarge'=>'bounceInDownLarge',
		'bounceInLeft'=>'bounceInLeft',
		'bounceInLeftLarge'=>'bounceInLeftLarge',
		'bounceInRight'=>'bounceInRight',
		'bounceInRightLarge'=>'bounceInRightLarge',
		'zoomIn'=>'zoomIn',
		'zoomInUp'=>'zoomInUp',
		'zoomInUpLarge'=>'zoomInUpLarge',
		'zoomInDown'=>'zoomInDown',
		'zoomInDownLarge'=>'zoomInDownLarge',
		'zoomInLeft'=>'zoomInLeft',
		'zoomInLeftLarge'=>'zoomInLeftLarge',
		'zoomInRight'=>'zoomInRight',
		'zoomInRightLarge'=>'zoomInRightLarge'
		);
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
			foreach($options as $key=>$value) {
				$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
			}
		$output .= '</select>';
		return $output;
	}
	
	/** 
	 * Generate text alignment
	 * 
	 */
	function herowp_text_align($field_id, $block_id, $options, $selected){
		$options=array(
		'left' => 'Left',
		'center' => 'Center',
		'right' => 'Right',
		);
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
			foreach($options as $key=>$value) {
				$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
			}
		$output .= '</select>';
		return $output;
	}	
	
	
	/** 
	 * Generate background position
	 * 
	 */
	function herowp_bg_position($field_id, $block_id, $options, $selected){
		$options=array(
		'top left' => 'Top Left',
		'top right' => 'Top Right',
		'top center' => 'Top Center',		
		'bottom left' => 'Bottom Left',
		'cottom right' => 'Bottom Right',
		'bottom center' => 'Bottom Center',
		'center' => 'Center'
		);
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
			foreach($options as $key=>$value) {
				$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
			}
		$output .= '</select>';
		return $output;
	}		
	
	
	/** 
	 * Generate text transform
	 * 
	 */
	function herowp_text_transform($field_id, $block_id, $options, $selected){
		$options=array(
		'0' => 'Select text transform',
		'none' => 'none',
		'capitalize' => 'capitalize',
		'uppercase' => 'uppercase',
		'lowercase' => 'lowercase',
		'initial' => 'initial',
		'inherit' => 'inherit',
		);
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
			foreach($options as $key=>$value) {
				$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
			}
		$output .= '</select>';
		return $output;
	}	
	
	/** 
	 * Generate google font family
	 * 
	 */
	function herowp_font_family($field_id, $block_id, $options, $selected){
		$options=array(
		'0' => 'Select Desired Font',
		"ABeeZee" => "ABeeZee",
		"Abel" => "Abel",
		"Abril Fatface" => "Abril Fatface",
		"Aclonica" => "Aclonica",
		"Acme" => "Acme",
		"Actor" => "Actor",
		"Adamina" => "Adamina",
		"Advent Pro" => "Advent Pro",
		"Aguafina Script" => "Aguafina Script",
		"Akronim" => "Akronim",
		"Aladin" => "Aladin",
		"Aldrich" => "Aldrich",
		"Alegreya" => "Alegreya",
		"Alegreya SC" => "Alegreya SC",
		"Alex Brush" => "Alex Brush",
		"Alfa Slab One" => "Alfa Slab One",
		"Alice" => "Alice",
		"Alike" => "Alike",
		"Alike Angular" => "Alike Angular",
		"Allan" => "Allan",
		"Allerta" => "Allerta",
		"Allerta Stencil" => "Allerta Stencil",
		"Allura" => "Allura",
		"Almendra" => "Almendra",
		"Almendra Display" => "Almendra Display",
		"Almendra SC" => "Almendra SC",
		"Amarante" => "Amarante",
		"Amaranth" => "Amaranth",
		"Amatic SC" => "Amatic SC",
		"Amethysta" => "Amethysta",
		"Anaheim" => "Anaheim",
		"Andada" => "Andada",
		"Andika" => "Andika",
		"Angkor" => "Angkor",
		"Annie Use Your Telescope" => "Annie Use Your Telescope",
		"Anonymous Pro" => "Anonymous Pro",
		"Antic" => "Antic",
		"Antic Didone" => "Antic Didone",
		"Antic Slab" => "Antic Slab",
		"Anton" => "Anton",
		"Arapey" => "Arapey",
		"Arbutus" => "Arbutus",
		"Arbutus Slab" => "Arbutus Slab",
		"Architects Daughter" => "Architects Daughter",
		"Archivo Black" => "Archivo Black",
		"Archivo Narrow" => "Archivo Narrow",
		"Arimo" => "Arimo",
		"Arizonia" => "Arizonia",
		"Armata" => "Armata",
		"Artifika" => "Artifika",
		"Arvo" => "Arvo",
		"Asap" => "Asap",
		"Asset" => "Asset",
		"Astloch" => "Astloch",
		"Asul" => "Asul",
		"Atomic Age" => "Atomic Age",
		"Aubrey" => "Aubrey",
		"Audiowide" => "Audiowide",
		"Autour One" => "Autour One",
		"Average" => "Average",
		"Average Sans" => "Average Sans",
		"Averia Gruesa Libre" => "Averia Gruesa Libre",
		"Averia Libre" => "Averia Libre",
		"Averia Sans Libre" => "Averia Sans Libre",
		"Averia Serif Libre" => "Averia Serif Libre",
		"Bad Script" => "Bad Script",
		"Balthazar" => "Balthazar",
		"Bangers" => "Bangers",
		"Basic" => "Basic",
		"Battambang" => "Battambang",
		"Baumans" => "Baumans",
		"Bayon" => "Bayon",
		"Belgrano" => "Belgrano",
		"Belleza" => "Belleza",
		"BenchNine" => "BenchNine",
		"Bentham" => "Bentham",
		"Berkshire Swash" => "Berkshire Swash",
		"Bevan" => "Bevan",
		"Bigelow Rules" => "Bigelow Rules",
		"Bigshot One" => "Bigshot One",
		"Bilbo" => "Bilbo",
		"Bilbo Swash Caps" => "Bilbo Swash Caps",
		"Bitter" => "Bitter",
		"Black Ops One" => "Black Ops One",
		"Bokor" => "Bokor",
		"Bonbon" => "Bonbon",
		"Boogaloo" => "Boogaloo",
		"Bowlby One" => "Bowlby One",
		"Bowlby One SC" => "Bowlby One SC",
		"Brawler" => "Brawler",
		"Bree Serif" => "Bree Serif",
		"Bubblegum Sans" => "Bubblegum Sans",
		"Bubbler One" => "Bubbler One",
		"Buda" => "Buda",
		"Buenard" => "Buenard",
		"Butcherman" => "Butcherman",
		"Butterfly Kids" => "Butterfly Kids",
		"Cabin" => "Cabin",
		"Cabin Condensed" => "Cabin Condensed",
		"Cabin Sketch" => "Cabin Sketch",
		"Caesar Dressing" => "Caesar Dressing",
		"Cagliostro" => "Cagliostro",
		"Calligraffitti" => "Calligraffitti",
		"Cambo" => "Cambo",
		"Candal" => "Candal",
		"Cantarell" => "Cantarell",
		"Cantata One" => "Cantata One",
		"Cantora One" => "Cantora One",
		"Capriola" => "Capriola",
		"Cardo" => "Cardo",
		"Carme" => "Carme",
		"Carrois Gothic" => "Carrois Gothic",
		"Carrois Gothic SC" => "Carrois Gothic SC",
		"Carter One" => "Carter One",
		"Caudex" => "Caudex",
		"Cedarville Cursive" => "Cedarville Cursive",
		"Ceviche One" => "Ceviche One",
		"Changa One" => "Changa One",
		"Chango" => "Chango",
		"Chau Philomene One" => "Chau Philomene One",
		"Chela One" => "Chela One",
		"Chelsea Market" => "Chelsea Market",
		"Chenla" => "Chenla",
		"Cherry Cream Soda" => "Cherry Cream Soda",
		"Cherry Swash" => "Cherry Swash",
		"Chewy" => "Chewy",
		"Chicle" => "Chicle",
		"Chivo" => "Chivo",
		"Cinzel" => "Cinzel",
		"Cinzel Decorative" => "Cinzel Decorative",
		"Clicker Script" => "Clicker Script",
		"Coda" => "Coda",
		"Coda Caption" => "Coda Caption",
		"Codystar" => "Codystar",
		"Combo" => "Combo",
		"Comfortaa" => "Comfortaa",
		"Coming Soon" => "Coming Soon",
		"Concert One" => "Concert One",
		"Condiment" => "Condiment",
		"Content" => "Content",
		"Contrail One" => "Contrail One",
		"Convergence" => "Convergence",
		"Cookie" => "Cookie",
		"Copse" => "Copse",
		"Corben" => "Corben",
		"Courgette" => "Courgette",
		"Cousine" => "Cousine",
		"Coustard" => "Coustard",
		"Covered By Your Grace" => "Covered By Your Grace",
		"Crafty Girls" => "Crafty Girls",
		"Creepster" => "Creepster",
		"Crete Round" => "Crete Round",
		"Crimson Text" => "Crimson Text",
		"Croissant One" => "Croissant One",
		"Crushed" => "Crushed",
		"Cuprum" => "Cuprum",
		"Cutive" => "Cutive",
		"Cutive Mono" => "Cutive Mono",
		"Damion" => "Damion",
		"Dancing Script" => "Dancing Script",
		"Dangrek" => "Dangrek",
		"Dawning of a New Day" => "Dawning of a New Day",
		"Days One" => "Days One",
		"Delius" => "Delius",
		"Delius Swash Caps" => "Delius Swash Caps",
		"Delius Unicase" => "Delius Unicase",
		"Della Respira" => "Della Respira",
		"Denk One" => "Denk One",
		"Devonshire" => "Devonshire",
		"Didact Gothic" => "Didact Gothic",
		"Diplomata" => "Diplomata",
		"Diplomata SC" => "Diplomata SC",
		"Domine" => "Domine",
		"Donegal One" => "Donegal One",
		"Doppio One" => "Doppio One",
		"Dorsa" => "Dorsa",
		"Dosis" => "Dosis",
		"Dr Sugiyama" => "Dr Sugiyama",
		"Droid Sans" => "Droid Sans",
		"Droid Sans Mono" => "Droid Sans Mono",
		"Droid Serif" => "Droid Serif",
		"Duru Sans" => "Duru Sans",
		"Dynalight" => "Dynalight",
		"EB Garamond" => "EB Garamond",
		"Eagle Lake" => "Eagle Lake",
		"Eater" => "Eater",
		"Economica" => "Economica",
		"Electrolize" => "Electrolize",
		"Elsie" => "Elsie",
		"Elsie Swash Caps" => "Elsie Swash Caps",
		"Emblema One" => "Emblema One",
		"Emilys Candy" => "Emilys Candy",
		"Engagement" => "Engagement",
		"Englebert" => "Englebert",
		"Enriqueta" => "Enriqueta",
		"Erica One" => "Erica One",
		"Esteban" => "Esteban",
		"Euphoria Script" => "Euphoria Script",
		"Ewert" => "Ewert",
		"Exo" => "Exo",
		"Expletus Sans" => "Expletus Sans",
		"Fanwood Text" => "Fanwood Text",
		"Fascinate" => "Fascinate",
		"Fascinate Inline" => "Fascinate Inline",
		"Faster One" => "Faster One",
		"Fasthand" => "Fasthand",
		"Federant" => "Federant",
		"Federo" => "Federo",
		"Felipa" => "Felipa",
		"Fenix" => "Fenix",
		"Finger Paint" => "Finger Paint",
		"Fjalla One" => "Fjalla One",
		"Fjord One" => "Fjord One",
		"Flamenco" => "Flamenco",
		"Flavors" => "Flavors",
		"Fondamento" => "Fondamento",
		"Fontdiner Swanky" => "Fontdiner Swanky",
		"Forum" => "Forum",
		"Francois One" => "Francois One",
		"Freckle Face" => "Freckle Face",
		"Fredericka the Great" => "Fredericka the Great",
		"Fredoka One" => "Fredoka One",
		"Freehand" => "Freehand",
		"Fresca" => "Fresca",
		"Frijole" => "Frijole",
		"Fruktur" => "Fruktur",
		"Fugaz One" => "Fugaz One",
		"GFS Didot" => "GFS Didot",
		"GFS Neohellenic" => "GFS Neohellenic",
		"Gabriela" => "Gabriela",
		"Gafata" => "Gafata",
		"Galdeano" => "Galdeano",
		"Galindo" => "Galindo",
		"Gentium Basic" => "Gentium Basic",
		"Gentium Book Basic" => "Gentium Book Basic",
		"Geo" => "Geo",
		"Geostar" => "Geostar",
		"Geostar Fill" => "Geostar Fill",
		"Germania One" => "Germania One",
		"Gilda Display" => "Gilda Display",
		"Give You Glory" => "Give You Glory",
		"Glass Antiqua" => "Glass Antiqua",
		"Glegoo" => "Glegoo",
		"Gloria Hallelujah" => "Gloria Hallelujah",
		"Goblin One" => "Goblin One",
		"Gochi Hand" => "Gochi Hand",
		"Gorditas" => "Gorditas",
		"Goudy Bookletter 1911" => "Goudy Bookletter 1911",
		"Graduate" => "Graduate",
		"Grand Hotel" => "Grand Hotel",
		"Gravitas One" => "Gravitas One",
		"Great Vibes" => "Great Vibes",
		"Griffy" => "Griffy",
		"Gruppo" => "Gruppo",
		"Gudea" => "Gudea",
		"Habibi" => "Habibi",
		"Hammersmith One" => "Hammersmith One",
		"Hanalei" => "Hanalei",
		"Hanalei Fill" => "Hanalei Fill",
		"Handlee" => "Handlee",
		"Hanuman" => "Hanuman",
		"Happy Monkey" => "Happy Monkey",
		"Headland One" => "Headland One",
		"Henny Penny" => "Henny Penny",
		"Herr Von Muellerhoff" => "Herr Von Muellerhoff",
		"Holtwood One SC" => "Holtwood One SC",
		"Homemade Apple" => "Homemade Apple",
		"Homenaje" => "Homenaje",
		"IM Fell DW Pica" => "IM Fell DW Pica",
		"IM Fell DW Pica SC" => "IM Fell DW Pica SC",
		"IM Fell Double Pica" => "IM Fell Double Pica",
		"IM Fell Double Pica SC" => "IM Fell Double Pica SC",
		"IM Fell English" => "IM Fell English",
		"IM Fell English SC" => "IM Fell English SC",
		"IM Fell French Canon" => "IM Fell French Canon",
		"IM Fell French Canon SC" => "IM Fell French Canon SC",
		"IM Fell Great Primer" => "IM Fell Great Primer",
		"IM Fell Great Primer SC" => "IM Fell Great Primer SC",
		"Iceberg" => "Iceberg",
		"Iceland" => "Iceland",
		"Imprima" => "Imprima",
		"Inconsolata" => "Inconsolata",
		"Inder" => "Inder",
		"Indie Flower" => "Indie Flower",
		"Inika" => "Inika",
		"Irish Grover" => "Irish Grover",
		"Istok Web" => "Istok Web",
		"Italiana" => "Italiana",
		"Italianno" => "Italianno",
		"Jacques Francois" => "Jacques Francois",
		"Jacques Francois Shadow" => "Jacques Francois Shadow",
		"Jim Nightshade" => "Jim Nightshade",
		"Jockey One" => "Jockey One",
		"Jolly Lodger" => "Jolly Lodger",
		"Josefin Sans" => "Josefin Sans",
		"Josefin Slab" => "Josefin Slab",
		"Joti One" => "Joti One",
		"Judson" => "Judson",
		"Julee" => "Julee",
		"Julius Sans One" => "Julius Sans One",
		"Junge" => "Junge",
		"Jura" => "Jura",
		"Just Another Hand" => "Just Another Hand",
		"Just Me Again Down Here" => "Just Me Again Down Here",
		"Kameron" => "Kameron",
		"Karla" => "Karla",
		"Kaushan Script" => "Kaushan Script",
		"Kavoon" => "Kavoon",
		"Keania One" => "Keania One",
		"Kelly Slab" => "Kelly Slab",
		"Kenia" => "Kenia",
		"Khmer" => "Khmer",
		"Kite One" => "Kite One",
		"Knewave" => "Knewave",
		"Kotta One" => "Kotta One",
		"Koulen" => "Koulen",
		"Kranky" => "Kranky",
		"Kreon" => "Kreon",
		"Kristi" => "Kristi",
		"Krona One" => "Krona One",
		"La Belle Aurore" => "La Belle Aurore",
		"Lancelot" => "Lancelot",
		"Lato" => "Lato",
		"League Script" => "League Script",
		"Leckerli One" => "Leckerli One",
		"Ledger" => "Ledger",
		"Lekton" => "Lekton",
		"Lemon" => "Lemon",
		"Libre Baskerville" => "Libre Baskerville",
		"Life Savers" => "Life Savers",
		"Lilita One" => "Lilita One",
		"Limelight" => "Limelight",
		"Linden Hill" => "Linden Hill",
		"Lobster" => "Lobster",
		"Lobster Two" => "Lobster Two",
		"Londrina Outline" => "Londrina Outline",
		"Londrina Shadow" => "Londrina Shadow",
		"Londrina Sketch" => "Londrina Sketch",
		"Londrina Solid" => "Londrina Solid",
		"Lora" => "Lora",
		"Love Ya Like A Sister" => "Love Ya Like A Sister",
		"Loved by the King" => "Loved by the King",
		"Lovers Quarrel" => "Lovers Quarrel",
		"Luckiest Guy" => "Luckiest Guy",
		"Lusitana" => "Lusitana",
		"Lustria" => "Lustria",
		"Macondo" => "Macondo",
		"Macondo Swash Caps" => "Macondo Swash Caps",
		"Magra" => "Magra",
		"Maiden Orange" => "Maiden Orange",
		"Mako" => "Mako",
		"Marcellus" => "Marcellus",
		"Marcellus SC" => "Marcellus SC",
		"Marck Script" => "Marck Script",
		"Margarine" => "Margarine",
		"Marko One" => "Marko One",
		"Marmelad" => "Marmelad",
		"Marvel" => "Marvel",
		"Mate" => "Mate",
		"Mate SC" => "Mate SC",
		"Maven Pro" => "Maven Pro",
		"McLaren" => "McLaren",
		"Meddon" => "Meddon",
		"MedievalSharp" => "MedievalSharp",
		"Medula One" => "Medula One",
		"Megrim" => "Megrim",
		"Meie Script" => "Meie Script",
		"Merienda" => "Merienda",
		"Merienda One" => "Merienda One",
		"Merriweather" => "Merriweather",
		"Merriweather Sans" => "Merriweather Sans",
		"Metal" => "Metal",
		"Metal Mania" => "Metal Mania",
		"Metamorphous" => "Metamorphous",
		"Metrophobic" => "Metrophobic",
		"Michroma" => "Michroma",
		"Milonga" => "Milonga",
		"Miltonian" => "Miltonian",
		"Miltonian Tattoo" => "Miltonian Tattoo",
		"Miniver" => "Miniver",
		"Miss Fajardose" => "Miss Fajardose",
		"Modern Antiqua" => "Modern Antiqua",
		"Molengo" => "Molengo",
		"Molle" => "Molle",
		"Monda" => "Monda",
		"Monofett" => "Monofett",
		"Monoton" => "Monoton",
		"Monsieur La Doulaise" => "Monsieur La Doulaise",
		"Montaga" => "Montaga",
		"Montez" => "Montez",
		"Montserrat" => "Montserrat",
		"Montserrat Alternates" => "Montserrat Alternates",
		"Montserrat Subrayada" => "Montserrat Subrayada",
		"Moul" => "Moul",
		"Moulpali" => "Moulpali",
		"Mountains of Christmas" => "Mountains of Christmas",
		"Mouse Memoirs" => "Mouse Memoirs",
		"Mr Bedfort" => "Mr Bedfort",
		"Mr Dafoe" => "Mr Dafoe",
		"Mr De Haviland" => "Mr De Haviland",
		"Mrs Saint Delafield" => "Mrs Saint Delafield",
		"Mrs Sheppards" => "Mrs Sheppards",
		"Muli" => "Muli",
		"Mystery Quest" => "Mystery Quest",
		"Neucha" => "Neucha",
		"Neuton" => "Neuton",
		"New Rocker" => "New Rocker",
		"News Cycle" => "News Cycle",
		"Niconne" => "Niconne",
		"Nixie One" => "Nixie One",
		"Nobile" => "Nobile",
		"Nokora" => "Nokora",
		"Norican" => "Norican",
		"Nosifer" => "Nosifer",
		"Nothing You Could Do" => "Nothing You Could Do",
		"Noticia Text" => "Noticia Text",
		"Noto Sans" => "Noto Sans",
		"Noto Serif" => "Noto Serif",
		"Nova Cut" => "Nova Cut",
		"Nova Flat" => "Nova Flat",
		"Nova Mono" => "Nova Mono",
		"Nova Oval" => "Nova Oval",
		"Nova Round" => "Nova Round",
		"Nova Script" => "Nova Script",
		"Nova Slim" => "Nova Slim",
		"Nova Square" => "Nova Square",
		"Numans" => "Numans",
		"Nunito" => "Nunito",
		"Odor Mean Chey" => "Odor Mean Chey",
		"Offside" => "Offside",
		"Old Standard TT" => "Old Standard TT",
		"Oldenburg" => "Oldenburg",
		"Oleo Script" => "Oleo Script",
		"Oleo Script Swash Caps" => "Oleo Script Swash Caps",
		"Open Sans" => "Open Sans",
		"Open Sans Condensed" => "Open Sans Condensed",
		"Oranienbaum" => "Oranienbaum",
		"Orbitron" => "Orbitron",
		"Oregano" => "Oregano",
		"Orienta" => "Orienta",
		"Original Surfer" => "Original Surfer",
		"Oswald" => "Oswald",
		"Over the Rainbow" => "Over the Rainbow",
		"Overlock" => "Overlock",
		"Overlock SC" => "Overlock SC",
		"Ovo" => "Ovo",
		"Oxygen" => "Oxygen",
		"Oxygen Mono" => "Oxygen Mono",
		"PT Mono" => "PT Mono",
		"PT Sans" => "PT Sans",
		"PT Sans Caption" => "PT Sans Caption",
		"PT Sans Narrow" => "PT Sans Narrow",
		"PT Serif" => "PT Serif",
		"PT Serif Caption" => "PT Serif Caption",
		"Pacifico" => "Pacifico",
		"Paprika" => "Paprika",
		"Parisienne" => "Parisienne",
		"Passero One" => "Passero One",
		"Passion One" => "Passion One",
		"Patrick Hand" => "Patrick Hand",
		"Patrick Hand SC" => "Patrick Hand SC",
		"Patua One" => "Patua One",
		"Paytone One" => "Paytone One",
		"Peralta" => "Peralta",
		"Permanent Marker" => "Permanent Marker",
		"Petit Formal Script" => "Petit Formal Script",
		"Petrona" => "Petrona",
		"Philosopher" => "Philosopher",
		"Piedra" => "Piedra",
		"Pinyon Script" => "Pinyon Script",
		"Pirata One" => "Pirata One",
		"Plaster" => "Plaster",
		"Play" => "Play",
		"Playball" => "Playball",
		"Playfair Display" => "Playfair Display",
		"Playfair Display SC" => "Playfair Display SC",
		"Podkova" => "Podkova",
		"Poiret One" => "Poiret One",
		"Poller One" => "Poller One",
		"Poly" => "Poly",
		"Pompiere" => "Pompiere",
		"Pontano Sans" => "Pontano Sans",
		"Port Lligat Sans" => "Port Lligat Sans",
		"Port Lligat Slab" => "Port Lligat Slab",
		"Prata" => "Prata",
		"Preahvihear" => "Preahvihear",
		"Press Start 2P" => "Press Start 2P",
		"Princess Sofia" => "Princess Sofia",
		"Prociono" => "Prociono",
		"Prosto One" => "Prosto One",
		"Puritan" => "Puritan",
		"Purple Purse" => "Purple Purse",
		"Quando" => "Quando",
		"Quantico" => "Quantico",
		"Quattrocento" => "Quattrocento",
		"Quattrocento Sans" => "Quattrocento Sans",
		"Questrial" => "Questrial",
		"Quicksand" => "Quicksand",
		"Quintessential" => "Quintessential",
		"Qwigley" => "Qwigley",
		"Racing Sans One" => "Racing Sans One",
		"Radley" => "Radley",
		"Raleway" => "Raleway",
		"Raleway Dots" => "Raleway Dots",
		"Rambla" => "Rambla",
		"Rammetto One" => "Rammetto One",
		"Ranchers" => "Ranchers",
		"Rancho" => "Rancho",
		"Rationale" => "Rationale",
		"Redressed" => "Redressed",
		"Reenie Beanie" => "Reenie Beanie",
		"Revalia" => "Revalia",
		"Ribeye" => "Ribeye",
		"Ribeye Marrow" => "Ribeye Marrow",
		"Righteous" => "Righteous",
		"Risque" => "Risque",
		"Roboto" => "Roboto",
		"Roboto Condensed" => "Roboto Condensed",
		"Roboto Slab" => "Roboto Slab",
		"Rochester" => "Rochester",
		"Rock Salt" => "Rock Salt",
		"Rokkitt" => "Rokkitt",
		"Romanesco" => "Romanesco",
		"Ropa Sans" => "Ropa Sans",
		"Rosario" => "Rosario",
		"Rosarivo" => "Rosarivo",
		"Rouge Script" => "Rouge Script",
		"Ruda" => "Ruda",
		"Rufina" => "Rufina",
		"Ruge Boogie" => "Ruge Boogie",
		"Ruluko" => "Ruluko",
		"Rum Raisin" => "Rum Raisin",
		"Ruslan Display" => "Ruslan Display",
		"Russo One" => "Russo One",
		"Ruthie" => "Ruthie",
		"Rye" => "Rye",
		"Sacramento" => "Sacramento",
		"Sail" => "Sail",
		"Salsa" => "Salsa",
		"Sanchez" => "Sanchez",
		"Sancreek" => "Sancreek",
		"Sansita One" => "Sansita One",
		"Sarina" => "Sarina",
		"Satisfy" => "Satisfy",
		"Scada" => "Scada",
		"Schoolbell" => "Schoolbell",
		"Seaweed Script" => "Seaweed Script",
		"Sevillana" => "Sevillana",
		"Seymour One" => "Seymour One",
		"Shadows Into Light" => "Shadows Into Light",
		"Shadows Into Light Two" => "Shadows Into Light Two",
		"Shanti" => "Shanti",
		"Share" => "Share",
		"Share Tech" => "Share Tech",
		"Share Tech Mono" => "Share Tech Mono",
		"Shojumaru" => "Shojumaru",
		"Short Stack" => "Short Stack",
		"Siemreap" => "Siemreap",
		"Sigmar One" => "Sigmar One",
		"Signika" => "Signika",
		"Signika Negative" => "Signika Negative",
		"Simonetta" => "Simonetta",
		"Sintony" => "Sintony",
		"Sirin Stencil" => "Sirin Stencil",
		"Six Caps" => "Six Caps",
		"Skranji" => "Skranji",
		"Slackey" => "Slackey",
		"Smokum" => "Smokum",
		"Smythe" => "Smythe",
		"Sniglet" => "Sniglet",
		"Snippet" => "Snippet",
		"Snowburst One" => "Snowburst One",
		"Sofadi One" => "Sofadi One",
		"Sofia" => "Sofia",
		"Sonsie One" => "Sonsie One",
		"Sorts Mill Goudy" => "Sorts Mill Goudy",
		"Source Code Pro" => "Source Code Pro",
		"Source Sans Pro" => "Source Sans Pro",
		"Special Elite" => "Special Elite",
		"Spicy Rice" => "Spicy Rice",
		"Spinnaker" => "Spinnaker",
		"Spirax" => "Spirax",
		"Squada One" => "Squada One",
		"Stalemate" => "Stalemate",
		"Stalinist One" => "Stalinist One",
		"Stardos Stencil" => "Stardos Stencil",
		"Stint Ultra Condensed" => "Stint Ultra Condensed",
		"Stint Ultra Expanded" => "Stint Ultra Expanded",
		"Stoke" => "Stoke",
		"Strait" => "Strait",
		"Sue Ellen Francisco" => "Sue Ellen Francisco",
		"Sunshiney" => "Sunshiney",
		"Supermercado One" => "Supermercado One",
		"Suwannaphum" => "Suwannaphum",
		"Swanky and Moo Moo" => "Swanky and Moo Moo",
		"Syncopate" => "Syncopate",
		"Tangerine" => "Tangerine",
		"Taprom" => "Taprom",
		"Tauri" => "Tauri",
		"Telex" => "Telex",
		"Tenor Sans" => "Tenor Sans",
		"Text Me One" => "Text Me One",
		"The Girl Next Door" => "The Girl Next Door",
		"Tienne" => "Tienne",
		"Tinos" => "Tinos",
		"Titan One" => "Titan One",
		"Titillium Web" => "Titillium Web",
		"Trade Winds" => "Trade Winds",
		"Trocchi" => "Trocchi",
		"Trochut" => "Trochut",
		"Trykker" => "Trykker",
		"Tulpen One" => "Tulpen One",
		"Ubuntu" => "Ubuntu",
		"Ubuntu Condensed" => "Ubuntu Condensed",
		"Ubuntu Mono" => "Ubuntu Mono",
		"Ultra" => "Ultra",
		"Uncial Antiqua" => "Uncial Antiqua",
		"Underdog" => "Underdog",
		"Unica One" => "Unica One",
		"UnifrakturCook" => "UnifrakturCook",
		"UnifrakturMaguntia" => "UnifrakturMaguntia",
		"Unkempt" => "Unkempt",
		"Unlock" => "Unlock",
		"Unna" => "Unna",
		"VT323" => "VT323",
		"Vampiro One" => "Vampiro One",
		"Varela" => "Varela",
		"Varela Round" => "Varela Round",
		"Vast Shadow" => "Vast Shadow",
		"Vibur" => "Vibur",
		"Vidaloka" => "Vidaloka",
		"Viga" => "Viga",
		"Voces" => "Voces",
		"Volkhov" => "Volkhov",
		"Vollkorn" => "Vollkorn",
		"Voltaire" => "Voltaire",
		"Waiting for the Sunrise" => "Waiting for the Sunrise",
		"Wallpoet" => "Wallpoet",
		"Walter Turncoat" => "Walter Turncoat",
		"Warnes" => "Warnes",
		"Wellfleet" => "Wellfleet",
		"Wendy One" => "Wendy One",
		"Wire One" => "Wire One",
		"Yanone Kaffeesatz" => "Yanone Kaffeesatz",
		"Yellowtail" => "Yellowtail",
		"Yeseva One" => "Yeseva One",
		"Yesteryear" => "Yesteryear",
		"Zeyada" => "Zeyada"
		);
		$output = '<select id="'. $block_id .'_'.$field_id.'" name="aq_blocks['.$block_id.']['.$field_id.']">';
			foreach($options as $key=>$value) {
				$output .= '<option value="'.$key.'" '.selected( $selected, $key, false ).'>'.htmlspecialchars($value).'</option>';
			}
		$output .= '</select>';
		return $output;
	}
	
	/** 
	* herowp_unique_id()
	* Generate a unique id - for responsive CSS purposes
	*/
	function herowp_unique_id(){
    $output = 'herowp-block-id-';
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $charArray = str_split($chars);
		for($i = 0; $i < 9; $i++){
			$randItem = array_rand($charArray);
			$output .= "".$charArray[$randItem];
		}
		$output .= '-'.rand(99999999, 999999999);
		return $output;
	}	
	
	
	/** 
	* herowp_responsive_css_text()
	* Add the text for CSS responsive blocks option
	*/
	function herowp_responsive_css_text(){
		$output = 'Add responsive CSS properties for this block. <strong>Note: Add !important after each property.</strong> <br/> Example: <strong>margin-top:100px !important; margin-bottom:10px !important;</strong>. Do NOT add braces.';
		return $output;
	}	
	
	
	/** 
	* herowp_css_unique_id_add()
	* Return the unique id if CSS properties are set - like this id="unique id"-
	*/
	function herowp_css_unique_id_add(){
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		if ($herowp_responsive_320 || $herowp_responsive_480 || $herowp_responsive_768 || $herowp_responsive_960){
				$herowp_css_unique_id_add = 'id="'.$herowp_css_unique_id.'"';
			}
			else{
				$herowp_css_unique_id_add = '';
			}
		return $herowp_css_unique_id_add;
	}

	/** 
	* herowp_add_responsive_css()
	* Output the responsive CSS to the page
	*/	
	function herowp_add_responsive_css() {
					global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
						?>
							
<?php if ($herowp_responsive_320 || $herowp_responsive_480 || $herowp_responsive_768 || $herowp_responsive_960) { ?>
<style type="text/css">
<?php if (!empty($herowp_responsive_320)) { ?>
@media only screen and (min-width : 120px) and (max-width : 320px)
{
	#<?php echo $herowp_css_unique_id; ?>
	
	{
		<?php echo $herowp_responsive_320; ?>
	
	}
	
}
<?php } ?>
<?php if (!empty($herowp_responsive_480)) { ?>
@media only screen and (min-width : 321px) and (max-width : 480px) 
{
	#<?php echo $herowp_css_unique_id; ?>
	
	{
		<?php echo $herowp_responsive_480; ?>
	
	}
}
<?php } ?>
<?php if (!empty($herowp_responsive_768)) { ?>
@media only screen and (min-width : 481px) and (max-width : 768px) 
{
	#<?php echo $herowp_css_unique_id; ?>
	
	{
		<?php echo $herowp_responsive_768; ?>
	
	}
}	
<?php } ?>
<?php if (!empty($herowp_responsive_960)) { ?>
@media only screen and (min-width : 769px) and (max-width : 960px) 
{
	#<?php echo $herowp_css_unique_id; ?>
	
	{
		<?php echo $herowp_responsive_960; ?>
	
	}
}	
<?php } ?>
</style>
<?php /*end if ($herowp_responsive_320 || $herowp_responsive_480 || $herowp_responsive_768 || $herowp_responsive_960)*/ } ?>						
	
	<?php
	/*end herowp_add_responsive_css() */ }

	/** 
	* herowp_progress_bar_script()
	* Output JS code for progress bars block when user change some settings
	*/		
	function herowp_progress_bar_script() {
		global $progress_bar_color, $progress_track_color, $progress_bar_width, $speed, $herowp_css_unique_id;
		$herowp_css_unique_id = isset($herowp_css_unique_id) ? $herowp_css_unique_id : 'pie_progress';
	?>
	<script type="text/javascript">
		//Initialize pie progress bars
		jQuery(document).ready(function($){

		$('.<?php echo $herowp_css_unique_id; ?>').asPieProgress({
			namespace: 'pie_progress',
			speed: <?php $speed = !empty($speed) ? $speed : '60'; echo $speed; ?>, // speed of 1/100
			barsize: '<?php $progress_bar_width = !empty($progress_bar_width) ? $progress_bar_width : '8'; echo $progress_bar_width; ?>',
			barcolor: '<?php $progress_bar_color = !empty($progress_bar_color) ? $progress_bar_color : '#ff6131'; echo $progress_bar_color; ?>',
			trackcolor: '<?php $progress_track_color = !empty($progress_track_color) ? $progress_track_color : '#f2f2f2'; echo $progress_track_color; ?>',
		});

		// Start the progress bar only when it's in the viewport
		$(function() {
			$('.<?php echo $herowp_css_unique_id; ?>').waypoint(function() {
			$('.<?php echo $herowp_css_unique_id; ?>').asPieProgress('start');
		}, {
			offset: '80%'
		});
		});

		});
		</script>
	<?php
	}
	
	
/*end if(class_exists('AQ_Page_Builder'))*/ }