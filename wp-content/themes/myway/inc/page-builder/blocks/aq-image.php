<?php
class AQ_Image_Block extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Image',
			'size' => 'span4',
			'resizable' => 1,
			"img_preview"=>'image.png',
			'fa_icon'=>'fa fa-image'
			
		);
		
		//create the block
		parent::__construct('AQ_Image_Block', $block_options);
	}
	
	function form($instance) {
		
		//default values when WP DEBUG is set to true to avoid the undefined index warning

		if (!isset($image))  $image='';
		if (!isset($margintop))  $margintop='0';
		if (!isset($marginbottom))  $marginbottom='0';
		if (!isset($url))  $url='';
		if (!isset($animation_image))  $animation_image='fadeIn';
		if (!isset($delayanimationimage))  $delayanimationimage='0';	
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();		
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
        
		
			 <p class="description">
					<label for="<?php echo $this->get_field_id('image') ?>">
						Upload Image 
						<?php echo aq_field_upload('image', $block_id, $image) ?>
					</label>
				</p>
			
			<p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation_image') ?>">
						<strong>Image Animation:</strong> Select animation you want to use for the image.
							<?php $options = isset($options) ? isset($options) : null; ?>
							<?php echo herowp_animations('animation_image', $block_id, $options, $animation_image); ?>
					</label>
		    </p><!--Animation-->
           
		    <p class="description">
                    <label for="<?php echo $this->get_field_id('url') ?>">
                        <strong>Image Link</strong> Where should the image link. Leave blank for no link.
                        <?php echo aq_field_input('url', $block_id, $url, $size = 'full') ?>
                    </label>
			</p>
              
            <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>            
			  
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>

			
			<p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimationimage') ?>">
                        <strong>Animation delay:</strong> Enter the delay appear animation of image. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimationimage', $block_id, $delayanimationimage, $size = 'full') ?>
                    </label>
			</p><!--Animation delay-->
            
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>
        
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);
		
		//custom responsive CSS code
		herowp_add_responsive_css();		
		
	?>

<?php
//if animations are set/unset for the image
if ($animation_image != '0'){
	$data_animate_image='class="not-animated col-md-12 image-block" data-animate="'.$animation_image.'" data-delay="'.$delayanimationimage.'"';
}
else{
	$data_animate_image='col-md-12 image-block';
}
?> 
 
<div <?php echo herowp_css_unique_id_add(); ?> class="static_content" style="<?php echo 'margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;' ;?>"><!--static_content START-->	    
<?php if (strlen($url) >= 1) : ?>
	<a href="<?php echo $url ?>" target="_blank"><img src="<?php echo $image ?>" alt="<?php the_title();?>" <?php echo $data_animate_image;?>></a>
<?php else : ?>					
 	<img src="<?php echo $image ?>" alt="<?php the_title();?>" <?php echo $data_animate_image;?>>
<?php endif; ?>	
</div><!--static_content END-->

<?php
	}
	
}