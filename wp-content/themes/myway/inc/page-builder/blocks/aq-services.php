<?php
if(!class_exists('AQ_Box_Services')) {
	class AQ_Box_Services extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Services',
				'size' => 'span4',
				'resizable' => 1,
				"class_css"=>'bf_services',
				"img_preview"=>'services_01.png',
				'fa_icon'=>'fa fa-th-large'
			);
			
			//create the widget
			parent::__construct('AQ_Box_Services', $block_options);
			
			//add ajax functions
			add_action('wp_ajax_aq_block_box_10_add_new', array($this, 'add_box'));
			
		}
		function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
	
		if (!isset($items_on_row))  $items_on_row='1';
		if (!isset($animation))  $animation='zoomIn';
		if (!isset($delayanimation))  $delayanimation='0';
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($h2color))  $h2color='';
		if (!isset($descriptioncolor))  $descriptioncolor='';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
			$defaults = array(
				'boxes' => array(
					1 => array(
						'title' => 'Refreshing design',
						'subtitle' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.',
						'awesomefont' => 'fa fa-laptop',
						'service_url' => ''
					),
				),
				'type'	=> 'tab',
	
			);
		
			$instance = wp_parse_args($instance, $defaults);	
			extract($instance);
			
			$tab_types = array(
				'tab' => 'boxes'
			);
			
			?>
			<div class="description cf">
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$boxes = is_array($boxes) ? $boxes : $defaults['boxes'];
					$count = 1;
					foreach($boxes as $tab) {	
						$this->tab3($tab, $count);
						$count++;
					}
					?>
				</ul>
				<p></p>
                
                <a href="#" rel="box_10" class="aq-sortable-add-new button">Add New</a>

             <p class="description"><!--Select 1,2,3,4,6-->
					<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
							<?php $options=array(12=>'1',6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
					</label>
				</p><!--Select 1,2,3,4,6-->
                
                <p class="description">
                    <label for="<?php echo $this->get_field_id('h2color') ?>">
                         Heading color: <br/>
                        <?php echo aq_field_color_picker('h2color', $block_id, $h2color, '#000000') ?>
                    </label>
            </p>
           
            <p class="description">
                    <label for="<?php echo $this->get_field_id('descriptioncolor') ?>">
                         Description color: <br/>
                        <?php echo aq_field_color_picker('descriptioncolor', $block_id, $descriptioncolor, '#8d8d8d') ?>
                    </label>
            </p>
			
             <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
                
             <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
            <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
		    </p><!--Animation delay-->

				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>
				
                <p></p>
                
			</div>
            
            <div class="font-awesome-select"><!--font-awesome-select START-->
            	<div class="font-awesome-plus-minus">
                	<a class="font-awesome-plus">+</a>
                    <a class="font-awesome-minus">-</a>
                	<a class="font_awesome_close">Close</a>
                </div>
           	<ul>
				<?php herowp_font_icon();?>
            </ul>
        </div><!--font-awesome-select END-->
		
			<div class="select-image-icons"><!--IMAGE ICON SELECT START-->
            	<div class="font-awesome-plus-minus1">
                	<a class="image_icons_close">Close</a>
                </div>
           	<ul>
				<?php herowp_generate_image_icons();?>
            </ul>
        </div><!--IMAGE ICON SELECT END-->
		
			<?php
		}
		
		function tab3($tab = array(), $count = 0) {

				
			?>
			<li id="<?php echo $this->get_field_id('boxes') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
				
				<div class="sortable-head cf">
					<div class="sortable-title">
						<strong><?php echo $tab['title'] ?></strong>
					</div>
					<div class="sortable-handle">
						<a href="#">Open / Close</a>
					</div>
				</div>
				
				<div class="sortable-body">
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-title">
							Box Title<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][title]" value="<?php echo $tab['title'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-subtitle">
							Box Subtitle<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-subtitle" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][subtitle]" value="<?php echo $tab['subtitle'] ?>" />
						</label>
					</p>	
                    <p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-service_url">
							Service URL (don't forget the http://, leave blank for no URL)<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-service_url" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][service_url]" value="<?php echo $tab['service_url'] ?>" />
						</label>
					</p>
					 <p class="tab-desc  description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-awesomefont">
							Select a font icon for this element.
							<input  type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-awesomefont" class="awesome-fonts-input input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][awesomefont]" value="<?php echo $tab['awesomefont'] ?>" />
							<a class="font-awesome-icon-select">Select Font Icon</a>
						</label>
					</p>
					
					<p class="tab-desc  description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-imageicon">
							<?php if (empty($tab['imageicon'])) {$tab['imageicon'] = '';} ?>
							Select an image icon for this element. <strong>The font icon should be empty if you choose this.</strong>
							<input  type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-imageicon" class="image-icons-input input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][imageicon]" value="<?php echo $tab['imageicon'] ?>" />
							<a class="image-icon-select">Select Image Icon</a>
						</label>
					</p>
					
					
				<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-reverse">
							Reverse service? If checked the service will look mirrored.<br/>
							<?php if (empty($tab['reverse'])) {$tab['reverse'] = '';} ?>
							<input type="hidden" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][reverse]" value="0" />
							<input type="checkbox" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-reverse" class="input-checkbox" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][reverse]" <?php if($tab['reverse']==1) {echo "checked=checked";}?> value="1"/>
						</label>
					</p>
					
		
	<p class="tab-desc description"><a href="#" class="sortable-delete">Delete</a></p>
				</div>
				
			</li>
			<?php
		}
		
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			extract($instance);
			
			//custom responsive CSS code
			herowp_add_responsive_css();			
			
			wp_enqueue_script('jquery-ui-boxes');
?>
			
			<?php echo '<div '.herowp_css_unique_id_add().' class="services_home" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--SERVICES 1 START-->'; ?>
			
		<?php
					$i = 0;
					$j = $delayanimation;
					$k = 1;
					$l = 1;
					$output='';
					$total = count($boxes);
					$counter=0;
					
					foreach( $boxes as $tab ){
					
					//we reverse the service if is checked
					if ($tab['reverse'] == 1){
						$class_reverse_number='number_reverse';
						$class_reverse_heading='heading_reverse';
						$class_reverse_paragraph='paragraph_reverse';
					}
					
					else{
						$class_reverse_number='';
						$class_reverse_heading='';
						$class_reverse_paragraph='';
					}
					
						 
							 if($l % 4 == 0): $class="lastbox"; else : $class=""; endif;
							 
							 if($l<=9): $zero="0"; else : $zero=""; endif;
							 
							 $items_on_row_divided=12/$items_on_row;
							// if($counter % $items_on_row_divided == 0): $clear="clear_box"; else : $clear=""; endif;
							 
							//if animations are set/unset
							if ($animation != '0'){
								$data_animate='class="col-md-'.$items_on_row.' col-sm-6 col-xs-12  not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
							}
							else{
								$data_animate='class="col-md-'.$items_on_row.' col-sm-6 col-xs-12 "';
							}							 
							
								$output .='
								
								
								<div '.$data_animate.'><!--COLS SERVICES START-->
									<div class="box-services"><!--box-services START-->';?>
										
										
															<?php if (strlen($tab['awesomefont']) >= 1){
																	$output .='<span class="servicesnumber '.$class_reverse_number.'"><i class="'.$tab['awesomefont'].'"></i></span>';
																  }
																  else{
																	$output .='<span class="servicesnumber-image '.$class_reverse_number.'"><img src="'.$tab['imageicon'].'" /></span>';
																  }
															?>

															<?php $output .='<div class="services-box">'; ?>
															
															<?php if (strlen($tab['service_url']) <= 1){
																	$output .='<h2 class="'.$class_reverse_heading.'" style="color:'.$h2color.'">'. $tab['title'] . '</h2>';
																  }
																  else{
																	$output .='<h2 class="'.$class_reverse_heading.'" style="color:'.$h2color.'"><a href="'.$tab['service_url'].'">'. $tab['title'] . '</a></h2>';
																  }
															?>
															
															<?php $output .='<p class="'.$class_reverse_paragraph.'" style="color:'.$descriptioncolor.'">'. $tab['subtitle'] . '</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START-->';	
								//$output .='1';
	
					$i++;
					$j=$j+$delayanimation;
					$k++;
					$l++;
					$counter++;
				}
					
			$output .='</div><!--SERVICES 1 END-->';
			
		
			echo $output;
		}
		
		
		function add_box() {
			$nonce = $_POST['security'];
			if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
			
			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
			
			//default key/value for the tab
			$tab = array(
						'title' => 'Refreshing design',
						'subtitle' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.',
						'awesomefont' => 'fa fa-laptop',
						'service_url' => ''
										
			);
			
			if($count) {
				$this->tab3($tab, $count);
			} else {
				die(-1);
			}
			
			die();
		}
		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}