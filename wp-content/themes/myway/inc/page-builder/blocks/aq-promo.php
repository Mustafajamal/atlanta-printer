<?php
/* Aqua Offer Block */
if(!class_exists('AQ_Promo')) {
	class AQ_Promo extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Promo',
				'size' => 'span12',
				"img_preview"=>'promo.png',
				'fa_icon'=>'fa fa-paper-plane'
				
			);
			
			//create the widget
			parent::__construct('AQ_Promo', $block_options);
			
			//add ajax functions
			add_action('wp_ajax_aq_block_offer_add_new', array($this, 'add_offer'));
			
		}
		
		function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning

		if (!isset($items_on_row))  $items_on_row='3';
		if (!isset($animation))  $animation='zoomIn';
		if (!isset($delayanimation))  $delayanimation='300';
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();	

		

		
			$defaults = array(
				'offer' => array(
					1 => array(
						'title' => 'Product <span>title</span>',
						'subtitle' => 'Subtitle',
						'old_price' => '99$',
						'old_price_text' => 'Was',
						'new_price' => '45$',
						'new_price_text' => 'Now',
						'book_url' => '#',
						'book_url_title'=>'Push Button',
						'image' => '',
					)
				),
				'type'	=> 'offer',
			);
			
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			
			$tab_types = array(
				'offer' => 'offer',
			);
			
			?>
		<div class="description cf">
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$offer = is_array($offer) ? $offer : $defaults['offer'];
					$count = 1;
					foreach($offer as $box) {	
						$this->tab3($box, $count);
						$count++;
					}
					?>
				</ul>
								<p></p>
				<a href="#" rel="offer" class="aq-sortable-add-new button">Add New</a>
				<p></p>
				

			<p class="description"><!--Select 1,2,3,4,6-->
				<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
						<?php $options=array(6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
				</label>
			</p><!--Select 1,2,3,4,6-->
			
 
             <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
                
            <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
                 <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elemtns appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
				</p><!--Animation delay-->
				
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>				
				
                <p></p>

		</div>
			<?php
		}
		
		function tab3($box = array(), $count = 0) {
		
			?>
			<li id="<?php echo $this->get_field_id('offer') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
				
				<div class="sortable-head cf">
					<div class="sortable-title">
						<strong><?php echo $box['title']; ?></strong>
					</div>
					<div class="sortable-handle">
						<a href="#">Open / Close</a>
					</div>
				</div>
				
				<div class="sortable-body">
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-content">
							Image<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-image" class="input-full input-upload" value="<?php echo $box['image'] ?>" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][image]">
							<a href="#" class="aq_upload_button button" rel="image">Upload</a><p></p>
						</label>				
					</p>	
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-title">
							Top Title<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][title]" value="<?php echo $box['title'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-subtitle">
							Small description<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-subtitle" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][subtitle]" value="<?php echo $box['subtitle'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-old_price">
							Old price<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-old_price" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][old_price]" value="<?php echo $box['old_price'] ?>" />
						</label>
					</p>					
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-old_price_text">
							Old price text<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-old_price_text" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][old_price_text]" value="<?php echo $box['old_price_text'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-new_price">
							New price<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-new_price" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][new_price]" value="<?php echo $box['new_price'] ?>" />
						</label>
					</p>					
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-new_price_text">
							New price text<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-new_price_text" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][new_price_text]" value="<?php echo $box['new_price_text'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-book_url">
							Link URL<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-book_url" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][book_url]" value="<?php echo $box['book_url'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-book_url_title">
							Link title<br/>
							<input type="text" id="<?php echo $this->get_field_id('offer') ?>-<?php echo $count ?>-book_url_title" class="input-full" name="<?php echo $this->get_field_name('offer') ?>[<?php echo $count ?>][book_url_title]" value="<?php echo $box['book_url_title'] ?>" />
						</label>
					</p>
					
					
							
					

					<p class="tab-desc description"><a href="#" class="sortable-delete">Delete</a></p>
				</div>
				
			</li>
			<?php
		}
		
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			extract($instance);

			//custom responsive CSS code
			herowp_add_responsive_css();			
			
			wp_enqueue_script('jquery-ui-boxes');
		

			echo'
	<div class="offer-wrap" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--OFFER WRAP START-->'; ?>
	
	
	
	<?php
			
					$i = 1;
					$j = $delayanimation;
					
					//if animations are set/unset
						
					$output='';
					
					foreach( $offer as $box ){
					
					if ($animation != '0'){
						$data_animate='class="offer-boxes not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
					}
					else{
						$data_animate='class="offer-boxes"';
					}
					
					if ($box['book_url']){
						$box_url='<a href="'.$box['book_url'].'" target="_blank">'.$box['book_url_title'].'</a>';
					}
					
					else{
						$box_url='';
					}
					
					if (!isset($box['title'])){
						$box['title'] = 'Product <span>title</span>';
					}
					
					if (empty($box['image'])) {
						$box['image'] = NULL;
					}
					else{
						$box['image'] = $box['image'];
					}
					
					
					
						$output.='
				<div class="col-md-'.$items_on_row.' col-sm-4 col-xs-6"><!--COLS START-->
					<div '.$data_animate.'><!--OFFER BOXES START-->
                        <div class="offer-thumb su-lightbox-gallery"><!--OFFER THUMB START-->';
								if ($box['image']){ 
								$image_resized=aq_resize($box['image'],690,690,true,true,true,true);
							$output .='
								<a href="'.esc_url($image_resized).'" data-lightbox="'.get_the_title().'">
									<img src="'.esc_url($image_resized).'" alt="'.get_the_title().'">
								</a>';
							}
							else{
							$output .='<img src="http://dummyimage.com/690x690/333333/fff.jpg" alt="">';
							}
							$output .='<div class="clear"></div>
							<div class="offer-info-bottom"><!--OFFER INFO BOTTOM START-->
									<p>'. $box['subtitle'] . '</p>
							</div><!--OFFER INFO BOTTOM END-->
                        </div><!--OFFER THUMB END-->
							<div class="offer-info"><!--OFFER INFO START-->
									<h2>'. html_entity_decode($box['title']) . '</h2>
							</div><!--OFFER INFO END-->
							
							<div class="offer-price"><!--OFFER PRICE START-->
								<p>
									'.$box['old_price_text'].'<span class="old"> '.$box['old_price'].'</span>
									'.$box['new_price_text'].'<span class="new"> '.$box['new_price'].'</span>
								</p>
								'.$box_url.'
							</div><!--OFFER PRICE END-->
					</div><!--OFFER BOXES END-->
				</div><!--COLS END-->';
					$i++;
					$j = $j+$delayanimation;
					}
      	echo $output;
		?>
	
			
	<?php echo '</div><!--offer WRAP END-->'; ?>
	
	<?php	}
			
		/* AJAX add tab */
		function add_offer() {
			$nonce = $_POST['security'];
			if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
			
			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
			
			//default key/value for the tab
			$offer = array(
						'title' => 'Product <span>title</span>',
						'subtitle' => 'Subtitle',
						'old_price' => '99$',
						'old_price_text' => 'Was',
						'new_price' => '45$',
						'new_price_text' => 'Now',
						'book_url' => '#',
						'book_url_title'=>'Push Button',
						'image' => '',
			);
			
			if($count) {
				$this->tab3($offer, $count);
			} else {
				die(-1);
			}
			
			die();
		}
		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}
