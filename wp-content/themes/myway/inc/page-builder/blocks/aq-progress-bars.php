<?php
if(!class_exists('AQ_Progress_Bars')) {
	class AQ_Progress_Bars extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Progress Bar',
				'size' => 'span4',
				'resizable' => 1,
				"class_css"=>'bf_services',
				"img_preview"=>'progress.png',
				'fa_icon'=>'fa fa-th-large'
			);
			
			//create the widget
			parent::__construct('AQ_Progress_Bars', $block_options);
			
			//add ajax functions
			add_action('wp_ajax_aq_block_box_bars_add_new', array($this, 'add_box'));
			
		}
		
		
		
		function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning	
		if (!isset($awesomefont_heading))  $awesomefont_heading='fa fa-laptop';
		if (!isset($items_on_row))  $items_on_row='1';
		if (!isset($animation))  $animation='fadeIn';
		if (!isset($delayanimation))  $delayanimation='150';
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($text_color))  $text_color='';
		if (!isset($number_color))  $number_color='';
		if (!isset($progress_bar_color))  $progress_bar_color='';
		if (!isset($progress_track_color))  $progress_track_color='';
		if (!isset($progress_bar_width))  $progress_bar_width='';
		if (!isset($speed))  $speed='';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
			$defaults = array(
				'boxes' => array(
					1 => array(
						'number' => '50',
						'subtitle' => 'Amazing stuff',
						'start_at' => '0',
					),
				),
				'type'	=> 'tab',
				
			
				
			);			
		
			$instance = wp_parse_args($instance, $defaults);	
			extract($instance);
			
			$tab_types = array(
				'tab' => 'boxes'
			);
			
			?>
			<div class="description cf">
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$boxes = is_array($boxes) ? $boxes : $defaults['boxes'];
					$count = 1;
					foreach($boxes as $tab) {	
						$this->tab3($tab, $count);
						$count++;
					}
					?>
				</ul>
				<p></p>
                
                <a href="#" rel="box_bars" class="aq-sortable-add-new button">Add New</a>
				

                
            <p class="description"><!--Select 1,2,3,4,6-->
					<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
							<?php $options=array(12=>'1',6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
					</label>
			</p><!--Select 1,2,3,4,6-->
			   
			<p class="description">
                    <label for="<?php echo $this->get_field_id('number_color') ?>">
                         Number color: <br/>
                        <?php echo aq_field_color_picker('number_color', $block_id, $number_color, '#000000') ?>
                    </label>
            </p>
			
			 <p class="description">
                    <label for="<?php echo $this->get_field_id('text_color') ?>">
                         Text color: <br/>
                        <?php echo aq_field_color_picker('text_color', $block_id, $text_color, '#000000') ?>
                    </label>
            </p>  			 
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('progress_bar_color') ?>">
                        Progress bar color: <br/>
                        <?php echo aq_field_color_picker('progress_bar_color', $block_id, $progress_bar_color, '#000000') ?>
                    </label>
            </p>  			
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('progress_track_color') ?>">
                        Progress track color: <br/>
                        <?php echo aq_field_color_picker('progress_track_color', $block_id, $progress_track_color, '#000000') ?>
                    </label>
            </p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('progress_bar_width') ?>">
                        <strong>Progress bar width:</strong> Enter only numbers no px.- i.e. 16 (means 16 px)
                        <?php echo aq_field_input('progress_bar_width', $block_id, $progress_bar_width, $size = 'full') ?>
                    </label>
			</p>                   
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('speed') ?>">
                        <strong>Speed</strong> Enter speed 1-100 (fast - slow)
                        <?php echo aq_field_input('speed', $block_id, $speed, $size = 'full') ?>
                    </label>
			</p>             
			
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
                
            <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
                 <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
				</p><!--Animation delay-->
				
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>

				
				

				
                <p></p>
                
			</div>
			<?php
		}
		
		function tab3($tab = array(), $count = 0) {

				
			?>
			<li id="<?php echo $this->get_field_id('boxes') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
				
				<div class="sortable-head cf">
					<div class="sortable-title">
						<strong><?php echo $tab['number'] ?></strong>
					</div>
					<div class="sortable-handle">
						<a href="#">Open / Close</a>
					</div>
				</div>
				
				<div class="sortable-body">
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-number">
							Percent: 1 - 100 (lowest - highest)<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-number" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][number]" value="<?php echo $tab['number'] ?>" />
						</label>
					</p>					
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-start_at">
							Start at: 0 - 100 (lowest - highest)<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-start_at" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][start_at]" value="<?php echo $tab['start_at'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-subtitle">
							Text bellow number<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-subtitle" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][subtitle]" value="<?php echo $tab['subtitle'] ?>" />
						</label>
					</p>	
                 
                    
				   
	
	<p class="tab-desc description"><a href="#" class="sortable-delete">Delete</a></p>
				</div>
				
			</li>
			<?php
		}
		
		
		
	
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			//options for progress bars - output in footer
			global $progress_bar_color, $progress_track_color, $progress_bar_width, $speed;
			
			extract($instance);
			
			//custom responsive CSS code
			herowp_add_responsive_css();
	
			
			//let's enqueue some javascript when user change a setting or more
			herowp_progress_bar_script();	
			
			wp_enqueue_script('jquery-ui-boxes');

?>
			
		<?php echo '<div class="progress_bars" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--progress_bars START-->'; ?>
			
		<?php
			
					if ($number_color){
						$number_color_final = 'style="color:'.$number_color.'"';
					}
					else{
						$number_color_final = '';
					}						
					
					if ($text_color){
						$text_color_final = 'style="color:'.$text_color.'"';
					}
					else{
						$text_color_final = '';
					}	
					
					$output='';
					$j = $delayanimation;
					$total = count($boxes);
					$counter=0;
					foreach( $boxes as $tab ){
					//if animations are set/unset
							if ($animation != '0'){
								$data_animate='class="col-md-'.$items_on_row.' '.$herowp_css_unique_id.' col-sm-6 col-xs-12 pie_progress not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
							}
							else{
								$data_animate='class="col-md-'.$items_on_row.' '.$herowp_css_unique_id.' col-sm-6 col-xs-12 pie_progress"';
							}
							  
$output .= '
<div '.$data_animate.' role="progressbar" data-goal="'.$tab['number'].'"><!--pie_progress START-->
	<div class="pie_progress__number" '.$number_color_final.'>'.$tab['start_at'].'%</div>
	<div class="pie_progress__label" '.$text_color_final.'>'.$tab['subtitle'].'</div>
</div><!--pie_progress END-->
';


					$j=$j+$delayanimation;

				}
					
			$output .='</div><!--progress_bars END-->';
			
		
			echo $output;
		}
		
		
		function add_box() {
			$nonce = $_POST['security'];
			if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
			
			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
			
			//default key/value for the tab
			$tab = array(
						'number' => '50',
						'subtitle' => 'Amazing stuff',
						'start_at' => '0',
			);
			
			if($count) {
				$this->tab3($tab, $count);
			} else {
				die(-1);
			}
			
			die();
		}
		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}