<?php
if(!class_exists('AQ_Heading')) {
	class AQ_Heading extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Heading',
				'size' => 'span12',
				'resizable' => 1,
				"class_css"=>'bf_services',
				"img_preview"=>'heading.png',
				'fa_icon'=>'fa fa-font'
			);
			
			//create the widget
			parent::__construct('AQ_Heading', $block_options);	
			
		}
		function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
		if (!isset($title_h1))  $title_h1='Our';
		if (!isset($title_h1_2))  $title_h1_2='title';
		if (!isset($desc))  $desc='Enter a cool description here';
		if (!isset($text_align))  $text_align='left';
		if (!isset($font_size))  $font_size='42';
		if (!isset($h1_color))  $h1_color='#4e4e4e';
		if (!isset($h1_2_color))  $h1_2_color='#4e4e4e';
		if (!isset($descriptioncolor))  $descriptioncolor='#4e4e4e';
		if (!isset($linecolor))  $linecolor='';
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='10';
		if (!isset($animation))  $animation='No animations';
		if (!isset($animation_desc))  $animation_desc='No animations';
		if (!isset($font_family_desc))  $font_family_desc='';
		if (!isset($font_family))  $font_family='';
		if (!isset($font_family2))  $font_family2='';
		if (!isset($text_transform_desc))  $text_transform_desc='';
		if (!isset($text_transform))  $text_transform='';
		if (!isset($text_transform2))  $text_transform2='';
		if (!isset($letter_spacing_desc))  $letter_spacing_desc='';
		if (!isset($letter_spacing))  $letter_spacing='';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();	
		
			$instance = wp_parse_args($instance);	
			extract($instance);
			
			
			?>
	<div class="description cf">
                
			<p class="description">
                <label for="<?php echo $this->get_field_id('title_h1') ?>">
                   Heading first title (You can add HTML characters as well)
                    <?php echo aq_field_input('title_h1', $block_id, $title_h1, $size = 'full') ?>
                </label>
			</p>
            
            <p class="description">
                <label for="<?php echo $this->get_field_id('title_h1_2') ?>">
                    Heading second title (You can add HTML characters as well)
                    <?php echo aq_field_input('title_h1_2', $block_id, $title_h1_2, $size = 'full') ?>
                </label>
            </p> 

			<p class="description">
                <label for="<?php echo $this->get_field_id('desc') ?>">
                    Description
                    <?php echo aq_field_textarea('desc', $block_id, $desc, $size = 'full') ?>
                </label>
			</p>
			
			<p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
					<?php $options = isset($options) ? $options : NULL; ?>
						<strong>Animations:</strong> Select animation for title
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
				</p><!--Animation-->			
				
				
				<p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation_desc') ?>">
						<strong>Animations:</strong> Select animation for description
							<?php echo herowp_animations('animation_desc', $block_id, $options, $animation_desc); ?>
					</label>
				</p><!--Animation-->
			
			<p class="description">
                <label for="<?php echo $this->get_field_id('font_size') ?>">
                    Font size for heading: Enter only numbers, no px.
                    <?php echo aq_field_input('font_size', $block_id, $font_size, $size = 'full') ?>
                </label>
            </p>
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('text_align') ?>">
						<strong>Text Align:</strong> Select the alignment of your Heading and Description text.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_text_align('text_align', $block_id, $options, $text_align); ?>
					</label>
			</p>				
			
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('text_transform') ?>">
						<strong>Text transform: </strong>Heading text transform.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_text_transform('text_transform', $block_id, $options, $text_transform); ?>
					</label>
			</p>				
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('text_transform2') ?>">
						<strong>Text transform: </strong>Heading 2nd text transform.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_text_transform('text_transform2', $block_id, $options, $text_transform2); ?>
					</label>
			</p>				
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('text_transform_desc') ?>">
						<strong>Text transform: </strong>Description text transform.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_text_transform('text_transform_desc', $block_id, $options, $text_transform_desc); ?>
					</label>
			</p>

			<p class="description">
                <label for="<?php echo $this->get_field_id('letter_spacing') ?>">
						Letter spacing heading. Enter letter spacing for heading, no px, only numbers. (eg -2)
                    <?php echo aq_field_input('letter_spacing', $block_id, $letter_spacing, $size = 'full') ?>
                </label>
			</p>			
			
			<p class="description">
                <label for="<?php echo $this->get_field_id('letter_spacing_desc') ?>">
						Letter spacing description. Enter letter spacing for description, no px, only numbers. (eg -2)
                    <?php echo aq_field_input('letter_spacing_desc', $block_id, $letter_spacing_desc, $size = 'full') ?>
                </label>
			</p>
			
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('font_family') ?>">
						<strong>Font Family</strong> Select the font family for this heading.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_font_family('font_family', $block_id, $options, $font_family); ?>
					</label>
			</p>			
			
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('font_family2') ?>">
						<strong>Font Family</strong> Select the font family for 2nd heading.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_font_family('font_family2', $block_id, $options, $font_family2); ?>
					</label>
			</p>
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('font_family_desc') ?>">
						<strong>Font Family</strong> Select the font family for description.
						<?php $options = isset($options) ? isset($options) : null;?>
						<?php echo herowp_font_family('font_family_desc', $block_id, $options, $font_family_desc); ?>
					</label>
			</p>
			
             <p class="description">
                    <label for="<?php echo $this->get_field_id('h1_color') ?>">
                     Heading Color: <br/>
                        <?php echo aq_field_color_picker('h1_color', $block_id, $h1_color, '#696969') ?>
                    </label>
            </p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('h1_2_color') ?>">
                     Heading Second Color: <br/>
                        <?php echo aq_field_color_picker('h1_2_color', $block_id, $h1_2_color, '#696969') ?>
                    </label>
            </p>
            
            <p class="description">
                    <label for="<?php echo $this->get_field_id('descriptioncolor') ?>">
                    Description Color: <br/>
                        <?php echo aq_field_color_picker('descriptioncolor', $block_id, $descriptioncolor, '#8d8d8d') ?>
                    </label>
            </p>
			
			 <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>			
                
	</div>
            

			<?php
		}
				
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			extract($instance);
			
			//custom responsive CSS code
			herowp_add_responsive_css();			
			
			wp_enqueue_script('jquery-ui-boxes');
			
			if ($font_family != '0'){
				
				//let's remove spaces from font name and lowercase all the characters
				$herowp_font_family_remove_spaces  = str_replace(' ','',$font_family);
				$herowp_font_family_name = strtolower($herowp_font_family_remove_spaces);
				
				//check if SSL it's being used
				$herowp_ssl_check = is_ssl() ? 's' : '';
				$herowp_url = 'http'.$herowp_ssl_check;
				
				//the script to be enqueued
				$herowp_font_script = $herowp_url.'://fonts.googleapis.com/css?family='.urlencode($font_family).':100,300,400,400italic,500,600,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese';
				
				//enqueue the google font
				wp_enqueue_style( 'herowp_builder_font_'.$herowp_font_family_name, $herowp_font_script, array(), NULL, 'all');
			}			
			
			
			if ($font_family2 != '0'){
				
				//let's remove spaces from font name and lowercase all the characters
				$herowp_font2_family_remove_spaces  = str_replace(' ','',$font_family2);
				$herowp_font2_family_name = strtolower($herowp_font2_family_remove_spaces);
				
				//check if SSL it's being used
				$herowp2_ssl_check = is_ssl() ? 's' : '';
				$herowp2_url = 'http'.$herowp2_ssl_check;
				
				//the script to be enqueued
				$herowp2_font_script = $herowp2_url.'://fonts.googleapis.com/css?family='.urlencode($font_family2).':100,300,400,400italic,500,600,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese';
				
				//enqueue the google font
				wp_enqueue_style( 'herowp_builder_font_'.$herowp_font2_family_name, $herowp2_font_script, array(), NULL, 'all');
			}			
			
			
			if ($font_family_desc != '0'){
				
				//let's remove spaces from font name and lowercase all the characters
				$herowp_font_desc_family_remove_spaces  = str_replace(' ','',$font_family);
				$herowp_font_desc_family_name = strtolower($herowp_font_desc_family_remove_spaces);
				
				//check if SSL it's being used
				$herowp_desc_ssl_check = is_ssl() ? 's' : '';
				$herowp_desc_url = 'http'.$herowp_desc_ssl_check;
				
				//the script to be enqueued
				$herowp_font_desc_script = $herowp_desc_url.'://fonts.googleapis.com/css?family='.urlencode($font_family).':100,300,400,400italic,500,600,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese';
				
				//enqueue the google font
				wp_enqueue_style( 'herowp_builder_font_'.$herowp_font_desc_family_name, $herowp_font_desc_script, array(), NULL, 'all');
			}
			
		
?>
			
			<?php echo '<div '.herowp_css_unique_id_add().' class="heading" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--HEADING START-->'; ?>
			
			<?php 
			
			if ($text_align == 'center') {
				$padding_center = 'padding:0 25%;';
			}
			else{
				$padding_center = '';
			}
			
			if ($animation != '0'){
				$data_animate='data-animate="'.$animation.'" class="not-animated"';
			}
			else{
				$data_animate='';
			}			
			
			if ($animation_desc != '0'){
				$data_animate_desc='data-animate="'.$animation_desc.'" class="heading not-animated"';
			}
			else{
				$data_animate_desc='class="heading"';
			}
			
			if ($font_family != 'Select Desired Font'){
				$font_family_style='font-family:'.$font_family.';';
			}
			else{
				$font_family_style='';
			}				
			
			if ($font_family2 != 'Select Desired Font'){
				$font2_family_style='font-family:'.$font_family2.';';
			}
			else{
				$font2_family_style='';
			}			
			
			if ($font_family_desc != 'Select Desired Font'){
				$font_desc_family_style='font-family:'.$font_family_desc.';';
			}
			else{
				$font_desc_family_style='';
			}			
			
			if ($text_transform_desc != 'Select text transform'){
				$text_transform_desc_style='text-transform:'.$text_transform_desc.';';
			}
			else{
				$text_transform_desc_style='';
			}			
			
			if ($text_transform != 'Select text transform'){
				$text_transform_style='text-transform:'.$text_transform.';';
			}
			else{
				$text_transform_style='';
			}			
			
			if ($text_transform2 != 'Select text transform'){
				$text_transform2_style='text-transform:'.$text_transform2.';';
			}
			else{
				$text_transform2_style='';
			}			
			
			if ($letter_spacing_desc || $letter_spacing_desc === '0'){
				$letter_spacing_desc_style='letter-spacing:'.$letter_spacing_desc.'px;';
			}
			else{
				$letter_spacing_desc_style='';
			}
			
			if ($letter_spacing || $letter_spacing === '0'){
				$letter_spacing_style='letter-spacing:'.$letter_spacing.'px;';
			}
			else{
				$letter_spacing_style='';
			}

			
			?>
			
			<div class="top_headings"><!--top_headings START-->
                     <div class="col-lg-12"><!--col-lg-12 START-->
                                <div class="title-heading">
									<?php if (strlen($title_h1)>=1 || strlen($title_h1_2)>=1) :?>
										<h1 <?php echo $data_animate; ?> style="color:<?php echo $h1_color ;?>; <?php echo $font_family_style; ?> <?php echo $text_transform_style; ?> <?php echo $letter_spacing_style; ?> text-align:<?php echo $text_align; ?>; font-size:<?php echo $font_size.'px'; ?>"><?php echo html_entity_decode($title_h1); ?> <span style="color:<?php echo $h1_2_color ;?>; <?php echo $font2_family_style; ?> <?php echo $text_transform2_style; ?>"><?php echo html_entity_decode($title_h1_2); ?></span></h1>
									<?php endif; ?>
									<?php if (strlen($desc)>=1) :?>
										<p <?php echo $data_animate_desc; ?> style="color:<?php echo $descriptioncolor; ?>; <?php echo $text_transform_desc_style; ?> <?php echo $letter_spacing_desc_style; ?> <?php echo $font_desc_family_style; ?> <?php echo $padding_center; ?> ; text-align:<?php echo $text_align; ?>"><?php echo html_entity_decode($desc); ?></p>
									<?php endif; ?>
                                </div>
                    </div><!--col-lg-12 END-->
				</div><!--top_headings END-->
			
			
		<?php
				
		echo'</div><!--HEADING END-->';
		}

		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}