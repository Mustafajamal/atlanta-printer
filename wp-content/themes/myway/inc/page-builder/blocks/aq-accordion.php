<?php
if(!class_exists('AQ_Box_Accordion')) {
	class AQ_Box_Accordion extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Accordions',
				'size' => 'span4',
				'resizable' => 1,
				"class_css"=>'bf_services',
				"img_preview"=>'accordion.png',
				'fa_icon'=>'fa fa-th-large'
			);
			
			//create the widget
			parent::__construct('AQ_Box_Accordion', $block_options);
			
			//add ajax functions
			add_action('wp_ajax_aq_block_box_acc_add_new', array($this, 'add_box'));
			
		}
		function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
	
		if (!isset($animation))  $animation='fadeInUp';
		if (!isset($delayanimation))  $delayanimation='0';
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($h2color))  $h2color='';
		if (!isset($descriptioncolor))  $descriptioncolor='';
		if (!isset($bordercolor))  $bordercolor='';
		if (!isset($iconcolor))  $iconcolor='';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
			$defaults = array(
				'boxes' => array(
					1 => array(
						'title' => 'Accordion title',
						'subtitle' => 'Equations are more important to me, because politics is for the present, but an equation is eternal. Equations are more important to me, because politics is for the present, but an equation is eternal.',
					),
				),
				'type'	=> 'tab',
	
			);
		
			$instance = wp_parse_args($instance, $defaults);	
			extract($instance);
			
			$tab_types = array(
				'tab' => 'boxes'
			);
			
			?>
			<div class="description cf">
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$boxes = is_array($boxes) ? $boxes : $defaults['boxes'];
					$count = 1;
					foreach($boxes as $tab) {	
						$this->tab3($tab, $count);
						$count++;
					}
					?>
				</ul>
				<p></p>
                
                <a href="#" rel="box_acc" class="aq-sortable-add-new button">Add New</a>
                
                
                <p class="description">
                    <label for="<?php echo $this->get_field_id('h2color') ?>">
                         Heading color: <br/>
                        <?php echo aq_field_color_picker('h2color', $block_id, $h2color, '#000000') ?>
                    </label>
            </p>
           
            <p class="description">
                    <label for="<?php echo $this->get_field_id('descriptioncolor') ?>">
                         Description color: <br/>
                        <?php echo aq_field_color_picker('descriptioncolor', $block_id, $descriptioncolor, '#8d8d8d') ?>
                    </label>
            </p>            
			
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('bordercolor') ?>">
                         Border color of accordions: <br/>
                        <?php echo aq_field_color_picker('bordercolor', $block_id, $bordercolor, '#8d8d8d') ?>
                    </label>
            </p>			
			
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('iconcolor') ?>">
                         Icon color of accordions: <br/>
                        <?php echo aq_field_color_picker('iconcolor', $block_id, $iconcolor, '#8d8d8d') ?>
                    </label>
            </p>
			
             <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
                
             <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php $options = isset($options) ? isset($options) : ''; ?>
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
            <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
		    </p><!--Animation delay-->
			
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>	

				
                <p></p>
                
			</div>

			<?php
		}
		
		function tab3($tab = array(), $count = 0) {

				
			?>
			<li id="<?php echo $this->get_field_id('boxes') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
				
				<div class="sortable-head cf">
					<div class="sortable-title">
						<strong><?php echo $tab['title'] ?></strong>
					</div>
					<div class="sortable-handle">
						<a href="#">Open / Close</a>
					</div>
				</div>
				
				<div class="sortable-body">
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-title">
							Title<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][title]" value="<?php echo $tab['title'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-subtitle">
							Subtitle<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-subtitle" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][subtitle]" value="<?php echo $tab['subtitle'] ?>" />
						</label>
					</p>	
				
	
	<p class="tab-desc description"><a href="#" class="sortable-delete">Delete</a></p>
				</div>
				
			</li>
			<?php
		}
		
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			extract($instance);
			
			//custom responsive CSS code
			herowp_add_responsive_css();			
			
			wp_enqueue_script('jquery-ui-boxes');
?>


			
			<?php echo '<div '.herowp_css_unique_id_add().' class="accordion" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--ACCORDION START-->'; ?>

			
		<?php
					$i = 1;
					$j = $delayanimation;

					
					$output='';
					$total = count($boxes);
					
					if ($h2color){
						$h2color_final = 'style="color:'.$h2color.'"';
					}
					else{
						$h2color_final = '';
					}					
					
					if ($descriptioncolor){
						$descriptioncolor_final = 'style="color:'.$descriptioncolor.'"';
					}
					else{
						$descriptioncolor_final = '';
					}					
					
					if ($bordercolor){
						$bordercolor_final = 'style="border-color:'.$bordercolor.'"';
					}
					else{
						$bordercolor_final = '';
					}


					foreach( $boxes as $tab ){
					
							//if animations are set/unset
							if ($animation != '0'){
								$data_animate='class="col-md-12 col-sm-12 col-xs-12 not-animated accordion-group" data-animate="'.$animation.'" data-delay="'.$j.'"';
							}
							else{
								$data_animate='class="col-md-12 col-sm-12 col-xs-12 accordion-group"';
							}
							
							if($i == 1){
								$in = 'in';
							}
							else{
								$in = '';
							}

							
$output .='
<div '.$data_animate.'>
    <div class="accordion-heading">
      <a class="accordion-toggle" '.$bordercolor_final.' data-toggle="collapse" href="#collapse-'.$herowp_css_unique_id.'-'.$i.'">
        <i class="fa fa-bars" style="color:'.$iconcolor.'; border-color:'.$bordercolor.';"></i><span '.$h2color_final.'> '.$tab['title'].'</span>
      </a>
    </div>
    <div id="collapse-'.$herowp_css_unique_id.'-'.$i.'" class="accordion-body collapse '.$in.'">
      <div class="accordion-inner">
         <p '.$descriptioncolor_final.'>'.$tab['subtitle'].'</p>
      </div>
    </div>
</div>';
						
	
					$i++;
					$j=$j+$delayanimation;

				}
					
			$output .='</div><!--ACCORDION END-->';
			
		
			echo $output;
		}
		
		
		function add_box() {
			$nonce = $_POST['security'];
			if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
			
			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
			
			//default key/value for the tab
			$tab = array(
						'title' => 'Accordion title',
						'subtitle' => 'Equations are more important to me, because politics is for the present, but an equation is eternal. Equations are more important to me, because politics is for the present, but an equation is eternal.',
										
			);
			
			if($count) {
				$this->tab3($tab, $count);
			} else {
				die(-1);
			}
			
			die();
		}
		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}