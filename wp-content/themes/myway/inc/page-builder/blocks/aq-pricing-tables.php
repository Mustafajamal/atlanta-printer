<?php
if(!class_exists('AQ_Pricing_Tables')) {
	class AQ_Pricing_Tables extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Pricing Tables',
				'size' => 'span4',
				'resizable' => 1,
				"class_css"=>'',
				"img_preview"=>'pricing.png',
				"fa_icon" => 'fa fa-usd'
			);
			
			//create the widget
			parent::__construct('AQ_Pricing_Tables', $block_options);
			
			//add ajax functions
			add_action('wp_ajax_aq_block_price_add_new', array($this, 'add_box'));
			
		}
		function form($instance) {
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($animation))  $animation='fadeIn';
		if (!isset($delayanimation))  $delayanimation='0';
		if (!isset($awesomefont_heading))  $awesomefont_heading='fa fa-laptop';
		if (!isset($pricing_features_color))  $pricing_features_color='#FFFFFF';
		if (!isset($pricing_heading_color))  $pricing_heading_color='#FFFFFF';
		if (!isset($vertical_line_color))  $vertical_line_color='#FFFFFF';
		if (!isset($horizontal_line_color))  $horizontal_line_color='#FFFFFF';
		if (!isset($icon_color))  $icon_color='#FFFFFF';
		if (!isset($items_on_row))  $items_on_row='1';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
			$defaults = array(
				'boxes' => array(
					1 => array(
						'title' => 'Tiny bear',
						'price' => '29',
						'billing' => 'per month',
						'awesomefont' => 'fa fa-laptop',
						'currency' => '$',
						'features' => '<li>Super duper feature</li> <li>yet another cool feature here</li> <li>i am the third feature</li> <li>also the  Fourth</li>',
						'buy_text' => 'Sign up today',
						'buy_url' => '#'
					),
				),
				'type'	=> 'tab',				
			);
			
			
			$instance = wp_parse_args($instance, $defaults);	
			extract($instance);
			
			$tab_types = array(
				'tab' => 'boxes'
			);
			
			?>
			<div class="description cf">
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$boxes = is_array($boxes) ? $boxes : $defaults['boxes'];
					$count = 1;
					foreach($boxes as $tab) {	
						$this->tab3($tab, $count);
						$count++;
					}
					?>
				</ul>
				<p></p>
                
                <a href="#" rel="price" class="aq-sortable-add-new button">Add New</a>
                
				
            <p class="description"><!--Select 1,2,3,4,6-->
					<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
							<?php $options=array(12=>'1',6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
					</label>
				</p><!--Select 1,2,3,4,6-->
				
			<p class="description">
                    <label for="<?php echo $this->get_field_id('icon_color') ?>">
                         Icon color: <br/>
                        <?php echo aq_field_color_picker('icon_color', $block_id, $icon_color, '#8d8d8d') ?>
                    </label>
            </p>
               
			<p class="description">
                    <label for="<?php echo $this->get_field_id('pricing_heading_color') ?>">
                         Pricing heading color: <br/>
                        <?php echo aq_field_color_picker('pricing_heading_color', $block_id, $pricing_heading_color, '#8d8d8d') ?>
                    </label>
            </p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('pricing_features_color') ?>">
                         Pricing features color: <br/>
                        <?php echo aq_field_color_picker('pricing_features_color', $block_id, $pricing_features_color, '#8d8d8d') ?>
                    </label>
            </p>


			<p class="description">
                    <label for="<?php echo $this->get_field_id('vertical_line_color') ?>">
                         Vertical line color: <br/>
                        <?php echo aq_field_color_picker('vertical_line_color', $block_id, $vertical_line_color, '#8d8d8d') ?>
                    </label>
            </p> 	
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('horizontal_line_color') ?>">
                          Horizontal line color: <br/>
                        <?php echo aq_field_color_picker('horizontal_line_color', $block_id, $horizontal_line_color, '#8d8d8d') ?>
                    </label>
            </p> 	
	
             <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
				</p><!--Animation-->
                
                 <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
				</p><!--Animation delay-->
                
                
                  <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin of services from the top in pixels - Use negative value for homepage. ex: -50. Do NOT include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
				</p>
				
                <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			   </p>
				
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>				
				
                <p></p>
                
			</div>
			
			
            
        <div class="font-awesome-select"><!--font-awesome-select START-->
            	<div class="font-awesome-plus-minus">
                	<a class="font-awesome-plus">+</a>
                    <a class="font-awesome-minus">-</a>
                	<a class="font_awesome_close">Close</a>
                </div>
           	<ul>			
				<?php herowp_font_icon();?>
            </ul>
        </div><!--font-awesome-select END-->
		
		<div class="select-image-icons"><!--IMAGE ICON SELECT START-->
            	<div class="font-awesome-plus-minus1">
                	<a class="image_icons_close">Close</a>
                </div>
           	<ul>
				<?php herowp_generate_image_icons();?>
            </ul>
        </div><!--IMAGE ICON SELECT END-->
		
			<?php
		}
		
		function tab3($tab = array(), $count = 0) {

				
			?>
<li id="<?php echo $this->get_field_id('boxes') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
				
				<div class="sortable-head cf">
					<div class="sortable-title">
						<strong><?php echo $tab['title'] ?></strong>
					</div>
					<div class="sortable-handle">
						<a href="#">Open / Close</a>
					</div>
				</div>
				
	<div class="sortable-body">
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-featured">
							Featured?<br/>
							<?php if (empty($tab['featured'])) {$tab['featured'] = '';} ?>
							<input type="hidden" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][featured]" value="0" />
							<input type="checkbox" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-featured" class="input-checkbox" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][featured]" <?php if($tab['featured']==1) {echo "checked=checked";}?> value="1"/>
						</label>
					</p>
					 <p class="tab-desc  description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-awesomefont">
							Select a font icon for this element.
							<input  type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-awesomefont" class="awesome-fonts-input input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][awesomefont]" value="<?php echo $tab['awesomefont'] ?>" />
							<a class="font-awesome-icon-select">Select Font Icon</a> 
						</label>
					</p>
					
					<p class="tab-desc  description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-imageicon">
							<?php if (empty($tab['imageicon'])) {$tab['imageicon'] = '';} ?>
							If you select this icons, please make sure <strong>the above icon field is empty.</strong>
							<input  type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-imageicon" class="image-icons-input input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][imageicon]" value="<?php echo $tab['imageicon'] ?>" />
							<a class="image-icon-select">Select Image Icon</a> 
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-title">
							Pricing heading title<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][title]" value="<?php echo $tab['title'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-price">
							Price<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-price" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][price]" value="<?php echo $tab['price'] ?>" />
						</label>
					</p>
					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-billing">
							Billing cycle (year, month, etc)<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-billing" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][billing]" value="<?php echo $tab['billing'] ?>" />
						</label>
					</p>	
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-currency">
							Currency<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-currency" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][currency]" value="<?php echo $tab['currency'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-features">
							List of features<br/>
							<textarea type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-features" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][features]" ><?php echo $tab['features'] ?></textarea>
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-buy_text">
							Buy text<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-buy_text" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][buy_text]" value="<?php echo $tab['buy_text'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-buy_url">
							Buy URL<br/>
							<input type="text" id="<?php echo $this->get_field_id('boxes') ?>-<?php echo $count ?>-buy_url" class="input-full" name="<?php echo $this->get_field_name('boxes') ?>[<?php echo $count ?>][buy_url]" value="<?php echo $tab['buy_url'] ?>" />
						</label>
					</p>
     	
					<p class="tab-desc description"><a href="#" class="sortable-delete">Delete</a></p>
    </div>
				
</li>

			<?php
		}
		
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			extract($instance);

			//custom responsive CSS code
			herowp_add_responsive_css();
			
			wp_enqueue_script('jquery-ui-boxes');
			
			?>
			
			<?php echo '<div '.herowp_css_unique_id_add().' class="pricing_tables" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--PRICING START-->'; ?>
			
		<?php 
			
					$i = 0;
					$j = $delayanimation;
					$l = 1;
					
					$total = count($boxes);
					$counter=0;
					
					$output='<div class="pricing_tables_holder"><!--PRICING TABLES HOLDER START-->';
					
					
					foreach( $boxes as $tab ){

					
						    if($tab['featured']==1){
								$featured='featured_plan';
								$class_featured = 'featured_pricing';
							}
							else{
								$featured='';
								$class_featured = '';
							}
							
							 
							
							 
							//if animations are set/unset
							if ($animation != '0'){
								$data_animate='class="box-pricing not-animated '.$class_featured.'" style="border-color:'.$vertical_line_color.'" data-animate="'.$animation.'" data-delay="'.$j.'"';
							}
							else{
								$data_animate='class="box-pricing '.$class_featured.'" style="border-color:'.$vertical_line_color.'"';
							} 
							  
							
								$output .='
								
								
								<div class="col-md-'.$items_on_row.' col-sm-6 col-xs-12"><!--PRICING TABLE END -->
											
									<div '.$data_animate.'><!--BOX PRICING START-->';?>
										
										<?php if (strlen($tab['awesomefont']) >= 1){
											$output .='<p class="plan_icon"><i class="'.$tab['awesomefont'].'" style="color:'.$icon_color.'"></i></p>';
											}
											else{					
											$output .='<p class="plan_icon_image"><img src="'.$tab['imageicon'].'" /></p>';
											}
										?>
										
										<?php $output .='<h1 style="color:'.$pricing_heading_color.'">'.$tab['title'].'<span class="dotcolor"></h1>
										
										<div class="price_circle"><!--PRICE CIRCLE START-->
											<div class="price_circle_inner"><!--PRICE CIRCLE INNER START-->
												<h1 class="price">'.$tab['price'].'</h1>
											</div><!--PRICE CIRCLE INNER END-->
												<p class="billing">'.$tab['currency'].'/'.$tab['billing'].'</p>
										</div><!--PRICE CIRCLE END-->
										
										<div class="line" style="background:'.$horizontal_line_color.'"></div>
										
										<ul style="color:'.$pricing_features_color.'">
											'.html_entity_decode($tab['features']).'
										</ul>
										
										<h2 class="buy"><a href="'.$tab['buy_url'].'" class="'.$featured.'"><i class="fa fa-paper-plane"></i>'.$tab['buy_text'].'</a></h2>

									</div><!--BOX PRICING END-->
									
									
								
								</div><!--PRICING TABLE END -->
								';	
								
	
					$i++;
					$j=$j+$delayanimation;
					$l++;
					$counter++;
				}
					
			$output .='
	</div><!--PRICING TABLES HOLDER END-->
</div><!--PRICING END-->';
			
		
			echo $output;
		}
		
		
		function add_box() {
			$nonce = $_POST['security'];
			if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
			
			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
			
			//default key/value for the tab
			$tab = array(
						'title' => 'Tiny bear',
						'price' => '29',
						'billing' => 'per month',
						'awesomefont' => 'fa fa-laptop',
						'currency' => '$',
						'features' => '<li>Super duper feature</li> <li>yet another cool feature here</li> <li>i am the third feature</li> <li>also the  Fourth</li>',
						'buy_text' => 'Sign up today',
						'buy_url' => '#'
										
			);
			
			if($count) {
				$this->tab3($tab, $count);
			} else {
				die(-1);
			}
			
			die();
		}
		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}