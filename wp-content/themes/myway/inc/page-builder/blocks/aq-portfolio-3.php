<?php
class AQ_Portfolio_Block3 extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Portfolio 3',
			'size' => 'span4',
			"img_preview"=>'portfolio-03.png',
			'fa_icon'=>'fa fa-suitcase',
			'resizable' => 1,
		);
		
		//create the block
		parent::__construct('AQ_Portfolio_Block3', $block_options);
	}
	
	function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
		
	
		
		if (!isset($descriptioncolor))  $descriptioncolor='#8d8d8d';
		if (!isset($number_of_posts))  $number_of_posts='4';
		if (!isset($items_on_row))  $items_on_row='3';
		if (!isset($margintop))  $margintop='0';
		if (!isset($marginbottom))  $marginbottom='0';
		if (!isset($animation))  $animation='zoomIn';
		if (!isset($delayanimation))  $delayanimation='0';
		if (!isset($h2color))  $h2color='#2d2d2d';
		if (!isset($desc_color))  $desc_color='#9f9fa0;';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
        


            
            <p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'cat' ) ); ?>"><?php _e( 'Category: ', 'myway' ); ?></label>

						<?php $categories = get_terms( 'types' ); ?>
                        
						<?php
						$categories2['showall']='Show All';
						foreach( $categories as $category ) { 
							$categories2[$category->term_id] =  $category->name;
						 } ?>
						<?php 
			
				
				$cat = isset($cat) ? isset($cat) : null;
				echo aq_field_select('cat', $block_id, $categories2, $cat); ?>
			</p>
			
			<p class="description">
                <label for="<?php echo $this->get_field_id('number_of_posts') ?>">
                    Number of Porfolio Posts
                    <?php echo aq_field_input('number_of_posts', $block_id, $number_of_posts, $size = 'full') ?>
                </label>
			</p>
            
            <p class="description"><!--Select 1,2,3,4,6-->
				<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
						<?php $options=array(6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
				</label>
			</p><!--Select 1,2,3,4,6-->	

			<p class="description">
                    <label for="<?php echo $this->get_field_id('h2color') ?>">
                         Heading color: <br/>
                        <?php echo aq_field_color_picker('h2color', $block_id, $h2color, '#000000') ?>
                    </label>
            </p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('desc_color') ?>">
                         Details color: <br/>
                        <?php echo aq_field_color_picker('desc_color', $block_id, $desc_color, '#000000') ?>
					</label>
            </p>  			
			
            <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php $options = isset($options) ? isset($options) : null; ?>
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
		    </p><!--Animation-->
                
            <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
			</p><!--Animation delay-->
				
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>
				
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);

		//custom responsive CSS code
		herowp_add_responsive_css();		
		
	?>

    
<div <?php echo herowp_css_unique_id_add(); ?> class="portfolio_block3" style="<?php echo 'margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;' ;?>"><!--portfolio_block1 START-->



			<?php 
			/*
			----------------------------------------------------------Query posts------------------------------------------------------------
			*/			
						//we get the selected category name
						$category_name_selected=get_term($cat, 'types');
						if ($cat !='showall' ){
								$args=array('post_type'=> 'portfolio','post_status'=> 'publish', 'order'=> 'DESC', 'posts_per_page'=>$number_of_posts,'types'=>$category_name_selected->slug); 
						}
						else
							{
								$args=array('post_type'=> 'portfolio','post_status'=> 'publish', 'order'=> 'DESC', 'posts_per_page'=>$number_of_posts); 	
							}
 
						if (!isset($tax)) $tax='';
						if (!isset($term)) $term='';
						$taxargs = array($tax=>$term);
						if($term!='' && $tax!='') { $args  = array_merge($args, $taxargs); }
						query_posts($args);

						
					
						
						
				
            ?>	
			
				
    <div class="portfolio_inner"><!--portfolio_inner START-->
    			
    		<?php 
				/*
			     ----------------------------------------------------------The loop------------------------------------------------------------
		      	*/												
							if (!$delayanimation) {$delayanimation='0';}
							$j = $delayanimation;
							
							while ( have_posts()):the_post();
							$categories = wp_get_object_terms( get_the_ID(), 'types');
					
							$separator = ' ';
							$output = '';
							$class = "";
							if($categories){
							foreach($categories as $category) {
							$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s ",'myway' ), $category->name ) ) . '">'.$category->name.'</a>'.		$separator;
							$class .= $category->slug." ";
								}
							}
			
		  ?>

<?php
//if animations are set/unset
if ($animation != '0'){
	$data_animate='class="mix planning not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
}
else{
	$data_animate='class="mix planning"';
	
} 

if ($animation != '0'){
	$data_animate_small_portfolio='class="mix planning mix-small not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
}
else{
	$data_animate_small_portfolio='class="mix planning"';
}
?>		  

		  <div class="col-lg-<?php echo $items_on_row ;?> col-md-4 col-sm-4 col-xs-12 portfolio-box" ><!--PORTFOLIO POST START-->
                            <section <?php echo $data_animate_small_portfolio; ?>><!--section START-->
                               
						<?php if ( 'video' == get_post_format() ) : //IF it's a video portfolio ?>	

								<div class="port_img"><!--port_img START-->	                
									
											<?php if (function_exists('get_field')) : //IF function get_field exists ?>
											
													<?php $video_id_youtube = get_field('video_id_youtube'); ?>
	
														<?php if ($video_id_youtube) : //IF it's youtube video ?>
												
															
																<div class="flex-video widescreen3">
																	
																	<iframe width="100%" height="100%" src="http://www.youtube.com/embed/<?php echo $video_id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
																
																</div>
																	
														
														<?php endif; //ENDIF it's youtube video ?>
														
														
													<?php $video_id_vimeo = get_field('video_id_vimeo'); ?>
													
														<?php if ($video_id_vimeo) : //IF it's vimeo video ?>
												
															
																<div class="flex-video widescreen3">
																	
																	<iframe src="//player.vimeo.com/video/<?php echo $video_id_vimeo; ?>?title=0&amp;byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
																
																</div>
					
														<?php endif;  //ENDIF it's vimeo video?>
								</div><!--port_img END-->  
								<div class="project_date_added_and_category">
									  <h2><a style="color:<?php echo $h2color; ?>" href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
									  <p class="category" style="color:<?php echo $desc_color; ?>"><i class="icon-files"></i><?php echo $categories[0]->name; ?><span><i class="icon-calendar-4"></i><?php the_time('d M Y') ?></span></p>
								</div>	
								 												
								<?php endif; //ENDIF function get_field exists ?>	
                        
								<?php else : //IF it's image portfolio  ?>
							   
							   <div class="port_img"><!--port_img START-->	
                                        <?php $title_clean = strtolower(str_replace(' ','',get_the_title())); ?>
										<?php
											$id_img = get_post_thumbnail_id();
                                            $img_url = wp_get_attachment_url( $id_img,'full');
                                            $thumb = aq_resize($img_url, 500, 460, true, true, true, true); if ( !$thumb ) $thumb = $img_url;
                                            if ( has_post_thumbnail() ) {      
                                        ?>
                                            <a href="<?php echo $thumb;?>" <?php echo 'data-lightbox="'.$title_clean.'"'; ?>><?php echo '<img src="'.$thumb.'" alt="'.$title_clean.'" class="img-responsive" />';?></a>
                                            
                                        <?php }
                                        else {
                                        echo '<img src="http://placehold.it/500x460/333333/ffffff" alt="' . esc_attr($title_clean) . '">';
                                            }
                                        ?>					
								<a href="<?php echo $thumb; ?>" <?php echo 'data-lightbox="'.$title_clean.'"'; ?> class="maginfy"><i class="icon-magnifi-glass2"></i></a>
								<a href="<?php the_permalink(); ?>" class="read-more"><i class="icon-broke-link2"></i></a>
                                <?php $j=$j+$delayanimation; ?>
                                </div><!--port_img END-->
                              <div class="project_date_added_and_category">
                                  <h2><a style="color:<?php echo $h2color; ?>" href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h2>
								  <p class="category" style="color:<?php echo $desc_color; ?>"><i class="icon-files"></i><?php echo $categories[0]->name; ?><span><i class="icon-calendar-4"></i><?php the_time('d M Y') ?></span></p>
                              </div>
                            
							<?php endif; //end big IF it's a video ?>          
                       
					   </section><!--section END-->
		  </div><!--PORTFOLIO POST END-->
	
       <?php endwhile; wp_reset_query(); ?>            
	</div><!--portfolio_inner END-->	
</div><!--portfolio_block1 END-->
<?php
	}	
}