<?php
class AQ_Button extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Button',
			'size' => 'span4',
			'resizable' => 1,
			"img_preview"=>'button.png',
			'fa_icon'=>'fa fa-plus-square-o'
			
		);
		
		//create the block
		parent::__construct('AQ_Button', $block_options);
	}
	
	function form($instance) {
		
		if (!isset($button_url))  $button_url='#';
		if (!isset($button_bg))  $button_bg='#FFFFFF';
		if (!isset($button_color))  $button_color='#333';
		if (!isset($margintop))  $margintop='';
		if (!isset($marginbottom))  $marginbottom='';
		if (!isset($button_text))  $button_text='Click here!';
		if (!isset($font_awesome))  $font_awesome='fa-leaf';
		if (!isset($text_align))  $text_align='center';
		if (!isset($options))  $options='';
		if (!isset($iconcolor))  $iconcolor='#FFFFFF';
		if (!isset($css))  $css='';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
        
			
			<p class="description">
                 <label for="<?php echo $this->get_field_id('button_bg') ?>">
                    Button background color: <br/>
                   <?php echo aq_field_color_picker('button_bg', $block_id, $button_bg, '#FFFFFF') ?>
                 </label>
            </p>
			
			<p class="description">
                 <label for="<?php echo $this->get_field_id('button_color') ?>">
                   Button text color: <br/>
                   <?php echo aq_field_color_picker('button_color', $block_id, $button_color, '#333') ?>
                 </label>
            </p>			
			<p class="description">
                 <label for="<?php echo $this->get_field_id('iconcolor') ?>">
                   Icon color: <br/>
                   <?php echo aq_field_color_picker('iconcolor', $block_id, $iconcolor, '#FFFFFF') ?>
                 </label>
            </p>
			
			<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('font_awesome') ?>">
							Select a font icon for this element.
							<input  type="text" id="<?php echo $this->get_field_id('font_awesome') ?>" class="awesome-fonts-input input-full" name="<?php echo $this->get_field_name('font_awesome') ?>" value="<?php echo $font_awesome ?>" />
							<a class="font-awesome-icon-select">Select Font Icon</a>
						</label>
			</p>
			
			<p class="description">
					<label for="<?php echo $this->get_field_id('text_align') ?>">
						<strong>Button Alignment:</strong> Select the alignment of button.
							<?php echo herowp_text_align('text_align', $block_id, $options, $text_align); ?>
					</label>
			</p>
			
             <p class="description">
                <label for="<?php echo $this->get_field_id('button_text') ?>">
                   Button text
                    <?php echo aq_field_input('button_text', $block_id, $button_text, $size = 'full') ?>
                </label>
            </p>
            
            <p class="description">
                <label for="<?php echo $this->get_field_id('button_url') ?>">
                   Button URL
                    <?php echo aq_field_input('button_url', $block_id, $button_url, $size = 'full') ?>
                </label>
            </p>
              
            <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of button, in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of button, in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>            
			
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('css') ?>">
                        <strong>Additonal button CSS</strong> Add any additonal css rules. Do NOT add braces -> "{}". Add only css rules separeted by semicolon.  
                        <?php echo aq_field_input('css', $block_id, $css, $size = 'full') ?>
                    </label>
			</p>
			
			
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>
			
			  <div class="font-awesome-select"><!--font-awesome-select START-->
            	<div class="font-awesome-plus-minus">
                	<a class="font-awesome-plus">+</a>
                    <a class="font-awesome-minus">-</a>
                	<a class="font_awesome_close">Close</a>
                </div>
           	<ul>
				<?php herowp_font_icon(); ?>
            </ul>
        </div><!--font-awesome-select END-->

            
        
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);
		
		//custom responsive CSS code
		herowp_add_responsive_css();
		
		
	?>
	

<div class="col-md-12">	
	<div <?php echo herowp_css_unique_id_add(); ?> class="static_content" style="<?php echo 'margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;' ;?>"><!--static_content START-->
						
							 <div class="global-buttons-center" style="text-align:<?php echo $text_align; ?>;">
									<a class="quote-button trim" href="<?php echo $button_url; ?>" style="color:<?php echo $button_color;?>; background:<?php echo $button_bg;?>; <?php echo $css; ?>"><span class="icon"><i class="fa <?php echo $font_awesome; ?>" style="color:<?php echo $iconcolor; ?>;"></i></span><span class="btn_text"><?php echo $button_text; ?></a>
							 </div>
					
	</div><!--static_content END-->
</div>




<?php
	}
	
}