<?php
/* Aqua Box Block */
if(!class_exists('AQ_Team_Block')) {
	class AQ_Team_Block extends AQ_Block {
	
		function __construct() {
			$block_options = array(
				'name' => 'Team Block',
				'size' => 'span4',
				"img_preview"=>'team.png',
				'fa_icon'=>'fa fa-users'
			);
			
			//create the widget
			parent::__construct('AQ_Team_Block', $block_options);
			
			//add ajax functions
			add_action('wp_ajax_aq_block_team_add_new', array($this, 'add_team'));
			
		}
		
		function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
	
		if (!isset($items_on_row))  $items_on_row='1';
		if (!isset($animation))  $animation='fadeIn';
		if (!isset($delayanimation))  $delayanimation='0';
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();		

		
			$defaults = array(
				'team' => array(
					1 => array(
						'title' => 'John Doe',
						'subtitle' => 'SEO Expert',
						'content' => 'My box contents',
						'image' => '',
						'quote' => 'Imagination is more important then knowledge.',
						'quote_author' => 'Albert Einstein'
					)
				),
				'type'	=> 'team',
			);
			
			$instance = wp_parse_args($instance, $defaults);
			extract($instance);
			
			$tab_types = array(
				'team' => 'team',
			);
			
			?>
		<div class="description cf">
				<ul id="aq-sortable-list-<?php echo $block_id ?>" class="aq-sortable-list" rel="<?php echo $block_id ?>">
					<?php
					$team = is_array($team) ? $team : $defaults['team'];
					$count = 1;
					foreach($team as $box) {	
						$this->tab3($box, $count);
						$count++;
					}
					?>
				</ul>
								<p></p>
				<a href="#" rel="team" class="aq-sortable-add-new button">Add New</a>
				<p></p>
				

			
			<p class="description"><!--Select 1,2,3,4,6-->
				<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
						<?php $options=array(6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
				</label>
			</p><!--Select 1,2,3,4,6-->

             <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
                
            <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
                 <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elemtns appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
				</p><!--Animation delay-->

				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>				
				
		</div>
			<?php
		}
		
		function tab3($box = array(), $count = 0) {
		
				if (!isset($box['subtitle']))  $box['subtitle']='';
				if (!isset($box['facebook']))  $box['facebook']='';
				if (!isset($box['quote']))  $box['quote']='Imagination is more important then knowledge.';
				if (!isset($box['quote_author']))  $box['quote_author']='Albert Einstein';
				if (!isset($box['twitter']))  $box['twitter']='';	
				if (!isset($box['google']))  $box['google']='';	
				if (!isset($box['linkedin']))  $box['linkedin']='';	
				if (!isset($tab['rss']))  $tab['rss']='';	
			?>
			<li id="<?php echo $this->get_field_id('team') ?>-sortable-item-<?php echo $count ?>" class="sortable-item" rel="<?php echo $count ?>">
				
				<div class="sortable-head cf">
					<div class="sortable-title">
						<strong><?php echo $box['title']; ?></strong>
					</div>
					<div class="sortable-handle">
						<a href="#">Open / Close</a>
					</div>
				</div>
				
				<div class="sortable-body">
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-content">
							Profile Image<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-image" class="input-full input-upload" value="<?php echo $box['image'] ?>" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][image]">
							<a href="#" class="aq_upload_button button" rel="image">Upload</a><p></p>
						</label>				
					</p>	
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-title">
							Profile Name<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-title" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][title]" value="<?php echo $box['title'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-subtitle">
							Job Title<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-subtitle" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][subtitle]" value="<?php echo $box['subtitle'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-quote">
							Quote<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-quote" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][quote]" value="<?php echo $box['quote'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-quote_author">
							Quote Author<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-quote_author" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][quote_author]" value="<?php echo $box['quote_author'] ?>" />
						</label>
					</p>
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-facebook">
							Facebook Link<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-facebook" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][facebook]" value="<?php echo $box['facebook'] ?>" />
						</label>
					</p>					
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-twitter">
							Twitter Link<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-twitter" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][twitter]" value="<?php echo $box['twitter'] ?>" />
						</label>
					</p>		
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-google">
							Google Link<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-google" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][google]" value="<?php echo $box['google'] ?>" />
						</label>
					</p>		
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-linkedin">
							Linkedin Link<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-linkedin" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][linkedin]" value="<?php echo $box['linkedin'] ?>" />
						</label>
					</p>			
					<p class="tab-desc description">
						<label for="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-rss">
							RSS Link<br/>
							<input type="text" id="<?php echo $this->get_field_id('team') ?>-<?php echo $count ?>-rss" class="input-full" name="<?php echo $this->get_field_name('team') ?>[<?php echo $count ?>][rss]" value="<?php echo $tab['rss'] ?>" />
						</label>
					</p>			
					

					<p class="tab-desc description"><a href="#" class="sortable-delete">Delete</a></p>
				</div>
				
			</li>
			<?php
		}
		
		function block($instance) {
			
			global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
			
			extract($instance);
			
			//custom responsive CSS code
			herowp_add_responsive_css();			
			
			wp_enqueue_script('jquery-ui-boxes');
		

			echo'
	<div '.herowp_css_unique_id_add().' class="team-wrap" style="margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;"><!--TEAM WRAP START-->'; ?>

	<?php
			
					$i = 1;
					$j = $delayanimation;
					
					//if animations are set/unset
						
					$output='';
					
					foreach( $team as $box ){
					
					if ($animation != '0'){
						$data_animate='class="team-boxes not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
					}
					else{
						$data_animate='class="team-boxes"';
					}
					if (empty($box['image'])) {
						$box['image'] = NULL;
					}
					else{
						$box['image'] = $box['image'];
					}
					
					
					
					
						$output.='
				<div class="col-md-'.$items_on_row.' col-sm-4 col-xs-12"><!--COLS START-->
					<div '.$data_animate.'><!--TEAM BOXES START-->
                        <div class="team-thumb overlay-image view-overlay"><!--TEAM THUMB START-->';
							if ($box['image']){ 
							$image_resized=aq_resize($box['image'],377,301,true);
						$output .='<img src="'.$image_resized.'" alt="'.get_the_title().'">';
						}
						else{
                        $output .='<img src="http://placehold.it/377x301/333333/ffffff" alt="">';
						}
                        $output .='<div class="clear"></div>
						<div class="mask team_quote"><!--MASK START-->
                        						<div class="port-zoom-link height80"><!--port-zoom-link START-->
                                                	  <h2>"'.$box['quote'].'"</h2>
													  <p>'.$box['quote_author'].'</p>
                                                </div><!--port-zoom-link END-->
                   							 </div><!--MASK END-->
                        </div><!--TEAM THUMB END-->
							<div class="team-info"><!--TEAM INFO START-->
									<h2>'. $box['title'] . '</h2>
									<p>'. $box['subtitle'] . '</p>
							</div><!--TEAM INFO END-->
							<div class="separator"></div>
							<div class="team-social"><!--TEAM SOCIAL START-->
									<a href="'.$box['facebook'].'"><i class="fa fa-facebook-square"></i></a>
									<a href="'.$box['twitter'].'"><i class="fa fa-twitter"></i></a>
									<a href="'.$box['google'].'"><i class="fa fa-google-plus"></i></a>
									<a href="'.$box['linkedin'].'"><i class="fa fa-linkedin"></i></a>
									<a href="'.$box['rss'].'"><i class="fa fa-rss"></i></a>
							</div><!--TEAM SOCIAL END-->
					</div><!--TEAM BOXES END-->
				</div><!--COLS END-->';
					$i++;
					$j = $j+$delayanimation;
					}
      	echo $output;
		?>

	<?php echo '</div><!--TEAM WRAP END-->'; ?>
	
	<?php	}
			
		/* AJAX add tab */
		function add_team() {
			$nonce = $_POST['security'];
			if (! wp_verify_nonce($nonce, 'aqpb-settings-page-nonce') ) die('-1');
			
			$count = isset($_POST['count']) ? absint($_POST['count']) : false;
			$this->block_id = isset($_POST['block_id']) ? $_POST['block_id'] : 'aq-block-9999';
			
			//default key/value for the tab
			$team = array(
				'title' => 'John Doe',
						'subtitle' => 'SEO Expert',
						'content' => 'My box contents',
						'image' => '',
						'quote' => 'Imagination is more important then knowledge.',
						'quote_author' => 'Albert Einstein'		
			);
			
			if($count) {
				$this->tab3($team, $count);
			} else {
				die(-1);
			}
			
			die();
		}
		
		function update($new_instance, $old_instance) {
			$new_instance = aq_recursive_sanitize($new_instance);
			return $new_instance;
		}
	}
}
