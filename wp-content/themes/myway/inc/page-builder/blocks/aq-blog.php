<?php
class AQ_Blog_Posts_Block extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Blog Posts',
			'size' => 'span4',
			'resizable' => 1,
			"img_preview"=>'blog.png',
			"fa_icon"=>'fa fa-pencil',
		);
		
		//create the block
		parent::__construct('AQ_Blog_Posts_Block', $block_options);
	}
	
	function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($animation))  $animation='fadeIn';
		if (!isset($delayanimation))  $delayanimation='0';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
        
			
            	
		
            
            <p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'cat' ) ); ?>"><?php _e( 'From what category you want to display the posts: ', 'myway' ); ?></label>

						<?php $categories = get_terms( 'category' ); ?>
                        
						<?php
						$categories2['showall']='Show All';
						foreach( $categories as $category ) { 
							$categories2[$category->term_id] =  $category->name;
						 } ?>
						<?php 
			
				
				$cat = isset($cat) ? isset($cat) : null;
				echo aq_field_select('cat', $block_id, $categories2, $cat); ?>
			</p>
            

            
                        
            <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php $options = isset($options) ? isset($options) : null; ?>
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
            <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
			</p><!--Animation delay-->
            
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>           
        
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);

		//custom responsive CSS code
		herowp_add_responsive_css();		
		
	?>

    
<div <?php echo herowp_css_unique_id_add(); ?> class="blog_posts_block1" style="<?php echo 'margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;' ;?>"><!--blog_posts_block1 START-->

			
<?php
	//if delay animation value is not set
	if (!isset($delayanimation)) {$delayanimation='0';}
	$j = $delayanimation;
	$i=1;
	//the loop
	if ($cat !='showall' ){
		$the_query = new WP_Query( "showposts=3&cat=$cat&order=DESC" );
	}
	else{
		$the_query = new WP_Query( "showposts=3&order=DESC" );
	}
	while ($the_query -> have_posts()) : $the_query -> the_post(); 
?>

<?php
//if animations are set/unset
if ($animation != '0'){
	$data_animate='class="blog-posts not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
}
else{
	$data_animate='class="blog-posts"';
} 
?>
		<?php if ($i==1) : ?>	
		
			<div class="col-md-6 nopadding"><!--COL-MD-6 START-->
                <div class="col-md-12 col-sm-4 col-xs-12 blog-box" ><!--BLOG POST START-->
							
                            <section <?php echo $data_animate; ?>><!--section START-->
                                <div class="blog_img"><!--blog_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
												$id_img = get_post_thumbnail_id();
												$img_url = wp_get_attachment_url( $id_img,'full');
												$thumb = aq_resize($img_url, 580, 643, true, true,true,true); if ( !$thumb ) $thumb = $img_url;
										?>
												<a href="<?php the_permalink(); ?>">
										<?php
													echo '<img src="'.$thumb.'" alt="'.get_the_title().'" class="img-responsive" />
												</a>';
											}
											else echo'<img src="http://placehold.it/595x643/333333/ffffff" alt="'.get_the_title().'">';
                                        ?>
										<a class="read_more" href="<?php the_permalink(); ?>"><?php _e('Read More','myway'); ?></a>	
                                <div class="date_added_and_category"><!--date_added_and_category START-->
									<div class="round_date">
										<p><?php echo the_time('d'); ?></p>
										<span><?php echo the_time('M'); ?></span>
									</div>
									
									<div class="round_comments">
										<p><?php echo get_comments_number(); ?></p>
										<span><?php _e('comm','myway'); ?></span>
									</div>
                               </div><!--date_added_and_category END-->
								
								</div><!--blog_img END-->
								


                                        
                               <h2><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?><span class="dotcolor"> .</span></a></h2>
                               <p><?php echo excerpt(15); ?></p>

                         <?php $j=$j+$delayanimation; ?>      
                       </section><!--section END-->
			</div><!--BLOG POST END-->
		</div><!--COL-MD-6 END-->
			
			
		<?php elseif ($i>1) : ?>
		
		
                <div class="col-md-6 col-sm-4 col-xs-12 blog-box" ><!--BLOG POST START-->
							
                            <section <?php echo $data_animate; ?>><!--section START-->
                                <div class="blog_img"><!--blog_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
												$id_img = get_post_thumbnail_id();
												$img_url = wp_get_attachment_url( $id_img,'full');
												$thumb = aq_resize($img_url, 580, 240, true); if ( !$thumb ) $thumb = $img_url;
										?>
												<a href="<?php the_permalink(); ?>">
										<?php
													echo '<img src="'.$thumb.'" alt="'.get_the_title().'" class="img-responsive" />
												</a>';
											}
											else echo'<img src="http://placehold.it/595x240/333333/ffffff" alt="'.get_the_title().'">';
                                        ?>
										<a class="read_more" href="<?php the_permalink(); ?>"><?php _e('Read More','myway'); ?></a>	
								                               <div class="date_added_and_category"><!--date_added_and_category START-->
								   <div class="round_date">
											<p><?php echo the_time('d'); ?></p>
											<span><?php echo the_time('M'); ?></span>
										</div>
										
										<div class="round_comments">
											<p><?php echo get_comments_number(); ?></p>
											<span><?php _e('comm','myway'); ?></span>
										</div>
								   </div><!--date_added_and_category END-->
							   
                               </div><!--blog_img END-->
                      
                               <h2><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?><span class="dotcolor"> .</span></a></h2>
                               <p><?php echo excerpt(15); ?></p>

                         <?php $j=$j+$delayanimation; ?>      
                       </section><!--section END-->
			</div><!--BLOG POST END-->
		
		
		
		<?php endif ;?>
                
       <?php $i++; endwhile; wp_reset_postdata(); ?>  

</div><!--blog_posts_block1 END-->


<?php
	}
	
}