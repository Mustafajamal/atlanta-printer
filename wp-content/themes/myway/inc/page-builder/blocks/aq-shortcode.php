<?php
/** A simple text block **/
class AQ_Shortcode extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Shortcode',
			'size' => 'span4',
			"img_preview"=>'shortcode.png',
			'fa_icon'=>'fa fa-plus-square'
		);
		
		//create the block
		parent::__construct('aq_shortcode', $block_options);
	}
	
	function form($instance) {
		
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();		
		
		$defaults = array(
			'text_shortcode' => '',
			'wp_shortcode' => 0
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		
		?>

		
		<p class="description">
			<label for="<?php echo $this->get_field_id('text_shortcode') ?>">
				Shortcode here:
				<?php echo aq_field_input('text_shortcode', $block_id, $text_shortcode, $size = 'full') ?>
			</label>

		</p>
		
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>		
		
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);
		
		//custom responsive CSS code
		herowp_add_responsive_css();		

		$wp_shortcode = ( isset($wp_shortcode) ) ? $wp_shortcode : 0;

		if($herowp_responsive_320 || $herowp_responsive_480 || $herowp_responsive_768 || $herowp_responsive_960){
			echo '<div '.herowp_css_unique_id_add().'><!--'.$herowp_css_unique_id.' START-->';
			echo "\r\n";
		}
		
		echo do_shortcode(htmlspecialchars_decode($text_shortcode));
		
		if($herowp_responsive_320 || $herowp_responsive_480 || $herowp_responsive_768 || $herowp_responsive_960){
			echo "\r\n";
			echo '<div><!--'.$herowp_css_unique_id.' END-->';
		}
	}
	
}
