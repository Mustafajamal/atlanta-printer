<?php
/** A simple rich textarea block **/
class AQ_Editor_Block extends AQ_Block {
	
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => __('Visual Editor', 'myway'),
			'size' => 'span6',
			'fa_icon' => 'fa fa-edit',
			"img_preview"=>'editor.png',
		);
		
		//create the block
		parent::__construct('aq_editor_block', $block_options);
	}
	
	function form($instance) {
		
		$defaults = array(
			'text' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>		
		<p class="description">
			<label for="<?php echo $this->get_field_id('text') ?>">
				Content
				<?php 
				$args = array (
				    'tinymce'       => true,
				    'quicktags'     => true,
				    'textarea_name' => $this->get_field_name('text')
				);
				wp_editor( htmlspecialchars_decode($text), $this->get_field_id('text'), $args );
				?>
			</label>
		</p>
		
		<?php
	}
	
	function block($instance) {
		echo '<div class="col-md-12 herowp_builder_editor" id="'.$this->get_field_id('text').'">';
			extract($instance);
			echo do_shortcode(htmlspecialchars_decode($text));
		echo '</div>';
	}
	
}