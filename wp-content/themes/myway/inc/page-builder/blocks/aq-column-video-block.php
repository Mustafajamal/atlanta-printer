<?php
/** A simple text block **/
class AQ_Column_Video_Block extends AQ_Block {
	
	/* PHP5 constructor */
	function __construct() {
		
		$block_options = array(
			'name' => '<i class="fa fa-film"></i><strong><span class="row-white"> Row Video</span></strong>',
			'size' => 'span12',
			"class_css"=>'bf_columns',
			"img_preview"=>'column.png',
			'resizable' => 0
		);
		
		//create the widget
		parent::__construct('aq_column_video_block', $block_options);
		
	}



	//form header
	function before_form($instance) {
		extract($instance);
		
		$title = $title ? '<span class="in-block-title"> : '.$title.'</span>' : '';
		$resizable = $resizable ? '' : 'not-resizable';
		
		echo '<li id="template-block-'.$number.'" class="block block-container block-'.$id_base.' '. $size .' '.$resizable.'">',
				'<dl class="block-bar">',
					'<dt class="block-handle">',
						'<div class="block-title">',
							$name , $title, 
						'</div>',
						'<span class="block-controls">',
							'<a class="block-edit" id="edit-'.$number.'" title="Edit Block" href="#block-settings-'.$number.'">Edit Block</a>',
						'</span>',
					'</dt>',
				'</dl>',
				'<div class="block-settings cf" id="block-settings-'.$number.'">';
	}

	function form($instance) {
	
	if (!isset($bk_video))  $bk_video='';
	if (!isset($image4))  $image4='';
	if (!isset($sound_volume))  $sound_volume='';
			echo 		'<h2 class="column_h2">',
						__('<i class="fa fa-film"></i>ROW VIDEO. DRAG BLOCKS INSIDE AT THE BOTTOM AREA.','myway'),
						'</h2>' ;?>
 

			<p class="description" style="margin-left:10px;">
                <label for="<?php echo $this->get_field_id('bk_video') ?>">
                    Row Background Video Id (eg: nLwML2PagbY)
                    <?php echo aq_field_input('bk_video', $this->block_id, $bk_video, $size = 'full') ?>
                </label>
			</p>
				 <p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('image4') ?>">
						Row Video Overlay Pattern Background:
						<?php echo aq_field_upload('image4', $this->block_id, $image4) ?>
					</label>
				</p>
				
             <p class="description" style="margin-left:10px;">
				<label for="<?php echo $this->get_field_id('sound_volume') ?>">
						<strong>Volume:</strong> Select the volume percentage.
						<?php $options=array(0=>'Mute',10=>'10',20=>'20',30=>'30',40=>'40',50=>'50',60=>'60',70=>'70',80=>'80',90=>'90',100=>'100'); echo aq_field_select('sound_volume', $this->block_id, $options, $sound_volume); ?>
				</label>
			</p>   
				<?php
			echo '<ul class="blocks column-blocks cf"></ul>';	

	}
	
	function form_callback($instance = array()) {
		$instance = is_array($instance) ? wp_parse_args($instance, $this->block_options) : $this->block_options;
		
		//insert the dynamic block_id & block_saving_id into the array
		$this->block_id = 'aq_block_' . $instance['number'];
		$instance['block_saving_id'] = 'aq_blocks[aq_block_'. $instance['number'] .']';

		extract($instance);
		
		$col_order = $order;
		
		//column block header
		if(isset($template_id)) {
			echo '<li id="template-block-'.$number.'" class="block block-container block-aq_column_block '.$size.'">',
					'<div class="block-settings-column cf" id="block-settings-'.$number.'">',
						'<h2 class="column_h2">',
						__('<i class="fa fa-film"></i>ROW VIDEO. DRAG BLOCKS INSIDE AT THE BOTTOM AREA.','myway'),
						'</h2>',
						'<button class="column_size_edit" type="button">Expand</button>';								?>

			<p class="description" style="margin-left:10px;">
                <label for="<?php echo $this->get_field_id('bk_video') ?>">
                    Row Background Video Id (eg: nLwML2PagbY)
                    <?php echo aq_field_input('bk_video', $this->block_id, $bk_video, $size = 'full') ?>
                </label>
			</p>
				 <p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('image4') ?>">
						Row Video Overlay Pattern Background:
						<?php echo aq_field_upload('image4', $this->block_id, $image4) ?>
					</label>
				</p>
				
             <p class="description" style="margin-left:10px;">
				<label for="<?php echo $this->get_field_id('sound_volume') ?>">
						<strong>Volume:</strong> Select the volume percentage.
						<?php $options=array(0=>'Mute',10=>'10',20=>'20',30=>'30',40=>'40',50=>'50',60=>'60',70=>'70',80=>'80',90=>'90',100=>'100'); echo aq_field_select('sound_volume', $this->block_id, $options, $sound_volume); ?>
				</label>
			</p>            				
				<?php
					echo 	'<ul class="blocks column-blocks cf">';
					
			//check if column has blocks inside it
			$blocks = aq_get_blocks($template_id);

			//outputs the blocks
			if($blocks) {
				foreach($blocks as $key => $child) {
					global $aq_registered_blocks;
					extract($child);
					
					//get the block object
					$block = $aq_registered_blocks[$id_base];
					
					if($parent == $col_order) {
						$block->form_callback($child);
					}
				}
			} 
			echo 		'</ul>';
			

			
		} else {
			$this->before_form($instance);
			$this->form($instance);
		}
				
		//form footer
		$this->after_form($instance);
	}
	
	//form footer
	function after_form($instance) {
		extract($instance);
		
		$block_saving_id = 'aq_blocks[aq_block_'.$number.']';
		
		$name_stripped = 'Row Video';
			
			echo '<div class="block-control-actions cf"><a href="#" class="delete submitdelete deletecolumn">Delete Row</a></div>';
			echo '<input type="hidden" class="id_base" name="'.$this->get_field_name('id_base').'" value="'.$id_base.'" />';
			echo '<input type="hidden" class="name" name="'.$this->get_field_name('name').'" value="'.$name_stripped.'" />';
			echo '<input type="hidden" class="order" name="'.$this->get_field_name('order').'" value="'.$order.'" />';
			echo '<input type="hidden" class="size" name="'.$this->get_field_name('size').'" value="'.$size.'" />';
			echo '<input type="hidden" class="parent" name="'.$this->get_field_name('parent').'" value="'.$parent.'" />';
			echo '<input type="hidden" class="number" name="'.$this->get_field_name('number').'" value="'.$number.'" />';
		echo '</div>',
			'</li>';
	}
	
	function block_callback($instance) {
		$instance = is_array($instance) ? wp_parse_args($instance, $this->block_options) : $this->block_options;
		
		extract($instance);
		$col_order = $order;
		$col_size = absint(preg_replace("/[^0-9]/", '', $size));
		
		//column block header
		if(isset($template_id)) {
		//if no image for background is selected we set the background to white
		echo '
<div class="video-column-holder-'.$template_id.'-'.$number.'"><!--VIDEO COLUMN HOLDER START-->';

				echo '<div style="position: absolute;
height: 100%;width: 100%; background:url('.$image4.') repeat"></div>';
		if ($sound_volume==0) $vol='mute:true'; else $vol='vol:'.$sound_volume;
		echo '
	<div class="player" data-property="{videoURL:\''.$bk_video.'\',containment:\'.video-column-holder-'.$template_id.'-'.$number.'\',autoPlay:true,showControls:false, '.$vol.', ratio : \'4/3\', quality:\'hd1080\', opacity:1}"></div>';
		
		echo '
	<div class="container"><!--CONTAINER START-->';						
			$this->before_block($instance);
			
			//define vars
			$overgrid = 0; $span = 0; $first = false;
			
			//check if column has blocks inside it
			$blocks = aq_get_blocks($template_id);

			//outputs the blocks
			if($blocks) {
				foreach($blocks as $key => $child) {
					global $aq_registered_blocks;
					extract($child);
					
					if(class_exists($id_base)) {
						//get the block object
						$block = $aq_registered_blocks[$id_base];
						
						//insert template_id into $child
						$child['template_id'] = $template_id;
						
						//display the block
						if($parent == $col_order) {
							
							$child_col_size = absint(preg_replace("/[^0-9]/", '', $size));
							
							$overgrid = $span + $child_col_size;

							if($overgrid > $col_size || $span == $col_size || $span == 0) {
								$span = 0;
								$first = true;
							}
							
							if($first == true) {
								$child['first'] = true;
							}
							$block->block_callback($child);
							
							$span = $span + $child_col_size;
							
							$overgrid = 0; //reset $overgrid
							$first = false; //reset $first
						}
					}
				}
			} 
			
			$this->after_block($instance);
			echo '
	</div><!--CONTAINER END-->
</div><!--VIDEO COLUMN HOLDER END-->

';
			
		} else {
			//show nothing
		}
	}
	
}