<?php
class AQ_Blog_Posts_Block2 extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Blog Posts 2',
			'size' => 'span4',
			'resizable' => 1,
			"img_preview"=>'blog-02.png',
			"fa_icon"=>'fa fa-pencil',
		);
		
		//create the block
		parent::__construct('AQ_Blog_Posts_Block2', $block_options);
	}
	
	function form($instance) {
		//default values when WP DEBUG is set to true to avoid the undefined index warning
		if (!isset($margintop))  $margintop='50';
		if (!isset($marginbottom))  $marginbottom='50';
		if (!isset($animation))  $animation='bounceInUp';
		if (!isset($delayanimation))  $delayanimation='300';
		if (!isset($number_of_posts))  $number_of_posts='4';
		if (!isset($items_on_row))  $items_on_row='1';
		if (!isset($h2color))  $h2color='#333333';
		if (!isset($desc))  $desc='#333333';
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
        
			
            	
		
            
            <p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'cat' ) ); ?>"><?php _e( 'From what category you want to display the posts: ', 'myway' ); ?></label>

						<?php $categories = get_terms( 'category' ); ?>
                        
						<?php
						$categories2['showall']='Show All';
						foreach( $categories as $category ) { 
							$categories2[$category->term_id] =  $category->name;
						 } ?>
						<?php 
			
				
				$cat = isset($cat) ? isset($cat) : NULL;
				echo aq_field_select('cat', $block_id, $categories2, $cat); ?>
			</p>
			
			<p class="description">
                <label for="<?php echo $this->get_field_id('number_of_posts') ?>">
                    Number of Posts
                    <?php echo aq_field_input('number_of_posts', $block_id, $number_of_posts, $size = 'full') ?>
                </label>
			</p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('h2color') ?>">
                         Heading post color: <br/>
                        <?php echo aq_field_color_picker('h2color', $block_id, $h2color, '#000000') ?>
                    </label>
            </p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('desc') ?>">
                         Details post color: <br/>
                        <?php echo aq_field_color_picker('desc', $block_id, $desc, '#000000') ?>
					</label>
            </p>    
            
			 <p class="description"><!--Select 1,2,3,4,6-->
				<label for="<?php echo $this->get_field_id('items_on_row') ?>">
						<strong>Items on row:</strong> How many items of this block, you want to be displayed on a line.
						<?php $options=array(6=>'2',4=>'3',3=>'4',2=>'6'); echo aq_field_select('items_on_row', $block_id, $options, $items_on_row); ?>
				</label>
			</p><!--Select 1,2,3,4,6-->
            
                        
            <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation') ?>">
						<strong>Animations:</strong> Select animation you want to use for this block elements.
							<?php echo herowp_animations('animation', $block_id, $options, $animation); ?>
					</label>
			</p><!--Animation-->
                
            <p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animations delay:</strong> Enter the delay between boxes appear animations. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
			</p><!--Animation delay-->
            
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>
            
        
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);
		
		//custom responsive CSS code
		herowp_add_responsive_css();

		
	?>

    
<div <?php echo herowp_css_unique_id_add(); ?> class="blog_posts_block2" style="<?php echo 'margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;' ;?>"><!--blog_posts_block2 START-->

	

			
<?php
	//if delay animation value is not set
	if (!isset($delayanimation)) {$delayanimation='0';}
	$j = $delayanimation;
	//the loop
	if ($cat !='showall' ){
		$the_query = new WP_Query( "showposts=$number_of_posts&cat=$cat&order=DESC" );
	}
	else{
		$the_query = new WP_Query( "showposts=$number_of_posts&order=DESC" );
	}
	while ($the_query -> have_posts()) : $the_query -> the_post(); 
?>

<?php
//if animations are set/unset
if ($animation != '0'){
	$data_animate='class="blog-posts not-animated" data-animate="'.$animation.'" data-delay="'.$j.'"';
}
else{
	$data_animate='class="blog-posts"';
} 
?>
			
		

                <div class="col-md-<?php echo $items_on_row; ?> col-sm-4 col-xs-12 blog-box" ><!--BLOG POST START-->
							
                            <section <?php echo $data_animate; ?>><!--section START-->
                                <div class="blog_img"><!--blog_img START-->	
                                        <?php 
                                            if ( has_post_thumbnail() ) { 
												$id_img = get_post_thumbnail_id();
												$img_url = wp_get_attachment_url( $id_img,'full');
												$thumb = aq_resize($img_url, 500, 500, true, true,true,true); if ( !$thumb ) $thumb = $img_url;
										?>
												<a href="<?php the_permalink(); ?>">
										<?php
													echo '<img src="'.esc_attr($thumb).'" alt="'.get_the_title().'" class="img-responsive" />
												</a>';
											}
											else echo'<img src="http://placehold.it/500x500/333333/ffffff" alt="'.get_the_title().'">';
                                        ?>
                                </div><!--blog_img END-->
								
							   <div class="date_holder">
										<span><?php echo the_time('d'); ?></span>
										<span class="month"><?php echo the_time('M'); ?></span>
							   </div>
                                        
                               <h2><a href="<?php the_permalink(); ?>" style="color:<?php echo $h2color; ?>"><?php echo the_title(); ?></a></h2>
							   
							   <div class="category">
										<span style="color:<?php echo $desc; ?>"> <i class="icon-administrator"></i><?php the_author(); ?> // <i class="icon-file-horizontaltext"></i> <?php $category = get_the_category(); echo $category[0]->cat_name;?></span>
							</div>
                              
                         <?php $j=$j+$delayanimation; ?>      
                       </section><!--section END-->
			</div><!--BLOG POST END-->              
       <?php endwhile; wp_reset_query(); ?>  

</div><!--blog_posts_block2 END-->


<?php
	}
	
}