<?php
/** A simple text block **/
class AQ_Column_Block extends AQ_Block {
	
	/* PHP5 constructor */
	function __construct() {
		
		$block_options = array(
			'name' => '<i class="fa fa-bars"></i><strong><span class="row-white"> Row Simple</span></strong>',
			'size' => 'span12',
			"class_css"=>'bf_columns',
			"img_preview"=>'column.png',
			'resizable' => 1
		);
		
		//create the widget
		parent::__construct('aq_column_block', $block_options);
		
	}



	//form header
	function before_form($instance) {
		extract($instance);
		
		$title = $title ? '<span class="in-block-title"> : '.$title.'</span>' : '';
		$resizable = $resizable ? '' : 'not-resizable';
		
		echo '<li id="template-block-'.$number.'" class="block block-container block-'.$id_base.' '. $size .' '.$resizable.'">',
				'<dl class="block-bar">',
					'<dt class="block-handle">',
						'<div class="block-title">',
							$name , $title, 
						'</div>',
						'<span class="block-controls">',
							'<a class="block-edit" id="edit-'.$number.'" title="Edit Block" href="#block-settings-'.$number.'">Edit Block</a>',
						'</span>',
					'</dt>',
				'</dl>',
				'<div class="block-settings cf" id="block-settings-'.$number.'">';
	}

	function form($instance) {
	
	if (!isset($image))  $image='';
	if (!isset($color))  $color='';
	if (!isset($is_pattern))  $is_pattern='';
	if (!isset($border_top))  $border_top='';
	if (!isset($border_bottom))  $border_bottom='';
	if (!isset($margin_top))  $margin_top='';
	//if (!isset($is_full_screen))  $is_full_screen='';
	
	echo 		'<h2 class="column_h2">',
						__('<i class="fa fa-bars"></i>ROW SIMPLE. DRAG BLOCKS INSIDE AT THE BOTTOM AREA.','myway'),
						'</h2>'; ?>
						
	               <p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('image') ?>">
						Row Background Image:
						<?php echo aq_field_upload('image', $this->block_id, $image) ?>
					</label>
				</p>
				
				<p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('bg_position') ?>">
						Row Background Position:
						<?php $options = isset($options) ? isset($options) : 'top center';?>
						<?php $bg_position = isset($bg_position) ? isset($bg_position) : 'top center';?>
						<?php echo herowp_bg_position('bg_position', $this->block_id, $options, $bg_position); ?>
					</label>
					</p>
				
				<p style="margin-left:10px;" class="description">					
					<label for="<?php echo $this->get_field_id('is_pattern') ?>">
						<?php echo aq_field_checkbox('is_pattern', $this->block_id, $is_pattern) ?>
						Is Pattern?(Image will be repeated):
					</label>					
				</p>
				
				<p class="description" style="margin-left:10px;">
						<label for="<?php echo $this->get_field_id('color') ?>">
							 Row Background color: <br/>
							<?php echo aq_field_color_picker('color', $this->block_id, $color) ?>
						</label>
				</p>
				
				<p class="description" style="margin-left:10px;">
						<label for="<?php echo $this->get_field_id('border_top') ?>">
							 Border top (optional): <br/>
							<?php echo aq_field_color_picker('border_top', $this->block_id, $border_top) ?>
						</label>
				</p>
				
				<p class="description" style="margin-left:10px;">
						<label for="<?php echo $this->get_field_id('border_bottom') ?>">
							 Border bottom (optional): <br/>
							<?php echo aq_field_color_picker('border_bottom', $this->block_id, $border_bottom) ?>
						</label>
				</p>
				
				<p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('margin_top') ?>">
						Row margin top (in pixels). Enter only numbers no px - You can use negative values as well.
						<?php echo aq_field_input('margin_top', $this->block_id, $margin_top) ?>
					</label>
				</p>
				

					
			<ul class="blocks column-blocks cf"></ul>			

<?php
	}
	
	function form_callback($instance = array()) {
		$instance = is_array($instance) ? wp_parse_args($instance, $this->block_options) : $this->block_options;
		
		//insert the dynamic block_id & block_saving_id into the array
		$this->block_id = 'aq_block_' . $instance['number'];
		$instance['block_saving_id'] = 'aq_blocks[aq_block_'. $instance['number'] .']';

		extract($instance);
		
		$col_order = $order;
			
		//column block header
		if(isset($template_id)) {
			echo '<li id="template-block-'.$number.'" class="block block-container block-aq_column_block '.$size.'">',
					'<div class="block-settings-column cf" id="block-settings-'.$number.'">',
						'<h2 class="column_h2">',
						__('<i class="fa fa-bars"></i>ROW SIMPLE. DRAG BLOCKS INSIDE AT THE BOTTOM AREA.','myway'),
						'</h2>',
						'<button class="column_size_edit" type="button">Expand</button> '; ?>
						
	               <p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('image') ?>">
						Row Background Image:
						<?php echo aq_field_upload('image', $this->block_id, $image) ?>
					</label>
					</p>	               
					
					<p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('bg_position') ?>">
						Row Background Position:
						<?php $options = isset($options) ? isset($options) : 'top center';?>
						<?php echo herowp_bg_position('bg_position', $this->block_id, $options, $bg_position); ?>
					</label>
					</p>
					
					<p style="margin-left:10px;" class="description">					
					<label for="<?php echo $this->get_field_id('is_pattern') ?>">
						<?php echo aq_field_checkbox('is_pattern', $this->block_id, $is_pattern) ?>
						Is Pattern?(Image will be repeated):
					</label>					
					</p>
				
					<p class="description" style="margin-left:10px;">
							<?php /*If no color is selected */ if (!$color) $color='#fff'; ?>
							<label for="<?php echo $this->get_field_id('color') ?>">
								 Row Background color: <br/>
								<?php echo aq_field_color_picker('color', $this->block_id, $color) ?>
							</label>
					</p>

					<p class="description" style="margin-left:10px;">
						<label for="<?php echo $this->get_field_id('border_top') ?>">
							 Border top (optional): <br/>
							<?php echo aq_field_color_picker('border_top', $this->block_id, $border_top) ?>
						</label>
				</p>
				
				<p class="description" style="margin-left:10px;">
						<label for="<?php echo $this->get_field_id('border_bottom') ?>">
							 Border bottom (optional): <br/>
							<?php echo aq_field_color_picker('border_bottom', $this->block_id, $border_bottom) ?>
						</label>
				</p>
				
				<p style="margin-left:10px;" class="description">
					<label for="<?php echo $this->get_field_id('margin_top') ?>">
						Row margin top (in pixels). Enter only numbers no px - You can use negative values as well.
						<?php echo aq_field_input('margin_top', $this->block_id, $margin_top) ?>
					</label>
				</p>
				
		
					
				<ul class="blocks column-blocks cf">				
			<?php
			
			//check if column has blocks inside it
			$blocks = aq_get_blocks($template_id);
			
			//outputs the blocks
			if($blocks) {
				foreach($blocks as $key => $child) {
					global $aq_registered_blocks;
					extract($child);
					
					//get the block object
					$block = $aq_registered_blocks[$id_base];
					
					if($parent == $col_order) {
						$block->form_callback($child);
					}
				}
			} 
			echo 		'</ul>';
			
		} else {
			$this->before_form($instance);
			$this->form($instance);
		}
				
		//form footer
		$this->after_form($instance);
	}
	
	//form footer
	function after_form($instance) {
		extract($instance);
		
		$block_saving_id = 'aq_blocks[aq_block_'.$number.']';
		
			$name_stripped = 'Row Simple';
			
			echo '<div class="block-control-actions cf"><a href="#" class="delete deletecolumn">Delete Row</a></div>';
			echo '<input type="hidden" class="id_base" name="'.$this->get_field_name('id_base').'" value="'.$id_base.'" />';
			echo '<input type="hidden" class="name" name="'.$this->get_field_name('name').'" value="'.$name_stripped.'" />';
			echo '<input type="hidden" class="order" name="'.$this->get_field_name('order').'" value="'.$order.'" />';
			echo '<input type="hidden" class="size" name="'.$this->get_field_name('size').'" value="'.$size.'" />';
			echo '<input type="hidden" class="parent" name="'.$this->get_field_name('parent').'" value="'.$parent.'" />';
			echo '<input type="hidden" class="number" name="'.$this->get_field_name('number').'" value="'.$number.'" />';
		echo '</div>',
			'</li>';
	}
	
	function block_callback($instance) {
		$instance = is_array($instance) ? wp_parse_args($instance, $this->block_options) : $this->block_options;
		
		extract($instance);
		$col_order = $order;
		$col_size = absint(preg_replace("/[^0-9]/", '', $size));
		
		//column block header
		if(isset($template_id)) {
			
		if ($margin_top) {
			$margin_top_nr = 'position:relative; top:'.$margin_top.'px; margin-bottom:'.$margin_top.'px';
		}
		else{
			$margin_top_nr='';
		}
			
			if($is_pattern) $bk_repeat='repeat'; else $bk_repeat='no-repeat';
			if($border_top) $border_top_color='border-top:solid 1px '.$border_top.';'; else $border_top_color='';
			if($border_bottom) $border_bottom_color='border-bottom:solid 1px '.$border_bottom.';'; else $border_bottom_color='';
			if (!isset($bg_position)) { $bg_position == 'top center'; }
			
			if(isset($image) &&  isset($color)){ 
				$custom_bk = "background: url('$image') $bk_repeat; background-color:$color; background-position:$bg_position;";
			}
			elseif (isset($image)) { 
				$custom_bk = "background: url('$image') $bk_repeat;";
			}
			elseif (isset($color)) 	{ 
				$custom_bk = "background-color:$color;";
			}
			else {
				$custom_bk = "";
			}
				
			/*if ($is_full_screen){
				$div_output_before = '<div class="full_width_container"><!--full_width_container START-->';
				$div_output_after = '</div><!--full_width_container END-->';
			}
			else{
				$div_output_before = '<div class="container"><!--CONTAINER START-->';
				$div_output_after = '</div><!--CONTAINER END-->';
			}*/

				
			
			echo '
<div class="custom-column-background" style="'.$custom_bk.''.$border_top_color.''.$border_bottom_color.''.$margin_top_nr.';"><!--CUSTOM COLUMN BACKGROUND START-->';
			echo '<div class="container"><!--CONTAINER START-->';
			$this->before_block($instance);
			
			//define vars
			$overgrid = 0; $span = 0; $first = false;
			
			//check if column has blocks inside it
			$blocks = aq_get_blocks($template_id);

			//outputs the blocks
			if($blocks) {

				foreach($blocks as $key => $child) {
					global $aq_registered_blocks;
					extract($child);
					
					if(class_exists($id_base)) {
						//get the block object
						$block = $aq_registered_blocks[$id_base];
						
						//insert template_id into $child
						$child['template_id'] = $template_id;
						
						//display the block
						if($parent == $col_order) {
							
							$child_col_size = absint(preg_replace("/[^0-9]/", '', $size));
							
							$overgrid = $span + $child_col_size;

							if($overgrid > $col_size || $span == $col_size || $span == 0) {
								$span = 0;
								$first = true;
							}
							
							if($first == true) {
								$child['first'] = true;
							}
							$block->block_callback($child);
							
							$span = $span + $child_col_size;
							
							$overgrid = 0; //reset $overgrid
							$first = false; //reset $first
						}
					}
				}
	echo '</div><!--CONTAINER END-->';
echo '</div><!--CUSTOM COLUMN BACKGROUND END-->

';
			} 
			
			$this->after_block($instance);
			
			
		} else {
			//show nothing
		}
	}
	
}