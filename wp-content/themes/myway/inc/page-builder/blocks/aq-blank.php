<?php
class AQ_Blank extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Blank Space',
			'size' => 'span6',
			'resizable' => 1,
			"img_preview"=>'blank.png',
			'fa_icon'=>'fa fa-arrows'
			
		);
		
		//create the block
		parent::__construct('AQ_Blank', $block_options);
	}
	
	function form($instance) {
		
	
		
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
   
        
		<?php
	}
	
	function block($instance) {
		extract($instance);
		
		
	?>
	
<div class="blank_space"><!--BLANK START-->
		<div class="col-md-12"></div>
</div><!--BLANK END-->




<?php
	}
	
}