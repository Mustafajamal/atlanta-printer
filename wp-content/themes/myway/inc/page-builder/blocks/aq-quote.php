<?php
class AQ_Quote_Block extends AQ_Block {
	 
	//set and create block
	function __construct() {
		$block_options = array(
			'name' => 'Quote',
			'size' => 'span4',
			'resizable' => 1,
			"img_preview"=>'quote.png',
			'fa_icon'=>'fa fa-quote-left'
			
		);
		
		//create the block
		parent::__construct('AQ_Quote_Block', $block_options);
	}
	
	function form($instance) {
		
		//default values when WP DEBUG is set to true to avoid the undefined index warning
		
		
		if (!isset($quote_heading_bold))  $quote_heading_bold='Passion. ';
		if (!isset($quote_heading_slim))  $quote_heading_slim='Is the key';
		if (!isset($italic_text))  $italic_text='Step out of everyday routine and ordinary. We sure did and here are the results.';
		if (!isset($normal_text))  $normal_text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eget consequat est. Aliquam ac nunc mauris. ';
		if (!isset($h1_color))  $h1_color='#4e4e4e';
		if (!isset($descriptioncolor))  $descriptioncolor='#4e4e4e';
		if (!isset($margintop))  $margintop='0';
		if (!isset($marginbottom))  $marginbottom='0';
		if (!isset($animation_quote))  $animation_quote='zoomIn';
		if (!isset($delayanimation))  $delayanimation='0';	
		if (!isset($herowp_responsive_320))  $herowp_responsive_320='';
		if (!isset($herowp_responsive_480))  $herowp_responsive_480='';
		if (!isset($herowp_responsive_768))  $herowp_responsive_768='';
		if (!isset($herowp_responsive_960))  $herowp_responsive_960='';
		if (empty($herowp_css_unique_id))  $herowp_css_unique_id = herowp_unique_id();
		
		$defaults = array(
			'title' => ''
		);
		$instance = wp_parse_args($instance, $defaults);
		extract($instance);
		
		?>
        
		
		
			<p class="description">
                    <label for="<?php echo $this->get_field_id('quote_heading_bold') ?>">
                        <strong>Quote heading bold</strong>
                        <?php echo aq_field_input('quote_heading_bold', $block_id, $quote_heading_bold, $size = 'full') ?>
                    </label>
			</p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('quote_heading_slim') ?>">
                        <strong>Quote heading slim</strong>
                        <?php echo aq_field_input('quote_heading_slim', $block_id, $quote_heading_slim, $size = 'full') ?>
                    </label>
			</p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('italic_text') ?>">
                        <strong>Italic text</strong>
                        <?php echo aq_field_input('italic_text', $block_id, $italic_text, $size = 'full') ?>
                    </label>
			</p>
			
			<p class="description">
                    <label for="<?php echo $this->get_field_id('normal_text') ?>">
                        <strong>Normal text</strong>
                        <?php echo aq_field_input('normal_text', $block_id, $normal_text, $size = 'full') ?>
                    </label>
			</p>
			
			 <p class="description">
                    <label for="<?php echo $this->get_field_id('h1_color') ?>">
                     Heading Color: <br/>
                        <?php echo aq_field_color_picker('h1_color', $block_id, $h1_color, '#4e4e4e') ?>
                    </label>
            </p>
            
            <p class="description">
                    <label for="<?php echo $this->get_field_id('descriptioncolor') ?>">
                    Description Color: <br/>
                        <?php echo aq_field_color_picker('descriptioncolor', $block_id, $descriptioncolor, '#4e4e4e') ?>
                    </label>
            </p>
			
			<p class="description"><!--Animation-->
					<label for="<?php echo $this->get_field_id('animation_quote') ?>">
						<strong>Animation:</strong> Select animation you want to use for quote.
							<?php $options = isset($options) ? isset($options) : null; ?>
							<?php echo herowp_animations('animation_quote', $block_id, $options, $animation_quote); ?>
					</label>
		    </p><!--Animation-->
           
              
            <p class="description">
                    <label for="<?php echo $this->get_field_id('margintop') ?>">
                        <strong>Margin-top:</strong> Enter the margin from top of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('margintop', $block_id, $margintop, $size = 'full') ?>
                    </label>
			</p>
                
            <p class="description">
                    <label for="<?php echo $this->get_field_id('marginbottom') ?>">
                        <strong>Margin-bottom:</strong> Enter the margin from bottom of this block in pixels. Do not include the px.
                        <?php echo aq_field_input('marginbottom', $block_id, $marginbottom, $size = 'full') ?>
                    </label>
			</p>

			
			<p class="description"><!--Animation delay-->
                    <label for="<?php echo $this->get_field_id('delayanimation') ?>">
                        <strong>Animation delay:</strong> Enter the delay appear animation of image. Use miliseconds. 1000ms = 1s. Do NOT include the ms. If 0, all elements appear at once.
                        <?php echo aq_field_input('delayanimation', $block_id, $delayanimation, $size = 'full') ?>
                    </label>
			</p><!--Animation delay-->
			
				<p class="description">
					<?php echo herowp_responsive_css_text(); ?>
				</p>
				
				<p class="description">
                    <label for="<?php echo $this->get_field_id('herowp_css_unique_id') ?>">
                        <?php echo aq_field_input_hidden('herowp_css_unique_id', $block_id, $herowp_css_unique_id) ?>
                    </label>
			    </p>  
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_320') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_320', $block_id, $herowp_responsive_320,'Responsive up to 320px CSS'); ?>
                </label>

                <label for="<?php echo $this->get_field_id('herowp_responsive_480') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_480', $block_id, $herowp_responsive_480,'Responsive up to 420px CSS'); ?>
                </label>
				
                <label for="<?php echo $this->get_field_id('herowp_responsive_768') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_768', $block_id, $herowp_responsive_768,'Responsive up to 768px CSS'); ?>
                </label>                
				
				<label for="<?php echo $this->get_field_id('herowp_responsive_960') ?>">
                    <?php echo herowp_textarea_responsive('herowp_responsive_960', $block_id, $herowp_responsive_960,'Responsive up to 960px CSS'); ?>
                </label>			
            
        
		<?php
	}
	
	function block($instance) {
		
		global $herowp_responsive_320, $herowp_responsive_480, $herowp_responsive_768, $herowp_responsive_960, $herowp_css_unique_id;
		
		extract($instance);
		
		//custom responsive CSS code
		herowp_add_responsive_css();		
		
	?>

<?php
//if animations are set/unset for the image
if ($animation_quote != '0'){
	$data_animate='data-animate="'.$animation_quote.'" data-delay="'.$delayanimation.'"';
}
else{
	$data_animate='';
}
?> 
 
<div <?php echo herowp_css_unique_id_add(); ?> class="quote_block" style="<?php echo 'margin-top:'.$margintop.'px; margin-bottom:'.$marginbottom.'px;' ;?>"><!--quote_block START-->
	<div class="show_quote" <?php echo $data_animate; ?>>
		<h1 style="color:<?php echo $h1_color; ?>">
			<i class="fa fa-quote-left"></i>
				<?php echo html_entity_decode($quote_heading_bold); ?><span><?php echo html_entity_decode($quote_heading_slim); ?></span>
			<i class="fa fa-quote-right"></i>
		</h1>
		<span class="italic_text" style="color:<?php echo $descriptioncolor; ?>"><?php echo $italic_text; ?></span>
		<span class="normal_text" style="color:<?php echo $descriptioncolor; ?>"><?php echo $normal_text; ?></span>	
	</div>	
</div><!--quote_block END-->

<?php
	}
	
}