<?php 
/*
=================================================================================================================
Options for typography
=================================================================================================================
*/
global $herowp_data;
?>


<?php 
/*
=================================================================================================================
Heading Font Family
=================================================================================================================
*/
if(!empty($herowp_data['heading_font_face']) ) : ?>
	<?php if($herowp_data['heading_font_face'] != 'Select Desired Font'): ?>
	<link href='http<?php echo (is_ssl())? 's' : ''; ?>://fonts.googleapis.com/css?family=<?php echo urlencode($herowp_data['heading_font_face']); ?>:100,300,400,400italic,500,600,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese' rel='stylesheet' type='text/css' />
		 <style type="text/css">
				h1,h2,h3,h4,h5,h6,.tp-caption.very_big_white,.myway_thin_dark,.myway_dark,.myway_bold,.myway_regular,.myway_thin,.myway,.myway_ex_bold {font-family:'<?php echo $herowp_data['heading_font_face']; ?>' !important; }
		 </style>
	<?php endif; ?>
<?php endif; ?>


<?php 
/*
=================================================================================================================
Heading Font Weight
=================================================================================================================
*/
if(!empty($herowp_data['heading_font_weight'])) : ?>
	<?php if($herowp_data['heading_font_weight'] != 'Select Font Weight For Heading Font'): ?>
		 <style type="text/css">
				h1,h2,h3,h4,h5,h6,.tp-caption.very_big_white,.myway_thin_dark,.myway_dark,.myway_bold,.myway_regular,.myway_thin,.myway,.myway_ex_bold {font-weight:<?php echo $herowp_data['heading_font_weight']; ?> !important; }
		 </style
	<?php endif; ?>
<?php endif; ?>


<?php 
/*
=================================================================================================================
Body Font Family
=================================================================================================================
*/
if(!empty($herowp_data['body_font_face'])) : ?>
	<?php if($herowp_data['body_font_face'] != 'Select Desired Font'): ?>
	<link href='http<?php echo (is_ssl())? 's' : ''; ?>://fonts.googleapis.com/css?family=<?php echo urlencode($herowp_data['body_font_face']); ?>:100,300,400,400italic,500,600,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese' rel='stylesheet' type='text/css' />
	 <style type="text/css">
			body {font-family:'<?php echo $herowp_data['body_font_face']; ?>' !important; }
	 </style>
	<?php endif; ?>
<?php endif; ?>	


<?php 
/*
=================================================================================================================
Body Font Weight
=================================================================================================================
*/
if(!empty($herowp_data['body_font_weight'])) : ?>
	<?php if($herowp_data['body_font_weight'] != 'Select Font Weight For Heading Font'): ?>
	 <style type="text/css">
			body {font-weight:<?php echo $herowp_data['body_font_weight']; ?> !important;}
	 </style>
	<?php endif; ?>
<?php endif; ?>	