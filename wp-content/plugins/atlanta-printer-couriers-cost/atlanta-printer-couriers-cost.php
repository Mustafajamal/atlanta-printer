<?php

/*
  Plugin Name: Atlanta printer couriers cost
  Plugin URI: http://google.com
  Version: 4.0
  Author: Google
  Author URI: http://google.com
  Description: Courier Cost Calculator.
 */
global $wp_version;
// Wordppress Version Check
if (version_compare($wp_version, '3.0', '<')) {
    exit($exit_msg . " Please upgrade your wordpress.");
}


if(isset($_POST['couponname']))
{
	
	$ss88_coupons = json_decode(get_option('ss88_coupons'), true);
	$tmpCoupon['code'] = trim($_POST['couponname']);
	$tmpCoupon['value'] = (float)trim($_POST['couponvalue']);
	
	if(is_array($ss88_coupons))
	{
		foreach($ss88_coupons as $ID=>$Coupon)
		{
			if($Coupon['code'] == $tmpCoupon['code'])
			{
				$ss88_coupons[$ID]['value'] = (float)$tmpCoupon['value'];
				break;
			}
			else
			{
				$ss88_coupons[] = $tmpCoupon;
				break;
			}
		}
	}
	else
		$ss88_coupons[] = $tmpCoupon;
	
	update_option('ss88_coupons', json_encode($ss88_coupons));
	
}

if(is_numeric($_GET['remove_coupon']))
{
	$to_remove = trim($_GET['remove_coupon']);
	$ss88_coupons = json_decode(get_option('ss88_coupons'), true);
	unset($ss88_coupons[$to_remove]);
	update_option('ss88_coupons', json_encode($ss88_coupons));
}


//Accept only numeric values
function admin_scripts() { 
if(isset($_GET['page']) && $_GET['page'] == 'ap_couriers_options' ) {
?>
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		/*jQuery('#ab_sec_form input:text').not('#curr_format').keyup(function(){
			var this_field = jQuery(this).val();
			if (isNaN( this_field / 1) == true) {
				alert('Value should be numeric');
				jQuery(this).val('');
			}
		});*/
	});
	
</script>
<?php } }
add_action('admin_head','admin_scripts');
// Link javascript & css files
function abinclude_files($lang) {
    wp_enqueue_style('abdistance_calc_css', plugins_url('/css/styles.css', __FILE__));
    wp_enqueue_style('google_map_css', 'https://code.google.com/apis/maps/documentation/javascript/examples/default.css');
    wp_enqueue_script('google_map_js', 'https://maps.google.com/maps/api/js?sensor=false&language=' . get_option('language'));
    wp_enqueue_script('abdistance_calc_js', plugins_url('/js/ab-get-distance.js', __FILE__));
}
// Include files on page load
add_action('init', 'abinclude_files');
function map_onload($lat, $lng) {
    $output = '';
    $output .= '<script language="javascript" type="text/javascript">';
    $output .= 'window.onload=function(){initialize(' . $lat . ', ' . $lng . ');}';
    $output .= '</script>';
    echo $output;
}
function abdistance_calculator() {
    $lat = get_option('latitude');
    $lng = get_option('longitude');
    map_onload($lat, $lng);
    $result = "";
	?>
    <script type="text/javascript">
	function ajax_price_calculate_function(){
		var postData = 'miles='+jQuery("#miles").val()+'&place_to='+jQuery("#place_to").val() +'&weight='+jQuery("#total_weight").val()+'&boxes='+jQuery("#pieces").val()+'&vehicle='+
		jQuery("select[name=travel_time]").val()+'&waiting_time='+jQuery("#waiting_time").val()+'&delivery_ser='+jQuery("select[name=delivery_ser]").val();
		var formURL = "<?php echo plugins_url('atlanta-printer-couriers-cost/ajax_price_calc.php'); ?>";
		
		jQuery.ajax(
		{
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				jQuery('#total_price').val(data);
				jQuery('#distance').html('<div class="distance-inner">'+ "The distance between <strong><em>"+jQuery("#place_from").val()+"</em></strong> and <strong><em>"+jQuery("#place_to").val()+"</em></strong>: <strong>"+jQuery("#dist").val()+" / "+jQuery("#miles").val()+ " mi</strong>\n\
                <br/>\n\
                <br/><strong><span class='esttimate-charge-lbl'>Estimated Charge:</span> <span class='final-price-estimate'>$"+data+"</span></strong>\n\
                <br/>\n\<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post'><input type='hidden' name='cmd' value='_xclick'><input type='hidden' name='item_name' value='Org: "+jQuery("#place_from").val()+" to Dest: "+jQuery("#place_to").val()+"'><input type='hidden' name='upload' value='1'><input type='hidden' name='business' value='ali4diamonds@aol.com'><input type='hidden' name='currency_code' value='USD' /><input type='hidden' name='page_style' value='paypal' /><input type='hidden' name='charset' value='utf-8' /><input type='hidden' name='rm' value='2' /><input type='hidden' name='amount' value='"+data+"'><input type='submit' class='clsbtn' value='Click Here to Order'></form><br/><br/><br/><img src='https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png' alt='Buy now with PayPal' />'");
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				jQuery('#total_price').val(data);
				jQuery('#distance').html('<div class="distance-inner">'+ "The distance between <strong><em>"+jQuery("#place_from").val()+"</em></strong> and <strong><em>"+jQuery("#place_to").val()+"</em></strong>: <strong>"+jQuery("#dist").val()+" / "+jQuery("#miles").val()+ " mi</strong>\n\
                <br/>\n\
                <br/><strong><span class='esttimate-charge-lbl'>Estimated Charge:</span> <span class='final-price-estimate'>$"+data+"</span></strong>\n\
                <br/>\n\<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post'><input type='hidden' name='cmd' value='_xclick'><input type='hidden' name='item_name' value='Org: "+jQuery("#place_from").val()+" to Dest: "+jQuery("#place_to").val()+"'><input type='hidden' name='upload' value='1'><input type='hidden' name='business' value='ali4diamonds@aol.com'><input type='hidden' name='currency_code' value='USD' /><input type='hidden' name='page_style' value='paypal' /><input type='hidden' name='charset' value='utf-8' /><input type='hidden' name='rm' value='2' /><input type='hidden' name='amount' value='"+data+"'><input type='submit' class='clsbtn' value='Click Here to Order'></form><br/><br/><br/><img src='https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png' alt='Buy now with PayPal' />'");
			}
		});
	}
	</script>
    <?php /*?><script src="<?php echo plugins_url('atlanta-printer-couriers-cost/js/jquery.js'); ?>"></script><?php */?>
	<script src="<?php echo plugins_url('atlanta-printer-couriers-cost/js/jquery.validate.js'); ?>"></script>
	<script>
	jQuery().ready(function() {
		 jQuery('#validbtn').click( function() { 
			if (jQuery("#form1").valid()) {
				get_distance();
			}
		});	
		// validate signup form on keyup and submit
		jQuery("#form1").validate({
			rules: {
				org_name: "required",
				des_name: "required",
				org_phone: "required",
				des_phone: "required",
				org_telephone: "required",
				des_telephone: "required",
				org_add_1: "required",
				des_add_1: "required",
				org_state: "required",
				des_state: "required",
				org_country: "required",
				des_country: "required",
				travel_time: "required",
				total_weight: "required",
				place_from:"required",
				place_to:"required",
				pieces:"required",
				waiting_time:"required"
			},
			messages: {
				org_name: "Please enter your name",
				des_name: "Please enter your name",
				org_phone: "Please enter your phone",
				des_phone: "Please enter your phone",
				org_telephone: "Please enter your Telephone",
				des_telephone:"Please enter your Telephone",
				org_add_1: "Please enter your address line 1",
				des_add_1: "Please enter your address line 1",
				org_state: "Please enter your state",
				des_state: "Please enter your state",
				org_country: "Please enter your country",
				des_country: "Please enter your country",
				travel_time: "Please enter your travel time",
				total_weight: "Please enter your total weight",
				place_from:"Please enter your zip code",
				place_to:"Please enter your zip code",
				pieces:"Please enter your Pieces",
				waiting_time:"Please enter your Waiting time"	
			}
		});
	});
	
	</script>
	<?php

    $result .= '<script language="javascript">
 function get_distance(){
	 from = document.getElementById("place_from").value;
     to = document.getElementById("place_to").value;;
     calcRoute(from,to);
	 
 }
 
</script>
<div id="abgdc-wrap">
 <div id="map_canvas" style="position: relative;width:' . get_option('map_width') . 'px;height:' . get_option('map_height') . 'px;margin:0px auto;border:solid 5px #003;" ></div><!-- #map_canvas -->
 <h2>The Service is only on Georgia, U.S</h2>
<form action="" method="post" name="form1" id="form1">
<table class="abgdc-table">
<tr>
<div class="left-box">
Name<span class="required">*</span><br/>
<input type="text" name="org_name" id="org_name" class="txt" />
</div>
<div class="right-box">
Name<span class="required">*</span><br/>
<input type="text" name="des_name" id="des_name" class="txt" />
</div>

<div class="left-box">
Phone<span class="required">*</span><br/>
<input type="text" name="org_phone" id="org_phone" class="txt" />
</div>
<div class="right-box">
Phone<span class="required">*</span><br/>
<input type="text" name="des_phone" id="des_phone" class="txt" />
</div>

<div class="left-box">
Telephone<span class="required">*</span><br/>
<input type="text" name="org_telephone" id="org_telephone" class="txt" />
</div>
<div class="right-box">
Telephone<span class="required">*</span><br/>
<input type="text" name="des_telephone" id="des_telephone" class="txt" />
</div>

<div class="left-box">
Pickup (Stop1) Address:<span class="required">*</span><br/>
<input type="text" name="org_add_1" id="org_add_1" class="txt" placeholder="Address Line 1" />
</div>
<div class="right-box">
Destination: <span class="required">*</span><br/>
<input type="text" name="des_add_1" id="des_add_1" class="txt" placeholder="Address Line 1" />
</div>

<div class="left-box">
<input type="text" name="org_add_2" id="org_add_2" placeholder="Address Line 2" class="txt" />
</div>
<div class="right-box">
<input type="text" name="des_add_2" id="des_add_2" class="txt" placeholder="Address Line 2" />
</div>

<div class="left-box">
<input type="text" name="org_state" id="org_state" placeholder="State" class="txt" />
</div>
<div class="right-box">
<input type="text" name="des_state" id="des_state" class="txt" placeholder="State" />
</div>

<div class="left-box">
<input type="text" name="org_country" id="org_country" placeholder="Country" class="txt" />
</div>
<div class="right-box">
<input type="text" name="des_country" id="des_country" class="txt" placeholder="Country" />
</div>

<div class="left-box">
<input type="text" name="place_from" id="place_from" placeholder="Zip" class="txt" />
</div>
<div class="right-box">
<input type="text" name="place_to" id="place_to" class="txt" placeholder="Zip" />
</div>

<div class="left-box">
Vehicle<span class="required">*</span><br/>
 <select name="travel_time" id="travel_time">
  <!--<option value="any">Any</option>-->
  <option value="car">Car</option>
  <!--<option value="mini-van">Mini Van</option>-->
  <option value="cargo-van">Cargo Van</option>
  <option value="strt-truck">Truck</option>
  <!--<option value="bike">Bike</option>
  <option value="concierge">Concierge</option>
  <option value="reserve">Reserve</option>-->
</select>
</div>
<div class="right-box">
Boxes<span class="required">*</span><br/> <input type="text" name="pieces" id="pieces" class="txt" placeholder="1" />
</div>

<div class="left-box">
Total Weight<span class="required">*</span><br/> <input type="text" name="total_weight" id="total_weight" class="txt" placeholder="30 lbs" />
</div>
<div class="right-box">Delivery Service<span class="required">*</span><br/> <select name="delivery_ser" id="delivery_ser">
	<option value="direct">Direct</option>
	<option value="rush">Rush</option>
	<option value="regular">Regular</option>	
</select>

</div>

<div class="right-box">
Waiting Time<span class="required">*</span><br/> <input type="text" name="waiting_time" id="waiting_time" class="txt" placeholder="In Min" />
</div>

<!--<input type="radio" id="day_time" name="travel_time" value="car" checked="checked" /> Car 
<input type="radio" id="night_time" name="travel_time" value="mini-van" /> Mini Van -->
</td>
<br />
<td><input type="button" class="clsbtn" id="validbtn" value="Get Quote"/>
<input type="hidden" value="'. get_option('zoom').'" id="map_zoom"/>
<input type="hidden" value="'. get_option('curr_format').'" id="curr_format"/>
</td>
</tr><input type="hidden" value="" id="miles" name="miles"/><input type="hidden" value="" id="total_price" name="total_price"/><input type="hidden" value="" id="dist" name="dist"/>
</table>
</form><br/>
 <div id="distance"></div><!-- #distance -->
 <div id="steps"></div><!-- #steps -->
</div><!-- #abgdc-wrap -->';?>

<input type="hidden" value="<?php echo get_option('day_more_five_fare');?>" id="day_more_five" />
<input type="hidden" value="<?php echo get_option('day_less_five_fare');?>" id="day_less_five" />
<input type="hidden" value="<?php echo get_option('more_five_fare');?>" id="night_more_five" />
<input type="hidden" value="<?php echo get_option('less_five_fare');?>" id="night_less_five" />
<?php
	
	$result="";
    return $result;
}
function ab_shortcode($atts) {
    $result = abdistance_calculator();
    return $result;
}
// Add [ap-courier] shortcode
add_shortcode("ap-courier", "ab_shortcode");
/* ==============================================================================
 *
 *              Admin Section for the Plugin
 *
  ============================================================================== */
function ab_set_options() {
    add_option('latitude', '32.165622', 'Default Latitude');
    add_option('longitude', '-82.900075', 'Default Longitude');
    add_option('language', 'en', 'Default Longitude');
    add_option('map_width', '500', 'Default Longitude');
    add_option('map_height', '300', 'Default Longitude');
    add_option('zoom', '7', 'Default Zoom');
    add_option('less_five_fare', '3', 'Less Five');
    add_option('more_five_fare', '2.5', 'More Five');
    add_option('day_less_five_fare', '2', 'Reg Less Five');
    add_option('day_more_five_fare', '1.5', 'Reg More Five');
	
	add_option('curr_format', '$', 'Currency format');
}
function ab_unset_options() {
    delete_option('latitude');
    delete_option('longitude');
    delete_option('language');
    delete_option('map_width');
    delete_option('map_height');
    delete_option('zoom');
    delete_option('less_five_fare');
    delete_option('more_five_fare');
    delete_option('day_less_five_fare');
    delete_option('day_more_five_fare');
	delete_option('curr_format');
}
register_activation_hook(__FILE__, 'ab_set_options');
register_deactivation_hook(__FILE__, 'ab_unset_options');
function ab_options_page() { ?>
<script type="text/javascript">
var intTextBox=1;

//FUNCTION TO ADD TEXT BOX ELEMENT
function addElement()
{
	intTextBox = intTextBox + 1;
	var contentID = document.getElementById('con_list');
	var newTBDiv = document.createElement('tr');
	newTBDiv.setAttribute('id','strText'+intTextBox);
	newTBDiv.setAttribute('class','form');
	newTBDiv.innerHTML = '<td><select name="mile_country[]"><option value="">Select Country</option><?php $countries = get_option('country_name_array');if(isset($countries) && sizeof($countries) > 0){foreach($countries as $country){?><option value="<?php echo $country; ?>"><?php echo $country; ?></option><?php } } ?></select></td><td><textarea name="pincode_list[]" cols="25" rows="5" placeholder="Enter Pincode Seprated with Comma (,)"></textarea>&nbsp;<a href="javascript:removeElement();">Remove</a></td>';
	contentID.appendChild(newTBDiv);
}

//FUNCTION TO REMOVE TEXT BOX ELEMENT
function removeElement()
{
	if(intTextBox != 0)
	{
		var contentID = document.getElementById('con_list');
		contentID.removeChild(document.getElementById('strText'+intTextBox));
		intTextBox = intTextBox-1;
	}
}
</script>
<script type="text/javascript">
var intTextBox=1;

//FUNCTION TO ADD TEXT BOX ELEMENT
function addElement2()
{
	intTextBox = intTextBox + 1;
	var contentID = document.getElementById('con_list_2');
	var newTBDiv = document.createElement('tr');
	newTBDiv.setAttribute('id','strText2'+intTextBox);
	newTBDiv.setAttribute('class','form');
	newTBDiv.innerHTML = '<td><select name="mile_country_2[]"><option value="">Select Country</option><?php $countries = get_option('country_name_array');if(isset($countries) && sizeof($countries) > 0){foreach($countries as $country){?><option value="<?php echo $country; ?>"><?php echo $country; ?></option><?php } } ?></select></td><td><textarea name="pincode_list_2[]" cols="25" rows="5" placeholder="Enter Pincode Seprated with Comma (,)"></textarea>&nbsp;<a href="javascript:removeElement2();">Remove</a></td>';
	contentID.appendChild(newTBDiv);
}

//FUNCTION TO REMOVE TEXT BOX ELEMENT
function removeElement2()
{
	if(intTextBox != 0)
	{
		var contentID = document.getElementById('con_list_2');
		contentID.removeChild(document.getElementById('strText2'+intTextBox));
		intTextBox = intTextBox-1;
	}
}
</script>
<div class="wrap">
  <table class="widefat" style="width: 600px;" >
    <thead>
      <tr>
        <th colspan="2"><div class="icon32" id="icon-edit"></div>
          <h2>Shipping Cost</h2></th>
      </tr>
    </thead>
  </table>
  <br/>
  <?php
        if ($_REQUEST['submit']) {
            ab_update_options();
        }
        ab_print_options_form();
        ?>
</div>
<?php
}
function ab_update_options() {
    $lat = isset($_REQUEST['lat']) ? $_REQUEST['lat'] != "" ? $_REQUEST['lat'] : 9.93123  : 9.93123;
    update_option('latitude', $lat);
    $long = isset($_REQUEST['long']) ? $_REQUEST['long'] != "" ? $_REQUEST['long'] : 76.26730  : 76.26730;
    update_option('longitude', $long);
    $lang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] != "" ? $_REQUEST['lang'] : 'en'  : 'en';
    update_option('language', $lang);
    $map_width = isset($_REQUEST['map_width']) ? $_REQUEST['map_width'] != "" ? $_REQUEST['map_width'] : 500  : 500;
    update_option('map_width', $map_width);
    $map_height = isset($_REQUEST['map_height']) ? $_REQUEST['map_height'] != "" ? $_REQUEST['map_height'] : 300  : 300;
    update_option('map_height', $map_height);
    
    $zoom = isset($_REQUEST['zoom']) ? $_REQUEST['zoom'] != "" ? $_REQUEST['zoom'] : 7  : 7;
    update_option('zoom', $zoom);
	
	if(isset($_REQUEST['per_box']) && $_REQUEST['per_box']!=''){
		update_option('per_box', $_REQUEST['per_box']);
	}
	if(isset($_REQUEST['per_box_charge']) && $_REQUEST['per_box_charge']!=''){
		update_option('per_box_charge', $_REQUEST['per_box_charge']);
	}
	if(isset($_REQUEST['weight']) && $_REQUEST['weight']!=''){
		update_option('weight', $_REQUEST['weight']);
	}
	if(isset($_REQUEST['over_weight_charge']) && $_REQUEST['over_weight_charge']!=''){
		update_option('over_weight_charge', $_REQUEST['over_weight_charge']);
	}
	if(isset($_REQUEST['waiting_time']) && $_REQUEST['waiting_time']!=''){
		update_option('waiting_time', $_REQUEST['waiting_time']);
	}
	if(isset($_REQUEST['over_waiting_time']) && $_REQUEST['over_waiting_time']!=''){
		update_option('over_waiting_time', $_REQUEST['over_waiting_time']);
	}
	if(isset($_REQUEST['van_truck_charge']) && $_REQUEST['van_truck_charge']!=''){
		update_option('van_truck_charge', $_REQUEST['van_truck_charge']);
	}
	
	if(isset($_REQUEST['country_name']) && $_REQUEST['country_name']!=''){
		$options = get_option('country_name_array');
		$options[] = $_REQUEST['country_name'];
		update_option('country_name_array', $options);
	}
    if(isset($_REQUEST['miles_rate']) && $_REQUEST['miles_rate']!=''){
		$options = get_option('miles_rate_1');
		$options['miles_rate_1']['miles_rate'] = $_REQUEST['miles_rate'];
		if(isset($_REQUEST['mile_country']) && sizeof($_REQUEST['mile_country']) > 0){
			$data_array = array();
			for($i=0;$i<sizeof($_REQUEST['mile_country']);$i++){
				$data_array['country_list']['country_name'][] = $_REQUEST['mile_country'][$i];
				$data_array['country_list']['post_Codes'][] = $_REQUEST['pincode_list'][$i];
			}
			$options['miles_rate_1']['list'] = $data_array;
		}
		update_option('miles_rate_1', $options);
	}
	if(isset($_REQUEST['miles_rate_2']) && $_REQUEST['miles_rate_2']!=''){
		$options = get_option('miles_rate_2');
		$options['miles_rate_2']['miles_rate_2'] = $_REQUEST['miles_rate_2'];
		if(isset($_REQUEST['mile_country_2']) && sizeof($_REQUEST['mile_country_2']) > 0){
			$data_array = array();
			for($i=0;$i<sizeof($_REQUEST['mile_country_2']);$i++){
				$data_array['country_list']['country_name'][] = $_REQUEST['mile_country_2'][$i];
				$data_array['country_list']['post_Codes'][] = $_REQUEST['pincode_list_2'][$i];
			}
			$options['miles_rate_2']['list'] = $data_array;
		}
		update_option('miles_rate_2', $options);
	}
    echo '<div id="message" class="updated fade"><p><strong>Options Saved...</strong></p></div>';
}
function ab_print_options_form() {
    $default_latitude = get_option('latitude');
    $default_longitude = get_option('longitude');
    $default_language = get_option('language');
    $default_map_width = get_option('map_width');
    $default_map_height = get_option('map_height');
    $default_zoom = get_option('zoom');
    $per_box = get_option('per_box');
    $per_box_charge = get_option('per_box_charge');
    $weight = get_option('weight');
	$over_weight_charge = get_option('over_weight_charge');
    $waiting_time = get_option('waiting_time');
    $over_waiting_time = get_option('over_waiting_time');
	$van_truck_charge = get_option('van_truck_charge');
	$countries = get_option('country_name_array');
	$miles_rate_1 = get_option('miles_rate_1');
	$miles_rate_2 = get_option('miles_rate_2');
	/*echo '<pre>';
	print_r($miles_rate_1);exit;*/
    ?>
<form method="post" id="ab_sec_form">
  <table class="widefat" style="width: 600px;" >
    <thead>
      <tr>
        <th colspan="2">Configure The Quote</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th colspan="2"> </th>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <td><label for="latitude">Latitude: </label></td>
        <td><input type="text" name="lat" size="30" id="map_lat" value="<?php echo strip_tags($default_latitude); ?>" />
          <small>Default: 32.165622</small></td>
      </tr>
      <tr>
        <td><label for="longitude">Longitude: </label></td>
        <td><input type="text" name="long" size="30" id="map_long" value="<?php echo strip_tags($default_longitude); ?>" />
          <small>Default: -82.900075</small></td>
      </tr>
      <tr>
        <td><label for="language">Language: </label></td>
        <td><select name="lang" value="<?php echo $default_language; ?>" >
            <option value="ar" <?php echo ($default_language == "ar") ? 'selected="selected"' : ''; ?> >Arabic</option>
            <option value="eu" <?php echo ($default_language == "eu") ? 'selected="selected"' : ''; ?> >Basque</option>
            <option value="bg" <?php echo ($default_language == "bg") ? 'selected="selected"' : ''; ?> >Bulgarian</option>
            <option value="ca" <?php echo ($default_language == "ca") ? 'selected="selected"' : ''; ?> >Catalan</option>
            <option value="cs" <?php echo ($default_language == "cs") ? 'selected="selected"' : ''; ?> >Czech</option>
            <option value="da" <?php echo ($default_language == "da") ? 'selected="selected"' : ''; ?> >Danish</option>
            <option value="en" <?php echo ($default_language == "en") ? 'selected="selected"' : ''; ?> >English</option>
            <option value="de" <?php echo ($default_language == "de") ? 'selected="selected"' : ''; ?> >German</option>
            <option value="el" <?php echo ($default_language == "el") ? 'selected="selected"' : ''; ?> >Greek</option>
            <option value="es" <?php echo ($default_language == "es") ? 'selected="selected"' : ''; ?> >Spanish</option>
            <option value="fa" <?php echo ($default_language == "fa") ? 'selected="selected"' : ''; ?> >Farsi</option>
            <option value="fi" <?php echo ($default_language == "fi") ? 'selected="selected"' : ''; ?> >Finnish</option>
            <option value="fil" <?php echo ($default_language == "fil") ? 'selected="selected"' : ''; ?>>Filipino</option>
            <option value="fr" <?php echo ($default_language == "fr") ? 'selected="selected"' : ''; ?> >French</option>
            <option value="gl" <?php echo ($default_language == "gl") ? 'selected="selected"' : ''; ?> >Galician</option>
            <option value="gu" <?php echo ($default_language == "gu") ? 'selected="selected"' : ''; ?> >Gujarati</option>
            <option value="hi" <?php echo ($default_language == "hi") ? 'selected="selected"' : ''; ?> >Hindi</option>
            <option value="hr" <?php echo ($default_language == "hr") ? 'selected="selected"' : ''; ?> >Croatian</option>
            <option value="hu" <?php echo ($default_language == "hu") ? 'selected="selected"' : ''; ?> >Hungarian</option>
            <option value="id" <?php echo ($default_language == "id") ? 'selected="selected"' : ''; ?> >Indonesian</option>
            <option value="iw" <?php echo ($default_language == "iw") ? 'selected="selected"' : ''; ?> >Hebrew</option>
            <option value="ja" <?php echo ($default_language == "ja") ? 'selected="selected"' : ''; ?> >Japanese</option>
            <option value="kn" <?php echo ($default_language == "kn") ? 'selected="selected"' : ''; ?> >Kannada</option>
            <option value="ko" <?php echo ($default_language == "ko") ? 'selected="selected"' : ''; ?> >Korean</option>
            <option value="lt" <?php echo ($default_language == "lt") ? 'selected="selected"' : ''; ?> >Lithuanian</option>
            <option value="lv" <?php echo ($default_language == "lv") ? 'selected="selected"' : ''; ?> >Latvian</option>
            <option value="ml" <?php echo ($default_language == "ml") ? 'selected="selected"' : ''; ?> >Malayalam</option>
            <option value="mr" <?php echo ($default_language == "mr") ? 'selected="selected"' : ''; ?> >Marathi</option>
            <option value="nl" <?php echo ($default_language == "nl") ? 'selected="selected"' : ''; ?> >Dutch</option>
            <option value="nn" <?php echo ($default_language == "nn") ? 'selected="selected"' : ''; ?> >Norwegian Nynorsk</option>
            <option value="no" <?php echo ($default_language == "no") ? 'selected="selected"' : ''; ?> >Norwegian</option>
            <option value="or" <?php echo ($default_language == "or") ? 'selected="selected"' : ''; ?> >Oriya</option>
            <option value="pl" <?php echo ($default_language == "pl") ? 'selected="selected"' : ''; ?> >Polish</option>
            <option value="pt" <?php echo ($default_language == "pt") ? 'selected="selected"' : ''; ?> >PortuguesE</option>
            <option value="pt-BR" <?php echo ($default_language == "pt-BR") ? 'selected="selected"' : ''; ?> >Portuguese(Brazil)</option>
            <option value="pt-PT" <?php echo ($default_language == "pt-PT") ? 'selected="selected"' : ''; ?> >Portuguese(Portugal)</option>
            <option value="rm" <?php echo ($default_language == "rm") ? 'selected="selected"' : ''; ?> >Romansch</option>
            <option value="ro" <?php echo ($default_language == "ro") ? 'selected="selected"' : ''; ?> >Romanian</option>
            <option value="ru" <?php echo ($default_language == "ru") ? 'selected="selected"' : ''; ?> >Russian</option>
            <option value="sk" <?php echo ($default_language == "sk") ? 'selected="selected"' : ''; ?> >Slovak</option>
            <option value="sr" <?php echo ($default_language == "sr") ? 'selected="selected"' : ''; ?> >Serbian</option>
            <option value="sv" <?php echo ($default_language == "sv") ? 'selected="selected"' : ''; ?> >Swedish</option>
            <option value="tl" <?php echo ($default_language == "tl") ? 'selected="selected"' : ''; ?> >Tagalog</option>
            <option value="ta" <?php echo ($default_language == "ta") ? 'selected="selected"' : ''; ?> >Tamil</option>
            <option value="te" <?php echo ($default_language == "te") ? 'selected="selected"' : ''; ?> >Telugu</option>
            <option value="th" <?php echo ($default_language == "th") ? 'selected="selected"' : ''; ?> >Thai</option>
            <option value="tr" <?php echo ($default_language == "tr") ? 'selected="selected"' : ''; ?> >Turkish</option>
            <option value="uk" <?php echo ($default_language == "uk") ? 'selected="selected"' : ''; ?> >Ukrainian</option>
            <option value="vi" <?php echo ($default_language == "vi") ? 'selected="selected"' : ''; ?> >Vietnamese</option>
            <option value="zh-CN" <?php echo ($default_language == "zh-CN") ? 'selected="selected"' : ''; ?> >Chinese (Simplified)</option>
            <option value="zh-TW" <?php echo ($default_language == "zh-TW") ? 'selected="selected"' : ''; ?> >Chinese (Traditional)</option>
          </select>
          <small>Default language for directions</small></td>
      </tr>
      <tr>
        <td><label for="map_width">Map Width: </label></td>
        <td><input type="text" name="map_width" size="30" id="map_width" value="<?php echo strip_tags($default_map_width); ?>" />
          <small>Default: 500</small></td>
      </tr>
      <tr>
        <td><label for="map_height">Map Height: </label></td>
        <td><input type="text" name="map_height" size="30" id="map_height" value="<?php echo strip_tags($default_map_height); ?>" />
          <small>Default: 300</small></td>
      </tr>
      <tr>
        <td><label for="zoom">Map Zoom: </label></td>
        <td><input type="text" name="zoom" size="30" id="map_zoom" value="<?php echo strip_tags($default_zoom); ?>" />
          <small>Default: 7</small></td>
      </tr>
    </tbody>
  </table>
  <br />
  
  <table class="widefat" style="width: 800px;" >
    <thead>
      <tr>
        <th colspan="4"><strong>Courier Rate Sheet : </strong> To configure the courier charges update the following values</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th colspan="4">Add <strong>[ap-courier]</strong> shortcode to the post/page.</th>
      </tr>
    </tfoot>
    <tbody>
      <tr>
        <td><label for="per_box">Anything over : </label></td>
        <td><input type="text" name="per_box" size="10" value="<?php echo strip_tags($per_box); ?>" />
          <small>Boxes</small></td>
        <td><label for="per_box_charge">boxes is ($): </label></td>
        <td><input type="text" name="per_box_charge" size="10" value="<?php echo strip_tags($per_box_charge); ?>" />&nbsp;<small>Per Box</small>
         </td>
      </tr>
      <tr>
        <td><label for="weight">Weight : </label></td>
        <td><input type="text" name="weight" size="10" value="<?php echo strip_tags($weight); ?>" />
          <small>Free</small></td>
        <td><label for="over_weight_charge">over weight ($): </label></td>
        <td><input type="text" name="over_weight_charge" size="10" value="<?php echo strip_tags($over_weight_charge); ?>" />&nbsp;<small>Per lbs</small>
         </td>
      </tr>
      <tr>
        <td><label for="waiting_time">Waiting Time : Over</label></td>
        <td><input type="text" name="waiting_time" size="10" value="<?php echo strip_tags($waiting_time); ?>" />
          <small>Free Min</small></td>
        <td><label for="over_waiting_time">over min ($): </label></td>
        <td><input type="text" name="over_waiting_time" size="10" value="<?php echo strip_tags($over_waiting_time); ?>" />&nbsp;<small>Per Min</small>
         </td>
      </tr>
      <tr>
        <td><label for="van_truck_charge">Cargo Van Truck Charge ($): </label></td>
        <td><input type="text" name="van_truck_charge" size="10" value="<?php echo strip_tags($van_truck_charge); ?>" />
          <small>for anything that will not fit in car</small></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
      	<td colspan="4">*Redeliveries:Charged at full rate per zip code tariff</td>
      </tr>
      <tr>
      	<td colspan="4">*Rates do not include Fuel Surcharge</td>
      </tr>
      <tr>
      	<td colspan="4">Direct: Direct--30 Minute Pickup Direct Service triple fee</td>
      </tr>
      <tr>
      	<td colspan="4">Rush: Rush Service (Apprx 1.5 Hr) double fee</td>
      </tr>
      <tr>
      	<td colspan="4">Regular: Regular Service (Apprx 2.5)</td>
      </tr>
      <tr>
      	<td colspan="4">No Gas Fee included</td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
     
      <tr>
        <td></td>
        <td><input class="button-primary" type="submit" name="submit" value="Update Settings"/></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <br/>
   <table class="widefat" style="width: 800px;" >
    <thead>
      <tr>
        <th colspan="2"><strong>Country List / Milege Rate (1): </strong> </th>
      </tr>
    </thead>
    <tr>
        <td colspan="2"><input type="text" name="country_name" size="10" value="" placeholder="Enter Name" /></td>
      </tr>
      <tr>
      	<td colspan="2"><input class="button-primary" type="submit" name="submit" value="Add Country"/></td>
      </tr>
      <tr>
      	<td colspan="2">Milege Rate (1): <input type="text" name="miles_rate" size="20" value="<?php echo $miles_rate_1['miles_rate_1']['miles_rate']; ?>" placeholder="Enter Miles Rate" />&nbsp;&nbsp;&nbsp;<input class="button-primary" type="button" name="addnew" value="Add Country List" onclick="javascript:addElement();"/></td>
      </tr>
      <tr>
      	<td colspan="2">
        <table width="100%" border="0" id="con_list">
          <?php if($miles_rate_1['miles_rate_1']['list']){
			  		for($i=0;$i<sizeof($miles_rate_1['miles_rate_1']['list']['country_list']['country_name']);$i++){
				$con_nma = $miles_rate_1['miles_rate_1']['list']['country_list']['country_name'];
				$post_code = $miles_rate_1['miles_rate_1']['list']['country_list']['post_Codes'];
			   ?>
          <tr>
            <td><select name="mile_country[]">
            <option value="">Select Country</option>
            <?php if(isset($countries) && sizeof($countries) > 0){ 
                    foreach($countries as $country){
				
             ?>
             <option value="<?php echo $country; ?>" <?php if($con_nma[$i]==$country){ echo 'selected="selected"'; } ?>><?php echo $country; ?></option>
            <?php } } ?>
            </select></td>
            <td><textarea name="pincode_list[]" cols="25" rows="5" placeholder="Enter Pincode Seprated with Comma (,)"><?php echo $post_code[$i]; ?></textarea></td>
          </tr>
          <?php } } ?>
        </table>
        </td>
      </tr>
      
      <tr>
      	<td colspan="2"><input class="button-primary" type="submit" name="submit" value="Update Settings"/></td>
      </tr>
    </table>
    <br/>
    <table class="widefat" style="width: 800px;" >
    <thead>
      <tr>
        <th colspan="2"><strong>Milege Rate (2) : </strong> </th>
      </tr>
    </thead>
      <tr>
      	<td colspan="2">Milege Rate (2): <input type="text" name="miles_rate_2" size="20" value="<?php echo $miles_rate_2['miles_rate_2']['miles_rate_2']; ?>" placeholder="Enter Miles Rate" />&nbsp;&nbsp;&nbsp;<input class="button-primary" type="button" name="addnew" value="Add Country List" onclick="javascript:addElement2();"/></td>
      </tr>
      <tr>
      	<td colspan="2">
        <table width="100%" border="0" id="con_list_2">
          <?php if($miles_rate_2['miles_rate_2']['list']){
			  		for($i=0;$i<sizeof($miles_rate_2['miles_rate_2']['list']['country_list']['country_name']);$i++){
				$con_nma = $miles_rate_2['miles_rate_2']['list']['country_list']['country_name'];
				$post_code = $miles_rate_2['miles_rate_2']['list']['country_list']['post_Codes'];
			   ?>
          <tr>
            <td><select name="mile_country_2[]">
            <option value="">Select Country</option>
            <?php if(isset($countries) && sizeof($countries) > 0){ 
                    foreach($countries as $country){
				
             ?>
             <option value="<?php echo $country; ?>" <?php if($con_nma[$i]==$country){ echo 'selected="selected"'; } ?>><?php echo $country; ?></option>
            <?php } } ?>
            </select></td>
            <td><textarea name="pincode_list_2[]" cols="25" rows="5" placeholder="Enter Pincode Seprated with Comma (,)"><?php echo $post_code[$i]; ?></textarea></td>
          </tr>
          <?php } } ?>
        </table>
        </td>
      </tr>
      
      <tr>
      	<td colspan="2"><input class="button-primary" type="submit" name="submit" value="Update Settings"/></td>
      </tr>
    </table>
</form>

<form action="admin.php?page=ap_couriers_options#Coupons" method="post" name="form1" id="form1">
	
    <table class="widefat" id="Coupons" style="width: 800px; margin-top:50px;" >
    <thead>
      <tr>
        <th colspan="2"><strong>Coupons</strong> </th>
      </tr>
    </thead>
      <tr>
      	<td colspan="2">
<?php

$ss88_coupons = json_decode(get_option('ss88_coupons'), true);
	//echo '<pre>';
	//print_r($ss88_coupons);
	//echo '</pre>';
if(count($ss88_coupons) != 0)
{
?>

<table>
	<tbody>
		<tr>
			<th>Coupon Code</th>
			<th>Percentage Off (%)</th>
			<th>Action</th>
		</tr>
<?php
foreach($ss88_coupons as $ID=>$Coupon) {
?>
		<tr>
			<td><?php echo $Coupon['code']; ?></td>
			<td><?php echo $Coupon['value']; ?></td>
			<td><a href="admin.php?page=ap_couriers_options&remove_coupon=<?php echo $ID; ?>#Coupons">Remove</a></td>
		</tr>
<?php } ?>
	</tbody>
</table>

<?php
}
else
{
	echo '<p>There are no coupons. Please add one!</p>';
}

?>
        </td>
      </tr>
	  
		<tr>
      	<td colspan="2"><input type="text" name="couponname" required placeholder="Coupon code" onkeydown="upperCaseF(this)" /> <input type="text" name="couponvalue" placeholder="Type total % off" required /> <input class="button-primary" type="submit" name="addnewcoupon" value="Add Coupon" onclick=""/></td>
      </tr>
    </table>
<script>
function upperCaseF(a){
    setTimeout(function(){
        a.value = a.value.toUpperCase();
    }, 1);
}
</script>
<?php
}
function add_menu_item() {
    add_menu_page('AP-Couriers', 'AP-Couriers', 'add_users','ap_couriers_options', 'ab_options_page', plugins_url('/images/icon.png', __FILE__));
}
add_action('admin_menu', 'add_menu_item');
?>