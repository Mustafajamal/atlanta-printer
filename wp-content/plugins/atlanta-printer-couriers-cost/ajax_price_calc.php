<?php 
require_once( "../../../wp-config.php" );

if(isset($_POST['miles']) && $_POST['miles']!=''){
	$pincode = $_POST['place_to'];
	$miles = $_POST['miles'];
	$weight = $_POST['weight'];
	$boxes = $_POST['boxes'];
	$vehicle = $_POST['vehicle'];
	$waiting_time = $_POST['waiting_time'];
	$delivery_ser = $_POST['delivery_ser'];
	$over_waiting_time = get_option('waiting_time');
	$over_waiting_time_charge = get_option('over_waiting_time');
	$over_weight_charge = get_option('over_weight_charge');
	$van_truck_charge = get_option('van_truck_charge');
	$per_box = get_option('per_box');
	$weight_limit = get_option('weight');
    $per_box_charge = get_option('per_box_charge');
	if(isset($pincode) && $pincode!=''){
		$miles_rate_1 = get_option('miles_rate_1');
	    $miles_rate_2 = get_option('miles_rate_2');
		$post_code_1 = $miles_rate_1['miles_rate_1']['list']['country_list']['post_Codes'];
		$post_code_2 = $miles_rate_2['miles_rate_2']['list']['country_list']['post_Codes'];
		if(isset($post_code_1) && sizeof($post_code_1) > 0){
			$post_code_arr_1  = array();
			foreach($post_code_1 as $post_code){
				$post_code_arr_1[]=explode(",",$post_code);
			}
		}
		if(isset($post_code_2) && sizeof($post_code_2) > 0){
			$post_code_arr_2  = array();
			foreach($post_code_2 as $post_code){
				$post_code_arr_2[]=explode(",",$post_code);
			}
		}
		$price = 0;
		// get price per miles options 1
		if(filterDataByArray($pincode,$post_code_arr_1)){
			
			$price = $miles_rate_1['miles_rate_1']['miles_rate'];
		}
		
		// get price per miles options 2
		if(filterDataByArray($pincode,$post_code_arr_2)){
			$price = $miles_rate_2['miles_rate_2']['miles_rate_2'];
		}
		//calculate price per miles
		$miles_price = 0;
		$total_price = 0.00;
		if(isset($price) && $price > 0){
			$miles_price = ($miles*$price);
			$total_price += $miles_price;
			// calculate if service option selected display price as per service.
			//if select direcct option price set triple fee
			if(isset($delivery_ser) && $delivery_ser=='direct'){
				if(isset($total_price) && $total_price > 0){
					$total_price += ($miles_price*2);
				}
			}
			//if select rush option price set double fee
			if(isset($delivery_ser) && $delivery_ser=='rush'){
				if(isset($total_price) && $total_price > 0){
					$total_price += ($miles_price*1);
				}
			}
			//calculate if weight grater than 30 lbs
			if(isset($weight) && $weight > $weight_limit){
				$over_weight = ($weight - $weight_limit);
				$over_weight_price = ($over_weight*$over_weight_charge); 
				$total_price += $over_weight_price;
			}
			//calculate over boxes charge 
			if(isset($boxes) && $boxes > $per_box){
				$over_box_charge = ($boxes * $per_box_charge);
				$total_price += $over_box_charge;
			}
			//calculate if car not goods set proper
			if(isset($vehicle) && $vehicle!='car'){
				$total_price += $van_truck_charge;
			}
			//calculate waiting time charge
			if(isset($waiting_time) && $waiting_time >= $over_waiting_time){
				$over_waiting = ($waiting_time - $over_waiting_time);
				$over_wait_charge = ($over_waiting * $over_waiting_time_charge);
				$total_price += $over_wait_charge;
			}
			
			$total_price = number_format($total_price,2);
		}
		
		echo $total_price;exit;
	}
}
function filterDataByArray($post_code_en,$post_code_funn_1){
	if(isset($post_code_funn_1) && sizeof($post_code_funn_1) > 0){
		foreach($post_code_funn_1 as $post_code_fun){
			foreach($post_code_fun as $post_code_fu){
				if($post_code_fu==$post_code_en){
					return true;
				}
			}
		}
	}
}
exit;
 ?>