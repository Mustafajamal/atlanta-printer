var directionDisplay;
  var directionsService = new google.maps.DirectionsService();
  var map;
  function initialize(lat,lng) {
    directionsDisplay = new google.maps.DirectionsRenderer();
    //var location = new google.maps.LatLng(9.93123, 76.26730);
	var location = new google.maps.LatLng(lat, lng);
    
    var zm =  parseInt(document.getElementById('map_zoom').value);
    var myOptions = {
 
      zoom: zm,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: location
    }
    
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    directionsDisplay.setMap(map);
  }
  function calcRoute(from,to){
	var start = from;
    var end = to;
    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
		unitSystem: google.maps.DirectionsUnitSystem.METRIC 	//IMPERIAL     //METRIC
    };
    // function to round the decimal digits eg: round(123.456,2); gives 123.45
    function round(number,X) {
        X = (!X ? 2 : X);
        return Math.round(number*Math.pow(10,X))/Math.pow(10,X);
    }
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
		var distance = response.routes[0].legs[0].distance.text;
		
		var time_taken = response.routes[0].legs[0].duration.text;
                
                var calc_distance = response.routes[0].legs[0].distance.value;
                
				/*if(document.getElementById('any_vehicle').checked) {
				  	var less_five =  document.getElementById('day_less_five').value;
                	
					var travel_time = 'Any';
				}
				else if(document.getElementById('car_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	
					var travel_time = 'Car';
				}
				else if(document.getElementById('mini-van_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	var more_five =  document.getElementById('night_more_five').value;
					var travel_time = 'Mini Van';
				}
				else if(document.getElementById('cargo-van_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	var more_five =  document.getElementById('night_more_five').value;
					var travel_time = 'Cargo Van';
				}
				else if(document.getElementById('strt-truck_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	var more_five =  document.getElementById('night_more_five').value;
					var travel_time = 'Strt Truck';
				}
				else if(document.getElementById('bike_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	var more_five =  document.getElementById('night_more_five').value;
					var travel_time = 'Bike';
				}
				else if(document.getElementById('concierge_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	var more_five =  document.getElementById('night_more_five').value;
					var travel_time = 'Concierge';
				}
				else if(document.getElementById('reserve_vehicle').checked) {
				  	var less_five =  document.getElementById('night_less_five').value;
                	var more_five =  document.getElementById('night_more_five').value;
					var travel_time = 'Reserve';
				}
				var curr_format =  document.getElementById('curr_format').value;
                
                if (calc_distance <= 5010) {
                    var amount_to_pay = calc_distance * less_five;
                }
                else {
                    var amount_to_pay = calc_distance * more_five;
                }*/
	function roundNumber(numbr,decimalPlaces) 
	{
		var placeSetter = Math.pow(10, decimalPlaces);
		numbr = Math.round(numbr * placeSetter) / placeSetter;
		return numbr;
	}
	var mi =  calc_distance / 1.609;
	var mi = mi/1000;
	var mi = roundNumber(mi, 2);   //Sets value to 2 decimal places.
	document.getElementById('miles').value = mi;
	document.getElementById('dist').value = distance;
	ajax_price_calculate_function();
	//var total_price = document.getElementById('total_price').value;
	
	//var rounded_amount_to_pay = round(amount_to_pay/1000,2); 
		/*document.getElementById('distance').innerHTML = '<div class="distance-inner">'+ "The distance between <strong><em>"+from+"</em></strong> and <strong><em>"+to+"</em></strong>: <strong>"+distance+" / "+mi+ " mi</strong>\n\
                <br/>\n\
                <br/><strong><span class='esttimate-charge-lbl'>Estimated Charge:</span> <span class='final-price-estimate'></span></strong>\n\
                <br/>\n\<form action='https://www.sandbox.paypal.com/cgi-bin/webscr' method='post'><input type='hidden' name='cmd' value='_xclick'><input type='hidden' name='item_name' value='Org: "+from+" to Dest: "+to+"'><input type='hidden' name='upload' value='1'><input type='hidden' name='business' value='ali4diamonds@aol.com'><input type='hidden' name='currency_code' value='USD' /><input type='hidden' name='page_style' value='paypal' /><input type='hidden' name='charset' value='utf-8' /><input type='hidden' name='rm' value='2' /><input type='hidden' name='amount' value='"+total_price+"'><input type='submit' class='clsbtn' value='Click Here to Order'></form><br/><br/><br/><img src='https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png' alt='Buy now with PayPal' /> ";*/
				
                
		var steps = "<ul>";
		var myRoute = response.routes[0].legs[0];
		for (var i = 0; i < myRoute.steps.length; i++) {
		 steps += "<li>" + myRoute.steps[i].instructions + "</li>";
		}
		steps += "</ul>";
		/*document.getElementById('steps').innerHTML = '<div class="steps-inner"><h2>Driving directions to '+response.routes[0].legs[0].end_address+'</h2>'+steps+'</div>';*/
      }
	  else{
		document.getElementById('distance').innerHTML = '<span class="gdc-error">Google Map could not be created for the entered parameters. Please be specific while providing the destination location.</span>';
	  }
    });
  }
//window.onload=function(){initialize();}